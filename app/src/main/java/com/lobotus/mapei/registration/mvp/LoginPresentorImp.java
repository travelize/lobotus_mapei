package com.lobotus.mapei.registration.mvp;

import android.content.Context;

import com.lobotus.mapei.R;
import com.lobotus.mapei.utils.NetworkUtils;
import com.lobotus.mapei.utils.PermissionUtils;

/**
 * Created by user1 on 21-09-2017.
 */

public class LoginPresentorImp implements LoginPresentor,LoginIntrector.LoginRestLisner
        ,LoginIntrector.LoginValidationLisner {

    private LoginView loginView=null;
    private LoginIntrector loginIntrector=null;

    public LoginPresentorImp(LoginView loginView) {
        this.loginView = loginView;
        this.loginIntrector = new LoginIntrectorImp();
    }
////////////////////////////////////////////////////////////////////////////////////////////////// login validation

    @Override
    public void performLoginValidation(String email, String password) {
        loginIntrector.reqToValidateEmailAndPasswordFromIntrector(email,password,this);
    }
    @Override
    public void OnLoginValidationSuccess(String email, String password) {
        loginView.afterEmailAndPasswordValidationSuccess(email,password);
    }

    @Override
    public void OnLoginValidationFail(String ErrorMessage, boolean isEmailError) {
        loginView.afterEmailAndPasswordValidationUnSuccess(ErrorMessage,isEmailError);
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////// login rest
    @Override
    public void reqToLoginFromPresentor(String password, String email,  Context context) {
        if (NetworkUtils.checkInternetAndOpenDialog(context))
            if (PermissionUtils.checkLoactionPermission(context)) {
                loginView.showProgressDialog();
                loginIntrector.reqToLoginFromIntrector(password,email,context,this);
            } else {
                PermissionUtils.openPermissionDialog(context, context.getString(R.string.alert),
                        context.getString(R.string.please_grant_location_permission));
            }


    }

    @Override
    public void OnLoginRestResponseSuccess(String message) {
        loginView.dismissProgressDialog();
        loginView.afterLoginResonseSuccess(message);
    }

    @Override
    public void OnLoginRestResponseFail(String retrofitError) {
        loginView.dismissProgressDialog();
        loginView.afterLoginResponseFail(retrofitError);
    }


}
