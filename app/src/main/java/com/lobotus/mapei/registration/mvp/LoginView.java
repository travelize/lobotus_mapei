package com.lobotus.mapei.registration.mvp;

/**
 * Created by user1 on 21-09-2017.
 */

public interface LoginView {
    void showProgressDialog();
    void dismissProgressDialog();
    void afterEmailAndPasswordValidationSuccess(String email,String password);
    void afterEmailAndPasswordValidationUnSuccess(String validationErrorMessage,boolean isEmailError);
    void afterLoginResonseSuccess(String message);
    void afterLoginResponseFail(String retrofitError);

}
