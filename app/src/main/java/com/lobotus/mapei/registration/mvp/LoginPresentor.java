package com.lobotus.mapei.registration.mvp;

import android.content.Context;

/**
 * Created by user1 on 21-09-2017.
 */

public interface LoginPresentor {
    void performLoginValidation(String email,String password);
    void reqToLoginFromPresentor(String password,
                                 String email,
                                 Context context);
}
