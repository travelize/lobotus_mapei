package com.lobotus.mapei.registration.activities;

import android.Manifest;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.view.View;
import android.widget.Toast;


import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.gson.Gson;
import com.lobotus.mapei.R;
import com.lobotus.mapei.baseactivity.BaseActivity;
import com.lobotus.mapei.registration.model.LoginModel;
import com.lobotus.mapei.retrofitapi.APIService;
import com.lobotus.mapei.retrofitapi.ApiUtils;
import com.lobotus.mapei.utils.CommonFunc;
import com.lobotus.mapei.utils.ConstantsUtils;
import com.lobotus.mapei.utils.NetworkUtils;
import com.lobotus.mapei.utils.PermissionUtils;
import com.lobotus.mapei.utils.PreferenceUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.concurrent.TimeUnit;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.lobotus.mapei.utils.ConstantsUtils.NO_SIM_CARD;


public class ReqForMobileNo extends AppCompatActivity implements View.OnClickListener {

    private  Call<ResponseBody> callSendSms=null;
    private  Call<ResponseBody> callCheckUserPresentOrNot=null;
    private Call<ResponseBody> callLoginAfterOtpHasVerified=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        registerBroadCastStuff();
        findViewById(R.id.activity_req_for_mobile_no_generate_otp_button_id).setOnClickListener(this);
        try {
            alertMiUsers();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!isBroadcastRegistred)
            registerBroadCastStuff();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (isBroadcastRegistred) {
            unregisterReceiver(broadcastReceiver);
            isBroadcastRegistred=false;
        }
        if (callSendSms!=null)
            callSendSms.cancel();
        if (callCheckUserPresentOrNot!=null)
            callCheckUserPresentOrNot.cancel();
        if (callLoginAfterOtpHasVerified!=null)
            callLoginAfterOtpHasVerified.cancel();
        dismissProgressDialog();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.activity_req_for_mobile_no_generate_otp_button_id:
                if (CommonFunc.isGooglePlayServicesAvailable(ReqForMobileNo.this))
                {
                    if (((AppCompatEditText)findViewById(R.id.activity_req_for_mobile_no_editext_id)).getText().toString().isEmpty())
                    {
                        ((AppCompatEditText)findViewById(R.id.activity_req_for_mobile_no_editext_id)).setError("Please enter mobile no");
                    }else if (((AppCompatEditText)findViewById(R.id.activity_req_for_mobile_no_editext_id)).getText().toString().length()<10){
                        ((AppCompatEditText)findViewById(R.id.activity_req_for_mobile_no_editext_id)).setError("Please enter valid mobile no");
                    }else {
                        if (NetworkUtils.checkInternetAndOpenDialog(ReqForMobileNo.this))
                        {
                            String simJson= CommonFunc.formTheSimInfoString(ReqForMobileNo.this);
                            if (!simJson.equals(NO_SIM_CARD)) // check sim card in lolopop
                            {
                                if (PermissionUtils.Smspermissioncheck(ReqForMobileNo.this))
                                    checkUserIsPresentOrNot();
                                else
                                    requestForSmsPermission();
                            }else {
                                CommonFunc.commonDialog(ReqForMobileNo.this,getString(R.string.alert),"Without sim card you can not access Travelize",false);
                            }

                        }

                    }
                }

        }
    }


///////////////////////////////////////////////////////////////////////////////// check user is present or not

    private void checkUserIsPresentOrNot()
    {
        showProgressDialog();
        APIService apiService=ApiUtils.getAPIService(ConstantsUtils.postBaseUrl+ConstantsUtils.POST_TO_CHECK_USER);
        callCheckUserPresentOrNot=apiService.postReqToCheckUserIsPresentOrNot(((AppCompatEditText)
                findViewById(R.id.activity_req_for_mobile_no_editext_id)).getText().toString());
        callCheckUserPresentOrNot.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful())
                {
                    try {
                        JSONObject jsonObject=new JSONObject(response.body().string());
                        if (jsonObject.getInt("Success")==1)
                        {
                                    /*sendSms();*/
                            trigerFireBaseOTP();
                        }else {
                            dismissProgressDialog();
                            CommonFunc.commonDialog(ReqForMobileNo.this,getString(R.string.alert),
                                    jsonObject.getString("Msg"),false);
                        }

                    } catch (JSONException | IOException e) {
                        dismissProgressDialog();
                        CommonFunc.commonDialog(ReqForMobileNo.this,getString(R.string.internalError),
                                getString(R.string.justErrorCode)+ " 2",false);
                    }
                }else {
                    dismissProgressDialog();
                    CommonFunc.commonDialog(ReqForMobileNo.this,getString(R.string.internalError),
                            getString(R.string.justErrorCode)+ " 3",false);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dismissProgressDialog();
                CommonFunc.commonDialog(ReqForMobileNo.this,getString(R.string.noInternetAlert),
                        getString(R.string.noInternetMessage)+ " 4",true);
            }
        });
    }




    ////////////////////////////////// firebase
    ///////////////////////////////////////////////////////////////////////////////////////////////// generate sms. . . . . . .
    // global variable
    public String mVerificationId=null;
    public PhoneAuthProvider.ForceResendingToken mResendToken=null;
    private PhoneAuthCredential credential=null;
    ////
    private void trigerFireBaseOTP()
    {

        if (NetworkUtils.checkInternetAndOpenDialog(ReqForMobileNo.this))
        {

            PhoneAuthProvider.getInstance().verifyPhoneNumber("+91"+((AppCompatEditText)
                            findViewById(R.id.activity_req_for_mobile_no_editext_id)).getText().toString(),
                    30, TimeUnit.SECONDS, this, onVerificationStateChangedCallbacks);
        }


    }


    private PhoneAuthProvider.OnVerificationStateChangedCallbacks onVerificationStateChangedCallbacks=new
            PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
                @Override
                public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
                    signInWithPhoneAuthCredential(phoneAuthCredential,"",phoneAuthCredential.getSmsCode());
                }

                @Override
                public void onVerificationFailed(FirebaseException e) {
                    dismissProgressDialog();
                    Toast.makeText(ReqForMobileNo.this, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                    mVerificationId=s;
                    mResendToken=forceResendingToken;
                    Toast.makeText(ReqForMobileNo.this, "A verification code has been sent to your mobile no. Please wait... while we verify...", Toast.LENGTH_LONG).show();
                }
            };
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// sign-in the user. . .
    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential, final String verificationId, final String code) {
        FirebaseAuth mAuth=FirebaseAuth.getInstance();
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            reqForLoginAfterOTPMatched();
                        } else {
                            // Sign in failed, display a message and update the UI
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                // The verification code entered was invalid
                                /*Toast.makeText(LoginActivity.this, ""+task.getException().getLocalizedMessage(), Toast.LENGTH_SHORT).show();*/

                            }
                        }
                    }
                });
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////// progress. . .
    private Dialog dialogProgress=null;
    //
    public void showProgressDialog() {
        dialogProgress =new Dialog(ReqForMobileNo.this);
        dialogProgress.setCancelable(false);
        dialogProgress.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialogProgress.setContentView(R.layout.loaging_layout);
        dialogProgress.show();
    }

    public void dismissProgressDialog() {
        if (dialogProgress !=null)
        {
            if (dialogProgress.isShowing())
            {
                dialogProgress.dismiss();
            }
            dialogProgress =null;
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////////// sms permission. . .

    private int SMS_PERMISSION_REQ_CODE=10;
    private void requestForSmsPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(ReqForMobileNo.this, Manifest.permission.READ_SMS))
        {
            new AlertDialog.Builder(ReqForMobileNo.this)
                    .setTitle("Alert!!")
                    .setMessage("Please grant sms permission to travelize without it app wont work.")
                    .setCancelable(false)
                    .setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            Intent intent = new Intent();
                            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                            Uri uri = Uri.fromParts("package", getPackageName(), null);
                            intent.setData(uri);
                            startActivity(intent);

                        }

                    }).create().show();
        }else {
            ActivityCompat.requestPermissions(ReqForMobileNo.this,new String[]{Manifest.permission.READ_SMS},SMS_PERMISSION_REQ_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode==SMS_PERMISSION_REQ_CODE)
        {
            if (grantResults[0]==  PackageManager.PERMISSION_GRANTED)
            {
                checkUserIsPresentOrNot();
            }else {
                requestForSmsPermission();
            }
        }
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    ///////////////////////////////////////////////////////////////////////////////// broadcastreciver stuff
    private boolean isBroadcastRegistred=false;
    //
    private void registerBroadCastStuff()
    {
        registerReceiver(broadcastReceiver,new IntentFilter("OTP_RECIVER_ACTION"));
        isBroadcastRegistred=true;
    }
    BroadcastReceiver broadcastReceiver=new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundleFromReciver=intent.getExtras();
            String otp=bundleFromReciver.getString("OTP_RECIVER_VALUE_KEY");
            credential =PhoneAuthProvider.getCredential(mVerificationId, otp);
            signInWithPhoneAuthCredential(credential,mVerificationId,otp);
          /*  if (otp.equals(PreferenceUtils.getOTPvalueFromPreference(ReqForMobileNo.this)))
            {

                reqForLoginAfterOTPMatched();
            }else {
                CommonFunc.commonDialog(ReqForMobileNo.this,getString(R.string.internalError),getString(R.string.justErrorCode)+" 5",false);
            }*/
        }
    };
    /////////////////////////////////////////////////////////////////////////// req for login after otp has matched

    private void reqForLoginAfterOTPMatched()
    {

        String temp_manufacturer = null;
        String temp_model = null;
        String temp_version = null;
        try {
            temp_manufacturer = Build.MANUFACTURER;
            temp_model = Build.MODEL;
            temp_version = Build.PRODUCT;
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("ttttttttttttttttttttttttt------temp_manufacturer---------" + temp_manufacturer);
        System.out.println("ttttttttttttttttttttttttt------temp_model---------" + temp_model);
        System.out.println("ttttttttttttttttttttttttt------temp_version---------" + temp_version);

        APIService apiService = ApiUtils.getAPIService(ConstantsUtils.postBaseUrl + ConstantsUtils.postAppendToBaseUrlForLogin);
        apiService.postToLogin( ((AppCompatEditText)findViewById(R.id.activity_req_for_mobile_no_editext_id))
                .getText().toString(), temp_model == null ? "NA" : temp_model,
                temp_manufacturer == null ? "NA" : temp_manufacturer,
                temp_version == null ? "NA" : temp_version).enqueue(new Callback<LoginModel>() {
            @Override
            public void onResponse(Call<LoginModel> call, Response<LoginModel> response) {
                if (response.isSuccessful()) {
                    LoginModel loginModel = response.body();
                    if (loginModel != null) {
                        if (loginModel.getSuccess() == 1) {
                            System.out.println("gggggggggggggg------getProfileIcon---------" + response.body().getProfileIcon());
                            PreferenceUtils.addUserSessionDetailsWhileReg(ReqForMobileNo.this,
                                    loginModel.getEmail(),
                                    loginModel.getFullName(),
                                    loginModel.getSubscriptionID(),
                                    loginModel.getProfileIcon(),
                                    loginModel.getUserId(),
                                    loginModel.getPhone(),
                                    String.valueOf(loginModel.getDaysLeft()),
                                    String.valueOf(loginModel.getRoleID()),
                                    loginModel.getRoleName(),
                                    loginModel.getRegistrationID(),
                                    loginModel.getSessionId());

                            Intent intent=new Intent(ReqForMobileNo.this,BaseActivity.class);
                            intent.setAction("START_GEO_FENCE");
                            startActivity(intent);
                            finish();

                            Toast.makeText(ReqForMobileNo.this, "Successfully Login", Toast.LENGTH_SHORT).show();
                        } else {

                            CommonFunc.commonDialog(ReqForMobileNo.this,getString(R.string.alert),
                                    loginModel.getMsg(),false);
                        }
                    } else {
                        CommonFunc.commonDialog(ReqForMobileNo.this,getString(R.string.alert),
                                "Error code 1",false);
                    }

                } else {
                    CommonFunc.commonDialog(ReqForMobileNo.this,getString(R.string.alert),
                            "Error code 2",false);
                }
            }

            @Override
            public void onFailure(Call<LoginModel> call, Throwable t) {
                System.out.println("gggggggggg------------7-------" + t.getLocalizedMessage());
                CommonFunc.commonDialog(ReqForMobileNo.this,getString(R.string.alert),
                        "Error code 3",false);
            }
        });



    }



    /////////////////////////////// req for auto start by xiaomi
    private void alertMiUsers() throws Exception
    {
        String manufacturer = "xiaomi";
        if(manufacturer.equalsIgnoreCase(Build.MANUFACTURER)) {


            new AlertDialog.Builder(ReqForMobileNo.this)
                    .setTitle("Alert!!")
                    .setMessage("Dear Xiaomi user, Please enable Autostart for "+getString(R.string.app_name)+" , Skip if already enabled.")
                    .setCancelable(true)
                    .setPositiveButton("enable", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            try
                            {
                                Intent intent = new Intent();
                                intent.setComponent(new ComponentName("com.miui.securitycenter", "com.miui.permcenter.autostart.AutoStartManagementActivity"));
                                startActivity(intent);
                            }catch (Exception e)
                            {
                                Toast.makeText(ReqForMobileNo.this, "Seems like this is a stock android device.. you can directly proceed", Toast.LENGTH_LONG).show();
                            }
                        }

                    }).setNegativeButton("skip", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            })
                    .create().show();
        }
    }


}
