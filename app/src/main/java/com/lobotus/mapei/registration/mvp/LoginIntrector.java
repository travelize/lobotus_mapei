package com.lobotus.mapei.registration.mvp;

import android.content.Context;

/**
 * Created by user1 on 21-09-2017.
 */

public interface LoginIntrector {
    interface LoginValidationLisner
    {
        void OnLoginValidationSuccess(String email,String password);
        void OnLoginValidationFail(String ErrorMessage,
                                   boolean isEmailError);
    }
    void reqToValidateEmailAndPasswordFromIntrector(String email,String password,
                                                    LoginValidationLisner onLoginValidationLisner);
///////////////////////////////////////////////////////
    interface LoginRestLisner
    {
        void OnLoginRestResponseSuccess(String message);
        void OnLoginRestResponseFail(String retrofitError);
    }
    void reqToLoginFromIntrector(String password,
                                 String email,
                                 Context context,
                                 LoginRestLisner loginRestLisner);
}
