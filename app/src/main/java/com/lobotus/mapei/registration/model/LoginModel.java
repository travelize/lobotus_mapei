package com.lobotus.mapei.registration.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by user1 on 21-09-2017.
 */

public class LoginModel {
    @SerializedName("Email")
    @Expose
    private String email;
    @SerializedName("FullName")
    @Expose
    private String fullName;
    @SerializedName("SubscriptionID")
    @Expose
    private String subscriptionID;
    @SerializedName("ProfileIcon")
    @Expose
    private String profileIcon;
    @SerializedName("UserId")
    @Expose
    private String userId;
    @SerializedName("Phone")
    @Expose
    private String phone;
    @SerializedName("DaysLeft")
    @Expose
    private Integer daysLeft;
    @SerializedName("RoleID")
    @Expose
    private Integer roleID;
    @SerializedName("RoleName")
    @Expose
    private String roleName;
    @SerializedName("RegistrationID")
    @Expose
    private String registrationID;
    @SerializedName("Success")
    @Expose
    private Integer success;
    @SerializedName("Msg")
    @Expose
    private String msg;
    @SerializedName("AppSessionID")
    @Expose
    private String sessionId=null;

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getSubscriptionID() {
        return subscriptionID;
    }

    public void setSubscriptionID(String subscriptionID) {
        this.subscriptionID = subscriptionID;
    }

    public String getProfileIcon() {
        return profileIcon;
    }

    public void setProfileIcon(String profileIcon) {
        this.profileIcon = profileIcon;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getDaysLeft() {
        return daysLeft;
    }

    public void setDaysLeft(Integer daysLeft) {
        this.daysLeft = daysLeft;
    }

    public Integer getRoleID() {
        return roleID;
    }

    public void setRoleID(Integer roleID) {
        this.roleID = roleID;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRegistrationID() {
        return registrationID;
    }

    public void setRegistrationID(String registrationID) {
        this.registrationID = registrationID;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
