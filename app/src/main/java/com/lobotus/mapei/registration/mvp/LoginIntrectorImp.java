package com.lobotus.mapei.registration.mvp;

import android.content.Context;
import android.os.Build;
import android.util.Log;

import com.lobotus.mapei.R;
import com.lobotus.mapei.registration.model.LoginModel;
import com.lobotus.mapei.retrofitapi.APIService;
import com.lobotus.mapei.retrofitapi.ApiUtils;
import com.lobotus.mapei.utils.CommonFunc;
import com.lobotus.mapei.utils.ConstantsUtils;
import com.lobotus.mapei.utils.NetworkUtils;
import com.lobotus.mapei.utils.PermissionUtils;
import com.lobotus.mapei.utils.PreferenceUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by user1 on 21-09-2017.
 */

public class LoginIntrectorImp implements LoginIntrector {
    ////////////////////////////////////////////////////////////////////// login validation


    @Override
    public void reqToValidateEmailAndPasswordFromIntrector(String email, String password,
                                                           LoginValidationLisner onLoginValidationLisner) {
        if (email == null) {
            onLoginValidationLisner.OnLoginValidationFail("email needed!", true);
        } else if (password == null) {
            onLoginValidationLisner.OnLoginValidationFail("password needed!", false);
        } else if (email.isEmpty()) {
            onLoginValidationLisner.OnLoginValidationFail("email needed!", true);
        } else if (!CommonFunc.isValidEmail(email)) {
            onLoginValidationLisner.OnLoginValidationFail("email invalid!", true);
        } else if (password.isEmpty()) {
            onLoginValidationLisner.OnLoginValidationFail("password needed!", false);
        } else {
            onLoginValidationLisner.OnLoginValidationSuccess(email, password);
        }

    }

    //////////////////////////////////////////////////////////////////////////////// login rest
    @Override
    public void reqToLoginFromIntrector(String password, String email, final Context context, final LoginRestLisner loginRestLisner) {
/*
        String temp_manufacturer = null;
        String temp_model = null;
        String temp_version = null;
        try {
            temp_manufacturer = Build.MANUFACTURER;
            temp_model = Build.MODEL;
            temp_version = Build.PRODUCT;
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("ttttttttttttttttttttttttt------password---------" + password);
        System.out.println("ttttttttttttttttttttttttt------email---------" + email);
        System.out.println("ttttttttttttttttttttttttt------temp_manufacturer---------" + temp_manufacturer);
        System.out.println("ttttttttttttttttttttttttt------temp_model---------" + temp_model);
        System.out.println("ttttttttttttttttttttttttt------temp_version---------" + temp_version);

        APIService apiService = ApiUtils.getAPIService(ConstantsUtils.postBaseUrl + ConstantsUtils.postAppendToBaseUrlForLogin);
        apiService.postToLogin(password, email, temp_model == null ? "NA" : temp_model,
                temp_manufacturer == null ? "NA" : temp_manufacturer,
                temp_version == null ? "NA" : temp_version).enqueue(new Callback<LoginModel>() {
            @Override
            public void onResponse(Call<LoginModel> call, Response<LoginModel> response) {
                if (response.isSuccessful()) {
                    LoginModel loginModel = response.body();
                    if (loginModel != null) {
                        if (loginModel.getSuccess() == 1) {
                            System.out.println("gggggggggggggg------getProfileIcon---------" + response.body().getProfileIcon());
                            PreferenceUtils.addUserSessionDetailsWhileReg(context,
                                    loginModel.getEmail(),
                                    loginModel.getFullName(),
                                    loginModel.getSubscriptionID(),
                                    loginModel.getProfileIcon(),
                                    loginModel.getUserId(),
                                    loginModel.getPhone(),
                                    String.valueOf(loginModel.getDaysLeft()),
                                    String.valueOf(loginModel.getRoleID()),
                                    loginModel.getRoleName(),
                                    loginModel.getRegistrationID(),
                                    loginModel.getSessionId());
                            loginRestLisner.OnLoginRestResponseSuccess("Successfully Login");
                        } else {
                            loginRestLisner.OnLoginRestResponseFail(loginModel.getMsg());
                        }
                    } else {
                        loginRestLisner.OnLoginRestResponseFail("Error code 1");
                    }

                } else {
                    loginRestLisner.OnLoginRestResponseFail("Error code 2");
                }
            }

            @Override
            public void onFailure(Call<LoginModel> call, Throwable t) {
                System.out.println("gggggggggg------------7-------" + t.getLocalizedMessage());
                loginRestLisner.OnLoginRestResponseFail("Error code 3");
            }
        });*/

    }
}
