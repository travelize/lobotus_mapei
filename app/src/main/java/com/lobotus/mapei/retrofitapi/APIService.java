package com.lobotus.mapei.retrofitapi;


import android.support.annotation.Keep;
import android.support.annotation.StringRes;

import com.lobotus.mapei.flowfragments.model.ComplaintTypeModel;
import com.lobotus.mapei.flowfragments.model.CustomerModel;
import com.lobotus.mapei.flowfragments.model.CustomerTypeModel;
import com.lobotus.mapei.flowfragments.model.DemoMeetingsModel;
import com.lobotus.mapei.flowfragments.model.LeaveModel;
import com.lobotus.mapei.flowfragments.model.LeaveTypeModel;
import com.lobotus.mapei.flowfragments.model.MOTandPOTdataModel;
import com.lobotus.mapei.flowfragments.model.MotByUidModel;
import com.lobotus.mapei.flowfragments.model.ProductsDataModel;
import com.lobotus.mapei.flowfragments.model.ProductsModel;
import com.lobotus.mapei.flowfragments.model.ProjectHistoryModel;
import com.lobotus.mapei.flowfragments.model.ProjectsModel;
import com.lobotus.mapei.flowfragments.model.UserModel;
import com.lobotus.mapei.flowfragments.model.UserTravelModel;
import com.lobotus.mapei.fromlite.model.CommRespModel;
import com.lobotus.mapei.fromlite.model.MeetingModel;
import com.lobotus.mapei.fromlite.model.ModeOfTravelModel;
import com.lobotus.mapei.registration.model.LoginModel;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by user on 4/21/2017.
 */
@Keep
public interface APIService {
    //////////////////////////////////////////////////////////// login activity
    @POST(".")
    @FormUrlEncoded
    Call<LoginModel> postToLogin(
                                 @Field("Email") String Email,
                                 @Field("Model") String Model,
                                 @Field("DeviceName") String DeviceName,
                                 @Field("Version") String Version);

    /////////////////////////////////////////////////////////////////////////////////// demo activity
    @POST(".")
    @FormUrlEncoded
    Call<ResponseBody> postToSaveDemo(@Field("UserID") String UserID,
                                      @Field("DemoTypeID") String DemoTypeID,
                                      @Field("Remarks") String Remark,
                                      @Field("DemoSubTypeId") String ProductNotAvailable,
                                      @Field("CustID") String CustID);

    ///////////////////////////////////////////////////////////////////////////////// customers data download
    @POST(".")
    @FormUrlEncoded
    Call<CustomerModel> postReqTogetTheCustomerData(@Field("status") String status);

    //////////////////////////////////////////////////////////////////////////////////// post to get the list of leaves
    @POST(".")
    @FormUrlEncoded
    Call<LeaveModel> postToGetTheLeaveList(@Field("UserId") String UserId);

    ///////////////////////////////////////////////////////////////////////////////////// post to cancel leave
    @POST(".")
    @FormUrlEncoded
    Call<ResponseBody> postToCancelTheLeave(@Field("LeaveID") String LeaveID);

    ////////////////////////////////////////////////////////////////////////////////// post the leave req for same day
    @POST(".")
    @FormUrlEncoded
    Call<ResponseBody> postReqToApplyForLeaveSameDay(@Field("UserId") String UserID,
                                                     @Field("TypeID") String TypeId,
                                                     @Field("FromDate") String FromDate,
                                                     @Field("ToDate") String ToDate,
                                                     @Field("Remarks") String Remarks,
                                                     @Field("Status") String status,
                                                     @Field("Type") String sameDayType);

    ////////////////////////////////////////////////////////////////////////////////// post the leave req for diff day
    @POST(".")
    @FormUrlEncoded
    Call<ResponseBody> postReqToApplyForLeaveDiffDay(@Field("UserId") String UserID,
                                                     @Field("TypeID") String TypeId,
                                                     @Field("FromDate") String FromDate,
                                                     @Field("ToDate") String ToDate,
                                                     @Field("Remarks") String Remarks,
                                                     @Field("Status") String status);

    //////////////////////////////////////////////////////////////////////////////////// mode of travel and purpose of travel model
    @POST(".")
    Call<MOTandPOTdataModel> postToGetMOTandPOT(@Body String empty);

    /////////////////////////////////////////////////////////////////////////////// save the travel details
    @POST(".")
    @FormUrlEncoded
    Call<ResponseBody> postToSaveTheTravelDetails(@Field("UserId") String UserId,
                                                  @Field("Remarks") String Remarks,
                                                  @Field("CustID") String CustID,
                                                  @Field("MOT") String MOT,
                                                  @Field("Purpose") String Purpose,
                                                  @Field("Attachment") String Attachment);

    /////////////////////////////////////////////////////////////////////////////////////////// post to get the list of projects
    @POST(".")
    Call<ProjectsModel> postToGetTheListOfProjects(@Body String empty);

    ///////////////////////////////////////////////////////////////////////////////////////// post to get the list of projects
    @POST(".")
    Call<ProductsModel> postToGetTheListofProducts(@Body String empty);

    //////////////////////////////////////////////////////////////////////////////////// post to add the projects to server
    @POST(".")
    @FormUrlEncoded
    Call<ResponseBody> postToAddtheProjuectsToServer(@Field("UserId") String UserId,
                                                     @Field("CustID") String CustID,
                                                     @Field("Product") String Product,
                                                     @Field("Period") String Period,
                                                     @Field("Remarks") String Remarks,
                                                     @Field("Status") String Status,
                                                     @Field("Stage") String Stage,
                                                     @Field("ProjectName") String ProjectName);

    //////////////////////////////////////////////////////////////////////////////////////// post to downlode complaints
    @POST(".")
    Call<ComplaintTypeModel> postTogetComplaintsFromServer(@Body String empty);

    ///////////////////////////////////////////////////////////////////////////////// post the meeting type metting
    @POST(".")
    @FormUrlEncoded
    //sdf
    Call<ResponseBody> postTheMeetingTypeMeeting(@Field("ProjectID") String ProjectID,
                                                 @Field("UserId") String UserId,
                                                 @Field("MeetingTypeID") String MeetingTypeID,
                                                 @Field("MeetingSubTypeID") String SubmeetingId,
                                                 @Field("ProductList") String ProductList,
                                                 @Field("Status") String status,
                                                 @Field("OrderValue") String OrderValue,
                                                 @Field("OrderLost") String OrderLost,
                                                 @Field("Remarks") String Remarks,
                                                 @Field("Location") String location,
                                                 @Field("TotalValue") String TotalValue,
                                                 @Field("CustomerID") String CustomerID);

    //////////////////////////////////////////////////////////////////////// post the meeting type ar follwups
    //// posting ar follow types payment
    @POST(".")
    @FormUrlEncoded
    Call<ResponseBody> postTheMeetingTypeArFollowPayment(@Field("ProjectID") String ProjectID,
                                                         @Field("UserId") String UserId,
                                                         @Field("MeetingTypeID") String MeetingTypeID,
                                                         @Field("FollowupTypeID") String FollowupTypeID,
                                                         @Field("OnDate") String OnDate,
                                                         @Field("ProductIssues") String ProductIssues,
                                                         @Field("ProductDelay") String ProductDelay,
                                                         @Field("Status") String Status,
                                                         @Field("Remarks") String Remarks,
                                                         @Field("Location") String location,
                                                         @Field("CustomerID") String CustomerID);

    //// posting ar follow type  C form
    @POST(".")
    @FormUrlEncoded
    Call<ResponseBody> postTheMeetingTypeArFollowCform(@Field("ProjectID") String ProjectID,
                                                       @Field("UserId") String UserId,
                                                       @Field("MeetingTypeID") String MeetingTypeID,
                                                       @Field("FollowupTypeID") String FollowupTypeID,
                                                       @Field("OnDate") String OnDate,
                                                       @Field("CFCollected") String CFCollected,
                                                       @Field("CFNotTraceable") String CFNotTraceable,
                                                       @Field("Status") String Status,
                                                       @Field("Remarks") String Remarks,
                                                       @Field("Location") String location,
                                                       @Field("CustomerID") String CustomerID);

    //// posting ar follow type Reconciliation
    @POST(".")
    @FormUrlEncoded
    Call<ResponseBody> postTheMeetingTypeArFollowReconciliation(@Field("ProjectID") String ProjectID,
                                                                @Field("UserId") String UserId,
                                                                @Field("MeetingTypeID") String MeetingTypeID,
                                                                @Field("FollowupTypeID") String FollowupTypeID,
                                                                @Field("ShortPayments") String ShortPayments,
                                                                @Field("Status") String Status,
                                                                @Field("MissingInvoices") String MissingInvoices,
                                                                @Field("Statement") String Statement,
                                                                @Field("Remarks") String Remarks,
                                                                @Field("Location") String location,
                                                                @Field("CustomerID") String CustomerID);

    //////////////////////////////////////////////////////////////////////////// post the meeting type complaints
    @POST(".")
    @FormUrlEncoded
    Call<ResponseBody> postTheMeetingTypeComplaints(@Field("ProjectID") String ProjectID,
                                                    @Field("UserId") String UserId,
                                                    @Field("MeetingTypeID") String MeetingTypeID,
                                                    @Field("ComplaintTypeID") String ComplaintTypeID,
                                                    @Field("Status") String Status,
                                                    @Field("Remarks") String Remarks,
                                                    @Field("Location") String location,
                                                    @Field("CustomerID") String CustomerID);

    ///////////////////@@@@@@@@@@@@@@@  post the assign meetings type metting to server  @@@@@@@@@@@@@@@@@@///////////////
    @POST(".")
    @FormUrlEncoded
    //sdf
    Call<ResponseBody> postTheMeetingTypeMeetingAssign(@Field("ProjectID") String ProjectID,
                                                       @Field("UserId") String UserId,
                                                       @Field("MeetingTypeID") String MeetingTypeID,
                                                       @Field("MeetingSubTypeID") String SubmeetingId,
                                                       @Field("ProductList") String ProductList,
                                                       @Field("Status") String status,
                                                       @Field("OrderValue") String OrderValue,
                                                       @Field("OrderLost") String OrderLost,
                                                       @Field("Date") String Date,
                                                       @Field("MeetingTime") String time,
                                                       @Field("TotalValue") String TotalValue,
                                                       @Field("CustomerID") String CustomerID,
                                                       @Field("Location") String MeetingLocation);


    ///////////////////@@@@@@@@@@@@@@@  post the assign meeting type ar follwups  @@@@@@@@@@@@@@@@@@///////////////
    //// posting ar follow types payment
    @POST(".")
    @FormUrlEncoded
    Call<ResponseBody> postTheMeetingTypeArFollowPaymentAssign(@Field("ProjectID") String ProjectID,
                                                               @Field("UserId") String UserId,
                                                               @Field("MeetingTypeID") String MeetingTypeID,
                                                               @Field("FollowupTypeID") String FollowupTypeID,
                                                               @Field("OnDate") String OnDate,
                                                               @Field("ProductIssues") String ProductIssues,
                                                               @Field("ProductDelay") String ProductDelay,
                                                               @Field("Status") String Status,
                                                               @Field("Date") String Date,
                                                               @Field("MeetingTime") String time,
                                                               @Field("CustomerID") String CustomerID,
                                                               @Field("Location") String MeetingLocation);


    ///////////////////@@@@@@@@@@@@@@@  posting ar follow type  C form Assign  @@@@@@@@@@@@@@@@@@///////////////
    @POST(".")
    @FormUrlEncoded
    Call<ResponseBody> postTheMeetingTypeArFollowCformAssign(@Field("ProjectID") String ProjectID,
                                                             @Field("UserId") String UserId,
                                                             @Field("MeetingTypeID") String MeetingTypeID,
                                                             @Field("FollowupTypeID") String FollowupTypeID,
                                                             @Field("OnDate") String OnDate,
                                                             @Field("CFCollected") String CFCollected,
                                                             @Field("CFNotTraceable") String CFNotTraceable,
                                                             @Field("Status") String Status,
                                                             @Field("Date") String Date,
                                                             @Field("MeetingTime") String time,
                                                             @Field("CustomerID") String CustomerID,
                                                             @Field("Location") String MeetingLocation);

    ///////////////////@@@@@@@@@@@@@@@  posting ar follow type Reconciliation Assign  @@@@@@@@@@@@@@@@@@///////////////
    @POST(".")
    @FormUrlEncoded
    Call<ResponseBody> postTheMeetingTypeArFollowReconciliationAssign(@Field("ProjectID") String ProjectID,
                                                                      @Field("UserId") String UserId,
                                                                      @Field("MeetingTypeID") String MeetingTypeID,
                                                                      @Field("FollowupTypeID") String FollowupTypeID,
                                                                      @Field("ShortPayments") String ShortPayments,
                                                                      @Field("Status") String Status,
                                                                      @Field("MissingInvoices") String MissingInvoices,
                                                                      @Field("Statement") String Statement,
                                                                      @Field("Date") String Date,
                                                                      @Field("MeetingTime") String time,
                                                                      @Field("CustomerID") String CustomerID,
                                                                      @Field("Location") String MeetingLocation);

    ///////////////////@@@@@@@@@@@@@@@  post the meeting type complaints Assign  @@@@@@@@@@@@@@@@@@///////////////
    @POST(".")
    @FormUrlEncoded
    Call<ResponseBody> postTheMeetingTypeComplaintsAssign(@Field("ProjectID") String ProjectID,
                                                          @Field("UserId") String UserId,
                                                          @Field("MeetingTypeID") String MeetingTypeID,
                                                          @Field("ComplaintTypeID") String ComplaintTypeID,
                                                          @Field("Status") String Status,
                                                          @Field("Date") String Date,
                                                          @Field("MeetingTime") String time,
                                                          @Field("CustomerID") String CustomerID,
                                                          @Field("Location") String MeetingLocation);


    //////////////////////////////////////////////////////////////////////// post the office entry
    @POST(".")
    @FormUrlEncoded
    Call<ResponseBody> postToSendOfficeEntryToServer(@Field("UserId") String UserId, @Field("Location") String Location);

    //////////////////////////////////////////////////////////////////////  post the office visit purpose
    @POST(".")
    @FormUrlEncoded
    Call<ResponseBody> postToUpdatePurposeOfOfficeVisit(@Field("OfficeID") String OfficeID,
                                                        @Field("Purpose") String Purpose,
                                                        @Field("Remarks") String Remarks);

    ///////////////////////////////////////////////////////////////////// post the office exit
    @POST(".")
    @FormUrlEncoded
    Call<ResponseBody> postToSendOfficeExitToServer(@Field("OfficeID") String OfficeID);

    /////////////////////////////////////////////////////////////////// post logout to the server
    @POST(".")
    @FormUrlEncoded
    Call<ResponseBody> postLogoutToTheServer(@Field("UserId") String UserId);

    ///////////////////////////////////////////////////////////////// post to get the project history
    @POST(".")
    @FormUrlEncoded
    Call<ProjectHistoryModel> postToGetTheProjectHistory(@Field("ProjectID") String ProjectID);

    /////////////////////////////////////////////////////////////// post to get the products in project history
    @POST(".")
    @FormUrlEncoded
    Call<ProductsModel> postToGetTheProductsInHistory(@Field("MeetingID") String MeetingID);

    ////////////////////////////////////////////////////////// post to update the project
    @POST(".")
    @FormUrlEncoded
    Call<ResponseBody> postToUpdateTheProject(@Field("UserId") String UserId,
                                              @Field("ProjectID") String ProjectID,
                                              @Field("Stage") String Stage,
                                              @Field("Status") String Status,
                                              @Field("Remarks") String Remarks);

    /////////////////////////////////////////////////////// post to add the customer details
    @POST(".")
    @FormUrlEncoded
    Call<ResponseBody> postToSaveCustDetails(@Field("CustTypeId") String CustTypeId,
                                             @Field("FullName") String FullName,
                                             @Field("Location") String Location,
                                             @Field("Email") String Email,
                                             @Field("Phone") String Phone,
                                             @Field("Status") String Status);

    ///////////////////////////////////////////////////// post to get the customer type
    @POST(".")
    Call<CustomerTypeModel> postToGetTheCustomerTypeModel(@Body String empty);

    ///////////////////////////////////////////////////// post to get the leave types
    @POST(".")
    Call<LeaveTypeModel> postToGetTheLeaveTypeList(@Body String empty);

    //////////////////////////////////////////////////// post the user coordinates
    @POST(".")
    @FormUrlEncoded
    Call<ResponseBody> postTheUserCoordinates(@Field("Latitude") String Latitude,
                                              @Field("Longitude") String Longitude,
                                              @Field("BatteryStrength") String BatteryStrength,
                                              @Field("MobileNetwork") String MobileNetwork,
                                              @Field("Location") String Location,
                                              @Field("UserId") String UserId);

    //////////////////////////////////////////////////// post to get the distance travelled by the user
    @POST(".")
    @FormUrlEncoded
    Call<ResponseBody> postToGetTheDistanceTravelledByUser(@Field("UserId") String UserId);

    ///////////////////////////////////////////////// post the get the project list based on customer id
    @POST(".")
    @FormUrlEncoded
    Call<ProjectsModel> postToGetTheListOfProjectBasedOnCustomerName(@Field("CustID") String CustID);

    /////////////////////////////////////////////// post to get the reset password link
    @POST(".")
    @FormUrlEncoded
    Call<ResponseBody> postToResetThepasswordLink(@Field("Email") String Email);

    ////////////////////////////////////////////// post to reset the password
    @POST(".")
    @FormUrlEncoded
    Call<ResponseBody> postToChangeThepassword(@Field("UserId") String UserId,
                                               @Field("Password") String Password,
                                               @Field("NewPassword") String NewPassword,
                                               @Field("Flag") String Flag);

    ////////////////////////////////////////////////////// psot to perform check in
    @POST(".")
    @FormUrlEncoded
    Call<ResponseBody> postToPerformCheckIn(@Field("UserId") String UserId, @Field("location") String location);

    ////////////////////////////////////////////////////// psot to perform check out
    @POST(".")
    @FormUrlEncoded
    Call<ResponseBody> postToPerformCheckOut(@Field("AttendanceID") String AttendanceID, @Field("location") String location);

    /////////////////////////////////////////////////////post to get the particular user mod of travel. .
    @POST(".")
    @FormUrlEncoded
    Call<MotByUidModel> postToGetMotByUid(@Field("UserId") String UserId);


    ////////////////////// from lite. . .
    ///////////////////////////////////////////////////// POST TO GET THE MEETING LIST
    @POST(".")
    @FormUrlEncoded
    Call<List<MeetingModel>> postToGetTheMeetingList(@Field("UserId") String UserId,
                                                     @Field("FromDate") String FromDate);//


    /////////////////////////////////////////////// POST TO GET THE MOT LIST
    @POST(".")
    Call<ModeOfTravelModel> postToGetTheListOfMOT(@Body String empty);//

    ///////////////////////////////////////////////////// post to save the user coordinates to server in activity
    // for started
    @POST(".")
    @FormUrlEncoded
    Call<ResponseBody> postUserCoordinatesToServerInActivityForStarted(@Field("UserId") String UserId,
                                                                       @Field("Longitude") String Longitude,
                                                                       @Field("Latitude") String Latitude,
                                                                       @Field("BatteryStrength") String BatteryStrength,
                                                                       @Field("MobileNetwork") String MobileNetwork,
                                                                       @Field("Location") String Location,
                                                                       @Field("MeetingID") String MeetingID,
                                                                       @Field("StartPoint") String StartPoint,
                                                                       @Field("EndPoint") String EndPoint,
                                                                       @Field("Status") String Status,
                                                                       @Field("MockLocation") String MockLocation);//

    // for ended
    @POST(".")
    @FormUrlEncoded
    Call<ResponseBody> postUserCoordinatesToServerInActivityForEnded(@Field("UserId") String UserId,
                                                                     @Field("Longitude") String Longitude,
                                                                     @Field("Latitude") String Latitude,
                                                                     @Field("BatteryStrength") String BatteryStrength,
                                                                     @Field("MobileNetwork") String MobileNetwork,
                                                                     @Field("Location") String Location,
                                                                     @Field("MeetingID") String MeetingID,
                                                                     @Field("StartPoint") String StartPoint,
                                                                     @Field("EndPoint") String EndPoint,
                                                                     @Field("MockLocation") String MockLocation);//

    ///////////////////////////////////////////////// get distance travelled by the user. . .  .
    @POST(".")
    @FormUrlEncoded
    Call<ResponseBody> postToGetTheDistanceTravelled(@Field("MeetingID") String MeetingID);//

    /////////////////////////////////////////////// POST TO UPDATE THE FOLLOWUP STATUS OF THE MEETING. . .
    @POST(".")
    @FormUrlEncoded
    Call<CommRespModel> postToUpdateTheMeetingStatusOfFollowup(@Field("MeetingID") String MeetingID,
                                                               @Field("Status") String Status,
                                                               @Field("Remarks") String Remarks,
                                                               @Field("OnTime") String OnTime,
                                                               @Field("OnDate") String OnDate,
                                                               @Field("Attachment") String Attachment,
                                                               @Field("ModeOfTravel") String ModeOfTravel);// replaced with async task

    ///////////////////////////////////////////POST TO UPDATE THE OTHER THAN FOLLOWUP STATUS OF THE MEETING. .
    @POST(".")
    @FormUrlEncoded
    Call<CommRespModel> postToUpdateTheMeetingStatusOfOtherThanFollowup(@Field("MeetingID") String MeetingID,
                                                                        @Field("Status") String Status,
                                                                        @Field("Remarks") String Remarks,
                                                                        @Field("Attachment") String Attachment,
                                                                        @Field("ModeOfTravel") String ModeOfTravel);// replacd with async task

    ////////////////////////////////////////////////   POST TO SERVER AFTER USER EXITED THE GEOFENCED REGION. . .
    @POST(".")
    @FormUrlEncoded
    Call<CommRespModel> postToServerAfterUserExitedTheGeoFencedRegion(@Field("MeetingID") String MeetingID);//

    ///////////////////////////////////////////////////// post to save the user coordinates to server in service class
    @POST(".")
    @FormUrlEncoded
    Call<ResponseBody> postUserCoordinatesToServerInService(@Field("UserId") String UserId,
                                                            @Field("Longitude") String Longitude,
                                                            @Field("Latitude") String Latitude,
                                                            @Field("BatteryStrength") String BatteryStrength,
                                                            @Field("MobileNetwork") String MobileNetwork,
                                                            @Field("Location") String Location,
                                                            @Field("MeetingID") String MeetingID,
                                                            @Field("StartPoint") String StartPoint,
                                                            @Field("EndPoint") String EndPoint,
                                                            @Field("MockLocation") String MockLocation);//

    ////////////////////////////////////////////////   POST TO SERVER AFTER USER ENTERED THE GEOFENCED REGION. . .
    @POST(".")
    @FormUrlEncoded
    Call<ResponseBody> postToServerAfterUserEnteredTheGeoFencedRegion(@Field("MeetingID") String MeetingID);// in geo fence

    /////////////////////////////////////////////// POST TO UPDATE THE GPS STATUS TO THE SERVER. . .
    @POST(".")
    @FormUrlEncoded
    Call<ResponseBody> postToUpdateTheGPSChangeStatus(@Field("UserId") String UserId,
                                                      @Field("GPS") String GPS);// in tracker

    @POST(".")
    @FormUrlEncoded
    Call<UserModel> postToGetTheUserList(@Field("TypeID") String TypeID,
                                         @Field("UserId") String UserId);

    @POST(".")
    @FormUrlEncoded
    Call<CommRespModel> postToAssignMeetingToUser(@Field("UserId") String UserId,
                                                  @Field("MeetingTypeID") String MeetingTypeID,
                                                  @Field("ProjectID") String ProjectID,
                                                  @Field("Status") String Status,
                                                  @Field("CustomerID") String CustomerID,
                                                  @Field("OnDate") String OnDate,
                                                  @Field("OnTime") String OnTime);

    /////////////////////////////////////////////////////////////// psot to get the usr travel details. . . .
    @POST(".")
    @FormUrlEncoded
    Call<List<UserTravelModel>> postToGetTheUserTravelList(@Field("FromDate") String FromDate,
                                                           @Field("ToDate") String ToDate,
                                                           @Field("SearchTxt") String SearchTxt);

    ////////////////////////////////////////// post method to get all demo meetings from server
    @POST(".")
    @FormUrlEncoded
    Call<List<DemoMeetingsModel>> postToGetTheDemoMeetingList(@Field("UserId") String UserId);//
    /////////////////////////////////////////////// check user is present or not...
    @POST(".")
    @FormUrlEncoded
    Call<ResponseBody> postReqToCheckUserIsPresentOrNot(@Field("Phone") String Phone);//

}
