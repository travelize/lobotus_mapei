package com.lobotus.mapei.retrofitapi;


import android.support.annotation.Keep;

/**
 * Created by user on 4/21/2017.
 */
@Keep
public class ApiUtils {
    private ApiUtils() {}
    public static APIService getAPIService(String baseUrl) {
        return RetrofitClient.getClient(baseUrl).create(APIService.class);
    }

}
