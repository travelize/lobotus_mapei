package com.lobotus.mapei.fromlite.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.lobotus.mapei.R;
import com.lobotus.mapei.fromlite.fragment.MeetingsFragment;
import com.lobotus.mapei.fromlite.model.MeetingModel;
import com.lobotus.mapei.utils.PreferenceUtils;

import java.util.ArrayList;

/**
 * Created by user1 on 20-11-2017.
 */

public class MeetingAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context = null;
    private ArrayList<MeetingModel> meetingModels = null;
    private MeetingsFragment meetingsFragment = null;
    ///////////////////////////
    private int VIEW_TYPE_TITLE = 1;
    private int VIEW_TYPE_DATA = 2;


    public MeetingAdapter(Context context, ArrayList<MeetingModel> meetingModels, MeetingsFragment meetingsFragment) {
        this.context = context;
        this.meetingModels = meetingModels;
        this.meetingsFragment = meetingsFragment;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == VIEW_TYPE_TITLE) {
            View view = LayoutInflater.from(context).inflate(R.layout.meeting_list_top_row, parent, false);
            return new HolderMeetingTopRow(view);
        } else {
            View view = LayoutInflater.from(context).inflate(R.layout.meeting_list_data_row, parent, false);
            return new HolderMeetingDataRow(view);
        }

    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof HolderMeetingDataRow) {
            ((HolderMeetingDataRow) holder).textViewClientName.setText("" + meetingModels.get(holder.getAdapterPosition()).getFullName());
//            System.out.println("mmmmmmmmmm------Dateon=-------" + meetingModels.get(holder.getAdapterPosition()).getOnTime());
//            ((HolderMeetingDataRow) holder).textViewTime.setText(meetingModels.get(holder.getAdapterPosition()).getOnTime());
            ((HolderMeetingDataRow) holder).textViewTime.setText(meetingModels.get(holder.getAdapterPosition()).getOnTime().length() == 0 ? "NA" : meetingModels.get(holder.getAdapterPosition()).getOnTime());
            ((HolderMeetingDataRow) holder).textViewStatus.setText("" + meetingModels.get(holder.getAdapterPosition()).getStatus());
            ((HolderMeetingDataRow) holder).layoutCompatClick.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    meetingsFragment.onMeetingListClick(meetingModels.get(holder.getAdapterPosition()));
                }
            });
            if (PreferenceUtils.getMeetingIdFromStartRoutePreference(context) != null) {
                if (PreferenceUtils.getMeetingIdFromStartRoutePreference(context)
                        .equals(meetingModels.get(holder.getAdapterPosition()).getMeetingID())) {
                    ((HolderMeetingDataRow) holder).textViewClientName.setTypeface(null, Typeface.BOLD);
                    ((HolderMeetingDataRow) holder).textViewTime.setTypeface(null, Typeface.BOLD);
                    ((HolderMeetingDataRow) holder).textViewStatus.setTypeface(null, Typeface.BOLD);
                } else {
                    ((HolderMeetingDataRow) holder).textViewClientName.setTypeface(null, Typeface.NORMAL);
                    ((HolderMeetingDataRow) holder).textViewTime.setTypeface(null, Typeface.NORMAL);
                    ((HolderMeetingDataRow) holder).textViewStatus.setTypeface(null, Typeface.NORMAL);
                }
            }
        }


    }

    @Override
    public int getItemCount() {
        return meetingModels.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (meetingModels.get(position) == null) {
            return VIEW_TYPE_TITLE;
        } else {
            return VIEW_TYPE_DATA;
        }
    }

    /////////////////////////////////////////////////////////////////////////////// meeting to row layout

    private class HolderMeetingTopRow extends RecyclerView.ViewHolder {

        HolderMeetingTopRow(View itemView) {
            super(itemView);
        }
    }

    private class HolderMeetingDataRow extends RecyclerView.ViewHolder {
        AppCompatTextView textViewClientName = null, textViewTime = null, textViewStatus = null;
        LinearLayoutCompat layoutCompatClick = null;

        public HolderMeetingDataRow(View itemView) {
            super(itemView);
            this.textViewClientName = itemView.findViewById(R.id.meeting_list_data_row_client_id);
            this.textViewTime = itemView.findViewById(R.id.meeting_list_data_row_time_id);
            this.textViewStatus = itemView.findViewById(R.id.meeting_list_data_row_status_id);
            this.layoutCompatClick = itemView.findViewById(R.id.meeting_list_linear_layout);
        }
    }
}
