package com.lobotus.mapei.fromlite.mvp.intractor;

import android.content.Context;


import com.lobotus.mapei.fromlite.model.ModeOfTravelDataModel;

import java.util.ArrayList;

/**
 * Created by user1 on 23-11-2017.
 */

public interface MeetingDetailsIntractor {
    ///// get the MOT list from the server. . . .

    interface OnMOTDetailsDownlodeLisner
    {
        void OnMOTListDownlodeSuccess(ArrayList<ModeOfTravelDataModel> modeOfTravelDataModels);
        void OnMOTListDownlodeFail(String message, String dialogTitle, boolean isInternetError);
    }
    void reqToGetTheMOTListFromTheServer(Context context, OnMOTDetailsDownlodeLisner onMOTDetailsDownlodeLisner);
    void onStopMvpIntractor();
}
