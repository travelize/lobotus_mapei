package com.lobotus.mapei.fromlite.mvp.view;

/**
 * Created by user1 on 22-11-2017.
 */

public interface DistanceTravelledView {
    void showProgressDialog();
    void dismissProgressDialog();
    ///////////////////////////////////////////////////////////////// get the distance travelled and the time take. . .
    void afterDistanceTravelledDownlodeSuccess(String timeTaken, String distanceTravelled);
    void afterDistanceTravelledDownlodeFail(String message, String dialogTitle, boolean isInternetError);
}
