package com.lobotus.mapei.fromlite.mvp.presentorimp;

import android.content.Context;


import com.lobotus.mapei.fromlite.model.MeetingModel;
import com.lobotus.mapei.fromlite.mvp.intractor.MeetingIntractor;
import com.lobotus.mapei.fromlite.mvp.intractorimp.MeetingIntractorImp;
import com.lobotus.mapei.fromlite.mvp.presentor.MeetingPresentor;
import com.lobotus.mapei.fromlite.mvp.view.MeetingView;
import com.lobotus.mapei.utils.NetworkUtils;

import java.util.ArrayList;

/**
 * Created by user1 on 20-11-2017.
 */

public class MeetingPresentorImp implements MeetingPresentor,MeetingIntractor.MeetingListDownlodeLisner {


    private MeetingView meetingView=null;
    private MeetingIntractor meetingIntractor=null;

    public MeetingPresentorImp(MeetingView meetingView) {
        this.meetingView = meetingView;
        this.meetingIntractor = new MeetingIntractorImp();
    }

    ////////////////////////////////////// get the lis of the meeting. .
    @Override
    public void reqToGetTheListOfMeetingsFromPresentor(Context context, String date) {

        if (NetworkUtils.checkInternetAndOpenDialog(context))
        {
            meetingView.showProgressDialog();
            meetingIntractor.reqToServerToGetTheListOfMeetings(context,date,this);
        }

    }

    @Override
    public void OnMeetingListDownlodeSuccess(ArrayList<MeetingModel> meetingModels) {
        meetingView.dismissProgressDialog();
        meetingView.afterMeetingsListDownlodeSuccess(meetingModels);
    }

    @Override
    public void OnMeetingListDownlodeFail(String message, String dialogTitle, boolean isInternetError) {
        meetingView.dismissProgressDialog();
        meetingView.afterMeetingsListDownlodeFail(message,dialogTitle,isInternetError);
    }

    @Override
    public void onStopMvpPresentor() {
        meetingIntractor.onStopMvpIntractor();
    }
}
