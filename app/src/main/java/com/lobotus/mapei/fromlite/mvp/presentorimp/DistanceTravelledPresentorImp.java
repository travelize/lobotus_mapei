package com.lobotus.mapei.fromlite.mvp.presentorimp;

import android.content.Context;

import com.lobotus.mapei.fromlite.mvp.intractor.DistanceTravelledIntractor;
import com.lobotus.mapei.fromlite.mvp.intractorimp.DistanceTravelledIntractorImp;
import com.lobotus.mapei.fromlite.mvp.presentor.DistanceTravelledPresentor;
import com.lobotus.mapei.fromlite.mvp.view.DistanceTravelledView;
import com.lobotus.mapei.utils.NetworkUtils;


/**
 * Created by user1 on 22-11-2017.
 */

public class DistanceTravelledPresentorImp implements DistanceTravelledPresentor,DistanceTravelledIntractor.OnDistanceTravelledDownlodeLisner {

   private DistanceTravelledView distanceTravelledView=null;
    private DistanceTravelledIntractor distanceTravelledIntractor=null;

    public DistanceTravelledPresentorImp(DistanceTravelledView distanceTravelledView) {
        this.distanceTravelledView = distanceTravelledView;
        this.distanceTravelledIntractor = new DistanceTravelledIntractorImp();
    }

    @Override
    public void getTheDistanceTravelledAndTimetaken(Context context) {
        if (NetworkUtils.checkInternetAndOpenDialog(context))
        {
            distanceTravelledView.showProgressDialog();
            distanceTravelledIntractor.getDistanceTravelledAndTimeTakenFromTheServer(context,this);
        }

    }

    @Override
    public void OnDistanceTravelledDownlodeSuccess(String timeTaken, String DistanceTravelled) {
        distanceTravelledView.dismissProgressDialog();
        distanceTravelledView.afterDistanceTravelledDownlodeSuccess(timeTaken,DistanceTravelled);
    }

    @Override
    public void OnDistanceTravelledDownlodeFail(String message, String dialogTitle, boolean isInternetError) {
        distanceTravelledView.dismissProgressDialog();
        distanceTravelledView.afterDistanceTravelledDownlodeFail(message,dialogTitle,isInternetError);
    }

    @Override
    public void onStopMvpPresentor() {
        distanceTravelledIntractor.onStopMvpIntractor();
    }
}
