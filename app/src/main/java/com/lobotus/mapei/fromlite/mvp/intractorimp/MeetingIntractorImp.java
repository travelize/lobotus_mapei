package com.lobotus.mapei.fromlite.mvp.intractorimp;

import android.content.Context;


import com.google.android.gms.flags.IFlagProvider;
import com.google.gson.Gson;
import com.lobotus.mapei.R;
import com.lobotus.mapei.fromlite.model.MeetingModel;
import com.lobotus.mapei.fromlite.mvp.intractor.MeetingIntractor;
import com.lobotus.mapei.retrofitapi.APIService;
import com.lobotus.mapei.retrofitapi.ApiUtils;
import com.lobotus.mapei.utils.ConstantsUtils;
import com.lobotus.mapei.utils.PreferenceUtils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by user1 on 20-11-2017.
 */

public class MeetingIntractorImp implements MeetingIntractor {

    private Call<List<MeetingModel>> CallpostToGetTheMeetingList = null;

    ////////// get the list of meetings. . .  .
    @Override
    public void reqToServerToGetTheListOfMeetings(Context context, String date,
                                                  MeetingListDownlodeLisner meetingListDownlodeLisner) {
        if (PreferenceUtils.checkUserisLogedin(context)) {
            performMeetingListDownlode(context, date, meetingListDownlodeLisner);
        } else {
            meetingListDownlodeLisner.OnMeetingListDownlodeFail(context.getString(R.string.SessionLost),
                    context.getString(R.string.alert), false);
        }

    }

    @Override
    public void reqToServerToGetTheListOfMeetingsHistory(final Context context, String date, final MeetingListDownlodeLisner meetingListDownlodeLisner) {

        APIService apiService = ApiUtils.getAPIService(ConstantsUtils.postBaseUrl + ConstantsUtils.POST_TO_GET_MEETING_LIST_OR_MEETING_DETAILS_HISTORY);//ConstantsUtils.postBaseUrl + ConstantsUtils.POST_TO_GET_MEETING_LIST_OR_MEETING_DETAILS


        CallpostToGetTheMeetingList = apiService.postToGetTheMeetingList(PreferenceUtils.getUserIdFromPreference(context), date);
        CallpostToGetTheMeetingList.enqueue(new Callback<List<MeetingModel>>() {
            @Override
            public void onResponse(Call<List<MeetingModel>> call, Response<List<MeetingModel>> response) {

                if (response.isSuccessful()) {
                    if (response.body().size() > 0) {
                        if (response.body().get(0).getSuccess() == 1) {
                            System.out.println("cccccccccccc--performMeetingListDownlode-------" + new Gson().toJson(response.body()));
                            meetingListDownlodeLisner.OnMeetingListDownlodeSuccess((ArrayList<MeetingModel>) response.body());
                        } else {
                            meetingListDownlodeLisner.OnMeetingListDownlodeFail(response.body().get(0).getMsg(),
                                    context.getString(R.string.alert), false);
                        }
                    } else {
                        meetingListDownlodeLisner.OnMeetingListDownlodeFail(context.getString(R.string.justErrorCode) + " 30",
                                context.getString(R.string.alert), false);
                    }


                } else {
                    meetingListDownlodeLisner.OnMeetingListDownlodeFail(context.getString(R.string.justErrorCode) + " 33",
                            context.getString(R.string.internalError), false);
                }
            }

            @Override
            public void onFailure(Call<List<MeetingModel>> call, Throwable t) {
                System.out.println("ggggggggggggggg------------t-------------------" + t.getLocalizedMessage());

                meetingListDownlodeLisner.OnMeetingListDownlodeFail(context.getString(R.string.noInternetMessage) + " 34",
                        context.getString(R.string.noInternetAlert), true);
            }
        });


    }

    private void performMeetingListDownlode(final Context context, final String date,
                                            final MeetingListDownlodeLisner meetingListDownlodeLisner) {


        APIService apiService = ApiUtils.getAPIService(ConstantsUtils.postBaseUrl + ConstantsUtils.POST_TO_GET_MEETING_LIST_OR_MEETING_DETAILS);//ConstantsUtils.postBaseUrl + ConstantsUtils.POST_TO_GET_MEETING_LIST_OR_MEETING_DETAILS


        CallpostToGetTheMeetingList = apiService.postToGetTheMeetingList(PreferenceUtils.getUserIdFromPreference(context), date);
        CallpostToGetTheMeetingList.enqueue(new Callback<List<MeetingModel>>() {
            @Override
            public void onResponse(Call<List<MeetingModel>> call, Response<List<MeetingModel>> response) {

                if (response.isSuccessful()) {
                    if (response.body().size() > 0) {
                        if (response.body().get(0).getSuccess() == 1) {
                            System.out.println("cccccccccccc--performMeetingListDownlode-------" + new Gson().toJson(response.body()));
                            meetingListDownlodeLisner.OnMeetingListDownlodeSuccess((ArrayList<MeetingModel>) response.body());
                        } else {
                            meetingListDownlodeLisner.OnMeetingListDownlodeFail(response.body().get(0).getMsg(),
                                    context.getString(R.string.alert), false);
                        }
                    } else {
                        meetingListDownlodeLisner.OnMeetingListDownlodeFail(context.getString(R.string.justErrorCode) + " 30",
                                context.getString(R.string.alert), false);
                    }


                } else {
                    meetingListDownlodeLisner.OnMeetingListDownlodeFail(context.getString(R.string.justErrorCode) + " 33",
                            context.getString(R.string.internalError), false);
                }
            }

            @Override
            public void onFailure(Call<List<MeetingModel>> call, Throwable t) {
                System.out.println("ggggggggggggggg------------t-------------------" + t.getLocalizedMessage());

                meetingListDownlodeLisner.OnMeetingListDownlodeFail(context.getString(R.string.noInternetMessage) + " 34",
                        context.getString(R.string.noInternetAlert), true);
            }
        });

    }

    @Override
    public void onStopMvpIntractor() {

        if (CallpostToGetTheMeetingList != null)
            CallpostToGetTheMeetingList.cancel();
    }
}
