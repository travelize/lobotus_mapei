package com.lobotus.mapei.fromlite.mvp.intractor;

import android.content.Context;


import com.lobotus.mapei.fromlite.model.MeetingModel;

import java.util.ArrayList;

/**
 * Created by user1 on 20-11-2017.
 */

public interface MeetingIntractor {

    // get the meeting list
    interface MeetingListDownlodeLisner {
        void OnMeetingListDownlodeSuccess(ArrayList<MeetingModel> meetingModels);

        void OnMeetingListDownlodeFail(String message, String dialogTitle, boolean isInternetError);
    }

    void reqToServerToGetTheListOfMeetings(Context context, String date, MeetingListDownlodeLisner meetingListDownlodeLisner);

    void reqToServerToGetTheListOfMeetingsHistory(Context context, String date, MeetingListDownlodeLisner meetingListDownlodeLisner);

    //
    void onStopMvpIntractor();
}
