package com.lobotus.mapei.fromlite.mvp.presentorimp;

import android.content.Context;

import com.google.android.gms.maps.model.LatLng;
import com.lobotus.mapei.fromlite.mvp.intractor.DirectionIntractor;
import com.lobotus.mapei.fromlite.mvp.intractorimp.DirectionIntractorImp;
import com.lobotus.mapei.fromlite.mvp.presentor.DirectionPresentor;
import com.lobotus.mapei.fromlite.mvp.view.DirectionView;


/**
 * Created by user1 on 21-11-2017.
 */

public class DirectionPresentorImp implements DirectionPresentor,DirectionIntractor.ButtonTextLodeLisner,
        DirectionIntractor.OnRouteButtonClickResponse{
    private DirectionView directionView=null;
    private DirectionIntractor directionIntractor=null;

    public DirectionPresentorImp(DirectionView directionView) {
        this.directionView = directionView;
        this.directionIntractor = new DirectionIntractorImp();
    }

    @Override
    public void getTheButtontext(Context context) {
        directionIntractor.getTheButtonText(context,this);
    }

    @Override
    public void OnButtontextLodeSuccess(String text) {
        directionView.setTheButtonText(text);
    }

    ///////////////////////////// on route button click. . .

    @Override
    public void OnMapButtonClickHappened(Context context, LatLng latLngCurrentLoc, String meetingId, String mot, String clientName, boolean isMockLocEnabled) {
        directionView.showProgressDialog();
        directionIntractor.OnRouteButtonClick(context,latLngCurrentLoc,meetingId,this,mot,clientName,isMockLocEnabled);
    }

    @Override
    public void OnRouteButtonStartRouteSuccess(String message) {
        directionView.dismissProgressDialog();
        directionView.performStartRoute(message);

    }

    @Override
    public void OnRouteButtonStartRouteFail(String message, String dialogTitle, boolean isInternetError) {
        directionView.dismissProgressDialog();
        directionView.performStartRouteFail(message,dialogTitle,isInternetError);
    }

    @Override
    public void OnRouteButtonEndRouteSuccess(String message) {
        directionView.dismissProgressDialog();
        directionView.performEndRoute(message);
    }

    @Override
    public void OnRouteButtonEndRouteFail(String message, String dialogTitle, boolean isInternetError) {
        directionView.dismissProgressDialog();
        directionView.performEndRouteFail(message,dialogTitle,isInternetError);
    }

    @Override
    public void onStopMvpPresentor() {
        directionIntractor.onStopMvpIntractor();
    }
}
