package com.lobotus.mapei.fromlite.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by user1 on 23-11-2017.
 */

public class ModeOfTravelDataModel {
    @SerializedName("ModeOfTravel")
    @Expose
    private String modeOfTravel;

    public ModeOfTravelDataModel(String modeOfTravel) {
        this.modeOfTravel = modeOfTravel;
    }

    public String getModeOfTravel() {
        return modeOfTravel;
    }

    public void setModeOfTravel(String modeOfTravel) {
        this.modeOfTravel = modeOfTravel;
    }
}
