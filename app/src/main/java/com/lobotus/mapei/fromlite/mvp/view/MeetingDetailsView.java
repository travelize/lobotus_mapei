package com.lobotus.mapei.fromlite.mvp.view;


import com.lobotus.mapei.fromlite.model.ModeOfTravelDataModel;

import java.util.ArrayList;

/**
 * Created by user1 on 23-11-2017.
 */

public interface MeetingDetailsView {
    void showProgressDialog();
    void dismissProgressDialog();
    ///////////////////////////////////////////////////////// get the mot list  from the server...
    void afterMOTlistDownlodeSuccess(ArrayList<ModeOfTravelDataModel> modeOfTravelDataModels);
    void afterMOTlistDownlodeFail(String message, String dialogTitle, boolean isInternetError);


}
