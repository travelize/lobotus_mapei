package com.lobotus.mapei.fromlite.mvp.intractor;

import android.content.Context;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by user1 on 21-11-2017.
 */

public interface DirectionIntractor {
    ///////// get the button text. . . .
    interface ButtonTextLodeLisner
    {
        void OnButtontextLodeSuccess(String text);
    }
    void getTheButtonText(Context context, ButtonTextLodeLisner buttonTextLodeLisner);

    ////////// on direction button click
    interface OnRouteButtonClickResponse
    {
        void OnRouteButtonStartRouteSuccess(String message);
        void OnRouteButtonStartRouteFail(String message, String dialogTitle, boolean isInternetError);
        void OnRouteButtonEndRouteSuccess(String message);
        void OnRouteButtonEndRouteFail(String message, String dialogTitle, boolean isInternetError);
    }
    void OnRouteButtonClick(Context context, LatLng latLngCurrentLoc, String meetingId, OnRouteButtonClickResponse
            OnRouteButtonClickResponse, String mot, String clientName, boolean isMockLocEnabled);

    //////
    void onStopMvpIntractor();


}
