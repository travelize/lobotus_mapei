package com.lobotus.mapei.fromlite.mvp.view;

/**
 * Created by user1 on 21-11-2017.
 */

public interface DirectionView {
    void showProgressDialog();
    void dismissProgressDialog();
    ///////////////////////////////////////// setting the map button details. . . .
    void setTheButtonText(String text);
    //////////////////////////////////////// map button click action'
    void performStartRoute(String message);
    void performStartRouteFail(String message, String dialogTitle, boolean isInternetError);
    void performEndRoute(String message);
    void performEndRouteFail(String message, String dialogTitle, boolean isInternetError);
}
