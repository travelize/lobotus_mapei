package com.lobotus.mapei.fromlite.mvp.view;



import com.lobotus.mapei.fromlite.model.MeetingModel;

import java.util.ArrayList;

/**
 * Created by user1 on 20-11-2017.
 */

public interface MeetingView {
    void showProgressDialog();
    void dismissProgressDialog();
    ////////////////////////////////////////////////// get meeting data model
    void afterMeetingsListDownlodeSuccess(ArrayList<MeetingModel> meetingModels);
    void afterMeetingsListDownlodeFail(String message, String dialogTitle, boolean isInternetError);

}
