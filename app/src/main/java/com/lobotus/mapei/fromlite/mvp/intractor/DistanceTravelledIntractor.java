package com.lobotus.mapei.fromlite.mvp.intractor;

import android.content.Context;

/**
 * Created by user1 on 22-11-2017.
 */

public interface DistanceTravelledIntractor {

    interface OnDistanceTravelledDownlodeLisner
    {
        void OnDistanceTravelledDownlodeSuccess(String timeTaken, String DistanceTravelled);
        void OnDistanceTravelledDownlodeFail(String message, String dialogTitle, boolean isInternetError);
    }
    void getDistanceTravelledAndTimeTakenFromTheServer(Context context, OnDistanceTravelledDownlodeLisner onDistanceTravelledDownlodeLisner);
    //
    void onStopMvpIntractor();
}
