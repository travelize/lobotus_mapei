package com.lobotus.mapei.fromlite.mvp.presentorimp;

import android.content.Context;

import com.lobotus.mapei.R;
import com.lobotus.mapei.fromlite.mvp.intractor.MeetingStatusFragmentIntractor;
import com.lobotus.mapei.fromlite.mvp.intractorimp.MeetingStatusFragmentIntractorImp;
import com.lobotus.mapei.fromlite.mvp.presentor.MeetingStatusFragPresentor;
import com.lobotus.mapei.fromlite.mvp.view.MeetingStatusFragView;
import com.lobotus.mapei.utils.CommonFunc;
import com.lobotus.mapei.utils.NetworkUtils;

import static com.lobotus.mapei.utils.CommonFunc.*;


/**
 * Created by user1 on 22-11-2017.
 */

public class MeetingStatusFragPresentorImp implements MeetingStatusFragPresentor, MeetingStatusFragmentIntractor.OnMeetingStatusUplodeLisner {


    private MeetingStatusFragView meetingStatusFragView=null;
    private MeetingStatusFragmentIntractor meetingStatusFragmentIntractor=null;

    public MeetingStatusFragPresentorImp(MeetingStatusFragView meetingStatusFragView) {
        this.meetingStatusFragView = meetingStatusFragView;
        this.meetingStatusFragmentIntractor=new MeetingStatusFragmentIntractorImp();
    }

    @Override
    public void validateMeetingStatus(Context context, int statusSpinnerSelectedPos, String fromDate, String toDate) {
//        {"Select","Completed","Follow up","Cancelled"}

        if (statusSpinnerSelectedPos>0)
        {
            if (statusSpinnerSelectedPos==2)
            {
                // follow up
                if (fromDate.isEmpty())
                {
                    commonDialog(context,context.getString(R.string.alert),"Please enter follow-up  date",false);
                }else if (toDate.isEmpty())
                {
                    commonDialog(context,context.getString(R.string.alert),"Please enter follow-up time",false);

                }else {
                    meetingStatusFragView.afterMeetingStatusValidationSuccess();
                }
            }else {
                // completed or cancelled
//                if (expences.isEmpty())
//                {
//                    CommonFunc.commonDialog(context,context.getString(R.string.alert),"Please enter travel expenses",false);
//                }else {
                meetingStatusFragView.afterMeetingStatusValidationSuccess();
//                }
            }
        }else {
            commonDialog(context,context.getString(R.string.alert),"Please select meeting status",false);
        }


    }



    ///////////////////////////////// update the meeting status to the server. . . . .
    @Override
    public void reqToUpdateTheMeetingStatusToTheServer(Context context, int statusSpinnerSelectedPos,
                                                       String fromDate, String toDate,
                                                       String b64Image,
                                                       String notes,int meetingType,int meetingSubtype) {
        if (NetworkUtils.checkInternetAndOpenDialog(context))
        {
            meetingStatusFragView.showProgressDialog();
            meetingStatusFragmentIntractor.reqToUplodeTheMeetingStatusToTheServer(context,statusSpinnerSelectedPos,
                    fromDate,toDate,this,b64Image,notes,meetingType,meetingSubtype);
        }

    }

    @Override
    public void onMeetingStatusUplodeSuccess(String message) {

        meetingStatusFragView.afterMeetingStatusAndexpenceUpdatedToServerIsSuccess(message);
    }

    @Override
    public void onMeetingStatusUplodeFail(String message, String dialogTitle, boolean isInternetError) {
        meetingStatusFragView.dismissProgressDialog();
        meetingStatusFragView.afterMeetingStatusAndexpenceUpdatedToServerIsFail(message,dialogTitle,isInternetError);
    }

    @Override
    public void onStopMvpPresentor() {
        meetingStatusFragmentIntractor.onStopMvpIntractor();
    }
}
