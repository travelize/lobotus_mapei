package com.lobotus.mapei.fromlite.mvp.presentorimp;

import android.content.Context;


import com.lobotus.mapei.fromlite.model.ModeOfTravelDataModel;
import com.lobotus.mapei.fromlite.mvp.intractor.MeetingDetailsIntractor;
import com.lobotus.mapei.fromlite.mvp.intractorimp.MeetingDetailsIntractorImp;
import com.lobotus.mapei.fromlite.mvp.presentor.MeetingDetailsPresentor;
import com.lobotus.mapei.fromlite.mvp.view.MeetingDetailsView;
import com.lobotus.mapei.utils.NetworkUtils;

import java.util.ArrayList;

/**
 * Created by user1 on 23-11-2017.
 */

public class MeetingDetailsPresentorImp implements
        MeetingDetailsPresentor,
        MeetingDetailsIntractor.OnMOTDetailsDownlodeLisner {

    private MeetingDetailsView meetingDetailsView=null;
    private MeetingDetailsIntractor meetingDetailsIntractor=null;

    public MeetingDetailsPresentorImp(MeetingDetailsView meetingDetailsView) {
        this.meetingDetailsView = meetingDetailsView;
        this.meetingDetailsIntractor = new MeetingDetailsIntractorImp();
    }

    ////////////////////////////////////// get MOT list
    @Override
    public void reqToGetTheMOT(Context context) {
        if (NetworkUtils.checkInternetAndOpenDialog(context))
        {
            meetingDetailsView.showProgressDialog();
            meetingDetailsIntractor.reqToGetTheMOTListFromTheServer(context,this);
        }
    }

    @Override
    public void OnMOTListDownlodeSuccess(ArrayList<ModeOfTravelDataModel> modeOfTravelDataModels) {
//        meetingDetailsView.dismissProgressDialog();
        meetingDetailsView.afterMOTlistDownlodeSuccess(modeOfTravelDataModels);
    }

    @Override
    public void OnMOTListDownlodeFail(String message, String dialogTitle, boolean isInternetError) {
        meetingDetailsView.dismissProgressDialog();
        meetingDetailsView.afterMOTlistDownlodeFail(message,dialogTitle,isInternetError);

    }
    @Override
    public void onStopMvpPresentor() {
        meetingDetailsIntractor.onStopMvpIntractor();
    }
}
