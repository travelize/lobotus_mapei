package com.lobotus.mapei.fromlite.mvp.intractorimp;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.BatteryManager;

import com.google.android.gms.maps.model.LatLng;
import com.lobotus.mapei.R;
import com.lobotus.mapei.fromlite.mvp.intractor.DirectionIntractor;
import com.lobotus.mapei.retrofitapi.APIService;
import com.lobotus.mapei.retrofitapi.ApiUtils;
import com.lobotus.mapei.utils.CommonFunc;
import com.lobotus.mapei.utils.ConstantsUtils;
import com.lobotus.mapei.utils.PreferenceUtils;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by user1 on 21-11-2017.
 */

public class DirectionIntractorImp implements DirectionIntractor {

    private Call<ResponseBody> CallpostUserCoordinatesToServerInActivityForStarted=null;
    private Call<ResponseBody> CallpostUserCoordinatesToServerInActivityForEnded=null;
    // get the button text. .
    @Override
    public void getTheButtonText(Context context, ButtonTextLodeLisner buttonTextLodeLisner) {

        if (PreferenceUtils.isMeetingIdPresentInTheStartRoutePreference(context))
        {
            if (PreferenceUtils.getMeetingIdFromStartRoutePreference(context)!=null)
            {
                buttonTextLodeLisner.OnButtontextLodeSuccess("End Route");
            }else {
                buttonTextLodeLisner.OnButtontextLodeSuccess("Start Route");
            }
        }else {
            buttonTextLodeLisner.OnButtontextLodeSuccess("Start Route");
        }

    }


    //////////// on direction button click. . . .

    @Override
    public void OnRouteButtonClick(Context context, LatLng latLngCurrentLoc, String meetingId,
                                   OnRouteButtonClickResponse OnRouteButtonClickResponse
            , String mot, String clientName, boolean isMockLocEnabled) {
        if (PreferenceUtils.isMeetingIdPresentInTheStartRoutePreference(context))
        {
            if (PreferenceUtils.getMeetingIdFromStartRoutePreference(context)!=null)
            {
//                ("End Route");
                if (PreferenceUtils.getMeetingIdFromStartRoutePreference(context).equals(meetingId))
                {
                    performEndRouteAsyncTask(context,latLngCurrentLoc,meetingId,OnRouteButtonClickResponse,isMockLocEnabled);
                }else {
                    CommonFunc.commonDialog(context,context.getString(R.string.internalError),
                            context.getString(R.string.justErrorCode)+" 43",false);
                }
            }else {
//                ("Start Route");
                performStartRouteAsyncTask(context,latLngCurrentLoc,meetingId,OnRouteButtonClickResponse,mot,clientName,isMockLocEnabled);
            }
        }else {
//            ("Start Route");
            performStartRouteAsyncTask(context,latLngCurrentLoc,meetingId,OnRouteButtonClickResponse,mot,clientName,isMockLocEnabled);
        }
    }
    //// temp passage variables to asynctask
    private AsyncTaskToGetTheAddress asyncTaskToGetTheAddress=null;
    private Context RouteContext=null;
    private LatLng RouteLatLng=null;
    private String RouteMeetingId=null;
    private OnRouteButtonClickResponse RouteOnRouteButtonClickResponse=null;
    private boolean isStartRoute=false;
    private String RouteCurrentAddress="";
    private String RouteMOT=null;
    private String RouteClientName=null;
    private boolean RouteisMockLocEnabled=false;
    ////

    private void performStartRouteAsyncTask(Context context, LatLng latLngCurrentLoc, String meetingId,
                                            OnRouteButtonClickResponse OnRouteButtonClickResponse,
                                            String mot, String clientName, boolean isMockLocEnabled)
    {
        startAsynctaskToGetTheAddress(latLngCurrentLoc);
        this.RouteContext=context;
        this.RouteLatLng=latLngCurrentLoc;
        this.RouteMeetingId=meetingId;
        this.RouteOnRouteButtonClickResponse=OnRouteButtonClickResponse;
        this.isStartRoute=true;
        this.RouteMOT=mot;
        this.RouteClientName=clientName;
        this.RouteisMockLocEnabled=isMockLocEnabled;
    }

    private void performEndRouteAsyncTask(Context context, LatLng latLngCurrentLoc, String meetingId,
                                          OnRouteButtonClickResponse OnRouteButtonClickResponse, boolean isMockLocEnabled)
    {
        if (!PreferenceUtils.isStartedRouteEnded(context))
        {
            startAsynctaskToGetTheAddress(latLngCurrentLoc);
            this.RouteContext=context;
            this.RouteLatLng=latLngCurrentLoc;
            this.RouteMeetingId=meetingId;
            this.RouteOnRouteButtonClickResponse=OnRouteButtonClickResponse;
            this.isStartRoute=false;
            this.RouteisMockLocEnabled=isMockLocEnabled;
        }else {
            OnRouteButtonClickResponse.OnRouteButtonEndRouteSuccess(context.getString(R.string.already_ended_route));
        }

    }
    //
    private void startAsynctaskToGetTheAddress(LatLng latLngCurrentLoc)
    {
        String req="https://maps.googleapis.com/maps/api/geocode/json?latlng="+
                latLngCurrentLoc.latitude+","+latLngCurrentLoc.longitude;
        if (asyncTaskToGetTheAddress!=null)
        {
            if (asyncTaskToGetTheAddress.getStatus()!= AsyncTask.Status.RUNNING)
            {
                asyncTaskToGetTheAddress=new AsyncTaskToGetTheAddress();
                asyncTaskToGetTheAddress.execute(req);
            }
        }else {
            asyncTaskToGetTheAddress=new AsyncTaskToGetTheAddress();
            asyncTaskToGetTheAddress.execute(req);
        }
    }


    private class  AsyncTaskToGetTheAddress extends AsyncTask<String, Void, String>
    {

        @Override
        protected String doInBackground(String... strings) {
            return makeserverConnection(strings[0]);
        }
        @Override
        protected void onPostExecute(String s) {
            if (s!=null)
            {
                try {
                    JSONObject jsonObject=new JSONObject(s);
                    JSONArray jsonArray=jsonObject.getJSONArray("results");
                    if (jsonArray.length()>0)
                    {
                        String formatedAddress=jsonArray.getJSONObject(0).getString("formatted_address");
                        if (formatedAddress!=null && !formatedAddress.isEmpty()) {
                            RouteCurrentAddress=formatedAddress;
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            postTheRouteToTheServer();
        }


    }

    public String makeserverConnection(String requestURL) {
        URL url;
        String response = "";
        try {
            url = new URL(requestURL);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded");
            conn.setDoOutput(true);
            int responseCode = conn.getResponseCode();
            if (responseCode == HttpsURLConnection.HTTP_OK) {

                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line = br.readLine()) != null) {
                    response += line;
                }
            } else {
                response = "";
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }
    /////////////////////////////////////////////////////////// get the battery life
    /////////////////////////////////// battery life
    public static String batterylevel(Context context){
        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = context.registerReceiver(null, ifilter);
        int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
        float batteryPct = (level / (float)scale) * 100;
        int ss = Integer.valueOf((int) batteryPct);
        System.out.println("batteryPct" +ss + "%");
        if(ss == 0){
            ss = 0;
            return  ss + "%";
        }  else {

            return  ss + "%";
        }
    }

    ////////////////////////////////////////////////// post the start route to the server
    private void postTheRouteToTheServer()
    {
        if (isStartRoute)
        {
            postTheStartRouteToTheServer();
        }else {
            postTheEndRouteToTheServer();
        }
    }

    // post the start route to the server. . .
    private void postTheStartRouteToTheServer()
    {
        String mockRoute="";
        if (RouteisMockLocEnabled)
            mockRoute="Yes";
        else
            mockRoute="No";

        APIService apiService= ApiUtils.getAPIService(ConstantsUtils.postBaseUrl+ConstantsUtils.POST_USER_COORDINATES_TO_SERVER);
        CallpostUserCoordinatesToServerInActivityForStarted=
                apiService.postUserCoordinatesToServerInActivityForStarted(PreferenceUtils.getUserIdFromPreference(RouteContext),
                String.valueOf(RouteLatLng.longitude),
                String.valueOf(RouteLatLng.latitude),
                batterylevel(RouteContext),
                PreferenceUtils.getlatestSignalStatus(RouteContext),
                RouteCurrentAddress,
                RouteMeetingId,
                "Yes",
                "No",
                "Started",mockRoute);
        //////////////////////////////////////////////
        System.out.println("kkkkkkkkkkkkkkkkkkkk---------------------------"+PreferenceUtils.getUserIdFromPreference(RouteContext));
        System.out.println("kkkkkkkkkkkkkkkkkkkk---------------------------"+ String.valueOf(RouteLatLng.longitude));
        System.out.println("kkkkkkkkkkkkkkkkkkkk---------------------------"+String.valueOf(RouteLatLng.latitude));
        System.out.println("kkkkkkkkkkkkkkkkkkkk---------------------------"+PreferenceUtils.getUserIdFromPreference(RouteContext));
        System.out.println("kkkkkkkkkkkkkkkkkkkk---------------------------"+PreferenceUtils.getUserIdFromPreference(RouteContext));
        System.out.println("kkkkkkkkkkkkkkkkkkkk---------------------------"+PreferenceUtils.getUserIdFromPreference(RouteContext));
        System.out.println("kkkkkkkkkkkkkkkkkkkk---------------------------"+PreferenceUtils.getUserIdFromPreference(RouteContext));


        /////////////////////////////////////////////
        CallpostUserCoordinatesToServerInActivityForStarted.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful())
                {
                    try {
                        JSONObject jsonObject=new JSONObject(response.body().string());
                        if (jsonObject.getInt("Success")==1)
                        {
                            PreferenceUtils.addMeetingIdToTheStaredRoute(RouteContext,RouteMeetingId,RouteMOT,RouteClientName);
                            RouteOnRouteButtonClickResponse.OnRouteButtonStartRouteSuccess(jsonObject.getString("Msg"));
                        }else {
                            RouteOnRouteButtonClickResponse.OnRouteButtonStartRouteFail(jsonObject.getString("Msg"),
                                    RouteContext.getString(R.string.alert),false);
                        }
                    } catch (JSONException | IOException e) {
                        RouteOnRouteButtonClickResponse.OnRouteButtonStartRouteFail(RouteContext.getString(R.string.justErrorCode)+" 44",
                                RouteContext.getString(R.string.internalError),false);
                    }
                }else {
                    RouteOnRouteButtonClickResponse.OnRouteButtonStartRouteFail(RouteContext.getString(R.string.justErrorCode)+" 45",
                            RouteContext.getString(R.string.internalError),false);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                RouteOnRouteButtonClickResponse.OnRouteButtonStartRouteFail(RouteContext.getString(R.string.noInternetMessage)+" 46",
                        RouteContext.getString(R.string.noInternetAlert),true);
            }
        });
    }
    // post the end route to the server. . .
    private void postTheEndRouteToTheServer()
    {
        String mockRoute="";
        if (RouteisMockLocEnabled)
            mockRoute="Yes";
        else
            mockRoute="No";
        APIService apiService= ApiUtils.getAPIService(ConstantsUtils.postBaseUrl+ConstantsUtils.POST_USER_COORDINATES_TO_SERVER);
        CallpostUserCoordinatesToServerInActivityForEnded=apiService.postUserCoordinatesToServerInActivityForEnded(PreferenceUtils.getUserIdFromPreference(RouteContext),
                String.valueOf(RouteLatLng.longitude),
                String.valueOf(RouteLatLng.latitude),
                batterylevel(RouteContext),
                PreferenceUtils.getlatestSignalStatus(RouteContext),
                RouteCurrentAddress,
                RouteMeetingId,
                "No",
                "Yes",
                mockRoute);
        CallpostUserCoordinatesToServerInActivityForEnded.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful())
                {
                    try {
                        JSONObject jsonObject=new JSONObject(response.body().string());
                        if (jsonObject.getInt("Success")==1)
                        {
//                            PreferenceUtils.clearTheStartedRouteMeetingIdFromTheSharedPreference(RouteContext);
                            PreferenceUtils.setStartedRouteEnded(RouteContext);
                            RouteOnRouteButtonClickResponse.OnRouteButtonEndRouteSuccess(jsonObject.getString("Msg"));
                        }else {
                            RouteOnRouteButtonClickResponse.OnRouteButtonEndRouteFail(jsonObject.getString("Msg"),
                                    RouteContext.getString(R.string.alert),false);
                        }
                    } catch (JSONException | IOException e) {
                        RouteOnRouteButtonClickResponse.OnRouteButtonEndRouteFail(RouteContext.getString(R.string.justErrorCode)+" 47",
                                RouteContext.getString(R.string.internalError),false);
                    }
                }else {
                    RouteOnRouteButtonClickResponse.OnRouteButtonEndRouteFail(RouteContext.getString(R.string.justErrorCode)+" 48",
                            RouteContext.getString(R.string.internalError),false);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                RouteOnRouteButtonClickResponse.OnRouteButtonEndRouteFail(RouteContext.getString(R.string.noInternetMessage)+" 49",
                        RouteContext.getString(R.string.noInternetAlert),true);
            }
        });
    }


    @Override
    public void onStopMvpIntractor() {
        if (asyncTaskToGetTheAddress!=null)
        {
            if (asyncTaskToGetTheAddress.getStatus()== AsyncTask.Status.RUNNING)
                asyncTaskToGetTheAddress.cancel(true);
        }
        if (CallpostUserCoordinatesToServerInActivityForStarted!=null)
            CallpostUserCoordinatesToServerInActivityForStarted.cancel();
        if (CallpostUserCoordinatesToServerInActivityForEnded!=null)
            CallpostUserCoordinatesToServerInActivityForEnded.cancel();
    }
}
