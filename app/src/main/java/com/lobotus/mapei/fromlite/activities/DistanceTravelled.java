package com.lobotus.mapei.fromlite.activities;


import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.widget.RelativeLayout;

import com.lobotus.mapei.R;
import com.lobotus.mapei.fromlite.baseact.SupportActivity;
import com.lobotus.mapei.fromlite.model.MeetingModel;
import com.lobotus.mapei.fromlite.mvp.presentor.DistanceTravelledPresentor;
import com.lobotus.mapei.fromlite.mvp.presentorimp.DistanceTravelledPresentorImp;
import com.lobotus.mapei.fromlite.mvp.view.DistanceTravelledView;
import com.lobotus.mapei.utils.CommonFunc;


/**
 * A simple {@link Fragment} subclass.
 */
public class DistanceTravelled extends SupportActivity
        implements DistanceTravelledView,View.OnClickListener {

    private DistanceTravelledPresentor distanceTravelledPresentor=null;
    private Dialog dialog=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        RelativeLayout relativeLayout=findViewById(R.id.content_support);
        getLayoutInflater().inflate(R.layout.fragment_distance_travelled,relativeLayout);
        getSupportActionBar().setTitle(((MeetingModel) getIntent().getExtras().getSerializable("MEETING_MODEL")).getFullName()+"'s travel distance");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        findViewById(R.id.fragment_show_time_dest_next_button).setOnClickListener(this);
        distanceTravelledPresentor=new DistanceTravelledPresentorImp(this);
        distanceTravelledPresentor.getTheDistanceTravelledAndTimetaken(DistanceTravelled.this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.fragment_show_time_dest_next_button:
                Intent intent=new Intent(DistanceTravelled.this,MeetingStatus.class);
                intent.putExtra("MEETING_MODEL",getIntent().getExtras().getSerializable("MEETING_MODEL"));
                startActivity(intent);


        }
    }
    ////////////////////////////////////////////////////////////////////////////////////


    @Override
    protected void onStop() {
        super.onStop();
        if (distanceTravelledPresentor!=null)
        {
            distanceTravelledPresentor.onStopMvpPresentor();
        }
        dismissProgressDialog();
    }
    //////////////////////////////////////////////////////////////////////////////////////  mvp
    @Override
    public void showProgressDialog() {
        dialog=new Dialog(DistanceTravelled.this);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.loaging_layout);
        dialog.show();
    }

    @Override
    public void dismissProgressDialog() {

        if (dialog!=null)
        {
            if (dialog.isShowing())
            {
                dialog.dismiss();
            }
            dialog=null;
        }
    }

    @Override
    public void afterDistanceTravelledDownlodeSuccess(String timeTaken, String distanceTravelled) {
        ((AppCompatTextView)findViewById(R.id.fragment_show_time_distance_id)).setText(""+distanceTravelled);
        ((AppCompatTextView)findViewById(R.id.fragment_show_time_time_id)).setText(""+timeTaken);
        if (timeTaken.isEmpty())
        {
            ((AppCompatTextView)findViewById(R.id.fragment_show_time_time_id)).setText("NA");
        }
        if (distanceTravelled.isEmpty())
        {
            ((AppCompatTextView)findViewById(R.id.fragment_show_time_distance_id)).setText("NA");
        }
    }

    @Override
    public void afterDistanceTravelledDownlodeFail(String message, String dialogTitle, boolean isInternetError) {
        CommonFunc.commonDialog(DistanceTravelled.this,dialogTitle,message,isInternetError);
    }



////////////////////////////////////////////////////////////////////////////////////
}
