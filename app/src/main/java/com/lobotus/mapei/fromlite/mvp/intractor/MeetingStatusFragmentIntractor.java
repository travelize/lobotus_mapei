package com.lobotus.mapei.fromlite.mvp.intractor;

import android.content.Context;

/**
 * Created by user1 on 22-11-2017.
 */

public interface MeetingStatusFragmentIntractor {

    interface OnMeetingStatusUplodeLisner
    {
        void onMeetingStatusUplodeSuccess(String message);
        void onMeetingStatusUplodeFail(String message, String dialogTitle, boolean isInternetError);
    }
    void reqToUplodeTheMeetingStatusToTheServer(Context context,
                                                int statusSpinnerSelectedPos,
                                                String fromDate,
                                                String toDate,
                                                OnMeetingStatusUplodeLisner onMeetingStatusUplodeLisner,
                                                String b64Image,
                                                String notes,int meetingType,int meetingSubtype);
    void onStopMvpIntractor();
}
