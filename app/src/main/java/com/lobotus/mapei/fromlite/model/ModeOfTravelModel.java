package com.lobotus.mapei.fromlite.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by user1 on 23-11-2017.
 */

public class ModeOfTravelModel {
    @SerializedName("Success")
    @Expose
    private Integer success;
    @SerializedName("data")
    @Expose
    private List<ModeOfTravelDataModel> data = null;
    @SerializedName("Msg")
    @Expose
    private String errorMessage=null;

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public List<ModeOfTravelDataModel> getData() {
        return data;
    }

    public void setData(List<ModeOfTravelDataModel> data) {
        this.data = data;
    }

}
