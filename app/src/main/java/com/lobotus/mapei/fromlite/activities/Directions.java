package com.lobotus.mapei.fromlite.activities;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.lobotus.mapei.R;
import com.lobotus.mapei.flowfragments.mapdistance.DirectionsJSONParser;
import com.lobotus.mapei.fromlite.baseact.SupportActivity;
import com.lobotus.mapei.fromlite.model.MeetingModel;
import com.lobotus.mapei.fromlite.mvp.presentor.DirectionPresentor;
import com.lobotus.mapei.fromlite.mvp.presentorimp.DirectionPresentorImp;
import com.lobotus.mapei.fromlite.mvp.view.DirectionView;
import com.lobotus.mapei.utils.CommonFunc;
import com.lobotus.mapei.utils.ConstantsUtils;
import com.lobotus.mapei.utils.NetworkUtils;
import com.lobotus.mapei.utils.PermissionUtils;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

public class Directions extends SupportActivity implements OnMapReadyCallback, View.OnClickListener,
        DirectionView {

    private GoogleMap mMap;
    private Dialog dialog = null;
    //////////////////////////////////////////////////////////////////// temp lat long of the start and the end loc
    private LatLng latLngMyLocTemp = null;
    private LatLng latLngClientLocTemp = null;
    //////////////////////////////////////////////////// map marker
    private Marker markerMyLoc = null;
    private Marker markerClientLoc = null;
    ////////////////////////////////////////////////// temp distance and time storage. . . .
    private String tempDistanceAndTimeStorage = "";
    //////////////////////////////////////////////// temp variable to mock loc is enabled or not. . . .
    private boolean isMockLocEnabled = false;
    //////// mvp
    private DirectionPresentor directionPresentor = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        RelativeLayout relativeLayout = findViewById(R.id.content_support);
        getLayoutInflater().inflate(R.layout.activity_directions, relativeLayout);
        findViewById(R.id.activity_direction_button_id).setOnClickListener(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (((MeetingModel) getIntent().getExtras().getSerializable("MEETING_MODEL")) != null) {
            getSupportActionBar().setTitle("" + ((MeetingModel) getIntent().getExtras().getSerializable("MEETING_MODEL")).getFullName() +
                    "'s travel details");
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.activity_direction_maps_id);
        mapFragment.getMapAsync(this);
        // mvp
        directionPresentor = new DirectionPresentorImp(this);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.activity_direction_button_id:


                directionPresentor.OnMapButtonClickHappened(Directions.this, latLngMyLocTemp,
                        ((MeetingModel) getIntent().getExtras().getSerializable("MEETING_MODEL")).getMeetingID(),
                        getIntent().getStringExtra("MOT"),
                        ((MeetingModel) getIntent().getExtras().getSerializable("MEETING_MODEL")).getFullName(),
                        isMockLocEnabled);
                break;
            case R.id.activity_direction_navigat_float_button:
                if (latLngClientLocTemp != null) {
                    try {
                        Intent intent = new Intent(Intent.ACTION_VIEW,
                                Uri.parse("geo:0,0?q=" + latLngClientLocTemp.latitude + "," + latLngClientLocTemp.longitude + "(" + ""
                                        + ((MeetingModel) getIntent().getExtras().getSerializable("MEETING_MODEL")).getFullName() + "'s location" + ")"));
                        startActivity(intent);
                    } catch (NumberFormatException e) {
                        CommonFunc.commonDialog(Directions.this, getString(R.string.alert), getString(R.string.justErrorCode) + " 83", false);
                    }
                } else {
                    CommonFunc.commonDialog(Directions.this, getString(R.string.alert), "Location not routed... but you can continue..", false);
                }


        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {


        /*try {

            googleMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            this, R.raw.map_aubgrein_style));


        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }
*/

        mMap = googleMap;
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
        mMap.getUiSettings().setCompassEnabled(true);
        mMap.getUiSettings().setRotateGesturesEnabled(true);
        mMap.getUiSettings().setTiltGesturesEnabled(true);
        mMap.setBuildingsEnabled(true);
//        mMap.setMapStyle(new MapStyleOptions());
        connectToGoogleApiClient();
        directionPresentor.getTheButtontext(Directions.this);
    }

    public void attachFragment(Fragment fragment, boolean isAddToBackStack, String tag, Bundle bundle) {
        if (bundle != null)
            fragment.setArguments(bundle);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.activity_direction_content, fragment, tag);
        if (isAddToBackStack)
            fragmentTransaction.addToBackStack(tag);
        fragmentTransaction.commit();
    }

    @Override
    protected void onStop() {
        super.onStop();
        stopLocationUpdates();
        if (directionPresentor != null) {
            directionPresentor.onStopMvpPresentor();
        }
        dismissProgressDialog();
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////// mvp. . .

    @SuppressWarnings("ConstantConditions")
    @Override
    public void showProgressDialog() {
        dialog = new Dialog(Directions.this);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.loaging_layout);
        dialog.show();
    }

    @Override
    public void dismissProgressDialog() {
        if (dialog != null) {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
            dialog = null;
        }
    }

    @Override
    public void setTheButtonText(String text) {
        ((AppCompatButton) findViewById(R.id.activity_direction_button_id))
                .setText(text);
    }

    @Override
    public void performStartRoute(String message) {
        directionPresentor.getTheButtontext(Directions.this);
        Intent intent = new Intent(ConstantsUtils.GEO_FENCE_BROADCAST_ACTION);
        intent.putExtra(ConstantsUtils.IS_GEO_FENCE_EVENT_FOR_START_GEOFENCE_BOOLEAN, true);

        if (latLngClientLocTemp == null) {
            Toast.makeText(this, "Address Not Found", Toast.LENGTH_SHORT).show();
        } else {
            intent.putExtra("LAT", latLngClientLocTemp.latitude);
            intent.putExtra("LNG", latLngClientLocTemp.longitude);
            sendBroadcast(intent);
            findViewById(R.id.activity_direction_navigat_float_button).setVisibility(View.VISIBLE);
            findViewById(R.id.activity_direction_navigat_float_button).setOnClickListener(Directions.this);
            Toast.makeText(Directions.this, "" + message, Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void performStartRouteFail(String message, String dialogTitle, boolean isInternetError) {
        CommonFunc.commonDialog(Directions.this, dialogTitle, message, false);
    }


    @Override
    public void performEndRouteFail(String message, String dialogTitle, boolean isInternetError) {
        CommonFunc.commonDialog(Directions.this, dialogTitle, message, false);
    }

    @Override
    public void performEndRoute(String message) {
        if (message.equals(getString(R.string.already_ended_route))) {
            Toast.makeText(Directions.this, message, Toast.LENGTH_LONG).show();
            Intent intent = new Intent(Directions.this, MeetingStatus.class);
            intent.putExtra("MEETING_MODEL", ((MeetingModel) getIntent().getExtras().getSerializable("MEETING_MODEL")));
            startActivity(intent);
        } else {
            Intent intent = new Intent(Directions.this, DistanceTravelled.class);
            intent.putExtra("MEETING_MODEL", ((MeetingModel) getIntent().getExtras().getSerializable("MEETING_MODEL")));
            startActivity(intent);
        }
    }

    //////////////////////////////////////////////////////////////////////////////////// location and maps stuff without mvp. . . . . dirty code
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// graw loc on marker. . . . 

    ///////////////////////////////////////////////////////////////////////// google api client establishment to access address
    ////
    private GoogleApiClient googleApiClient = null;
    private LocationRequest locationRequest = null;
    private FusedLocationProviderClient mFusedLocationClient = null;

    ///
    private void connectToGoogleApiClient() {
        if (NetworkUtils.checkInternetAndOpenDialog(Directions.this)) {
            if (CommonFunc.isGooglePlayServicesAvailable(Directions.this)) {
                if (dialog == null) {
                    showProgressDialog();
                    connectGoogleApiClient();
                }
            }
        }
    }

    private void connectGoogleApiClient() {

        googleApiClient = new GoogleApiClient.Builder(Directions.this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(@Nullable Bundle bundle) {
                        checkForLocationSettings();
                    }

                    @Override
                    public void onConnectionSuspended(int i) {
                    }
                })
                .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                        Toast.makeText(Directions.this, "Google play service not responding... please refresh your mobile",
                                Toast.LENGTH_LONG).show();
                        googleApiClient = null;
                        dismissProgressDialog();

                    }
                }).build();
        googleApiClient.connect();


    }

    private void checkForLocationSettings() {
        ////////////////////////////////////////////////////////

        locationRequest = new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);

        //**************************
        builder.setAlwaysShow(true); //this is the key ingredient
        //**************************

        Task<LocationSettingsResponse> locationSettingsResponseTask =
                LocationServices.getSettingsClient(Directions.this).checkLocationSettings(builder.build());
        //
        locationSettingsResponseTask.addOnCompleteListener(new OnCompleteListener<LocationSettingsResponse>() {
            @Override
            public void onComplete(@NonNull Task<LocationSettingsResponse> task) {
                try {
                    LocationSettingsResponse response = task.getResult(ApiException.class);
                    // All location settings are satisfied. The client can initialize location
                    // requests here.
                    if (PermissionUtils.checkLoactionPermission(Directions.this) && NetworkUtils.isConnected(Directions.this)) {
                        getLastLocation();
                    } else {
                        dismissProgressDialog();
                        if (!PermissionUtils.checkLoactionPermission(Directions.this)) {
                            PermissionUtils.openPermissionDialog(Directions.this, "Permission needed!", "Please grant location permission");

                        } else {
                            NetworkUtils.checkInternetAndOpenDialog(Directions.this);
                        }
                    }

                } catch (ApiException exception) {
                    dismissProgressDialog();

                    switch (exception.getStatusCode()) {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // Location settings are not satisfied. But could be fixed by showing the
                            // user a dialog.
                            try {
                                NetworkUtils.openGpsSettings(Directions.this);
                            } catch (ClassCastException e) {
                                CommonFunc.commonDialog(Directions.this, getString(R.string.alert),
                                        getString(R.string.justErrorCode) + " 39", false);
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            CommonFunc.commonDialog(Directions.this, "GPS not working",
                                    getString(R.string.justErrorCode) + " 40", false);
                            break;
                    }
                }
            }
        });
        //

    }

    @SuppressWarnings("MissingPermission")
    private void getLastLocation() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(Directions.this);
        mFusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);

    }

    private LocationCallback locationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {

            for (Location location : locationResult.getLocations()) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                    isMockLocEnabled = locationResult.getLastLocation().isFromMockProvider();
                } else {
                    isMockLocEnabled = false;
                }
                latLngMyLocTemp = new LatLng(location.getLatitude(), location.getLongitude());
                LatLngGetter latLngGetter = new LatLngGetter();
                if (((MeetingModel) getIntent().getExtras().getSerializable("MEETING_MODEL")) != null) {
                    try {
                        latLngGetter.execute(URLEncoder.encode(((MeetingModel) getIntent().getExtras().getSerializable("MEETING_MODEL")).getLocation(), "utf-8"));
                    } catch (UnsupportedEncodingException e) {
                        Toast.makeText(Directions.this, "" + e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                } else {
                    dismissProgressDialog();
                    CommonFunc.commonDialog(Directions.this,
                            getString(R.string.justErrorCode) + " 41", "No client address found", false);
                }

            }
        }
    };


    public class LatLngGetter extends AsyncTask<String, Void, String[]> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        public String[] doInBackground(String... params) {
            String response;
            try {


                response = makeserverConnection("https://maps.google.com/maps/api/geocode/json?address=" + params[0] + "&key=" + getString(R.string.google_key));
                Log.d("response", "" + response);
                return new String[]{response};
            } catch (Exception e) {
                return new String[]{"error"};
            }
        }

        @Override
        public void onPostExecute(String... result) {
            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject(result[0]);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            double lng = 0;
            try {
                if (jsonObject != null) {
                    lng = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                            .getJSONObject("geometry").getJSONObject("location")
                            .getDouble("lng");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            double lat = 0;
            try {
                if (jsonObject != null) {
                    lat = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                            .getJSONObject("geometry").getJSONObject("location")
                            .getDouble("lat");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (lat == 0 || lng == 0) {
                dismissProgressDialog();
                CommonFunc.commonDialog(Directions.this,
                        getString(R.string.justErrorCode) + " 42", "Unable to route location,but You can start the route", false);
            } else {
                latLngClientLocTemp = new LatLng(lat, lng);
                String url = getDirectionsUrl(latLngMyLocTemp, latLngClientLocTemp);
                DownloadTask downloadTask = new DownloadTask();
                downloadTask.execute(url);
            }


        }
    }

    //////////////////////////////////////////////////////////////////////////////// get the direction and draw the polyline

    public String getDirectionsUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";

        // google api key
        String googleApiKey = "key=" + getString(R.string.google_key);

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + googleApiKey;

        // Output format
        String output = "json";

        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;


        return url;
    }

    // Fetches data from url passed
    public class DownloadTask extends AsyncTask<String, Void, String> {
        @Override
        public void onPreExecute() {
            super.onPreExecute();

        } //

        // Downloading data in non-ui thread
        @Override
        public String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = makeserverConnection(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
                Toast.makeText(Directions.this, "Network error... check internet", Toast.LENGTH_LONG).show();
            }
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        public void onPostExecute(String result) {
            super.onPostExecute(result);


            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);
        }
    }


    public class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        public List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        @SuppressWarnings("unchecked")
        @Override
        public void onPostExecute(List<List<HashMap<String, String>>> result) {
            dismissProgressDialog();
            mMap.clear();
            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;

            String distance = "";
            String duriation = "";

            // Traversing through all the routes
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);
                    if (j == 0) {    // Get distance from the list
                        distance = (String) point.get("distance");
                        continue;
                    } else if (j == 1) { // Get duration from the list
                        duriation = (String) point.get("duration");
                        continue;
                    }
                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(5);
                lineOptions.color(Color.RED);
            }
//            Toast.makeText(getActivity(), "Distance- " + distance + "time " + duriation, Toast.LENGTH_SHORT).show();
           /* textViewDistance.setText(""+distance);
            textViewTime.setText(""+duriation);*/
            // Drawing polyline in the Google Map for the i-th route
            try {
                mMap.addPolyline(lineOptions);
                List<LatLng> points1 = lineOptions.getPoints(); // route is instance of PolylineOptions
                new PostioinCamara().execute(points1);

            } catch (Exception e) {
                e.printStackTrace();
//                Toast.makeText(getActivity(), "Please select location within banglore", Toast.LENGTH_SHORT).show();
            }
            tempDistanceAndTimeStorage = "Distance : " + distance + ", Time : " + duriation;
        }
    }


    private class PostioinCamara extends AsyncTask<List<LatLng>, Void, Boolean> {
        Double maxLat = null, minLat = null, minLon = null, maxLon = null;

        @SafeVarargs
        @Override
        protected final Boolean doInBackground(List<LatLng>... params) {


            for (LatLng coordinate : params[0]) {
                // Find out the maximum and minimum latitudes & longitudes
                // Latitude
                maxLat = maxLat != null ? Math.max(coordinate.latitude, maxLat) : coordinate.latitude;
                minLat = minLat != null ? Math.min(coordinate.latitude, minLat) : coordinate.latitude;

                // Longitude
                maxLon = maxLon != null ? Math.max(coordinate.longitude, maxLon) : coordinate.longitude;
                minLon = minLon != null ? Math.min(coordinate.longitude, minLon) : coordinate.longitude;
            }
            return true;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            dismissProgressDialog();
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            builder.include(new LatLng(maxLat, maxLon));
            builder.include(new LatLng(minLat, minLon));
            LatLngBounds bounds = builder.build();
            mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 15));
            mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 15));
            mMap.setLatLngBoundsForCameraTarget(bounds);
            if (markerMyLoc == null) {
                markerMyLoc = mMap.addMarker(new MarkerOptions().position(latLngMyLocTemp).draggable(true).title("My Location").icon(BitmapDescriptorFactory
                        .defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
            } else {
                markerMyLoc.setPosition(latLngMyLocTemp);
            }
            //
            if (markerClientLoc == null) {
                markerClientLoc = mMap.addMarker(new MarkerOptions().position(latLngClientLocTemp).draggable(true).title("Client Location")
                        .icon(BitmapDescriptorFactory
                                .defaultMarker(BitmapDescriptorFactory.HUE_RED)).snippet(tempDistanceAndTimeStorage));
                markerClientLoc.showInfoWindow();
            } else {
                markerClientLoc.setPosition(latLngClientLocTemp);
            }
            if (((AppCompatButton) findViewById(R.id.activity_direction_button_id)).getText().toString().equalsIgnoreCase("END ROUTE")) {
                findViewById(R.id.activity_direction_navigat_float_button).setVisibility(View.VISIBLE);
                findViewById(R.id.activity_direction_navigat_float_button).setOnClickListener(Directions.this);
            }
        }
    }


    ///////////////////////////////////////////////////////////////////////////////////////// http conn
    public String makeserverConnection(String requestURL) {
        URL url;
        String response = "";
        try {
            url = new URL(requestURL);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded");
            conn.setDoOutput(true);
            int responseCode = conn.getResponseCode();
            if (responseCode == HttpsURLConnection.HTTP_OK) {

                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line = br.readLine()) != null) {
                    response += line;
                }
            } else {
                response = "";
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    private void stopLocationUpdates() {
        if (mFusedLocationClient != null)
            mFusedLocationClient.removeLocationUpdates(locationCallback);

        if (googleApiClient != null) {
            if (googleApiClient.isConnected())
                googleApiClient.disconnect();
        }
    }


}
