package com.lobotus.mapei.fromlite.mvp.presentor;

import android.content.Context;

/**
 * Created by user1 on 22-11-2017.
 */

public interface MeetingStatusFragPresentor {


    ///////// validate the meeting status before submiting in the presentor itself. . .
    void validateMeetingStatus(Context context, int statusSpinnerSelectedPos,
                               String fromDate, String toDate);

    /////////// update the  meeting status to the server. . .

    void reqToUpdateTheMeetingStatusToTheServer(Context context, int statusSpinnerSelectedPos,
                                                String fromDate, String toDate, String b64Image,
                                                String notes,int meetingType,int meetingSubtype);
    void onStopMvpPresentor();
}
