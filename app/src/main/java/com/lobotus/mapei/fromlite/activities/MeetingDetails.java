package com.lobotus.mapei.fromlite.activities;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.lobotus.mapei.R;
import com.lobotus.mapei.customwidgits.LabelledSpinner;
import com.lobotus.mapei.fromlite.baseact.SupportActivity;
import com.lobotus.mapei.fromlite.model.MeetingModel;
import com.lobotus.mapei.fromlite.model.ModeOfTravelDataModel;
import com.lobotus.mapei.fromlite.mvp.presentor.MeetingDetailsPresentor;
import com.lobotus.mapei.fromlite.mvp.presentorimp.MeetingDetailsPresentorImp;
import com.lobotus.mapei.fromlite.mvp.view.MeetingDetailsView;
import com.lobotus.mapei.utils.CommonFunc;
import com.lobotus.mapei.utils.NetworkUtils;
import com.lobotus.mapei.utils.PermissionUtils;
import com.lobotus.mapei.utils.PreferenceUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;

public class MeetingDetails extends SupportActivity implements View.OnClickListener, MeetingDetailsView {

    private Dialog dialog = null;
    /////////////////////////////////////////// temp data
    private ArrayList<ModeOfTravelDataModel> TempModeOfTravelDataModels = new ArrayList<>();
    ////////////////////////////////////////// mvp
    private MeetingDetailsPresentor meetingDetailsPresentor = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        RelativeLayout relativeLayout = findViewById(R.id.content_support);
        getLayoutInflater().inflate(R.layout.activity_meeting_details, relativeLayout);
        findViewById(R.id.activity_meeting_details_direction_button_id).setOnClickListener(this);
        findViewById(R.id.activity_meeting_details_call_button_id).setOnClickListener(this);
        findViewById(R.id.activity_meeting_details_back_button_id).setOnClickListener(this);


        if (((MeetingModel) getIntent().getExtras().getSerializable("MEETING_MODEL")) != null) {
            getSupportActionBar().setTitle("" + ((MeetingModel) getIntent().getExtras().getSerializable("MEETING_MODEL")).getFullName() +
                    "'s details");
        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        setTheMeetDataToAllViews(); // set the data. . .
        meetingDetailsPresentor = new MeetingDetailsPresentorImp(this);
        meetingDetailsPresentor.reqToGetTheMOT(MeetingDetails.this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.activity_meeting_details_direction_button_id:

                if (PermissionUtils.checkLoactionPermission(MeetingDetails.this)) {
                    // mvp deviation. . .
                    if (((LabelledSpinner) findViewById(R.id.activity_meeting_details_travel_by_spinner_id)).getSpinner().getSelectedItemPosition() == 0) {
                        CommonFunc.commonDialog(MeetingDetails.this,
                                getString(R.string.alert), "Please select mode of travel", false);
                    } else {

                        Intent intent = new Intent(MeetingDetails.this, Directions.class);
                        intent.putExtra("MEETING_MODEL", ((MeetingModel) getIntent().getExtras().getSerializable("MEETING_MODEL")));
                        intent.putExtra("MOT", TempModeOfTravelDataModels.
                                get(((LabelledSpinner) findViewById(R.id.activity_meeting_details_travel_by_spinner_id)).
                                        getSpinner().getSelectedItemPosition())
                                .getModeOfTravel());
                        startActivity(intent);
                    }


                } else {
                    PermissionUtils.openPermissionDialog(MeetingDetails.this, "Permission needed", "Please grant location permission to continue");
                }
                break;
            case R.id.activity_meeting_details_call_button_id:
                if (PermissionUtils.checkCallPermission(MeetingDetails.this)) {
                    if (PreferenceUtils.getIsUserCheckedIn(MeetingDetails.this)) {
                        Intent callIntent = new Intent(Intent.ACTION_CALL);
                        callIntent.setData(Uri.parse("tel:" + ((MeetingModel) getIntent().getExtras().getSerializable("MEETING_MODEL")).getPhone()));
                        startActivity(callIntent);
                    } else {
                        CommonFunc.commonDialog(MeetingDetails.this, getString(R.string.alert), getString(R.string.check_in_error), false);
                    }
                } else {
                    PermissionUtils.openPermissionDialog(MeetingDetails.this, "Permission needed!", "Please grant phone permission to launch call's directly through travelize");
                }
                break;
            case R.id.activity_meeting_details_back_button_id:
                onBackPressed();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (meetingDetailsPresentor != null)
            meetingDetailsPresentor.onStopMvpPresentor();
        dismissProgressDialog();
    }

    /////////////////////////////////// set the data to all views
    private void setTheMeetDataToAllViews() {

        ((AppCompatTextView) findViewById(R.id.activity_meeting_details__client_name_id))
                .setText("" + ((MeetingModel) getIntent().getExtras().getSerializable("MEETING_MODEL")).getFullName());
        ((AppCompatTextView) findViewById(R.id.activity_meeting_details_client_address_id))
                .setText("" + ((MeetingModel) getIntent().getExtras().getSerializable("MEETING_MODEL")).getLocation());
        ((AppCompatTextView) findViewById(R.id.activity_meeting_details_client_phno_id))
                .setText("" + ((MeetingModel) getIntent().getExtras().getSerializable("MEETING_MODEL")).getPhone());
        ((AppCompatTextView) findViewById(R.id.activity_meeting_details_client_timing_id))
                .setText(((MeetingModel) getIntent().getExtras().getSerializable("MEETING_MODEL")).getOnTime().length() == 0 ?
                        "NA" : ((MeetingModel) getIntent().getExtras().getSerializable("MEETING_MODEL")).getOnTime());


        //checking meeting and id to name
        String meetingType = checkMeetingType(((MeetingModel) getIntent().getExtras().getSerializable("MEETING_MODEL")).getMeetingType());

        //checking meeting sub type and id to name
        String meetingSubType = checkMeetingSubType(((MeetingModel) getIntent().getExtras().getSerializable("MEETING_MODEL")).getMeetingType(),
                ((MeetingModel) getIntent().getExtras().getSerializable("MEETING_MODEL")).getMeetingSubType());

        ((AppCompatTextView) findViewById(R.id.activity_meeting_details_client_meeting_type_id))
                .setText("" + meetingType);

        ((AppCompatTextView) findViewById(R.id.activity_meeting_details_client_meeting_sub_type_id))
                .setText("" + meetingSubType);

        //////// get the user current loc address. . .


    }

    private String checkMeetingSubType(int meetingType, int meetingSubType) {

        if (meetingType == 1) {

            switch (meetingSubType) {
                case 1:
                    return "Submittance";
                case 2:
                    return "Presentation";
                case 3:
                    return "Follow-up";
                default:
                    return "NA";
            }

        } else if (meetingType == 2) {

            switch (meetingSubType) {
                case 1:
                    return "Payment followup";
                case 2:
                    return "C form followup";
                case 3:
                    return "Reconciliation";
                default:
                    return "NA";
            }
        } else if (meetingType == 3) {
            switch (meetingSubType) {
                case 1:
                    return "Product";
                case 2:
                    return "Application";
                case 3:
                    return "Commercial";
                default:
                    return "NA";
            }
        } else return "NA";

    }

    private String checkMeetingType(int meetingId) {

        switch (meetingId) {
            case 1:
                return "Meeting";

            case 2:
                return "Arfollow up";

            case 3:
                return "Complaints";
            default:
                return "NA";
        }

    }

    //////////////////////////////////// get the user current loc and set it in the textview. . .
    ////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////// google api client establishment to access address
    ////
    private GoogleApiClient googleApiClient = null;
    private LocationRequest locationRequest = null;
    private FusedLocationProviderClient mFusedLocationClient = null;
    private AsyncTaskToGetTheAddress asyncTaskToGetTheAddress = null;

    ///
    private void connectToGoogleApiClientAndGetTheAddress() {
        if (NetworkUtils.checkInternetAndOpenDialog(MeetingDetails.this)) {
            if (CommonFunc.isGooglePlayServicesAvailable(MeetingDetails.this)) {
//                if (dialog == null)
//                {
//                    showProgressDialog();
                connectGoogleApiClient();
//                }
            }
        }
    }

    private void connectGoogleApiClient() {

        googleApiClient = new GoogleApiClient.Builder(MeetingDetails.this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(@Nullable Bundle bundle) {
                        checkForLocationSettings();
                    }

                    @Override
                    public void onConnectionSuspended(int i) {
                    }
                })
                .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                        Toast.makeText(MeetingDetails.this, "Google play service not responding... please refresh your mobile",
                                Toast.LENGTH_LONG).show();
                        googleApiClient = null;
                        dismissProgressDialog();

                    }
                }).build();
        googleApiClient.connect();


    }

    private void checkForLocationSettings() {
        ////////////////////////////////////////////////////////

        locationRequest = new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);

        //**************************
        builder.setAlwaysShow(true); //this is the key ingredient
        //**************************

        Task<LocationSettingsResponse> locationSettingsResponseTask =
                LocationServices.getSettingsClient(MeetingDetails.this).checkLocationSettings(builder.build());
        //
        locationSettingsResponseTask.addOnCompleteListener(new OnCompleteListener<LocationSettingsResponse>() {
            @Override
            public void onComplete(@NonNull Task<LocationSettingsResponse> task) {
                try {
                    LocationSettingsResponse response = task.getResult(ApiException.class);
                    // All location settings are satisfied. The client can initialize location
                    // requests here.
                    if (PermissionUtils.checkLoactionPermission(MeetingDetails.this) && NetworkUtils.isConnected(MeetingDetails.this)) {
                        getLastLocation();
                    } else {
                        dismissProgressDialog();
                        if (!PermissionUtils.checkLoactionPermission(MeetingDetails.this)) {
                            PermissionUtils.openPermissionDialog(MeetingDetails.this, "Permission needed!", "Please grant location permission");

                        } else {
                            NetworkUtils.checkInternetAndOpenDialog(MeetingDetails.this);
                        }
                    }

                } catch (ApiException exception) {
                    dismissProgressDialog();

                    switch (exception.getStatusCode()) {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // Location settings are not satisfied. But could be fixed by showing the
                            // user a dialog.
                            try {
                                NetworkUtils.openGpsSettings(MeetingDetails.this);
                            } catch (ClassCastException e) {
                                CommonFunc.commonDialog(MeetingDetails.this, getString(R.string.alert),
                                        getString(R.string.justErrorCode) + " 86", false);
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            CommonFunc.commonDialog(MeetingDetails.this, "GPS not working",
                                    getString(R.string.justErrorCode) + " 87", false);
                            break;
                    }
                }
            }
        });
        //

    }

    @SuppressWarnings("MissingPermission")
    private void getLastLocation() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(MeetingDetails.this);
        mFusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);

    }

    private LocationCallback locationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {

            for (Location location : locationResult.getLocations()) {
                decodeAddress(new LatLng(location.getLatitude(),
                        location.getLongitude()));
                stopLocationUpdates();
            }
        }
    };

    private void decodeAddress(final LatLng latLng) {
        String req = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" +
                latLng.latitude + "," + latLng.longitude + "&key=" + getString(R.string.google_key);


        if (asyncTaskToGetTheAddress != null) {
            if (asyncTaskToGetTheAddress.getStatus() != AsyncTask.Status.RUNNING) {
                asyncTaskToGetTheAddress = new AsyncTaskToGetTheAddress();
                asyncTaskToGetTheAddress.execute(req);
            }
        } else {
            asyncTaskToGetTheAddress = new AsyncTaskToGetTheAddress();
            asyncTaskToGetTheAddress.execute(req);
        }
    }


    private class AsyncTaskToGetTheAddress extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... strings) {
            return makeserverConnection(strings[0]);
        }

        @Override
        protected void onPostExecute(String s) {
            dismissProgressDialog();
            if (s != null) {
                try {
                    JSONObject jsonObject = new JSONObject(s);
                    JSONArray jsonArray = jsonObject.getJSONArray("results");
                    if (jsonArray.length() > 0) {
                        String formatedAddress = jsonArray.getJSONObject(0).getString("formatted_address");
                        if (formatedAddress != null && !formatedAddress.isEmpty()) {
                            ((AppCompatTextView) findViewById(R.id.activity_meeting_details_current_loc_address_id))
                                    .setText("" + formatedAddress);
                        } else {
                            CommonFunc.commonDialog(MeetingDetails.this, "Alert!", "Unable to detect your location", false);
                        }

                    } else {
                        CommonFunc.commonDialog(MeetingDetails.this, "Alert!", "Unable to detect your location", false);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                CommonFunc.commonDialog(MeetingDetails.this, "Alert!", "Unable to detect your location", false);
            }
        }


    }

    public String makeserverConnection(String requestURL) {
        URL url;
        String response = "";
        try {
            url = new URL(requestURL);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded");
            conn.setDoOutput(true);
            int responseCode = conn.getResponseCode();
            if (responseCode == HttpsURLConnection.HTTP_OK) {

                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line = br.readLine()) != null) {
                    response += line;
                }
            } else {
                response = "";
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    private void stopLocationUpdates() {
        if (mFusedLocationClient != null)
            mFusedLocationClient.removeLocationUpdates(locationCallback);

        if (googleApiClient != null) {
            if (googleApiClient.isConnected())
                googleApiClient.disconnect();
        }


    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////////// mvp. . . 

    @SuppressWarnings("ConstantConditions")
    @Override
    public void showProgressDialog() {
        dialog = new Dialog(MeetingDetails.this);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.loaging_layout);
        dialog.show();
    }

    @Override
    public void dismissProgressDialog() {
        if (dialog != null) {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
            dialog = null;
        }


    }

    @Override
    public void afterMOTlistDownlodeSuccess(ArrayList<ModeOfTravelDataModel> modeOfTravelDataModels) {
        TempModeOfTravelDataModels.clear();
        TempModeOfTravelDataModels.add(new ModeOfTravelDataModel("Select"));
        for (int i = 0; i < modeOfTravelDataModels.size(); i++) {
            TempModeOfTravelDataModels.add(modeOfTravelDataModels.get(i));
        }
        String[] stringsMOTStrings = new String[TempModeOfTravelDataModels.size()];
        for (int i = 0; i < stringsMOTStrings.length; i++) {
            stringsMOTStrings[i] = TempModeOfTravelDataModels.get(i).getModeOfTravel();
        }

        ((LabelledSpinner) findViewById(R.id.activity_meeting_details_travel_by_spinner_id))
                .setItemsArray(stringsMOTStrings);
        ((LabelledSpinner) findViewById(R.id.activity_meeting_details_travel_by_spinner_id)).setDefaultErrorEnabled(false);
        connectToGoogleApiClientAndGetTheAddress();

    }

    @Override
    public void afterMOTlistDownlodeFail(String message, String dialogTitle, boolean isInternetError) {
        CommonFunc.commonDialog(MeetingDetails.this, dialogTitle, message, isInternetError);
    }


}
