package com.lobotus.mapei.fromlite.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by user1 on 20-11-2017.
 */

public class MeetingModel implements Serializable {
    @SerializedName("MeetingID")
    @Expose
    private String meetingID;
    @SerializedName("UserId")
    @Expose
    private String userId;
    @SerializedName("FullName")
    @Expose
    private String fullName;
    @SerializedName("Contact")
    @Expose
    private String phone;
    @SerializedName("Status")
    @Expose
    private String status;
    @SerializedName("MOT")
    @Expose
    private String mOT;
    @SerializedName("OnDate")
    @Expose
    private String onDate;
    @SerializedName("OnTime")
    @Expose
    private String onTime;

    @SerializedName("MeetingTypeID")
    @Expose
    private int meetingType;

    @SerializedName("MeetingSubTypeID")
    @Expose
    private int meetingSubType;


    @SerializedName("Location")
    @Expose
    private String location;
    @SerializedName("Success")
    @Expose
    private Integer success;
    @SerializedName("Msg")
    @Expose
    private String msg;


    public MeetingModel(String meetingID, String userId, String fullName, String phone,
                        String status, String mOT, String onDate,
                        String onTime, String location, Integer success, String msg) {
        this.meetingID = meetingID;
        this.userId = userId;
        this.fullName = fullName;
        this.phone = phone;
        this.status = status;
        this.mOT = mOT;
        this.onDate = onDate;
        this.onTime = onTime;
        this.location = location;
        this.success = success;
        this.msg = msg;
    }

    public String getMeetingID() {
        return meetingID;
    }

    public void setMeetingID(String meetingID) {
        this.meetingID = meetingID;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMOT() {
        return mOT;
    }

    public void setMOT(String mOT) {
        this.mOT = mOT;
    }

    public String getOnDate() {
        return onDate;
    }

    public void setOnDate(String onDate) {
        this.onDate = onDate;
    }

    public String getOnTime() {
        return onTime;
    }

    public int getMeetingType() {
        return meetingType;
    }

    public void setMeetingType(int meetingType) {
        this.meetingType = meetingType;
    }

    public int getMeetingSubType() {
        return meetingSubType;
    }

    public void setMeetingSubType(int meetingSubType) {
        this.meetingSubType = meetingSubType;
    }

    public void setOnTime(String onTime) {
        this.onTime = onTime;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        return "MeetingModel{" +
                "meetingID='" + meetingID + '\'' +
                ", userId='" + userId + '\'' +
                ", fullName='" + fullName + '\'' +
                ", phone='" + phone + '\'' +
                ", status='" + status + '\'' +
                ", mOT='" + mOT + '\'' +
                ", onDate='" + onDate + '\'' +
                ", onTime='" + onTime + '\'' +
                ", meetingType='" + meetingType + '\'' +
                ", meetingSubType='" + meetingSubType + '\'' +
                ", location='" + location + '\'' +
                ", success=" + success +
                ", msg='" + msg + '\'' +
                '}';
    }
}
