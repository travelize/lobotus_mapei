package com.lobotus.mapei.fromlite.mvp.intractorimp;

import android.content.Context;


import com.lobotus.mapei.R;
import com.lobotus.mapei.fromlite.model.ModeOfTravelDataModel;
import com.lobotus.mapei.fromlite.model.ModeOfTravelModel;
import com.lobotus.mapei.fromlite.mvp.intractor.MeetingDetailsIntractor;
import com.lobotus.mapei.retrofitapi.APIService;
import com.lobotus.mapei.retrofitapi.ApiUtils;
import com.lobotus.mapei.utils.ConstantsUtils;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by user1 on 23-11-2017.
 */

public class MeetingDetailsIntractorImp implements MeetingDetailsIntractor {

    private Call<ModeOfTravelModel> CallpostToGetTheListOfMOT=null;

    @Override
    public void reqToGetTheMOTListFromTheServer(final Context context,
                                                final OnMOTDetailsDownlodeLisner onMOTDetailsDownlodeLisner) {

        APIService apiService= ApiUtils.getAPIService(ConstantsUtils.postBaseUrl+ConstantsUtils.POST_TO_GET_THE_MOT_LIST);
        CallpostToGetTheListOfMOT=apiService.postToGetTheListOfMOT("");
        CallpostToGetTheListOfMOT.enqueue(new Callback<ModeOfTravelModel>() {
            @Override
            public void onResponse(Call<ModeOfTravelModel> call, Response<ModeOfTravelModel> response) {
                if (response.isSuccessful())
                {
                    if (response.body()!=null)
                    {
                        if (response.body().getSuccess()==1)
                        {
                            if (response.body().getData()!=null)
                            {
                                onMOTDetailsDownlodeLisner.OnMOTListDownlodeSuccess((ArrayList<ModeOfTravelDataModel>) response.body().getData());
                            }else {
                                // error
                                onMOTDetailsDownlodeLisner.OnMOTListDownlodeFail(context.getString(R.string.justErrorCode)+" 66",
                                        context.getString(R.string.internalError),true);
                            }
                        }else {
                            onMOTDetailsDownlodeLisner.OnMOTListDownlodeFail(response.body().getErrorMessage(),
                                    context.getString(R.string.alert),false);
                        }
                    }else {
                        // error
                        onMOTDetailsDownlodeLisner.OnMOTListDownlodeFail(context.getString(R.string.justErrorCode)+" 65",
                                context.getString(R.string.internalError),true);
                    }
                }else {
                    // error
                    onMOTDetailsDownlodeLisner.OnMOTListDownlodeFail(context.getString(R.string.justErrorCode)+" 64",
                            context.getString(R.string.internalError),true);

                }
            }

            @Override
            public void onFailure(Call<ModeOfTravelModel> call, Throwable t) {
                // error
                onMOTDetailsDownlodeLisner.OnMOTListDownlodeFail(context.getString(R.string.noInternetMessage)+" 63",
                        context.getString(R.string.noInternetAlert),true);

            }
        });

    }

    @Override
    public void onStopMvpIntractor() {
        if (CallpostToGetTheListOfMOT!=null)
        {
            CallpostToGetTheListOfMOT.cancel();
        }
    }
}
