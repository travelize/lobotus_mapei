package com.lobotus.mapei.fromlite.mvp.intractorimp;

import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.Gson;
import com.lobotus.mapei.R;
import com.lobotus.mapei.fromlite.model.CommRespModel;
import com.lobotus.mapei.fromlite.mvp.intractor.MeetingStatusFragmentIntractor;
import com.lobotus.mapei.retrofitapi.APIService;
import com.lobotus.mapei.retrofitapi.ApiUtils;
import com.lobotus.mapei.utils.ConstantsUtils;
import com.lobotus.mapei.utils.PreferenceUtils;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by user1 on 22-11-2017.
 */

public class MeetingStatusFragmentIntractorImp implements MeetingStatusFragmentIntractor {
    @Override
    public void reqToUplodeTheMeetingStatusToTheServer(Context context, int statusSpinnerSelectedPos, String fromDate,
                                                       String toDate,
                                                       OnMeetingStatusUplodeLisner onMeetingStatusUplodeLisner, String b64Image,
                                                       String notes,int meetingType,int meetingSubtype) {
        /////
        System.out.println("hhhhhhhhhhhhhhhhhhhhhh--------1--------" + statusSpinnerSelectedPos);
        System.out.println("hhhhhhhhhhhhhhhhhhhhhh--------2--------" + fromDate);
        System.out.println("hhhhhhhhhhhhhhhhhhhhhh---------3-------" + toDate);
        System.out.println("hhhhhhhhhhhhhhhhhhhhhh----------4------" + toDate);
        System.out.println("hhhhhhhhhhhhhhhhhhhhhh------------5----" + PreferenceUtils.getMeetingIdFromStartRoutePreference(context));
        System.out.println("hhhhhhhhhhhhhhhhhhhhhh--------------6--" + PreferenceUtils.getMOTFromStartRoutePreference(context));
        System.out.println("hhhhhhhhhhhhhhhhhhhhhh--------------7--" +meetingType);
        System.out.println("hhhhhhhhhhhhhhhhhhhhhh--------------8--" +meetingSubtype);

        ////
        TempContext = context;
        TemponMeetingStatusUplodeLisner = onMeetingStatusUplodeLisner;
        TempMeetingID = PreferenceUtils.getMeetingIdFromStartRoutePreference(context);
        TempRemarks = notes;
        TempMeetingType=String.valueOf(meetingType);
        TempMeetingSubType=String.valueOf(meetingSubtype);
        if (b64Image == null)
            TempAttachment = "";
        else
            TempAttachment = b64Image;
        TempModeOfTravel = PreferenceUtils.getMOTFromStartRoutePreference(context);
        if (statusSpinnerSelectedPos == 2) {
            // perform followup. . .
            TempStatus = "Follow up";
            TempOnTime = fromDate;
            TempOnDate = toDate;
            TempIsStatusFollowup = true;


        } else {
            // perform other than follow up. . .
            switch (statusSpinnerSelectedPos) {
                case 1:
                    TempStatus = "Completed";
                    break;
                case 3:
                    TempStatus = "Cancelled";
            }
            TempIsStatusFollowup = false;

        }
        postMeetStatusToTheServer = new PostMeetStatusToTheServer();
        postMeetStatusToTheServer.execute();

    }

    ////// perform followup post to the server. . .
    private void performFollowupPostToTheServer(final Context context, int statusSpinnerSelectedPos, String fromDate,
                                                String toDate,
                                                final OnMeetingStatusUplodeLisner onMeetingStatusUplodeLisner, String b64Image,
                                                String notes) {
        APIService apiService = ApiUtils.getAPIService(ConstantsUtils.postBaseUrl + ConstantsUtils.POST_TO_UPDATE_THE_MEETING_STATUS);

        if (b64Image == null)
            b64Image = "";
        apiService.postToUpdateTheMeetingStatusOfFollowup(PreferenceUtils.getMeetingIdFromStartRoutePreference(context),
                "Follow up",
                notes,
                fromDate,
                toDate, b64Image
                , PreferenceUtils.getMOTFromStartRoutePreference(context)).enqueue(new Callback<CommRespModel>() {
            @Override
            public void onResponse(Call<CommRespModel> call, Response<CommRespModel> response) {
                if (response.isSuccessful()) {

                    if (response.body().getSuccess() == 1) {

                        onMeetingStatusUplodeLisner.onMeetingStatusUplodeSuccess(response.body().getMsg());
                    } else {
                        onMeetingStatusUplodeLisner.onMeetingStatusUplodeFail(response.body().getMsg(),
                                context.getString(R.string.alert), false);
                    }

                } else {
                    onMeetingStatusUplodeLisner.onMeetingStatusUplodeFail(context.getString(R.string.justErrorCode) + " 54b",
                            context.getString(R.string.internalError), false);
                }
            }

            @Override
            public void onFailure(Call<CommRespModel> call, Throwable t) {
                onMeetingStatusUplodeLisner.onMeetingStatusUplodeFail(context.getString(R.string.noInternetMessage) + " 53",
                        context.getString(R.string.noInternetAlert), true);
            }
        });
    }

    ////// perform other than followup post to the server. . .
    private void performOtherThanFollowupPostToTheServer(final Context context, int statusSpinnerSelectedPos, String fromDate,
                                                         String toDate,
                                                         final OnMeetingStatusUplodeLisner onMeetingStatusUplodeLisner, String b64Image,
                                                         String notes)

    {
        String stringStatus = "";
        switch (statusSpinnerSelectedPos) {
            case 1:
                stringStatus = "Completed";
                break;
            case 3:
                stringStatus = "Cancelled";
        }
        if (b64Image == null)
            b64Image = "";
        ////////////////////////

        ///////////////////////
        APIService apiService = ApiUtils.getAPIService(ConstantsUtils.postBaseUrl + ConstantsUtils.POST_TO_UPDATE_THE_MEETING_STATUS);
        apiService.postToUpdateTheMeetingStatusOfOtherThanFollowup(PreferenceUtils.getMeetingIdFromStartRoutePreference(context),
                stringStatus, notes, b64Image, PreferenceUtils.getMOTFromStartRoutePreference(context)).enqueue(new Callback<CommRespModel>() {
            @Override
            public void onResponse(Call<CommRespModel> call, Response<CommRespModel> response) {
                if (response.isSuccessful()) {

                    if (response.body().getSuccess() == 1) {

                        onMeetingStatusUplodeLisner.onMeetingStatusUplodeSuccess(response.body().getMsg());
                    } else {
                        onMeetingStatusUplodeLisner.onMeetingStatusUplodeFail(response.body().getMsg(),
                                context.getString(R.string.alert), false);
                    }


                } else {
                    onMeetingStatusUplodeLisner.onMeetingStatusUplodeFail(context.getString(R.string.justErrorCode) + " 54a",
                            context.getString(R.string.internalError), false);
                }
            }

            @Override
            public void onFailure(Call<CommRespModel> call, Throwable t) {
                onMeetingStatusUplodeLisner.onMeetingStatusUplodeFail(context.getString(R.string.noInternetMessage) + " 53",
                        context.getString(R.string.noInternetAlert), true);
            }
        });


    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////// async task. . . .
    private String TempMeetingID = null;
    private String TempStatus = null;
    private String TempRemarks = null;
    private String TempAttachment = null;
    private String TempModeOfTravel = null;
    private String TempMeetingType=null;
    private String  TempMeetingSubType=null;
    // for follow up
    private String TempOnTime = null;
    private String TempOnDate = null;
    //
    private boolean TempIsStatusFollowup = false;
    //
    private OnMeetingStatusUplodeLisner TemponMeetingStatusUplodeLisner = null;
    //
    private Context TempContext = null;
    private PostMeetStatusToTheServer postMeetStatusToTheServer = null;


    private class PostMeetStatusToTheServer extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {

            HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put("MeetingID", TempMeetingID);
            hashMap.put("Status", TempStatus);
            hashMap.put("Remarks", TempRemarks);
            hashMap.put("Attachment", TempAttachment);
            hashMap.put("ModeOfTravel", TempModeOfTravel);
            hashMap.put("MeetingTypeID",TempMeetingType);
            hashMap.put("MeetingSubTypeID",TempMeetingSubType);
            if (TempIsStatusFollowup) {
                hashMap.put("OnTime", TempOnTime);
                hashMap.put("OnDate", TempOnDate);
            }

            return performPostCall(ConstantsUtils.postBaseUrl + ConstantsUtils.POST_TO_UPDATE_THE_MEETING_STATUS, hashMap);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (s != null) {
                if (!s.equals("")) {
                    Gson gson = new Gson();
                    CommRespModel response = gson.fromJson(s, CommRespModel.class);
                    if (response.getSuccess() == 1) {

                        TemponMeetingStatusUplodeLisner.onMeetingStatusUplodeSuccess(response.getMsg());
                    } else {
                        TemponMeetingStatusUplodeLisner.onMeetingStatusUplodeFail(response.getMsg(),
                                TempContext.getString(R.string.alert), false);
                    }
                } else {
                    TemponMeetingStatusUplodeLisner.onMeetingStatusUplodeFail(TempContext.getString(R.string.noInternetMessage) + " 53",
                            TempContext.getString(R.string.noInternetAlert), true);
                }

            } else {
                TemponMeetingStatusUplodeLisner.onMeetingStatusUplodeFail(TempContext.getString(R.string.noInternetMessage) + " 53",
                        TempContext.getString(R.string.noInternetAlert), true);
            }


        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////  server connection. . .

    ///////////////////////////////
    private static String performPostCall(String requestURL,
                                          HashMap<String, String> postDataParams) {

        URL url;
        String response = "";
        try {
            url = new URL(requestURL);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(30000);
            conn.setConnectTimeout(30000);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(getPostDataString(postDataParams));
            writer.flush();
            writer.close();
            os.close();

            int responseCode = conn.getResponseCode();
            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line = br.readLine()) != null) {
                    response += line;
                }
            } else {
                response = "";
                // throw new HttpException(responseCode + "");
            }
            conn.disconnect();

        } catch (Exception e) {
            e.printStackTrace();
        }

        //   Log.e("Otpresponse ", Otpresponse);
        return response;
    }

    public static String getPostDataString(HashMap<String, String> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for (Map.Entry<String, String> entry : params.entrySet()) {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }
        //   Log.e("url", result.toString());
        return result.toString();
    }

    @Override
    public void onStopMvpIntractor() {
        if (postMeetStatusToTheServer != null) {
            if (postMeetStatusToTheServer.getStatus() == AsyncTask.Status.RUNNING)
                postMeetStatusToTheServer.cancel(true);

        }
    }
}
