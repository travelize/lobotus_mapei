package com.lobotus.mapei.fromlite.mvp.view;

/**
 * Created by user1 on 22-11-2017.
 */

public interface MeetingStatusFragView {

    void showProgressDialog();
    void dismissProgressDialog();
    ////////////////////////////////////////////////////////////////// after meeting status validation
    void afterMeetingStatusValidationSuccess();

    ////////////////////////////////////////////////////////// afer meeting status and expence update success. . . .
    void afterMeetingStatusAndexpenceUpdatedToServerIsSuccess(String message);
    void afterMeetingStatusAndexpenceUpdatedToServerIsFail(String message, String dialogTitle, boolean isInternetError);
    ////////
}
