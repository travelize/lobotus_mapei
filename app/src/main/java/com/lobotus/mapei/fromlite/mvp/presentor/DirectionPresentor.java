package com.lobotus.mapei.fromlite.mvp.presentor;

import android.content.Context;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by user1 on 21-11-2017.
 */

public interface DirectionPresentor {
    //// get the button text as start route or end route. . .

    void getTheButtontext(Context context);

    ////////////////////// on map button click
    void OnMapButtonClickHappened(Context context, LatLng latLngCurrentLoc, String meetingId, String mot, String clientName, boolean isMockLocEnabled);
    //
    void onStopMvpPresentor();

}
