package com.lobotus.mapei.fromlite.mvp.presentor;

import android.content.Context;

/**
 * Created by user1 on 20-11-2017.
 */

public interface MeetingPresentor {

    // get the meeting list from the presentor
    void reqToGetTheListOfMeetingsFromPresentor(Context context, String date);
    // on stop
    void onStopMvpPresentor();
}
