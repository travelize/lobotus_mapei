package com.lobotus.mapei.fromlite.fragment;


import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;
import com.lobotus.mapei.R;
import com.lobotus.mapei.baseactivity.BaseActivity;
import com.lobotus.mapei.flowfragments.fragments.AllMeetingsFrag;
import com.lobotus.mapei.flowfragments.fragments.AssignMeetingFrag;
import com.lobotus.mapei.flowfragments.fragments.MeetingFrag;
import com.lobotus.mapei.fromlite.activities.Directions;
import com.lobotus.mapei.fromlite.activities.MeetingDetails;
import com.lobotus.mapei.fromlite.activities.MeetingStatus;
import com.lobotus.mapei.fromlite.adapter.MeetingAdapter;
import com.lobotus.mapei.fromlite.model.MeetingModel;
import com.lobotus.mapei.fromlite.mvp.presentor.MeetingPresentor;
import com.lobotus.mapei.fromlite.mvp.presentorimp.MeetingPresentorImp;
import com.lobotus.mapei.fromlite.mvp.view.MeetingView;
import com.lobotus.mapei.utils.CommonFunc;
import com.lobotus.mapei.utils.NetworkUtils;
import com.lobotus.mapei.utils.PermissionUtils;
import com.lobotus.mapei.utils.PreferenceUtils;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * A simple {@link Fragment} subclass.
 */
public class MeetingsFragment extends Fragment implements MeetingView, SwipeRefreshLayout.OnRefreshListener,
        View.OnClickListener {

    private View viewFragment = null;
    private Dialog dialog = null;

    /// mvp
    private MeetingPresentor meetingPresentor = null;
    // temp data
    private ArrayList<MeetingModel> meetingModelsTemp = new ArrayList<>();
    // meeting adapter
    private MeetingAdapter meetingAdapter = null;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        viewFragment = inflater.inflate(R.layout.fragment_meetings, container, false);
        ((SwipeRefreshLayout) viewFragment.findViewById(R.id.fragment_meeting_swipe_referesh_id)).setOnRefreshListener(this);
        ((AppCompatEditText) viewFragment.findViewById(R.id.fragment_meeting_date_id)).setOnClickListener(this);
        ((AppCompatImageView) viewFragment.findViewById(R.id.fragment_all_meeting_list_id)).setOnClickListener(this);
        viewFragment.findViewById(R.id.fragment_meeting_add_meeting).setOnClickListener(this);
        meetingPresentor = new MeetingPresentorImp(this);
        return viewFragment;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((AppCompatEditText) viewFragment.findViewById(R.id.fragment_meeting_date_id)).setText(CommonFunc.getTodayDate());
        meetingPresentor.reqToGetTheListOfMeetingsFromPresentor(getActivity(),
                ((AppCompatEditText) viewFragment.findViewById(R.id.fragment_meeting_date_id)).getText().toString());
    }

    @Override
    public void onRefresh() {
        meetingPresentor.reqToGetTheListOfMeetingsFromPresentor(getActivity(),
                ((AppCompatEditText) viewFragment.findViewById(R.id.fragment_meeting_date_id)).getText().toString());
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fragment_meeting_date_id:
                opendatePickerToGetTheMeetingDate();
                break;
            case R.id.fragment_all_meeting_list_id:
//                meetingPresentor.reqToGetTheListOfMeetingsFromPresentor(getActivity(), "");
                ((BaseActivity) getActivity()).attachFragment(new AllMeetingsFrag(), true, "Meeting History", null);
                break;
            case R.id.fragment_meeting_add_meeting:
                if (!PreferenceUtils.getTheUserRoleNameFromPreference(getActivity()).equals("Manager")) {
                    BaseActivity baseActivity = (BaseActivity) getActivity();
                    baseActivity.attachFragment(new MeetingFrag(), true, "Add Meeting", null);
                } else {
                    OpenDialogToChooseAction();
                }

        }
    }

    /////////////////////////////////////////////////////////////////////////////////
    private void OpenDialogToChooseAction() {
        AlertDialog levelDialog;

// Strings to Show In Dialog with Radio Buttons
        final CharSequence[] items = {"Add meeting", "Assign meeting"};

        // Creating and Building the Dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Select Action");
        builder.setSingleChoiceItems(items, -1, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {

                BaseActivity baseActivity = (BaseActivity) getActivity();
                switch (item) {
                    case 0:
                        baseActivity.attachFragment(new MeetingFrag(), true, getString(R.string.Addmeetings), null);
                        break;
                    case 1:
                        baseActivity.attachFragment(new AssignMeetingFrag(), true, getString(R.string.Assignmeetings), null);


                }
                dialog.dismiss();
            }
        });
        levelDialog = builder.create();
        levelDialog.show();
    }
    ///////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////// ui settings. .  .


    private void opendatePickerToGetTheMeetingDate() {

        SimpleDateFormat mDF = new SimpleDateFormat("yyyy-mm-dd");
        Date today = new Date();
        mDF.format(today);
        final Calendar c = Calendar.getInstance();
        c.setTime(today);
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        String formattedDay = (String.valueOf(dayOfMonth));
                        int m = monthOfYear + 1;


                        String formattedMonth = (String.valueOf(m));

                        if (dayOfMonth < 10) {
                            formattedDay = "0" + dayOfMonth;
                        }

                        if (m < 10) {
                            formattedMonth = "0" + m;
                            Log.e("month ", " " + m);
                        }

                        ((AppCompatEditText) viewFragment.findViewById(R.id.fragment_meeting_date_id)).setText(formattedDay + "/" + (formattedMonth) + "/" + year);
                        meetingPresentor.reqToGetTheListOfMeetingsFromPresentor(getActivity(),
                                ((AppCompatEditText) viewFragment.findViewById(R.id.fragment_meeting_date_id)).getText().toString());


                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {


            }
        });
        datePickerDialog.show();
        /*datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());*/
    }


    /////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////// mvp

    @Override
    public void showProgressDialog() {
        dialog = new Dialog(getActivity());
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.loaging_layout);
        dialog.show();
    }

    @Override
    public void dismissProgressDialog() {
        if (((SwipeRefreshLayout) viewFragment.findViewById(R.id.fragment_meeting_swipe_referesh_id))
                .isRefreshing()) {
            ((SwipeRefreshLayout) viewFragment.findViewById(R.id.fragment_meeting_swipe_referesh_id)).setRefreshing(false);

        }
        if (dialog != null) {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
            dialog = null;
        }
    }

    @Override
    public void afterMeetingsListDownlodeSuccess(ArrayList<MeetingModel> meetingModels) {
        dismissProgressDialog();
        meetingModelsTemp.clear();
        meetingModelsTemp.add(null);
        for (int i = 0; i < meetingModels.size(); i++) {
            meetingModelsTemp.add(meetingModels.get(i));
        }

        System.out.println("nnnnnnn-meetingModels------" + new Gson().toJson(meetingModels));


        meetingAdapter = new MeetingAdapter(getActivity(), meetingModelsTemp, this);
        ((RecyclerView) viewFragment.findViewById(R.id.fragment_meeting_recycleview_id)).setLayoutManager(new LinearLayoutManager(getActivity(),
                LinearLayoutManager.VERTICAL, false));
        ((RecyclerView) viewFragment.findViewById(R.id.fragment_meeting_recycleview_id)).setAdapter(meetingAdapter);
      /*  if (meetingModels.size() <= 1) {
            try {
                ((HomeFragment) ((NavigationActivity) getActivity()).adapter.getItem(0))
                        .textViewNoOfMeetings.setText(meetingModels.size() + " meeting ");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else {
            try {
                ((HomeFragment) ((NavigationActivity) getActivity()).adapter.getItem(0))
                        .textViewNoOfMeetings.setText(meetingModels.size() + " meetings ");
            }catch (Exception e)
            {
                e.printStackTrace();
            }

        }
        ////////////////////////////////////////////////*/
        if (!meetingModels.isEmpty()) {
            viewFragment.findViewById(R.id.fragment_meeting_no_meeting_found).setVisibility(View.GONE);
        } else {
            viewFragment.findViewById(R.id.fragment_meeting_no_meeting_found).setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void afterMeetingsListDownlodeFail(String message, String dialogTitle, boolean isInternetError) {
        System.out.println("nnnnnnnnnnnnnnnnnnnnn----afterMeetingsListDownlodeFail--" + message);

        CommonFunc.commonDialog(getActivity(), dialogTitle, message, false);
        try {
            meetingModelsTemp.clear();
            if (meetingAdapter != null) {
                meetingAdapter.notifyDataSetChanged();
            }
         /*   if (((NavigationActivity)getActivity()).navigation.getSelectedItemId()==R.id.navigation_meeting)
                CommonFunc.commonDialog(getActivity(),dialogTitle,message,isInternetError);
            ((HomeFragment)((NavigationActivity)getActivity()).adapter.getItem(0))
                    .textViewNoOfMeetings.setText("0 meetings ");*/

            viewFragment.findViewById(R.id.fragment_meeting_no_meeting_found).setVisibility(View.VISIBLE);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    ////////////////////////////////////////////// list click
    public void onMeetingListClick(MeetingModel meetingModel) {
        if (meetingModel.getOnDate().equals(CommonFunc.getTodayDate())) {
            if (PermissionUtils.checkLoactionPermission(getActivity())) {
                checkGPSisOnAndOpenMeetingDetailsActivity(meetingModel);
            } else {
                PermissionUtils.openPermissionDialog(getActivity(), "Please grant location permission", "Permission req");
            }
        } else {
            CommonFunc.commonDialog(getActivity(), getString(R.string.alert),
                    "You can start only same day meeting...", false);
        }
    }
    //// check gps is on and open the meeting details act

    private void checkGPSisOnAndOpenMeetingDetailsActivity(final MeetingModel meetingModel) {
        LocationRequest locationRequest = new LocationRequest();
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        Task<LocationSettingsResponse> locationSettingsResponseTask =
                LocationServices.getSettingsClient(getActivity()).checkLocationSettings(builder.build());
        locationSettingsResponseTask.addOnCompleteListener(new OnCompleteListener<LocationSettingsResponse>() {
            @Override
            public void onComplete(@NonNull Task<LocationSettingsResponse> task) {
                try {

                    LocationSettingsResponse response = task.getResult(ApiException.class);
                    // All location settings are satisfied. The client can initialize location
                    // requests here.
                    if (PermissionUtils.checkLoactionPermission(getActivity()) && NetworkUtils.isConnected(getActivity())) {
                        if (PreferenceUtils.getIsUserCheckedIn(getActivity())) {
                            if (PreferenceUtils.isMeetingIdPresentInTheStartRoutePreference(getActivity())) {
                                if (PreferenceUtils.getMeetingIdFromStartRoutePreference(getActivity()) != null) {
                                    if (PreferenceUtils.getMeetingIdFromStartRoutePreference(getActivity()).equals(meetingModel.getMeetingID())) {
                                        if (PreferenceUtils.isStartedRouteEnded(getActivity())) {
                                            // open meeting status directly. . . . .
                                            Intent intent = new Intent(getActivity(), MeetingStatus.class);
                                            intent.putExtra("MEETING_MODEL", meetingModel);
                                            startActivity(intent);
                                        } else {
                                            Intent intent = new Intent(getActivity(), Directions.class);
                                            intent.putExtra("MEETING_MODEL", meetingModel);
                                            intent.putExtra("MOT", "NA");
                                            startActivity(intent);
                                        }

                                    } else {
                                        CommonFunc.commonDialog(getActivity(), getString(R.string.alert), "Please finish the started meeting first", false);
                                    }
                                } else {
                                    Intent intent = new Intent(getActivity(), MeetingDetails.class);
                                    intent.putExtra("MEETING_MODEL", meetingModel);
                                    getActivity().startActivity(intent);
                                }
                            } else {
                                Intent intent = new Intent(getActivity(), MeetingDetails.class);
                                intent.putExtra("MEETING_MODEL", meetingModel);
                                getActivity().startActivity(intent);
                            }
                        } else {
                            CommonFunc.commonDialog(getActivity(), getString(R.string.alert), "Please check in first.", false);
                        }

                    } else {

                        if (!PermissionUtils.checkLoactionPermission(getActivity())) {
                            PermissionUtils.openPermissionDialog(getActivity(), "Please grant location permission.", "Permission req");
                            /*Utility.alertMessage(Login.this,"Location permission not granted!","Please grant LOCATION permission and login again");*/

                        } else {
                            NetworkUtils.checkInternetAndOpenDialog(getActivity());
                            /*Utility.alertMessage(Login.this,"Mobile data and wi-fi not enabled!","Please enable your Mobile data or Wi-Fi and login again");*/
                        }
                    }

                } catch (ApiException exception) {


                    switch (exception.getStatusCode()) {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // Location settings are not satisfied. But could be fixed by showing the
                            // user a dialog.
                            try {
                                NetworkUtils.openGpsSettings(getActivity());
                                /*Utility.alertMessage(Login.this,"GPS not enabled!","Please enable your device GPS  and login again");*/

                            } catch (ClassCastException e) {

                                NetworkUtils.openGpsSettings(getActivity());
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:

                            NetworkUtils.openGpsSettings(getActivity());
                            break;
                    }
                }
            }
        });


    }

    @Override
    public void onStop() {
        super.onStop();
        if (meetingPresentor != null) {
            meetingPresentor.onStopMvpPresentor();
        }
        dismissProgressDialog();
    }
}
