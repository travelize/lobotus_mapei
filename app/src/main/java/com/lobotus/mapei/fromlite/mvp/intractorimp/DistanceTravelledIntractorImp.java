package com.lobotus.mapei.fromlite.mvp.intractorimp;

import android.content.Context;


import com.lobotus.mapei.R;
import com.lobotus.mapei.fromlite.mvp.intractor.DistanceTravelledIntractor;
import com.lobotus.mapei.retrofitapi.APIService;
import com.lobotus.mapei.retrofitapi.ApiUtils;
import com.lobotus.mapei.utils.ConstantsUtils;
import com.lobotus.mapei.utils.PreferenceUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by user1 on 22-11-2017.
 */

public class DistanceTravelledIntractorImp implements DistanceTravelledIntractor {
    private Call<ResponseBody> postToGetTheDistanceTravelled=null;
    @Override
    public void getDistanceTravelledAndTimeTakenFromTheServer(final Context context,
                                                              final OnDistanceTravelledDownlodeLisner onDistanceTravelledDownlodeLisner) {

        APIService apiService= ApiUtils.getAPIService(ConstantsUtils.postBaseUrl+ConstantsUtils.POST_GET_THE_DISTANCE_TRAVELLED);
        postToGetTheDistanceTravelled=apiService.postToGetTheDistanceTravelled(PreferenceUtils.getMeetingIdFromStartRoutePreference(context));
        postToGetTheDistanceTravelled.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                if (response.isSuccessful())
                                {
                                    try {
                                        JSONObject jsonObject=new JSONObject(response.body().string());
                                        onDistanceTravelledDownlodeLisner.OnDistanceTravelledDownlodeSuccess(jsonObject.getString("TimeTaken"),
                                                jsonObject.getString("DistanceTravelled"));
                                    } catch (JSONException | IOException e) {
                                        onDistanceTravelledDownlodeLisner.OnDistanceTravelledDownlodeFail(context.getString(R.string.justErrorCode)+" 50",
                                                context.getString(R.string.internalError),false);
                                    }
                                }else {
                                    onDistanceTravelledDownlodeLisner.OnDistanceTravelledDownlodeFail(context.getString(R.string.justErrorCode)+" 51",
                                            context.getString(R.string.internalError),false);
                                }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        onDistanceTravelledDownlodeLisner.OnDistanceTravelledDownlodeFail(context.getString(R.string.noInternetMessage)+" 52",
                                context.getString(R.string.noInternetAlert),false);
                    }
                });
    }

    @Override
    public void onStopMvpIntractor() {
        if (postToGetTheDistanceTravelled!=null)
            postToGetTheDistanceTravelled.cancel();
    }
}
