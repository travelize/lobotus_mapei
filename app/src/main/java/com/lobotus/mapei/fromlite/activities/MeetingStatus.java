package com.lobotus.mapei.fromlite.activities;


import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.RelativeLayout;
import android.widget.TimePicker;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.lobotus.mapei.R;
import com.lobotus.mapei.baseactivity.BaseActivity;
import com.lobotus.mapei.customwidgits.LabelledSpinner;
import com.lobotus.mapei.fromlite.baseact.SupportActivity;
import com.lobotus.mapei.fromlite.model.CommRespModel;
import com.lobotus.mapei.fromlite.model.MeetingModel;
import com.lobotus.mapei.fromlite.model.TravelImageModel;
import com.lobotus.mapei.fromlite.mvp.presentor.MeetingStatusFragPresentor;
import com.lobotus.mapei.fromlite.mvp.presentorimp.MeetingStatusFragPresentorImp;
import com.lobotus.mapei.fromlite.mvp.view.MeetingStatusFragView;
import com.lobotus.mapei.retrofitapi.APIService;
import com.lobotus.mapei.retrofitapi.ApiUtils;
import com.lobotus.mapei.utils.CommonFunc;
import com.lobotus.mapei.utils.ConstantsUtils;
import com.lobotus.mapei.utils.ImageUtility;
import com.lobotus.mapei.utils.PermissionUtils;
import com.lobotus.mapei.utils.PreferenceUtils;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class MeetingStatus extends SupportActivity implements View.OnClickListener, LabelledSpinner.OnItemChosenListener,
        MeetingStatusFragView {

    //
    ///// request codes
    private  final int SELECT_FILE = 20;
    private final  int REQUEST_CAMERA=21;
    //// uri cache buffer
    private Uri uriCachedBuffer=null;
    //// IMAGE BASE 64
    private Dialog dialog=null;

    //////// mvp
    private MeetingStatusFragPresentor meetingStatusFragPresentor=null;
    ///////////////////////////////////////////////////////////////////////////////////
    private LinkedList<TravelImageModel> travelImageModels=new LinkedList<>();
    //
    private TravelImageAdapter travelImageAdapter=null;
    ////////////
    private Call<CommRespModel> callpostToServerAfterUserExitedTheGeoFencedRegion=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        RelativeLayout relativeLayout=findViewById(R.id.content_support);
        getLayoutInflater().inflate(R.layout.fragment_meeting_status,relativeLayout);
        getSupportActionBar().setTitle(((MeetingModel) getIntent().getExtras().getSerializable("MEETING_MODEL")).getFullName()+"'s meeting status");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        findViewById(R.id.fragment_uplode_meet_result_submit_button).setOnClickListener(this);
        setTheInitialPlaceholderImage();
        meetingStatusFragPresentor=new MeetingStatusFragPresentorImp(this);
        setTheMeetingStatusSpinner();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {

            case R.id.fragment_uplode_meet_result_submit_button:
                meetingStatusFragPresentor.validateMeetingStatus(MeetingStatus.this,
                        ((LabelledSpinner)findViewById(R.id.fragment_meeting_status_status_spinner_id)).getSpinner().getSelectedItemPosition(),
                        (findViewById(R.id.fragment_uplode_meet_result_followup_stub_from_date_id))
                                ==null?"":((AppCompatEditText)findViewById(R.id.fragment_uplode_meet_result_followup_stub_from_date_id)).getText().toString(),
                        (findViewById(R.id.fragment_uplode_meet_result_followup_stub_to_date_id))
                                ==null?"":((AppCompatEditText)findViewById(R.id.fragment_uplode_meet_result_followup_stub_to_date_id)).getText().toString());
                break;
            case R.id.fragment_uplode_meet_result_followup_stub_to_date_id:
                myDatePicker(v.getId());
                break;
            case R.id.fragment_uplode_meet_result_followup_stub_from_date_id:
                selectTime(v.getId());


        }
    }
    @Override
    protected void onStop() {
        super.onStop();
        if (meetingStatusFragPresentor!=null)
        {
            meetingStatusFragPresentor.onStopMvpPresentor();
        }
        if (callpostToServerAfterUserExitedTheGeoFencedRegion!=null)
            callpostToServerAfterUserExitedTheGeoFencedRegion.cancel();
        dismissProgressDialog();
    }
    private void setTheMeetingStatusSpinner()
    {
        String[]  stringsMeetingStatus=new String[]{"Select","Completed","Follow up","Cancelled"};
        ((LabelledSpinner)findViewById(R.id.fragment_meeting_status_status_spinner_id))
                .setItemsArray(stringsMeetingStatus);
        ((LabelledSpinner)findViewById(R.id.fragment_meeting_status_status_spinner_id)).setDefaultErrorEnabled(false);
        ((LabelledSpinner)findViewById(R.id.fragment_meeting_status_status_spinner_id)).setOnItemChosenListener(this);
    }

    @Override
    public void onItemChosen(View labelledSpinner, AdapterView<?> adapterView, View itemView, int position, long id) {

        switch (position)
        {
            case 2:
                if (((ViewStub)findViewById(R.id.fragment_uplode_meet_result_followup_viewstub))!=null)
                {
                    ((ViewStub)findViewById(R.id.fragment_uplode_meet_result_followup_viewstub))
                            .inflate();
                    findViewById(R.id.fragment_uplode_meet_result_followup_stub_from_date_id).setOnClickListener(this);
                    findViewById(R.id.fragment_uplode_meet_result_followup_stub_to_date_id).setOnClickListener(this);
                }else {
                    findViewById(R.id.fragment_uplode_meet_result_followup_viewstub_inflateId).setVisibility(View.VISIBLE);
                }
                break;
            default:
                if (((ViewStub)findViewById(R.id.fragment_uplode_meet_result_followup_viewstub))==null)
                {
                    findViewById(R.id.fragment_uplode_meet_result_followup_viewstub_inflateId).setVisibility(View.GONE);
                }
        }

    }

    @Override
    public void onNothingChosen(View labelledSpinner, AdapterView<?> adapterView) {

    }



/////////////////////////////////////////////////////////////////////////////////////////////////// uplode image stuff

    private void selectUploadImage() {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(MeetingStatus.this);
        builder.setTitle("Upload Pictures Option");
        builder.setMessage("How do you want to set your picture?");
        builder.setPositiveButton("Gallery", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        if (PermissionUtils.checkStoragePermission(MeetingStatus.this)) {
                            openGalary();
                        }else {
                            PermissionUtils.openPermissionDialog(MeetingStatus.this,"Permission needed!","Please grant Storage Permission");

                        }
                    }
                }
        );
        builder.setNegativeButton("Camera", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        if (PermissionUtils.checkCameraPermissionAndStoragePermission(MeetingStatus.this)) {
                            openCamera();
                        }else {
                            PermissionUtils.openPermissionDialog(MeetingStatus.this,"Permission needed!","Please grant Camera and Storage Permission");
                        }
                    }
                }
        );
        android.app.AlertDialog dialog = builder.create();
        dialog.show();
    }


    private void openGalary()
    {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    private void openCamera()
    {
        uriCachedBuffer=null;
        uriCachedBuffer= ImageUtility.saveImageInCachaFolder(TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis())+"",MeetingStatus.this);
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.putExtra(MediaStore.EXTRA_OUTPUT,uriCachedBuffer);
        startActivityForResult(intent, REQUEST_CAMERA);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode==MeetingStatus.this.RESULT_OK)
        {
            showProgressDialog();
            if (requestCode==SELECT_FILE)
            {
                onSelectFromGalleryResult(data);
            }
            if (requestCode==REQUEST_CAMERA)
            {
                onCaptureImageResult(data);

            }

        }

    }

    private void onSelectFromGalleryResult(Intent data) {
        loadImageFromUri(data.getData());
    }
    private void onCaptureImageResult(Intent data) {
        loadImageFromUri(uriCachedBuffer);
    }

    private void loadImageFromUri(Uri uri)
    {
        Glide.with(MeetingStatus.this).load(uri).asBitmap()
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                        if (resource!=null)
                        {
                            Bitmap bitmap=ImageUtility.getResizedBitmap(resource,400);
                            addattachmentImageToLinkedList(bitmap);
                        }
                    }
                });
    }

  /*  private void OnFileResult(Intent intent)
    {
        Bitmap bm=null;
        if (intent != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(MeetingStatus.this.getApplicationContext().getContentResolver(), Uri.parse(intent.getStringExtra("File_uri")));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (bm!=null)
        {
            Bitmap bitmap=ImageUtility.getResizedBitmap(bm,400);
            imageB64TempStorageString=ImageUtility.convertImageToBase64(bitmap,MeetingStatus.this);
        }

    }
    private void OnCameraResult(final Intent intent)
    {
        Glide.with(MeetingStatus.this).load(intent.getStringExtra("File_uri")).asBitmap()
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                        if (resource!=null)
                        {
                            Bitmap bitmap=ImageUtility.getResizedBitmap(resource,400);
                            imageB64TempStorageString=ImageUtility.convertImageToBase64(bitmap,MeetingStatus.this);

                        }
                    }
                });
    }*/

    ///////////////////////////////////////////////////////////////////////////////////// mvp stuff. . . .
    @Override
    public void showProgressDialog() {
        dialog=new Dialog(MeetingStatus.this);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.loaging_layout);
        dialog.show();
    }

    @Override
    public void dismissProgressDialog() {

        if (dialog!=null)
        {
            if (dialog.isShowing())
            {
                dialog.dismiss();
            }
            dialog=null;
        }
    }

    @Override
    public void afterMeetingStatusValidationSuccess() {
        //////////////
        String   tempB64Image="";
        if (travelImageModels.size()>1)
        {
            ArrayList<TravelImageModel> models=new ArrayList<>();
            for (int i=0;i<travelImageModels.size();i++)
            {
                if (!travelImageModels.get(i).isAddImageButton())
                {
                    models.add(travelImageModels.get(i));
                }
            }
            for (int i=0;i<models.size();i++)
            {
                if (models.size()==1)
                {
                    tempB64Image=models.get(i).get_b64String();
                }else {
                    if (!tempB64Image.isEmpty())
                        tempB64Image=tempB64Image+","+models.get(i).get_b64String();
                    else
                        tempB64Image=models.get(i).get_b64String();

                }
            }
        }
        //////////////
        meetingStatusFragPresentor.reqToUpdateTheMeetingStatusToTheServer(MeetingStatus.this,
                ((LabelledSpinner)findViewById(R.id.fragment_meeting_status_status_spinner_id)).getSpinner().getSelectedItemPosition(),
                (findViewById(R.id.fragment_uplode_meet_result_followup_stub_from_date_id))
                        ==null?"":((AppCompatEditText)findViewById(R.id.fragment_uplode_meet_result_followup_stub_from_date_id)).getText().toString(),
                (findViewById(R.id.fragment_uplode_meet_result_followup_stub_to_date_id))
                        ==null?"":((AppCompatEditText)findViewById(R.id.fragment_uplode_meet_result_followup_stub_to_date_id)).getText().toString(),
                tempB64Image,
                ((AppCompatEditText)findViewById(R.id.fragment_uplode_meet_result_notes_id)).getText().toString(),
                ((MeetingModel) getIntent().getExtras().getSerializable("MEETING_MODEL")).getMeetingType(),
                ((MeetingModel) getIntent().getExtras().getSerializable("MEETING_MODEL")).getMeetingSubType());
    }

    @Override
    public void afterMeetingStatusAndexpenceUpdatedToServerIsSuccess(String message) {
      /*  Toast.makeText(MeetingStatus.this, ""+message, Toast.LENGTH_SHORT).show();
        MeetingStatus.this.finishAffinity();
        Intent intent=new Intent(MeetingStatus.this, NavigationActivity.class);
        startActivity(intent);*/

        afterGeoFenceExitEventTOTheServer();

    }

    @Override
    public void afterMeetingStatusAndexpenceUpdatedToServerIsFail(String message, String dialogTitle, boolean isInternetError) {
        CommonFunc.commonDialog(MeetingStatus.this,dialogTitle,message,isInternetError);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////// from date  and to date picker of the followup


    private void myDatePicker(final int viewId) {

        SimpleDateFormat mDF = new SimpleDateFormat("yyyy-mm-dd");
        Date today = new Date();
        mDF.format(today);
        final Calendar c = Calendar.getInstance();
        c.setTime(today);
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(MeetingStatus.this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        String formattedDay = (String.valueOf(dayOfMonth));
                        int m = monthOfYear + 1;


                        String formattedMonth = (String.valueOf(m));

                        if (dayOfMonth < 10) {
                            formattedDay = "0" + dayOfMonth;
                        }

                        if (m < 10) {
                            formattedMonth = "0" + m;

                        }

                      /*  case R.id.fragment_uplode_meet_result_followup_stub_to_date_id:
                        break;
                        case R.id.fragment_uplode_meet_result_followup_stub_from_date_id:*/


                        ((AppCompatEditText)findViewById(viewId)).setText(formattedDay + "/" + (formattedMonth) + "/" + year);
                        ((AppCompatEditText)findViewById(viewId)).clearFocus();


                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {


            }
        });
        datePickerDialog.show();
        datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());
    }


    ///////////// time picker. . .
    private void selectTime(final int viewId)
    {
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
         int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(MeetingStatus.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

                String s = selectedHour+":"+selectedMinute;
                DateFormat f1 = new SimpleDateFormat("HH:mm"); //HH for hour of the day (0 - 23)
                Date d = null;
                try {
                    d = f1.parse(s);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                DateFormat f2 = new SimpleDateFormat("h:mma");

                f2.format(d).toLowerCase(); // "12:18am"
                ((AppCompatEditText)findViewById(viewId))
                        .setText(f2.format(d).toUpperCase());
                ((AppCompatEditText)findViewById(viewId)).setError(null);
                ((AppCompatEditText)findViewById(viewId)).clearFocus();



            }
        }, hour, minute, false);//Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();

    }

    //////////////////////////////////////////////////////////////////////////////// mvp deviation

    private void afterGeoFenceExitEventTOTheServer()
    {
        APIService apiService= ApiUtils.getAPIService(ConstantsUtils.postBaseUrl+ConstantsUtils.POST_TO_SERVER_AFTER_USER_EXITS_THE_GEOFENCED_REGION);
        callpostToServerAfterUserExitedTheGeoFencedRegion=apiService.postToServerAfterUserExitedTheGeoFencedRegion(PreferenceUtils.getMeetingIdFromStartRoutePreference(MeetingStatus.this));
        callpostToServerAfterUserExitedTheGeoFencedRegion.enqueue(new Callback<CommRespModel>() {
            @Override
            public void onResponse(Call<CommRespModel> call, Response<CommRespModel> response) {
                dismissProgressDialog();
                if (response.isSuccessful())
                {
                           /* if (response.body().getSuccess()==1)
                            {*/
                    PreferenceUtils.clearTheStartedRouteMeetingIdFromTheSharedPreference(MeetingStatus.this);
                    Intent intentBroadast=new Intent(ConstantsUtils.GEO_FENCE_BROADCAST_ACTION);
                    intentBroadast.putExtra(ConstantsUtils.IS_GEO_FENCE_EVENT_FOR_START_GEOFENCE_BOOLEAN,false);
                    MeetingStatus.this.sendBroadcast(intentBroadast);
                    MeetingStatus.this.finishAffinity();
                    Intent intent=new Intent(MeetingStatus.this, BaseActivity.class);
                    startActivity(intent);
                    Toast.makeText(MeetingStatus.this, ""+response.body().getMsg(), Toast.LENGTH_SHORT).show();
                           /* }else {
                                CommonFunc.commonDialog(MeetingStatus.this,getString(R.string.alert),response.body().getMsg(),false);
                            }*/

                }else {
                    // error
                    CommonFunc.commonDialog(MeetingStatus.this,getString(R.string.internalError),getString(R.string.justErrorCode)+" 73",true);
                }
            }

            @Override
            public void onFailure(Call<CommRespModel> call, Throwable t) {
                dismissProgressDialog();
                // error
                CommonFunc.commonDialog(MeetingStatus.this,getString(R.string.noInternetAlert),getString(R.string.noInternetMessage)+" 74",true);
            }
        });
    }
    ///////////////////////////////////////////////////////////////////// image uplode stuff. . .  .
    ///////////////////////////////////////// image adapter
    private class TravelImageAdapter extends RecyclerView.Adapter<TravelImageAdapter.TravelImageHolder>
    {
        @Override
        public TravelImageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view=LayoutInflater.from(MeetingStatus.this).inflate(R.layout.fragment_meetst_image_row,parent,false);
            return new TravelImageHolder(view);
        }

        @Override
        public void onBindViewHolder(final TravelImageAdapter.TravelImageHolder holder, int position) {
            if (travelImageModels.get(holder.getAdapterPosition()).isAddImageButton())
            {
                holder.imageViewDelete.setVisibility(View.GONE);
                holder.imageViewTravelImageView.setImageDrawable(ContextCompat.getDrawable(MeetingStatus.this,R.drawable.ic_add_image));
            }else {
                holder.imageViewDelete.setVisibility(View.VISIBLE);
                holder.imageViewTravelImageView.setImageBitmap(travelImageModels.get(holder.getAdapterPosition()).getBitmap());



            }
            holder.imageViewTravelImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (travelImageModels.get(holder.getAdapterPosition()).isAddImageButton())
                    {
                        if (travelImageModels.size()<4)
                            selectUploadImage();
                        else
                            Toast.makeText(MeetingStatus.this, "Maximum image upload limit is 3.", Toast.LENGTH_SHORT).show();
                    }

                }
            });
            holder.imageViewDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    travelImageModels.remove(holder.getAdapterPosition());
                    notifyDataSetChanged();
                }
            });

        }

        @Override
        public int getItemCount() {
            return travelImageModels.size();
        }

        class TravelImageHolder extends RecyclerView.ViewHolder
        {
            AppCompatImageView imageViewTravelImageView=null,imageViewDelete=null;
            AppCompatTextView textViewImageCaption=null;
            AppCompatTextView textViewAmount=null;
            TravelImageHolder(View itemView) {
                super(itemView);
                this.imageViewTravelImageView=itemView.findViewById(R.id.fragment_travel_image_row_imageview_id);
                this.imageViewDelete=itemView.findViewById(R.id.fragment_travel_image_row_delete_imageview);

            }
        }
    }

    /////// image uplode stuff
    private void setTheInitialPlaceholderImage()
    {
        travelImageModels.clear();
        travelImageModels.add(new TravelImageModel(null,null,true));
        travelImageAdapter=new TravelImageAdapter();
        ((RecyclerView)findViewById(R.id.fragment_meeting_status_add_imege_recycleview)).setLayoutManager(new
                LinearLayoutManager(MeetingStatus.this, LinearLayoutManager.HORIZONTAL,false));
        ((RecyclerView)findViewById(R.id.fragment_meeting_status_add_imege_recycleview)).setAdapter(travelImageAdapter);
    }
    private void addattachmentImageToLinkedList(Bitmap bitmap)
    {

        String b64=ImageUtility.convertImageToBase64(bitmap,MeetingStatus.this);
        travelImageModels.addFirst(new TravelImageModel(b64,bitmap,false));
        travelImageAdapter.notifyDataSetChanged();
        dismissProgressDialog();
    }
}
