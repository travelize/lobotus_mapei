package com.lobotus.mapei.phonestate;

import android.content.Context;
import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;

import com.lobotus.mapei.utils.PreferenceUtils;

import java.lang.reflect.Method;

/**
 * Created by user1 on 07-11-2017.
 */

public class CustomPhoneStateListener extends PhoneStateListener {

    Context mContext=null;
    private int signalPercentage=-1;

    public CustomPhoneStateListener(Context context) {
        mContext = context;
    }

    @Override
    public void onSignalStrengthsChanged(SignalStrength signalStrength) {
        super.onSignalStrengthsChanged(signalStrength);
        if (signalStrength.isGsm()) {
            signalPercentage=signalStrength.getGsmSignalStrength();
        } else if (signalStrength.getCdmaDbm() > 0) {
            signalPercentage=signalStrength.getCdmaEcio();
        } else {
            try {
                signalPercentage=signalStrength.getEvdoSnr();
            }catch (Exception e)
            {
                e.printStackTrace();
            }

        }
        PreferenceUtils.addlatestSignalStatusToPref(mContext,convertSignalPercentageToLevel(signalPercentage));
        // Reflection code starts from here
        try {
            Method[] methods = SignalStrength.class
                    .getMethods();
            for (Method mthd : methods) {
                if (mthd.getName().equals("getLteSignalStrength")
                        || mthd.getName().equals("getLteRsrp")
                        || mthd.getName().equals("getLteRsrq")
                        || mthd.getName().equals("getLteRssnr")
                        || mthd.getName().equals("getLteCqi")) {
//                    Log.i(LOG_TAG,
//                            "onSignalStrengthsChanged: " + mthd.getName() + " "
//                                    + mthd.invoke(signalStrength));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        // Reflection code ends here
    }

    private String convertSignalPercentageToLevel(int signalPercentage)
    {
        if (signalPercentage<=1)
        {
            return "NA";
        }
        if (signalPercentage<=30)
        {
            return "Weak";
        }
        if (signalPercentage<=40)
        {
            return "Fair";
        }
        if (signalPercentage<=60)
        {
            return "Good";
        }
        if (signalPercentage<=80)
        {
            return "Very Good";
        }
        if (signalPercentage>80){
            return "Excellent";
        }
        return "NA";
    }
}
