package com.lobotus.mapei.models;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by user1 on 12-12-2017.
 */

public class GeoFenceModel {

    private LatLng latLng=null;
    private String geoFenceId =null;

    public GeoFenceModel(LatLng latLng, String geoFenceIdOrTaskId) {
        this.latLng = latLng;
        this.geoFenceId = geoFenceIdOrTaskId;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }

    public String getGeoFenceId() {
        return geoFenceId;
    }

    public void setGeoFenceId(String geoFenceId) {
        this.geoFenceId = geoFenceId;
    }
}
