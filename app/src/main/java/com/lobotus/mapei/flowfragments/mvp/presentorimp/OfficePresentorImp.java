package com.lobotus.mapei.flowfragments.mvp.presentorimp;

import android.content.Context;

import com.lobotus.mapei.flowfragments.mvp.intrector.OfficeIntractor;
import com.lobotus.mapei.flowfragments.mvp.intrectorimp.OfficeIntractorImp;
import com.lobotus.mapei.flowfragments.mvp.presentor.OfficePresentor;
import com.lobotus.mapei.flowfragments.mvp.views.OfficeView;
import com.lobotus.mapei.utils.NetworkUtils;

/**
 * Created by user1 on 12-10-2017.
 */

public class OfficePresentorImp  implements OfficePresentor,OfficeIntractor.OnOfficeVisitPostLisner,
        OfficeIntractor.OnOfficeCheckInLisner,OfficeIntractor.OnOfficeCheckOutLisner
{
    //// post the purpose of the office visit
    private OfficeView officeView=null;
    private OfficeIntractor officeIntractor=null;

    public OfficePresentorImp(OfficeView officeView) {
        this.officeView = officeView;
        this.officeIntractor = new OfficeIntractorImp();
    }

    @Override
    public void reqToPostThePurposeOfOfficeVisit(Context context, String purpose, String notes) {
        if (NetworkUtils.checkInternetAndOpenDialog(context))
        {
            officeView.showProgressDialog();
            officeIntractor.reqToPostThePurposeOfOfficeVisit(context,purpose,notes,this);
        }
    }



    @Override
    public void OnOfficeVisitPostSuccess(String message) {
        officeView.dismissProgressDialog();
        officeView.afterRecordingTheOfficeVisitSuccess(message);
    }

    @Override
    public void OnOfficeVisitPostFail(String message, String dialogTitle, boolean isInternetError) {
        officeView.dismissProgressDialog();
        officeView.afterRecordingTheOfficeVisitFail(message,dialogTitle,isInternetError);
    }

    @Override
    public void reqToPostTheOfficeCheckIn(Context context,String officeLoc) {
        if (NetworkUtils.isConnected(context))
        {

            officeIntractor.reqToPostTheOfficeCheckIn(context,this,officeLoc);
        }else {
            officeView.dismissProgressDialog();
            NetworkUtils.checkInternetAndOpenDialog(context);
        }

    }

    @Override
    public void reqToPostTheOfficeCheckOut(Context context) {
        if (NetworkUtils.checkInternetAndOpenDialog(context))
        {
            officeView.showProgressDialog();
            officeIntractor.reqToPostTheOfficeCheckOut(context,this);
        }

    }

    @Override
    public void OnofficeCheckInSuccess(String message) {
        officeView.dismissProgressDialog();
        officeView.afterOfficeCheckInSuccess(message);
    }

    @Override
    public void OnofficeCheckInFail(String message, String dialogTitle, boolean isInternetError) {
        officeView.dismissProgressDialog();
        officeView.afterOfficeCheckInFail(message,dialogTitle,isInternetError);
    }

    @Override
    public void OnofficeCheckOutSuccess(String message) {
        officeView.dismissProgressDialog();
        officeView.afterOfficeCheckOutSuccess(message);
    }

    @Override
    public void OnofficeCheckOutFail(String message, String dialogTitle, boolean isInternetError) {
        officeView.dismissProgressDialog();
        officeView.afterOfficeCheckOutFail(message,dialogTitle,isInternetError);
    }
}
