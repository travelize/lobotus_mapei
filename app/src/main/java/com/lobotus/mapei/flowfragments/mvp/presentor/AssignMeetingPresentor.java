package com.lobotus.mapei.flowfragments.mvp.presentor;

import android.content.Context;

import com.lobotus.mapei.flowfragments.model.ProductsDataModel;
import com.lobotus.mapei.flowfragments.model.ProjectsDataModel;

import java.util.ArrayList;

/**
 * Created by User on 20-03-2018.
 */

public interface AssignMeetingPresentor {

    void reqToGetTheComplaintsDataFromTheServer(Context context);
    void reqToDownlodeUserData(Context context,String TypeID,String UserId);
    //////////////////////////// downlode the customer data
    void reqToGetTheCustomerDataFromPresentor(Context context);
    /////////////////////////////
    void reqToGetTheProjectNameBasedOnCustomerName(Context context,String customerId);
    //////////////////////////////////////
    /*void reqToAssignMeetingToUser(String UserId,String MeetingTypeID,String ProjectID,String Status,String CustomerID,
                                  String OnDate,String OnTime,Context context);*/

    void reqToGetTheProductsDataFromPresentor(Context context);

    void reqToPostTheMeetingToTheServer(String userId,Context context, String projectName,
                                        ArrayList<ProjectsDataModel> projectsDataModels,
                                        String customerName, String customerType,
                                        int meetingTypeSpinnerSelectedPos, int submeetingTypeSelectedSpinner, String valueOfFllowUpOrder,
                                        String orderLostOfFollowupOrder,
                                        int complaintsTypesSpinnerSelectedPos,
                                        int arFollowTypesSpinnerSelectedPos,
                                        String arFollowPaymentNewDate,
                                        String arFollowPaymentProjectissue,
                                        String arFollowPaymentProjectDeley,
                                        String arFollowCformFollowupCollected,
                                        String arFollowCformFollowupNewDate,
                                        String arFollowCformFollowupNotTracable,
                                        String arFollowCformReconcilationMissingInvoice,
                                        String arFollowCformReconcilationShortPayment,
                                        String arFollowCformReconcilationStatement,
                                        String notes,
                                        ArrayList<ProductsDataModel> productsDataModels_Submitance,
                                        ArrayList<ProductsDataModel> productsDataModels_Presentation,
                                        ArrayList<ProductsDataModel> productsDataModels_Followup,
                                        String location,
                                        String meetingSubTypeValue, String customerId,String meetingLocation);

}
