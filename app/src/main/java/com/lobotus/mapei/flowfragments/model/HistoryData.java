package com.lobotus.mapei.flowfragments.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by user1 on 16-10-2017.
 */

public class HistoryData {


    @SerializedName("UserName")
    @Expose
    private String userName;
    @SerializedName("UserPhone")
    @Expose
    private String userPhone;
    @SerializedName("MeetingID")
    @Expose
    private String meetingID;
    @SerializedName("MeetingTypeID")
    @Expose
    private Integer meetingTypeID;
    @SerializedName("MeetingType")
    @Expose
    private String meetingType;
    @SerializedName("FollowupTypeID")
    @Expose
    private Integer followupTypeID;
    @SerializedName("FollowUpType")
    @Expose
    private String followUpType;
    @SerializedName("Remarks")
    @Expose
    private String remarks;
    @SerializedName("OnDate")
    @Expose
    private String onDate;
    @SerializedName("OrderValue")
    @Expose
    private Integer orderValue;
    @SerializedName("OrderLost")
    @Expose
    private Integer orderLost;
    @SerializedName("ComplaintType")
    @Expose
    private String complaintType;
    @SerializedName("PaymentDate")
    @Expose
    private String paymentDate;
    @SerializedName("ProductIssues")
    @Expose
    private String productIssues;
    @SerializedName("ProductDelay")
    @Expose
    private String productDelay;
    @SerializedName("CFormCollected")
    @Expose
    private String cFormCollected;
    @SerializedName("CFormNewDate")
    @Expose
    private String cFormNewDate;
    @SerializedName("CFNotTraceable")
    @Expose
    private String cFNotTraceable;
    @SerializedName("MissingInvoices")
    @Expose
    private String missingInvoices;
    @SerializedName("ShortPayments")
    @Expose
    private String shortPayments;
    @SerializedName("Statement")
    @Expose
    private String statement;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getMeetingID() {
        return meetingID;
    }

    public void setMeetingID(String meetingID) {
        this.meetingID = meetingID;
    }

    public Integer getMeetingTypeID() {
        return meetingTypeID;
    }

    public void setMeetingTypeID(Integer meetingTypeID) {
        this.meetingTypeID = meetingTypeID;
    }

    public String getMeetingType() {
        return meetingType;
    }

    public void setMeetingType(String meetingType) {
        this.meetingType = meetingType;
    }

    public Integer getFollowupTypeID() {
        return followupTypeID;
    }

    public void setFollowupTypeID(Integer followupTypeID) {
        this.followupTypeID = followupTypeID;
    }

    public String getFollowUpType() {
        return followUpType;
    }

    public void setFollowUpType(String followUpType) {
        this.followUpType = followUpType;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getOnDate() {
        return onDate;
    }

    public void setOnDate(String onDate) {
        this.onDate = onDate;
    }

    public Integer getOrderValue() {
        return orderValue;
    }

    public void setOrderValue(Integer orderValue) {
        this.orderValue = orderValue;
    }

    public Integer getOrderLost() {
        return orderLost;
    }

    public void setOrderLost(Integer orderLost) {
        this.orderLost = orderLost;
    }

    public String getComplaintType() {
        return complaintType;
    }

    public void setComplaintType(String complaintType) {
        this.complaintType = complaintType;
    }

    public String getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getProductIssues() {
        return productIssues;
    }

    public void setProductIssues(String productIssues) {
        this.productIssues = productIssues;
    }

    public String getProductDelay() {
        return productDelay;
    }

    public void setProductDelay(String productDelay) {
        this.productDelay = productDelay;
    }

    public String getCFormCollected() {
        return cFormCollected;
    }

    public void setCFormCollected(String cFormCollected) {
        this.cFormCollected = cFormCollected;
    }

    public String getCFormNewDate() {
        return cFormNewDate;
    }

    public void setCFormNewDate(String cFormNewDate) {
        this.cFormNewDate = cFormNewDate;
    }

    public String getCFNotTraceable() {
        return cFNotTraceable;
    }

    public void setCFNotTraceable(String cFNotTraceable) {
        this.cFNotTraceable = cFNotTraceable;
    }

    public String getMissingInvoices() {
        return missingInvoices;
    }

    public void setMissingInvoices(String missingInvoices) {
        this.missingInvoices = missingInvoices;
    }

    public String getShortPayments() {
        return shortPayments;
    }

    public void setShortPayments(String shortPayments) {
        this.shortPayments = shortPayments;
    }

    public String getStatement() {
        return statement;
    }

    public void setStatement(String statement) {
        this.statement = statement;
    }
}
