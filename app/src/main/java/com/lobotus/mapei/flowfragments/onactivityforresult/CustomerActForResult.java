package com.lobotus.mapei.flowfragments.onactivityforresult;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;
import android.widget.TextView;

import com.lobotus.mapei.R;
import com.lobotus.mapei.flowfragments.model.CustomerDataModel;
import com.lobotus.mapei.flowfragments.mvp.presentor.DemoPresentor;
import com.lobotus.mapei.flowfragments.mvp.presentorimp.DemoPresentorImp;
import com.lobotus.mapei.flowfragments.mvp.views.DemoView;
import com.lobotus.mapei.utils.CommonFunc;

import java.util.ArrayList;

public class CustomerActForResult extends AppCompatActivity
        implements  android.widget.SearchView.OnQueryTextListener ,DemoView {

    private DemoPresentor demoPresentor=null;
    //
    private  ArrayList<CustomerDataModel> customerDataModelsGlobel=new ArrayList<>();
    private ArrayList<CustomerDataModel> customerDataModelsInAdapter=new ArrayList<>();
    private CustomerAdapter customerAdapter=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_act_for_result);
        Toolbar toolbar=findViewById(R.id.customer_for_act_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        SearchView searchView= (SearchView) findViewById(R.id.customer_for_act_searchview);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            searchView.setQueryHint(Html.fromHtml("<font color = #ffffff>" + "Search by customer name" + "</font>",0));
        }else {
            searchView.setQueryHint(Html.fromHtml("<font color = #ffffff>" + "Search by customer name" + "</font>"));
        }

        int id = searchView.getContext()
                .getResources()
                .getIdentifier("android:id/search_src_text", null, null);
        TextView textView = (TextView) searchView.findViewById(id);
        textView.setTextColor(Color.WHITE);
        ((SearchView)findViewById(R.id.customer_for_act_searchview)).setOnQueryTextListener(this);
        demoPresentor=new DemoPresentorImp(this);
        demoPresentor.reqToGetTheCustomerDataFromPresentor(this);
    }
    @Override
    public boolean onQueryTextSubmit(String query) {


        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        customerDataModelsInAdapter.clear();
        for (int i=0;i<customerDataModelsGlobel.size();i++)
        {
            if (customerDataModelsGlobel.get(i).getCustomerName().toLowerCase().contains(newText.toLowerCase()))
                customerDataModelsInAdapter.add(customerDataModelsGlobel.get(i));
        }
        System.out.println("bbbbbbbbbbbbbbb-customerDataModelsInAdapter-----------------"+customerDataModelsInAdapter.size());
        System.out.println("bbbbbbbbbbbbbbb-customerDataModelsGlobel-----------------"+customerDataModelsGlobel.size());
        if (customerAdapter!=null)
        {
            customerAdapter.notifyDataSetChanged();
        }else {
            customerAdapter=new CustomerAdapter();
            ((RecyclerView)findViewById(R.id.customer_for_act_recycleview_id)).
                    setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
            ((RecyclerView)findViewById(R.id.customer_for_act_recycleview_id)).setAdapter(customerAdapter);
        }

        return false;
    }


    ////////////////////////////////////////////////////////// mvp stuff. . . .  .
    private Dialog dialogProgress =null;
    @Override
    public void showProgressDialog() {
        dialogProgress =new Dialog(CustomerActForResult.this);
        dialogProgress.setCancelable(false);
        dialogProgress.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialogProgress.setContentView(R.layout.loaging_layout);
        dialogProgress.show();
    }

    @Override
    public void dismissProgressDialog() {
        if (dialogProgress !=null)
            {
                if (dialogProgress.isShowing())
                {
                    dialogProgress.dismiss();
                }
                dialogProgress =null;
            }
    }

    @Override
    public void afterCustomerModelDownlodeSuccess(ArrayList<CustomerDataModel> customerDataModels) {
        customerDataModelsGlobel.clear();
        customerDataModelsGlobel.addAll(customerDataModels);
        customerDataModelsInAdapter.addAll(customerDataModels);
        ((RecyclerView)findViewById(R.id.customer_for_act_recycleview_id)).
                setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        customerAdapter=new CustomerAdapter();
        ((RecyclerView)findViewById(R.id.customer_for_act_recycleview_id)).setAdapter(customerAdapter);


    }

    @Override
    public void afterCustomerModelDownlodeFail(String message, String dialogTitle, boolean isInternetError) {
        CommonFunc.commonDialog(CustomerActForResult.this,dialogTitle,message,isInternetError);
    }

    @Override
    public void afterDemoTypeListLoaded(String[] stringsDemos) {
        /// no use
    }

    @Override
    public void afterDemoPostSuccess(String message) {
            // no use
    }

    @Override
    public void afterDemoPostFail(String message, String dialogTitle, boolean isInternetError) {
            // no use
    }

    ///////////// adapter. . .
    public class CustomerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {



        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

            View view= LayoutInflater.from(CustomerActForResult.this).inflate(R.layout.selection_row,parent,false);
            return new SelectionHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, int position) {
            ((SelectionHolder)holder).textView.setText(""+customerDataModelsInAdapter.get(
                    holder.getAdapterPosition()).getCustomerName());
            ((SelectionHolder)holder).textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent();
                    intent.putExtra("customer_model",customerDataModelsInAdapter.get(holder.getAdapterPosition()));
                    setResult(RESULT_OK,intent);
                    finish();
                }
            });
        }

        @Override
        public int getItemCount() {
            return customerDataModelsInAdapter.size();
        }

        private class SelectionHolder extends RecyclerView.ViewHolder
        {
            private AppCompatTextView textView=null;
            public SelectionHolder(View itemView) {
                super(itemView);
                this.textView=itemView.findViewById(R.id.selection_row_textview_id);
            }
        }
    }

}
