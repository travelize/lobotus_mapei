package com.lobotus.mapei.flowfragments.mvp.presentor;

import android.content.Context;
import android.view.View;

import com.lobotus.mapei.flowfragments.model.CustomerTypeDataModel;

import java.util.ArrayList;

/**
 * Created by user1 on 08-11-2017.
 */

public interface AddCustomerPresentor {
    ///////////////////////////// validate customer  in presentor itself
    void validateAddCustomerInPresentorItself(String customerName, String email, String mobileNo, ArrayList<CustomerTypeDataModel> customerTypeDataModels,
                                              int customerTypeSelectedSpinner, String location, Context context, View viewParent);
    ///////////////////////////// req to get the customer types
    void reqToGetTheCustomerTypes(Context context);
    /////////////////////////////  req To Add the customer details
    void reqToAddTheCustomerFromThePresentor(Context context, String CustTypeId, String FullName,
                                             String Location, String Email, String Phone,
                                             String Status);
}
