package com.lobotus.mapei.flowfragments.mvp.intrectorimp;

import android.content.Context;

import com.lobotus.mapei.R;
import com.lobotus.mapei.flowfragments.model.DemoMeetingsModel;
import com.lobotus.mapei.flowfragments.mvp.intrector.ViewDemoMeetingsInteractor;
import com.lobotus.mapei.fromlite.model.MeetingModel;
import com.lobotus.mapei.fromlite.mvp.intractor.MeetingIntractor;
import com.lobotus.mapei.retrofitapi.APIService;
import com.lobotus.mapei.retrofitapi.ApiUtils;
import com.lobotus.mapei.utils.ConstantsUtils;
import com.lobotus.mapei.utils.PreferenceUtils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewDemoMeetingsInteractorImp implements ViewDemoMeetingsInteractor {

    private Call<List<DemoMeetingsModel>> CallpostToGetTheMeetingList = null;


    @Override
    public void reqToServerToGetTheListOfDemoMeetings(Context context, DemoMeetingListDownlodeLisner demoMeetingListDownlodeLisner) {

        if (PreferenceUtils.checkUserisLogedin(context)) {
            performMeetingListDownlode(context, demoMeetingListDownlodeLisner);
        } else {
            demoMeetingListDownlodeLisner.OnDemoMeetingListDownlodeFail(context.getString(R.string.SessionLost),
                    context.getString(R.string.alert), false);
        }

    }

    @Override
    public void onStopMvpIntractor() {

    }


    private void performMeetingListDownlode(final Context context,
                                            final ViewDemoMeetingsInteractor.DemoMeetingListDownlodeLisner meetingListDownlodeLisner) {

//        APIService apiService = ApiUtils.getAPIService(ConstantsUtils.postBaseUrl + ConstantsUtils.POST_TO_GET_MEETING_LIST_OR_MEETING_DETAILS);
        APIService apiService = ApiUtils.getAPIService(ConstantsUtils.postBaseUrl + ConstantsUtils.GET_DEMO_MEETINGS_DETAILS);
        CallpostToGetTheMeetingList = apiService.postToGetTheDemoMeetingList(PreferenceUtils.getUserIdFromPreference(context));
        CallpostToGetTheMeetingList.enqueue(new Callback<List<DemoMeetingsModel>>() {
            @Override
            public void onResponse(Call<List<DemoMeetingsModel>> call, Response<List<DemoMeetingsModel>> response) {

                if (response.isSuccessful()) {
                    if (response.body().size() > 0) {
                        if (response.body().get(0).getSuccess() == 1) {
                            meetingListDownlodeLisner.OnDemoMeetingListDownlodeSuccess((ArrayList<DemoMeetingsModel>) response.body());
                        } else {
                            meetingListDownlodeLisner.OnDemoMeetingListDownlodeFail(response.body().get(0).getMsg(),
                                    context.getString(R.string.alert), false);
                        }
                    } else {
                        meetingListDownlodeLisner.OnDemoMeetingListDownlodeFail(context.getString(R.string.justErrorCode) + " 126",
                                context.getString(R.string.alert), false);
                    }


                } else {
                    meetingListDownlodeLisner.OnDemoMeetingListDownlodeFail(context.getString(R.string.justErrorCode) + " 127",
                            context.getString(R.string.internalError), false);
                }
            }

            @Override
            public void onFailure(Call<List<DemoMeetingsModel>> call, Throwable t) {
                meetingListDownlodeLisner.OnDemoMeetingListDownlodeFail(context.getString(R.string.noInternetMessage) + " 128",
                        context.getString(R.string.noInternetAlert), true);
            }
        });

    }


}
