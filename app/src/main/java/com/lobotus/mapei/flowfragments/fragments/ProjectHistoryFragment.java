package com.lobotus.mapei.flowfragments.fragments;


import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.lobotus.mapei.R;
import com.lobotus.mapei.flowfragments.adapters.ProjectHistoryAdapter;
import com.lobotus.mapei.flowfragments.model.HistoryData;
import com.lobotus.mapei.flowfragments.model.HistoryProjectData;
import com.lobotus.mapei.flowfragments.model.ProjectHistoryModel;
import com.lobotus.mapei.flowfragments.mvp.presentor.ProjectHistoryPresentor;
import com.lobotus.mapei.flowfragments.mvp.presentorimp.ProjectHistoryPresentorImp;
import com.lobotus.mapei.flowfragments.mvp.views.ProjectHistoryView;
import com.lobotus.mapei.utils.CommonFunc;

import java.util.ArrayList;
import java.util.LinkedList;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProjectHistoryFragment extends Fragment implements ProjectHistoryView, SwipeRefreshLayout.OnRefreshListener {

    View viewFragment=null;
    private Dialog dialog=null;
    // mvp
    private ProjectHistoryPresentor projectHistoryPresentor=null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        viewFragment= inflater.inflate(R.layout.fragment_leave, container, false);
        ((SwipeRefreshLayout)viewFragment.findViewById(R.id.fragment_leave_swipe_refresh_id))
                .setOnRefreshListener(this);
        viewFragment.findViewById(R.id.fragment_leave_add_leave_floatbutton).setVisibility(View.GONE);
        projectHistoryPresentor=new ProjectHistoryPresentorImp(this);
        projectHistoryPresentor.reqToDownlodeTheProjectHistoryData(getActivity(),getArguments().getString("projectID"));
        return viewFragment;
    }
    @Override
    public void onRefresh() {
        projectHistoryPresentor.reqToDownlodeTheProjectHistoryData(getActivity(),getArguments().getString("projectID"));
    }
/////////////////////////////////////////////////////////////////////////////// mvp stuff
@Override
public void showProgressDialog() {
    dialog=new Dialog(getActivity());
    dialog.setCancelable(false);
    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
    dialog.setContentView(R.layout.loaging_layout);
    dialog.show();
}

    @Override
    public void dismissProgressDialog() {
        if (getActivity()!=null)
        {
            if ( ((SwipeRefreshLayout)viewFragment.findViewById(R.id.fragment_leave_swipe_refresh_id))!=null)
            {
                if ( ((SwipeRefreshLayout)viewFragment.findViewById(R.id.fragment_leave_swipe_refresh_id)).isRefreshing())
                    ((SwipeRefreshLayout)viewFragment.findViewById(R.id.fragment_leave_swipe_refresh_id)).setRefreshing(false);
            }
            if (dialog!=null)
            {
                if (dialog.isShowing())
                {
                    dialog.dismiss();
                }
                dialog=null;
            }
        }

    }

    @Override
    public void afterProjectHistoryDownlodeSuccess(ProjectHistoryModel projectHistoryModel) {
        ((RecyclerView)viewFragment.findViewById(R.id.fragment_leave_recycleview_id)).setLayoutManager(
                new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false)
        );

        LinkedList<HistoryData> historyDatas=new LinkedList<>();
        for (int i=0;i<projectHistoryModel.getHistory().size();i++)
        {
            historyDatas.add(projectHistoryModel.getHistory().get(i));
        }
        historyDatas.addFirst(null);
        ((RecyclerView)viewFragment.findViewById(R.id.fragment_leave_recycleview_id))
                .setAdapter(new ProjectHistoryAdapter((ArrayList<HistoryProjectData>) projectHistoryModel.getProjectData(),
                        historyDatas,getActivity()));
    }

    @Override
    public void afterProjectHistoryDownlodeFail(String message, String dialogTitle, boolean isInternetError) {
        CommonFunc.commonDialog(getActivity(),dialogTitle,message,isInternetError);
    }


}
