package com.lobotus.mapei.flowfragments.fragments;


import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.Toast;

import com.lobotus.mapei.R;
import com.lobotus.mapei.flowfragments.adapters.TravelDataAdapter;
import com.lobotus.mapei.flowfragments.model.UserTravelModel;
import com.lobotus.mapei.flowfragments.mvp.presentor.TravelDataPresentor;
import com.lobotus.mapei.flowfragments.mvp.presentorimp.TravelDataPresentorImp;
import com.lobotus.mapei.flowfragments.mvp.views.TravelDataView;
import com.lobotus.mapei.utils.CommonFunc;
import com.lobotus.mapei.utils.PreferenceUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * A simple {@link Fragment} subclass.
 */
public class TravelDataFrag extends Fragment implements TravelDataView, View.OnClickListener {


    private ArrayList<UserTravelModel> userTravelModelsGlobel=new ArrayList<>();

    private View viewFragment=null;
    private TravelDataPresentor travelDataPresentor=null;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        viewFragment= inflater.inflate(R.layout.fragment_travel_data, container, false);
        viewFragment.findViewById(R.id.fragment_travel_data_from_date_id).setOnClickListener(this);
        viewFragment.findViewById(R.id.fragment_travel_data_to_date_id).setOnClickListener(this);
        ((AppCompatEditText)viewFragment.findViewById(R.id.fragment_travel_data_from_date_id)).setText(""+CommonFunc.getTodayDate());
        ((AppCompatEditText)viewFragment.findViewById(R.id.fragment_travel_data_to_date_id)).setText(""+CommonFunc.getTodayDate());
        travelDataPresentor=new TravelDataPresentorImp(this);
        travelDataPresentor.reqToGetTheTravelData(getActivity(), CommonFunc.getTodayDate(),CommonFunc.getTodayDate(),
                PreferenceUtils.getUserIdFromPreference(getActivity()));
        ((SwipeRefreshLayout)viewFragment.findViewById(R.id.fragment_travel_data_swipe_refresh_layout_id))
                .setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        ((SwipeRefreshLayout)viewFragment.findViewById(R.id.fragment_travel_data_swipe_refresh_layout_id)).setRefreshing(false);
                        travelDataPresentor.reqToGetTheTravelData(getActivity(),
                                ((AppCompatEditText)viewFragment.findViewById(R.id.fragment_travel_data_from_date_id))
                                        .getText().toString(),
                                ((AppCompatEditText)viewFragment.findViewById(R.id.fragment_travel_data_to_date_id))
                                        .getText().toString(),
                                PreferenceUtils.getUserIdFromPreference(getActivity()));
                    }
                });
        return viewFragment;
    }



    private Dialog dialog=null;
    @Override
    public void showProgressDialog() {
        try {
            dialog=new Dialog(getActivity());
            dialog.setCancelable(false);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.setContentView(R.layout.loaging_layout);
            dialog.show();
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void dismissProgressDialog() {
        try {
            if (dialog!=null)
            {
                if (dialog.isShowing())
                {
                    dialog.dismiss();
                }
                dialog=null;
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void afterTravelDataDownlodeSuccess(ArrayList<UserTravelModel> userTravelModels) {
        userTravelModelsGlobel.clear();
        userTravelModelsGlobel.addAll(userTravelModels);
        ((RecyclerView)viewFragment.findViewById(R.id.fragment_travel_data_recycleview_id))
                .setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,
                        false));
        ((RecyclerView)viewFragment.findViewById(R.id.fragment_travel_data_recycleview_id))
                .setAdapter(new TravelDataAdapter(getActivity(),userTravelModelsGlobel));
    }

    @Override
    public void afterTravelDataDOwnlodeFail(String message, String dialogTitle, boolean isInternetError) {
        Toast.makeText(getActivity(), ""+message, Toast.LENGTH_SHORT).show();
        userTravelModelsGlobel.clear();
        ((RecyclerView)viewFragment.findViewById(R.id.fragment_travel_data_recycleview_id))
                .setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false));
        ((RecyclerView)viewFragment.findViewById(R.id.fragment_travel_data_recycleview_id))
                .setAdapter(new TravelDataAdapter(getActivity(),new ArrayList<UserTravelModel>()));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.fragment_travel_data_from_date_id:
            case R.id.fragment_travel_data_to_date_id:
                myDatePicker(v.getId());
        }
    }


    ////
    ///////////////////////////// date picker
    private void myDatePicker(final int viewId) {

        SimpleDateFormat mDF = new SimpleDateFormat("yyyy-mm-dd");
        Date today = new Date();
        mDF.format(today);
        final Calendar c = Calendar.getInstance();
        c.setTime(today);
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        String formattedDay = (String.valueOf(dayOfMonth));
                        int m = monthOfYear + 1;


                        String formattedMonth = (String.valueOf(m));

                        if (dayOfMonth < 10) {
                            formattedDay = "0" + dayOfMonth;
                        }

                        if (m < 10) {
                            formattedMonth = "0" + m;

                        }
                        ((AppCompatEditText)viewFragment.findViewById(viewId)).setText(formattedDay + "/" + (formattedMonth) + "/" + year);
                        ((AppCompatEditText)viewFragment.findViewById(viewId)).clearFocus();
                        travelDataPresentor.reqToGetTheTravelData(getActivity(),
                                ((AppCompatEditText)viewFragment.findViewById(R.id.fragment_travel_data_from_date_id))
                                .getText().toString(),
                                ((AppCompatEditText)viewFragment.findViewById(R.id.fragment_travel_data_to_date_id))
                                        .getText().toString(),
                                PreferenceUtils.getUserIdFromPreference(getActivity()));




                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                dialog.dismiss();

            }
        });
        datePickerDialog.show();
        datePickerDialog.getDatePicker().setMaxDate(c.getTimeInMillis());
    }
}
