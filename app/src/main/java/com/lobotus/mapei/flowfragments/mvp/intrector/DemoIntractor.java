package com.lobotus.mapei.flowfragments.mvp.intrector;

import android.content.Context;

/**
 * Created by user1 on 25-09-2017.
 */

public interface DemoIntractor {
    /////////////////////////////////////////////////////////// loade the demo spinner
    interface  DemoTypesLoadLisner
    {
        void  onDemotypesLoadedLisner(String[] stringsDemos);
    }
    void reqToGetDemoListFromIntrector(Context context,DemoTypesLoadLisner demoTypesLoadLisner);

    //////////////////////////////////////////////////////////////// post the demo to the server
    interface DemoPostResponseLisner
    {
        void onDemoPostSuccessLisner(String message);
        void onDemoPostFailLisner(String message,String dialogTitle,boolean isInternetError);
    }
    void reqToPostTheDemoToServer(Context context,
                                  int demoTypeId,
                                  String remark,
                                  int demosubtype,
                                  DemoPostResponseLisner demoPostResponseLisner,
                                  String customerId);
}
