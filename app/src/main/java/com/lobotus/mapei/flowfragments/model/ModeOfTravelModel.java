package com.lobotus.mapei.flowfragments.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by user1 on 05-10-2017.
 */

public class ModeOfTravelModel {

    @SerializedName("MOT")
    @Expose
    private String mOT;

    public String getMOT() {
        return mOT;
    }

    public void setMOT(String mOT) {
        this.mOT = mOT;
    }
}
