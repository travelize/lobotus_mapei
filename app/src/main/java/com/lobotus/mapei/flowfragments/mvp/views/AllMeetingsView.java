package com.lobotus.mapei.flowfragments.mvp.views;

import com.lobotus.mapei.fromlite.model.MeetingModel;

import java.util.ArrayList;

public interface AllMeetingsView {

    void showProgressDialog();
    void dismissProgressDialog();
    ////////////////////////////////////////////////// get meeting data model
    void afterMeetingsListDownlodeSuccess(ArrayList<MeetingModel> meetingModels);
    void afterMeetingsListDownlodeFail(String message, String dialogTitle, boolean isInternetError);
}
