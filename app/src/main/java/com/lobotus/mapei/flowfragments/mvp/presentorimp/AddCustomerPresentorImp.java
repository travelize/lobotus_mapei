package com.lobotus.mapei.flowfragments.mvp.presentorimp;

import android.content.Context;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.AppCompatEditText;
import android.view.View;

import com.lobotus.mapei.R;
import com.lobotus.mapei.flowfragments.model.CustomerTypeDataModel;
import com.lobotus.mapei.flowfragments.mvp.intrector.AddCustomerIntractor;
import com.lobotus.mapei.flowfragments.mvp.intrectorimp.AddCustomerIntratorImp;
import com.lobotus.mapei.flowfragments.mvp.views.AddCustomerView;
import com.lobotus.mapei.utils.CommonFunc;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by user1 on 08-11-2017.
 */

public class AddCustomerPresentorImp implements com.lobotus.mapei.flowfragments.mvp.presentor.AddCustomerPresentor,
        AddCustomerIntractor.AddCustomerLisner,AddCustomerIntractor.CustomerTypeLisner
{
    private AddCustomerView addCustomerView=null;
    private AddCustomerIntractor addCustomerIntractor=null;

    public AddCustomerPresentorImp(AddCustomerView addCustomerView) {
        this.addCustomerView = addCustomerView;
        this.addCustomerIntractor = new AddCustomerIntratorImp();
    }

    @Override
    public void validateAddCustomerInPresentorItself(String customerName, String email, String mobileNo, ArrayList<CustomerTypeDataModel>
            customerTypeDataModels, int customerTypeSelectedSpinner, String location, Context context, View viewParent) {
        if (customerName.isEmpty())
        {
            ((TextInputLayout)viewParent.findViewById(R.id.fragment_add_customer_name_input_id)).setError("Please enter customer name");
            ((AppCompatEditText)viewParent.findViewById(R.id.fragment_add_customer_name_edittext_id)).requestFocus();
        }else if (!email.isEmpty() && !isValidEmail(email))
        {
            ((TextInputLayout)viewParent.findViewById(R.id.fragment_add_customer_email_input_id)).setError("Please enter valid email id");
            ((AppCompatEditText)viewParent.findViewById(R.id.fragment_add_customer_email_edittext_id)).requestFocus();
        }else if (!mobileNo.isEmpty() && mobileNo.length()!=10)
        {
            ((TextInputLayout)viewParent.findViewById(R.id.fragment_add_customer_mobile_input_id)).setError("Please enter valid mobile no");
            ((AppCompatEditText)viewParent.findViewById(R.id.fragment_add_customer_mobile_edittext_id)).requestFocus();
        }else if (customerTypeSelectedSpinner==0)
        {
            CommonFunc.commonDialog(context,context.getString(R.string.alert),"Please select customer type",false);
        }else if (location.isEmpty())
        {
            ((TextInputLayout)viewParent.findViewById(R.id.fragment_add_customer_location_inputlayout_id)).setError("Please enter location");
            ((AppCompatEditText)viewParent.findViewById(R.id.fragment_add_customer_location_edittext_id)).requestFocus();
        }else {
            addCustomerView.afterAddCustomerValidationSuccess();
        }

    }

    private boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    //////////////////// get customer type
    @Override
    public void reqToGetTheCustomerTypes(Context context) {
        addCustomerView.showProgressDialog();
        addCustomerIntractor.reqToGetTheCustomerTypeFromIntractor(context,this);

    }


    @Override
    public void DownlodeCustomerTypeSuccessLisner(ArrayList<CustomerTypeDataModel> customerTypeDataModels) {
        addCustomerView.dismissProgressDialog();
        addCustomerView.afterCustomerTypeDownlodeSuccess(customerTypeDataModels);

    }

    @Override
    public void DownlodeCustomerTypeFailLisner(String message, String dialogTitle, boolean isInternetError) {
        addCustomerView.dismissProgressDialog();
        addCustomerView.afterCustomerTypeDownlodeFail(message,dialogTitle,isInternetError);
    }

    ///////////////////// add customer
   /* Context context, String CustTypeId, String FullName, String Location, String Email, String Phone,
    String Status, AddCustomerIntractor.AddCustomerLisner addCustomerLisner)*/
    @Override
    public void reqToAddTheCustomerFromThePresentor(Context context, String CustTypeId, String FullName, String Location,
                                                    String Email, String Phone, String Status) {
        addCustomerView.showProgressDialog();
        addCustomerIntractor.reqToAddCustomerFromIntractor(context,CustTypeId,FullName,Location,Email,Phone,
                Status,this);
    }

    @Override
    public void AddCustomerSuccessLisner(String message) {
        addCustomerView.dismissProgressDialog();
        addCustomerView.afterAddCustomerSuccess(message);

    }

    @Override
    public void AddCustomerFailLisner(String message, String dialogTitle, boolean isInternetError) {
        addCustomerView.dismissProgressDialog();
        addCustomerView.afterAddCustomerFail(message,dialogTitle,isInternetError);
    }


}
