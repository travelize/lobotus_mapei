package com.lobotus.mapei.flowfragments.adapters;

import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.lobotus.mapei.R;
import com.lobotus.mapei.flowfragments.model.ProductsDataModel;

import java.util.ArrayList;

/**
 * Created by user1 on 16-10-2017.
 */

public class ProjectHistProductAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<ProductsDataModel> productsDataModels=null;
    private Context context=null;

    public ProjectHistProductAdapter(ArrayList<ProductsDataModel> productsDataModels, Context context) {
        this.productsDataModels = productsDataModels;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.project_his_product_row,parent,false);
        return new ProductHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ProductHolder productHolder= (ProductHolder) holder;
        productHolder.textViewProductName.setText(productsDataModels.get(holder.getAdapterPosition()).getProductName());
        productHolder.textViewQty.setText(""+
        productsDataModels.get(holder.getAdapterPosition()).getQuantity());
        productHolder.textViewPrice.setText(""+
                productsDataModels.get(holder.getAdapterPosition()).getPrice());
    }

    @Override
    public int getItemCount() {
        return productsDataModels.size();
    }

    private class ProductHolder extends RecyclerView.ViewHolder
    {
        private AppCompatTextView textViewProductName=null,textViewQty=null,textViewPrice=null;
         ProductHolder(View itemView) {
            super(itemView);
            this.textViewProductName=itemView.findViewById(R.id.project_his_product_row_product_name);
            this.textViewQty=itemView.findViewById(R.id.project_his_product_row_product_quantity);
            this.textViewPrice=itemView.findViewById(R.id.project_his_product_row_product_price);
        }
    }
}
