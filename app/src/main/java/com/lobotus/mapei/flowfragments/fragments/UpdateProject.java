package com.lobotus.mapei.flowfragments.fragments;


import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatEditText;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Toast;

import com.lobotus.mapei.R;
import com.lobotus.mapei.customwidgits.LabelledSpinner;
import com.lobotus.mapei.retrofitapi.APIService;
import com.lobotus.mapei.retrofitapi.ApiUtils;
import com.lobotus.mapei.utils.CommonFunc;
import com.lobotus.mapei.utils.ConstantsUtils;
import com.lobotus.mapei.utils.NetworkUtils;
import com.lobotus.mapei.utils.PreferenceUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
 /* bundle.putString("projectID",projectsDataModels.get(holder.getAdapterPosition()).getProjectID());
          bundle.putString("projectName",projectsDataModels.get(holder.getAdapterPosition()).getProjectName());
          bundle.putString("projectStatus",projectsDataModels.get(holder.getAdapterPosition()).getStatus());
          bundle.putString("projectStage",projectsDataModels.get(holder.getAdapterPosition()).getProductStage());
                          bundle.putString("projectNotes",projectsDataModels.get(holder.getAdapterPosition()).getRemarks());
*/
public class UpdateProject extends Fragment implements View.OnClickListener {

    private View viewFragment=null;
    //
    private String[] stringsStatues=new String[]{"Select project status","Created","InProcess","Cancelled","Completed"};
    private Dialog dialog=null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        viewFragment= inflater.inflate(R.layout.fragment_update_project, container, false);
        setTheData();
        viewFragment.findViewById(R.id.fragment_update_project_submit_button_id).setOnClickListener(this);
        return viewFragment;
    }

    private void setTheData()
    {
        ((AppCompatEditText)viewFragment.findViewById(R.id.fragment_update_project_name_edittext_id))
                .setText(getArguments().getString("projectName"));
        ((LabelledSpinner)viewFragment.findViewById(R.id.fragment_update_project_stage_spinner_id)).setItemsArray(R
                .array.projectStages);
        ((LabelledSpinner)viewFragment.findViewById(R.id.fragment_update_project_status_spinner_id))
                .setDefaultErrorEnabled(false);
        ((AppCompatEditText)viewFragment.findViewById(R.id.fragment_update_project_notes_edittext_id))
                .setText(getArguments().getString("projectNotes"));
        ((LabelledSpinner)viewFragment.findViewById(R.id.fragment_update_project_status_spinner_id))
                .setItemsArray(stringsStatues);
        ((LabelledSpinner)viewFragment.findViewById(R.id.fragment_update_project_status_spinner_id))
                .setDefaultErrorEnabled(false);

        switch (getArguments().getString("projectStatus"))
        {
            case "Created":
                ((LabelledSpinner)viewFragment.findViewById(R.id.fragment_update_project_status_spinner_id))
                        .setSelection(1);
                break;
            case "InProcess":
                ((LabelledSpinner)viewFragment.findViewById(R.id.fragment_update_project_status_spinner_id))
                        .setSelection(2);
                break;
            case "Cancelled":
                ((LabelledSpinner)viewFragment.findViewById(R.id.fragment_update_project_status_spinner_id))
                        .setSelection(3);
                break;
            case "Completed":
                ((LabelledSpinner)viewFragment.findViewById(R.id.fragment_update_project_status_spinner_id))
                        .setSelection(4);
        }
    }

    private boolean validate()
    {
        if (((LabelledSpinner)viewFragment.findViewById(R.id.fragment_update_project_status_spinner_id)).getSpinner().getSelectedItemPosition()==0)
        {
            CommonFunc.commonDialog(getActivity(),getString(R.string.alert),"Please choose project status",false);
            return false;
        }else if (((LabelledSpinner)viewFragment.findViewById(R.id.fragment_update_project_stage_spinner_id)).getSpinner().getSelectedItemPosition()==0)
        {
            CommonFunc.commonDialog(getActivity(),getString(R.string.alert),"Please select project stage",false);
            return false;
        }else {
            return true;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.fragment_update_project_submit_button_id:
                if (validate())
                    if (NetworkUtils.checkInternetAndOpenDialog(getActivity()))
                    {
                        if (PreferenceUtils.checkUserisLogedin(getActivity()))
                        {
                            postTheData();
                        } else {
                            CommonFunc.commonDialog(getActivity(),getString(R.string.alert),"Session Lost! please login",false);
                        }
                    }
        }
    }

    private void postTheData()
    {
        showProgressDialog();
        String remark="";
        if (!((AppCompatEditText)viewFragment.findViewById(R.id.fragment_update_project_notes_edittext_id)).getText().toString().isEmpty())
            remark=((AppCompatEditText)viewFragment.findViewById(R.id.fragment_update_project_notes_edittext_id)).getText().toString();
        APIService apiService= ApiUtils.getAPIService(ConstantsUtils.postBaseUrl + ConstantsUtils.postToUpdateTheProject);
        apiService.postToUpdateTheProject(PreferenceUtils.getUserIdFromPreference(getActivity()),
                getArguments().getString("projectID"),
                String.valueOf(((LabelledSpinner)viewFragment.findViewById(R.id.fragment_update_project_stage_spinner_id)).getSpinner().getSelectedItem()),
                stringsStatues[((LabelledSpinner)viewFragment.findViewById(R.id.fragment_update_project_status_spinner_id)).getSpinner().getSelectedItemPosition()],
                remark).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dismissProgressDialog();

                if (response.isSuccessful())
                {
                    try {
                        JSONObject jsonObject=new JSONObject(response.body().string());
                        if (jsonObject.getInt("Success")==1)
                        {
                            Toast.makeText(getActivity(), ""+jsonObject.getString("Msg"), Toast.LENGTH_SHORT).show();
                            getActivity().getSupportFragmentManager().popBackStack();
                        }else {
                            CommonFunc.commonDialog(getActivity(),getString(R.string.alert),
                                    jsonObject.getString("Msg"),false);
                        }
                    } catch (JSONException e) {
                        CommonFunc.commonDialog(   getActivity(),getString(R.string.justErrorCode)+" 85",
                                getString(R.string.internalError),
                                false);
                    } catch (IOException e) {
                        CommonFunc.commonDialog(   getActivity(),getString(R.string.justErrorCode)+" 85",
                                getString(R.string.internalError),
                                false);
                    }
                }else {
                    CommonFunc.commonDialog(   getActivity(),getString(R.string.justErrorCode)+" 84",
                            getString(R.string.ServerNotresponding),
                            false);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dismissProgressDialog();
                CommonFunc.commonDialog(getActivity(),getString(R.string.noInternetMessage)+" 83",
                        getString(R.string.noInternetAlert),
                        true);
            }
        });
    }
    /////////////////////
    public void showProgressDialog() {
        dialog=new Dialog(getActivity());
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.loaging_layout);
        dialog.show();
    }

    public void dismissProgressDialog() {
        if (getActivity()!=null)
            if (dialog!=null)
            {
                if (dialog.isShowing())
                {
                    dialog.dismiss();
                }
                dialog=null;
            }
    }
}
