package com.lobotus.mapei.flowfragments.onactivityforresult;

import android.graphics.Color;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.SearchView;
import android.widget.TextView;

import com.lobotus.mapei.R;
import com.lobotus.mapei.flowfragments.fragments.AssignMeetingFrag;
import com.lobotus.mapei.flowfragments.fragments.MeetingFrag;
import com.lobotus.mapei.flowfragments.model.ProductsDataModel;
import com.lobotus.mapei.utils.CommonFunc;

import java.util.ArrayList;

import static com.lobotus.mapei.flowfragments.fragments.MeetingFrag.REQ_CODE_TO_GET_THE_PRODUCTS_FOLLOW_UP;
import static com.lobotus.mapei.flowfragments.fragments.MeetingFrag.REQ_CODE_TO_GET_THE_PRODUCTS_PRESENTATION;
import static com.lobotus.mapei.flowfragments.fragments.MeetingFrag.REQ_CODE_TO_GET_THE_PRODUCTS_SUBMITANCE;

import static com.lobotus.mapei.flowfragments.fragments.AssignMeetingFrag.REQ_CODE_TO_GET_THE_PRODUCTS_FOLLOW_UP_ASSIGN;
import static com.lobotus.mapei.flowfragments.fragments.AssignMeetingFrag.REQ_CODE_TO_GET_THE_PRODUCTS_PRESENTATION_ASSIGN;
import static com.lobotus.mapei.flowfragments.fragments.AssignMeetingFrag.REQ_CODE_TO_GET_THE_PRODUCTS_SUBMITANCE_ASSIGN;

public class MeetingProductOnActiForResult extends AppCompatActivity
        implements android.widget.SearchView.OnQueryTextListener {
    private ArrayList<ProductsDataModel> productsDataModelsInitialy = new ArrayList<>();
    /////
    private int REQ_CODE = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meeting_product_on_acti_for_result);
        Toolbar toolbar = findViewById(R.id.meetings_sel_products_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        //////////////////
        SearchView searchView = (SearchView) findViewById(R.id.meetings_sel_products_searchview);
        REQ_CODE = getIntent().getIntExtra("REQ_CODE", 0);

//        Log.e("request code=", REQ_CODE + "");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            searchView.setQueryHint(Html.fromHtml("<font color = #ffffff>" + "Search by product name" + "</font>", 0));
        } else {
            searchView.setQueryHint(Html.fromHtml("<font color = #ffffff>" + "Search by product name" + "</font>"));
        }

        int id = searchView.getContext()
                .getResources()
                .getIdentifier("android:id/search_src_text", null, null);
        TextView textView = (TextView) searchView.findViewById(id);
        textView.setTextColor(Color.WHITE);


/////////////////////////////////////////
        findViewById(R.id.meetings_sel_products_submit_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitTheResult();
            }
        });

        //Add meatings
        if (REQ_CODE == REQ_CODE_TO_GET_THE_PRODUCTS_SUBMITANCE) {
            for (int i = 0; i < MeetingFrag.productsDataModels_Submitance.size(); i++) {
                productsDataModelsInitialy.add(MeetingFrag.productsDataModels_Submitance.get(i));
            }
        }
        if (REQ_CODE == REQ_CODE_TO_GET_THE_PRODUCTS_PRESENTATION) {
            for (int i = 0; i < MeetingFrag.productsDataModels_Presentation.size(); i++) {
                productsDataModelsInitialy.add(MeetingFrag.productsDataModels_Presentation.get(i));
            }
        }
        if (REQ_CODE == REQ_CODE_TO_GET_THE_PRODUCTS_FOLLOW_UP) {
            for (int i = 0; i < MeetingFrag.productsDataModels_Follow_up.size(); i++) {
                productsDataModelsInitialy.add(MeetingFrag.productsDataModels_Follow_up.get(i));
            }
        }


        //Assign Meetings
        if (REQ_CODE == REQ_CODE_TO_GET_THE_PRODUCTS_SUBMITANCE_ASSIGN) {
            for (int i = 0; i < AssignMeetingFrag.productsDataModels_Submitance.size(); i++) {
                productsDataModelsInitialy.add(AssignMeetingFrag.productsDataModels_Submitance.get(i));
            }
        }
        if (REQ_CODE == REQ_CODE_TO_GET_THE_PRODUCTS_PRESENTATION_ASSIGN) {
            for (int i = 0; i < AssignMeetingFrag.productsDataModels_Presentation.size(); i++) {
                productsDataModelsInitialy.add(AssignMeetingFrag.productsDataModels_Presentation.get(i));
            }
        }
        if (REQ_CODE == REQ_CODE_TO_GET_THE_PRODUCTS_FOLLOW_UP_ASSIGN) {
            for (int i = 0; i < AssignMeetingFrag.productsDataModels_Follow_up.size(); i++) {
                productsDataModelsInitialy.add(AssignMeetingFrag.productsDataModels_Follow_up.get(i));
            }
        }


        ((SearchView) findViewById(R.id.meetings_sel_products_searchview)).setOnQueryTextListener(this);
        setTheAdapter(productsDataModelsInitialy);
    }

    @Override
    public void onBackPressed() {
        if (!isblankDataExist()) {
            addNewDataToOldStatic();
            setResult(RESULT_CANCELED);
            finish();
        } /*else {
            CommonFunc.commonDialog(MeetingProductOnActiForResult.this,
                    getString(R.string.alert), "Please fill the checked product quantity or price", false);
        }*/
        submitTheResult();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        ArrayList<ProductsDataModel> productsDataModels = new ArrayList<>();
        for (int i = 0; i < productsDataModelsInitialy.size(); i++) {
            if (productsDataModelsInitialy.get(i).getProductName().toLowerCase().contains(newText.toLowerCase())) {
                productsDataModels.add(productsDataModelsInitialy.get(i));
            }
        }
        setTheAdapter(productsDataModels);
        return false;
    }

    private void setTheAdapter(ArrayList<ProductsDataModel> productsDataModels) {
        ((RecyclerView) findViewById(R.id.meetings_sel_products_recycleview_id)).
                setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        ((RecyclerView) findViewById(R.id.meetings_sel_products_recycleview_id)).setAdapter(new SelectProductsAdapter(productsDataModels));
    }

    private void submitTheResult() {
        CommonFunc.hideTheKeypad(MeetingProductOnActiForResult.this, findViewById(R.id.meetings_sel_products_searchview));
        if (!isblankDataExist()) {
            addNewDataToOldStatic();
            setResult(RESULT_OK);
            finish();
        } else {
            CommonFunc.commonDialog(MeetingProductOnActiForResult.this,
                    getString(R.string.alert), "Please fill the checked product quantity or price", false);
        }
    }


//////////////////////////////////////////////////////////////////////////////////////////////////// add products adapter

    private class SelectProductsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        private ArrayList<ProductsDataModel> productsDataModelsInSideAdapter = null;

        SelectProductsAdapter(ArrayList<ProductsDataModel> productsDataModelsInSideAdapter) {
            this.productsDataModelsInSideAdapter = productsDataModelsInSideAdapter;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(MeetingProductOnActiForResult.this).inflate(R.layout.meet_frag_sel_product_row, parent, false);
            return new Holder(view, new AmountTextwatcher(), new QuantityTextwatcher());
        }

        @Override
        public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
            final Holder holder1 = (Holder) holder;
            holder1.textViewProductName.setText(productsDataModelsInSideAdapter.get(holder.getAdapterPosition()).getProductName());
            holder1.appCompatCheckBox.setChecked(productsDataModelsInSideAdapter.get(holder.getAdapterPosition()).isChecked());

            if (productsDataModelsInSideAdapter.get(holder.getAdapterPosition()).isChecked()) {


                if (!holder1.textVieweEditableAmount.isEnabled())
                    holder1.textVieweEditableAmount.setEnabled(true);

                if (!holder1.textVieweEditableQuantity.isEnabled())
                    holder1.textVieweEditableQuantity.setEnabled(true);

                holder1.textVieweEditableAmount.setText(productsDataModelsInSideAdapter.get(holder.getAdapterPosition()).getPrice());
                holder1.textVieweEditableQuantity.setText(productsDataModelsInSideAdapter.get(holder.getAdapterPosition()).getQty());
            } else {
                holder1.textVieweEditableAmount.setText("");
                holder1.textVieweEditableAmount.setEnabled(false);
                holder1.textVieweEditableQuantity.setText("");
                holder1.textVieweEditableQuantity.setEnabled(false);

            }
            holder1.appCompatCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        ((Holder) holder).textVieweEditableAmount.setEnabled(true);
                        ((Holder) holder).textVieweEditableQuantity.setEnabled(true);
                        productsDataModelsInSideAdapter.get(holder.getAdapterPosition()).setChecked(true);

                        productsDataModelsInSideAdapter.get(holder.getAdapterPosition()).setQty("1");
                        //
                    } else {
                        productsDataModelsInSideAdapter.get(holder.getAdapterPosition()).setChecked(false);
                        productsDataModelsInSideAdapter.get(holder.getAdapterPosition()).setPrice("");
                        productsDataModelsInSideAdapter.get(holder.getAdapterPosition()).setQty("");
                        //
                        ((Holder) holder).textVieweEditableAmount.setText("");
                        ((Holder) holder).textVieweEditableAmount.setEnabled(false);
                        //
                        ((Holder) holder).textVieweEditableQuantity.setText("");
                        ((Holder) holder).textVieweEditableQuantity.setEnabled(false);

                    }


                }
            });
            holder1.amountTextwatcher.updatePosition(holder.getAdapterPosition(), (Holder) holder);
            holder1.quantityTextwatcher.updatePosition(holder.getAdapterPosition(), (Holder) holder);
            ((Holder) holder).textVieweEditableAmount.setText(productsDataModelsInSideAdapter.get(holder.getAdapterPosition())
                    .getPrice());
            ((Holder) holder).textVieweEditableQuantity.setText(productsDataModelsInSideAdapter.get(holder.getAdapterPosition())
                    .getQty());

        }

        @Override
        public int getItemCount() {
            return productsDataModelsInSideAdapter.size();
        }

        private class Holder extends RecyclerView.ViewHolder {
            AppCompatTextView textViewProductName = null;
            AppCompatCheckBox appCompatCheckBox = null;
            AppCompatEditText textVieweEditableAmount = null;
            AppCompatEditText textVieweEditableQuantity = null;
            //
            AmountTextwatcher amountTextwatcher = null;
            QuantityTextwatcher quantityTextwatcher = null;

            Holder(View itemView, AmountTextwatcher amountTextwatcher, QuantityTextwatcher quantityTextwatcher) {
                super(itemView);
                this.textViewProductName = itemView.findViewById(R.id.meet_frag_sel_product_row_textview_productname_id);
                this.textVieweEditableAmount = itemView.findViewById(R.id.meet_frag_sel_product_row_price_textview_id);
                this.textVieweEditableQuantity = itemView.findViewById(R.id.meet_frag_sel_product_row_quantity_textview_id);
                this.amountTextwatcher = amountTextwatcher;
                this.quantityTextwatcher = quantityTextwatcher;
                this.textVieweEditableAmount.addTextChangedListener(this.amountTextwatcher);
                this.textVieweEditableQuantity.addTextChangedListener(this.quantityTextwatcher);
                this.appCompatCheckBox = itemView.findViewById(R.id.meet_frag_sel_product_row_checkbox_id);
            }
        }

        // amount text watcher
        private class AmountTextwatcher implements TextWatcher {
            private int position;
            private Holder holder = null;

            void updatePosition(int position, Holder holder) {
                this.position = position;
                this.holder = holder;
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (holder != null) {
                    if (productsDataModelsInSideAdapter.get(position).isChecked()) {

                        if (s.toString().trim() != null) {
                            if (!s.toString().trim().isEmpty()) {
                                if (!s.toString().equals("")) {
                                    productsDataModelsInSideAdapter.get(position).setPrice(s.toString().trim());

                                    /*holder.textVieweEditableAmount.setText(""+s.toString().trim());*/
                                    changeDataInInitialArraylistPrice(productsDataModelsInSideAdapter.get(position).getProductID(),
                                            productsDataModelsInSideAdapter.get(position).isChecked(),
                                            productsDataModelsInSideAdapter.get(position).getPrice());
                                }
                            } else {
                         /*   if (before!=0)
                            {
                                productsDataModelsInSideAdapter.get(position).setPrice("");
                                holder.textVieweEditableAmount.setText("");
                                changeDataInInitialArraylist(productsDataModelsInSideAdapter.get(position).getProductID(),
                                        productsDataModelsInSideAdapter.get(position).isChecked(),productsDataModelsInSideAdapter.get(position).getPrice());
                            }*/

                            }
                        }
                    }
                }
            }


            @Override
            public void afterTextChanged(Editable s) {

            }
        }

        //quantity text watcher

        private class QuantityTextwatcher implements TextWatcher {
            private int position;
            private Holder holder = null;

            void updatePosition(int position, Holder holder) {
                this.position = position;
                this.holder = holder;
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (holder != null) {
                    if (productsDataModelsInSideAdapter.get(position).isChecked()) {
                        if (s.toString().trim() != null) {
                            if (!s.toString().trim().isEmpty()) {
                                if (!s.toString().equals("")) {
                                    productsDataModelsInSideAdapter.get(position).setQty(s.toString().trim());

                                    /*holder.textVieweEditableAmount.setText(""+s.toString().trim());*/
                                    changeDataInInitialArraylistQuantity(productsDataModelsInSideAdapter.get(position).getProductID(),
                                            productsDataModelsInSideAdapter.get(position).isChecked(),
                                            productsDataModelsInSideAdapter.get(position).getQty());
                                } else {
                                    /* productsDataModelsInSideAdapter.get(position).setQty("1");

                                     *//*holder.textVieweEditableAmount.setText(""+s.toString().trim());*//*
                                changeDataInInitialArraylistQuantity(productsDataModelsInSideAdapter.get(position).getProductID(),
                                        productsDataModelsInSideAdapter.get(position).isChecked(),
                                        productsDataModelsInSideAdapter.get(position).getQty());*/
                                }
                            } else {
                         /*   if (before!=0)
                            {
                                productsDataModelsInSideAdapter.get(position).setPrice("");
                                holder.textVieweEditableAmount.setText("");
                                changeDataInInitialArraylist(productsDataModelsInSideAdapter.get(position).getProductID(),
                                        productsDataModelsInSideAdapter.get(position).isChecked(),productsDataModelsInSideAdapter.get(position).getPrice());
                            }*/

                            }
                        }
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        }

        private void changeDataInInitialArraylistPrice(String productId, boolean isCheck, String amount) {
            for (int i = 0; i < productsDataModelsInitialy.size(); i++) {
                if (productsDataModelsInitialy.get(i).getProductID().equals(productId)) {
                    productsDataModelsInitialy.get(i).setPrice(amount);
                    productsDataModelsInitialy.get(i).setChecked(isCheck);
                    break;
                }
            }
        }

        private void changeDataInInitialArraylistQuantity(String productId, boolean isCheck, String quantity) {
            for (int i = 0; i < productsDataModelsInitialy.size(); i++) {
                if (productsDataModelsInitialy.get(i).getProductID().equals(productId)) {
                    productsDataModelsInitialy.get(i).setQty(quantity);
                    productsDataModelsInitialy.get(i).setChecked(isCheck);
                    break;
                }
            }
        }
    }


    private boolean isblankDataExist() {
        boolean isblankDataExist = false;
        for (int i = 0; i < productsDataModelsInitialy.size(); i++) {
            if (productsDataModelsInitialy.get(i).isChecked()) {
                if (productsDataModelsInitialy.get(i).getPrice().isEmpty() || productsDataModelsInitialy.get(i).getPrice().equals("0")) {
                    isblankDataExist = true;
                    break;
                }
            }
        }
        return isblankDataExist;

    }

    private void addNewDataToOldStatic() {
        //ADD MEETING DATA TO BE REMOVED FOR REDUSING DUPLICANCY
        if (REQ_CODE == REQ_CODE_TO_GET_THE_PRODUCTS_SUBMITANCE) {
            MeetingFrag.productsDataModels_Submitance.clear();
            MeetingFrag.productsDataModels_Submitance.addAll(productsDataModelsInitialy);
        }
        if (REQ_CODE == REQ_CODE_TO_GET_THE_PRODUCTS_PRESENTATION) {
            MeetingFrag.productsDataModels_Presentation.clear();
            MeetingFrag.productsDataModels_Presentation.addAll(productsDataModelsInitialy);

        }
        if (REQ_CODE == REQ_CODE_TO_GET_THE_PRODUCTS_FOLLOW_UP) {
            MeetingFrag.productsDataModels_Follow_up.clear();
            MeetingFrag.productsDataModels_Follow_up.addAll(productsDataModelsInitialy);
        }


        //ASSIGN MEETING DATA TO BE REMOVED FOR REDUSING DUPLICANCY
        if (REQ_CODE == REQ_CODE_TO_GET_THE_PRODUCTS_SUBMITANCE_ASSIGN) {
            AssignMeetingFrag.productsDataModels_Submitance.clear();
            AssignMeetingFrag.productsDataModels_Submitance.addAll(productsDataModelsInitialy);
        }
        if (REQ_CODE == REQ_CODE_TO_GET_THE_PRODUCTS_PRESENTATION_ASSIGN) {
            AssignMeetingFrag.productsDataModels_Presentation.clear();
            AssignMeetingFrag.productsDataModels_Presentation.addAll(productsDataModelsInitialy);

        }
        if (REQ_CODE == REQ_CODE_TO_GET_THE_PRODUCTS_FOLLOW_UP_ASSIGN) {
            AssignMeetingFrag.productsDataModels_Follow_up.clear();
            AssignMeetingFrag.productsDataModels_Follow_up.addAll(productsDataModelsInitialy);
        }

    }


}
