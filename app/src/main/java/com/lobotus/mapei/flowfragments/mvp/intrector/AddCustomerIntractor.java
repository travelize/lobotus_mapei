package com.lobotus.mapei.flowfragments.mvp.intrector;

import android.content.Context;

import com.lobotus.mapei.flowfragments.model.CustomerTypeDataModel;

import java.util.ArrayList;

/**
 * Created by user1 on 08-11-2017.
 */

public interface AddCustomerIntractor {


    /////////////////////////////////// get the customer type

    interface CustomerTypeLisner
    {
        void DownlodeCustomerTypeSuccessLisner(ArrayList<CustomerTypeDataModel> customerTypeDataModels);
        void DownlodeCustomerTypeFailLisner(String message, String dialogTitle, boolean isInternetError);
    }

    void reqToGetTheCustomerTypeFromIntractor(Context context,CustomerTypeLisner customerTypeLisner);


    ///////////////////////////////// add customer intractor

    interface AddCustomerLisner
    {
        void AddCustomerSuccessLisner(String message);
        void AddCustomerFailLisner(String message, String dialogTitle, boolean isInternetError);
    }

    void reqToAddCustomerFromIntractor(Context context, String CustTypeId,
                                       String FullName,
                                       String Location,
                                       String Email,
                                       String Phone,
                                       String Status,
                                       AddCustomerLisner addCustomerLisner );

}
