package com.lobotus.mapei.flowfragments.mvp.presentorimp;

import android.content.Context;
import android.support.v7.widget.AppCompatEditText;
import android.view.View;

import com.lobotus.mapei.R;
import com.lobotus.mapei.flowfragments.model.CustomerDataModel;
import com.lobotus.mapei.flowfragments.model.ModeOfTravelModel;
import com.lobotus.mapei.flowfragments.model.PurposeOfTravelModel;
import com.lobotus.mapei.flowfragments.mvp.intrector.MeetingInterator;
import com.lobotus.mapei.flowfragments.mvp.intrector.TravelInteractor;
import com.lobotus.mapei.flowfragments.mvp.intrectorimp.TravelInterectorImp;
import com.lobotus.mapei.flowfragments.mvp.presentor.TravelPresentor;
import com.lobotus.mapei.flowfragments.mvp.views.TravelView;
import com.lobotus.mapei.utils.CommonFunc;
import com.lobotus.mapei.utils.NetworkUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user1 on 04-10-2017.
 */

public class TravelPresentorImp implements TravelPresentor,TravelInteractor.OnTravelCustomerDataDownlodeLisner
,TravelInteractor.OnMOTAndPOTDataDownlodeLisner,TravelInteractor.OnTravelDetailsPostLisner{
    
    private TravelView travelView=null;
    private TravelInteractor travelInteractor=null;

    public TravelPresentorImp(TravelView travelView) {
        this.travelView = travelView;
        this.travelInteractor=new TravelInterectorImp();
    }
    ////////////////////////////////////////////// customer data
    @Override
    public void reqToGetTheCustomerDataFromPresentor(Context context) {
        if (NetworkUtils.checkInternetAndOpenDialog(context))
        {
            travelView.showProgressDialog();
            travelInteractor.reqToPostCustomerDataReqToServer(context,this);
        }

    }


    ////////////////////////////////////////////// customer data lisners
    @Override
    public void OnTravelCustomerDataDownlodeSuccess(ArrayList<CustomerDataModel> customerDataModels) {
        travelView.afterCustomerModelDownlodeSuccess(customerDataModels);
    }

    @Override
    public void OnTravelCustomerDataDownlodeFail(String message, String dialogTitle, boolean isInternetError) {
        travelView.dismissProgressDialog();
        travelView.afterCustomerModelDownlodeFail(message,dialogTitle,isInternetError);
    }
    //////////////////////////////////////////////// MOT and POT
    @Override
    public void reqToGetMOTandPOTFromPresentor(Context context) {
        travelInteractor.reqToPostMOTandPOTdataToServer(context,this);
    }

    @Override
    public void OnMOTandPOTdataDOwnlodeSuccess(List<ModeOfTravelModel> modeOfTravelModels, List<PurposeOfTravelModel> purposeOfTravelModels) {
        travelView.dismissProgressDialog();
        travelView.afterMOTandPOTDownlodeSuccess(modeOfTravelModels,purposeOfTravelModels);
    }

    @Override
    public void OnMOTandPOTdownlodeFail(String message, String dialogTitle, boolean isInternetError) {
        travelView.dismissProgressDialog();
        travelView.afterMOTandPOTDownlodeFail(message,dialogTitle,isInternetError);
    }
    /////////////////////////////////////////////////// validate before posting travel

    @Override
    public void reqToValidateAllInPresentorItself(View view, Context context,
                                                  String customerNameInAutoCompletetextView,
                                                  int modeOfTravelSelectedPos,
                                                  boolean isModeOfTravelArraylistEmpty,
                                                  int meeetingPurposeSelectedPos,
                                                  boolean isMeetingPurposeArraylistEmpty,int ownvehicalspinnerselectedpos,
                                                  int fuletypespinnerselectedpos) {
        if ( isModeOfTravelArraylistEmpty || isMeetingPurposeArraylistEmpty)
        {
            CommonFunc.commonDialog(context,context.getString(R.string.alert),"Please refresh the screen",false);
        }
        else  if (customerNameInAutoCompletetextView==null)
        {
            CommonFunc.commonDialog(context,context.getString(R.string.alert),"Please enter customer name",false);
        }else if (customerNameInAutoCompletetextView.isEmpty())
        {
            CommonFunc.commonDialog(context,context.getString(R.string.alert),"Please enter customer name",false);
        }else if (modeOfTravelSelectedPos==0)
        {
            CommonFunc.commonDialog(context,context.getString(R.string.alert),"Please select mode of travel",false);
        }/*else if (modeOfTravelSelectedPos==4)
        {


            if (ownvehicalspinnerselectedpos==0)
            {
                CommonFunc.commonDialog(context,context.getString(R.string.alert),"Please select own vehicle type",false);
            }else if (ownvehicalspinnerselectedpos==2)
            {
                if (fuletypespinnerselectedpos==0)
                {
                    CommonFunc.commonDialog(context,context.getString(R.string.alert),"Please select fuel type",false);
                }else {
                    //
                    ///// all ok
                    if (meeetingPurposeSelectedPos==0)
                    {
                        CommonFunc.commonDialog(context,context.getString(R.string.alert),"Please select meeting purpose",false);
                    }else {
                        if (meeetingPurposeSelectedPos==4)
                        {
                            if (((AppCompatEditText)view.findViewById(R.id.fragment_travel_start_location_edittext_id)).getText().toString().isEmpty())
                            {
                                CommonFunc.commonDialog(context,context.getString(R.string.alert),"Please enter start location",false);
                            }else if (((AppCompatEditText)view.findViewById(R.id.fragment_travel_client_location_edittext_id)).getText().toString().isEmpty())
                            {
                                CommonFunc.commonDialog(context,context.getString(R.string.alert),"Please enter end location",false);

                            }else {
                                travelView.afterValidationSuccess();
                            }
                        }else {
                            if (((AppCompatEditText)view.findViewById(R.id.fragment_travel_start_location_edittext_id)).getText().toString().isEmpty())
                            {
                                CommonFunc.commonDialog(context,context.getString(R.string.alert),"Please enter start location",false);
                            }else if (((AppCompatEditText)view.findViewById(R.id.fragment_travel_client_location_edittext_id)).getText().toString().isEmpty())
                            {
                                CommonFunc.commonDialog(context,context.getString(R.string.alert),"Please enter client location",false);
                            }else {
                                travelView.afterValidationSuccess();
                            }
                        }
                    }
                    //////
                    //
                }



            }else {
                ///// all ok
                if (meeetingPurposeSelectedPos==0)
                {
                    CommonFunc.commonDialog(context,context.getString(R.string.alert),"Please select meeting purpose",false);
                }else {
                    if (meeetingPurposeSelectedPos==4)
                    {
                        if (((AppCompatEditText)view.findViewById(R.id.fragment_travel_start_location_edittext_id)).getText().toString().isEmpty())
                        {
                            CommonFunc.commonDialog(context,context.getString(R.string.alert),"Please enter start location",false);
                        }else if (((AppCompatEditText)view.findViewById(R.id.fragment_travel_client_location_edittext_id)).getText().toString().isEmpty())
                        {
                            CommonFunc.commonDialog(context,context.getString(R.string.alert),"Please enter end location",false);

                        }else {
                            travelView.afterValidationSuccess();
                        }
                    }else {
                        if (((AppCompatEditText)view.findViewById(R.id.fragment_travel_start_location_edittext_id)).getText().toString().isEmpty())
                        {
                            CommonFunc.commonDialog(context,context.getString(R.string.alert),"Please enter start location",false);
                        }else if (((AppCompatEditText)view.findViewById(R.id.fragment_travel_client_location_edittext_id)).getText().toString().isEmpty())
                        {
                            CommonFunc.commonDialog(context,context.getString(R.string.alert),"Please enter client location",false);
                        }else {
                            travelView.afterValidationSuccess();
                        }
                    }
                }
                //////
            }



        }*/




        else {
            ///// all ok
            if (meeetingPurposeSelectedPos==0)
            {
                CommonFunc.commonDialog(context,context.getString(R.string.alert),"Please select meeting purpose",false);
            }else {
                if (meeetingPurposeSelectedPos==4)
                {
                    if (((AppCompatEditText)view.findViewById(R.id.fragment_travel_start_location_edittext_id)).getText().toString().isEmpty())
                    {
                        CommonFunc.commonDialog(context,context.getString(R.string.alert),"Please enter start location",false);
                    }else if (((AppCompatEditText)view.findViewById(R.id.fragment_travel_client_location_edittext_id)).getText().toString().isEmpty())
                    {
                        CommonFunc.commonDialog(context,context.getString(R.string.alert),"Please enter end location",false);

                    }else {
                        travelView.afterValidationSuccess();
                    }
                }else {
                    if (((AppCompatEditText)view.findViewById(R.id.fragment_travel_start_location_edittext_id)).getText().toString().isEmpty())
                    {
                        CommonFunc.commonDialog(context,context.getString(R.string.alert),"Please enter start location",false);
                    }else if (((AppCompatEditText)view.findViewById(R.id.fragment_travel_client_location_edittext_id)).getText().toString().isEmpty())
                    {
                        CommonFunc.commonDialog(context,context.getString(R.string.alert),"Please enter client location",false);
                    }else {
                        travelView.afterValidationSuccess();
                    }
                }
            }
            //////
        }






    }



    private boolean checkCustomerNameIsMatchingTheList(ArrayList<CustomerDataModel> customerDataModels,
                                                       String customerNameInAutoCompletetextView)
    {
        boolean flag=false;
       for (int i=0;i<customerDataModels.size();i++)
       {
           if (customerDataModels.get(i).getCustomerName().equals(customerNameInAutoCompletetextView))
           {
               flag=true;
               break;
           }
       }
       return flag;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////// post the travel details to the server
    @Override
    public void reqToSaveTheTravelDetails(Context context, String remark, String custId, String modeOfTravel, String purpose, String attachmentB64,
                                          String startLoc,String endLoc,String imagecaption,String ownvehicalspinnerselectedpos,
                                          String fuletypespinnerselectedpos,String imageAmount) {
        if (NetworkUtils.checkInternetAndOpenDialog(context))
        {
            travelView.showProgressDialog();
            travelInteractor.reqToPostTheTravelDetailsToServer(context,remark,custId,modeOfTravel,purpose,attachmentB64,this,
                    startLoc,endLoc,imagecaption,ownvehicalspinnerselectedpos,fuletypespinnerselectedpos,imageAmount);
        }
    }

    @Override
    public void OnTravelDetailsPostSuccess(String message) {
        travelView.dismissProgressDialog();
        travelView.afterTravelPostSuccess(message);
    }

    @Override
    public void OnTravelDetailsPostFail(String message, String dialogTitle, boolean isInternetError) {
        travelView.dismissProgressDialog();
        travelView.afterTravelPostFail(message,dialogTitle,isInternetError);

    }
}
