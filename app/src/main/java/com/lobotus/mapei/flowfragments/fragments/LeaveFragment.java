package com.lobotus.mapei.flowfragments.fragments;


import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.lobotus.mapei.R;
import com.lobotus.mapei.baseactivity.BaseActivity;
import com.lobotus.mapei.flowfragments.adapters.LeaveListAdapter;
import com.lobotus.mapei.flowfragments.model.LeaveDataModel;
import com.lobotus.mapei.flowfragments.mvp.presentor.LeaveFragPresentor;
import com.lobotus.mapei.flowfragments.mvp.presentorimp.LeaveFragPresentorImp;
import com.lobotus.mapei.flowfragments.mvp.views.LeaveFragmentView;
import com.lobotus.mapei.utils.CommonFunc;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class LeaveFragment extends Fragment implements LeaveFragmentView,SwipeRefreshLayout.OnRefreshListener,View.OnClickListener {

    private View viewFragment=null;
    private Dialog dialog=null;
    //// mvp
    private LeaveFragPresentor leaveFragPresentor=null;
///////////////////////////////////////////////////////////////// life cycle stuff
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        viewFragment= inflater.inflate(R.layout.fragment_leave, container, false);
        ((SwipeRefreshLayout)viewFragment.findViewById(R.id.fragment_leave_swipe_refresh_id)).setOnRefreshListener(this);
        viewFragment.findViewById(R.id.fragment_leave_add_leave_floatbutton).setOnClickListener(this);
        leaveFragPresentor=new LeaveFragPresentorImp(this);
        leaveFragPresentor.reqToLodeTheLeaveList(getActivity());
        /////////////////////////////////////////
        ((RecyclerView)viewFragment.findViewById(R.id.fragment_leave_recycleview_id)).addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                switch (newState)
                {
                    case  RecyclerView.SCROLL_STATE_IDLE:
                        ((FloatingActionButton)viewFragment.findViewById(R.id.fragment_leave_add_leave_floatbutton)).show();
                        break;
                    case RecyclerView.SCROLL_STATE_DRAGGING:
                        ((FloatingActionButton)viewFragment.findViewById(R.id.fragment_leave_add_leave_floatbutton)).hide();
                }
            }
        });
        return viewFragment;
    }
///////////////////////////////////////////////////////////////////////////////
    @Override
    public void onRefresh() {
        leaveFragPresentor.reqToLodeTheLeaveList(getActivity());
    }
    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.fragment_leave_add_leave_floatbutton:
                ((BaseActivity)getActivity()).attachFragment(new LeaveApplyFragment(),true,"Apply for leave",null);
        }
    }
/////////////////////////////////////////////////////////////////////////////// mvp stuff
@SuppressWarnings("ConstantConditions")
@Override
public void showProgressDialog() {
    dialog=new Dialog(getActivity());
    dialog.setCancelable(false);
    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
    dialog.setContentView(R.layout.loaging_layout);
    dialog.show();
}

    @Override
    public void dismissProgressDialog() {

        if (getActivity()!=null)
        {
            if ( ((SwipeRefreshLayout)viewFragment.findViewById(R.id.fragment_leave_swipe_refresh_id))!=null)
            {
                if ( ((SwipeRefreshLayout)viewFragment.findViewById(R.id.fragment_leave_swipe_refresh_id)).isRefreshing())
                    ((SwipeRefreshLayout)viewFragment.findViewById(R.id.fragment_leave_swipe_refresh_id)).setRefreshing(false);
            }

            if (dialog!=null)
            {
                if (dialog.isShowing())
                {
                    dialog.dismiss();
                }
                dialog=null;
            }
        }

    }

    @Override
    public void afterLeavesListDownlodeSuccess(ArrayList<LeaveDataModel> leaveDataModels) {
        ((RecyclerView)viewFragment.findViewById(R.id.fragment_leave_recycleview_id))
                .setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false));
        ((RecyclerView)viewFragment.findViewById(R.id.fragment_leave_recycleview_id)).setAdapter(new LeaveListAdapter(getActivity(),leaveDataModels,this));

    }

    @Override
    public void afterLeavesListDownlodeFail(String message, String dialogTitle, boolean isInternetError) {
        CommonFunc.commonDialog(getActivity(),dialogTitle,message,isInternetError);
    }

    @Override
    public void afterLeaveCancelSuccess(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
        leaveFragPresentor.reqToLodeTheLeaveList(getActivity());
    }

    @Override
    public void afterLeaveCancelFail(String message, String dialogTitle, boolean isInternetError) {
        CommonFunc.commonDialog(getActivity(),dialogTitle,message,isInternetError);
    }


    //////////////////////////////////////////////////////////////////////////////////////////// on cancel button click
    public void OnCancelButtonClick(String leaveId)
    {
        leaveFragPresentor.reqToCancelTheLeave(leaveId,getActivity());
    }


}
