package com.lobotus.mapei.flowfragments.mvp.intrector;

import android.content.Context;

import com.lobotus.mapei.flowfragments.model.ComplaintDataTypeModel;
import com.lobotus.mapei.flowfragments.model.ProductsDataModel;
import com.lobotus.mapei.flowfragments.model.ProjectsDataModel;
import com.lobotus.mapei.flowfragments.model.UsersDataModel;

import java.util.ArrayList;

/**
 * Created by User on 20-03-2018.
 */

public interface AssignMeetingIntractor {

    interface OnUserListDownlodeLisner {
        void OnUserListDownlodeSuccess(ArrayList<UsersDataModel> usersDataModels);

        void OnUserListDownlodeFail(String message, String dialogTitle, boolean isInternetError);
    }

    void reqToFetchUserListFromServer(Context context,
                                      OnUserListDownlodeLisner onUserListDownlodeLisner, String TypeID, String UserId);

    interface OnMeetingAssignLisner {
        void OnMeetingAssignSuccess(String message);

        void OnMeetingAssignFail(String message, String dialogTitle, boolean isInternetError);
    }

    /////////////////////////// downlkode the complaints
    interface  OnClomplaintsDataDownlodeLisner
    {
        void OnComplaintsDataDownlodeSuccess(ArrayList<ComplaintDataTypeModel> complaintDataTypeModels);
        void OnComplaintsDataDownlodeFail(String message,String dialogTitle,boolean isInternetError);
    }

    interface OnMeetingPostDataDownlodeLisner
    {
        void OnMeetingPostSuccess(String message,String meetingId);
        void OnMeetingPostFail(String message,String dialogTitle,boolean isInternetError);
    }

    void getTheComplaintsDataFromTheServer(Context context,AssignMeetingIntractor.OnClomplaintsDataDownlodeLisner onClomplaintsDataDownlodeLisner);

    void reqToAssignMeetingTOUser(String UserId, String MeetingTypeID, String ProjectID, String Status,
                                  String CustomerID, String OnDate, String OnTime, Context context,
                                  OnMeetingAssignLisner onMeetingAssignLisner);

    void reqToPostTheMeetingsDataToServer(String userId,Context context,
                                          String projectName,
                                          ArrayList<ProjectsDataModel> projectsDataModels,
                                          String customerName,
                                          String customerType,
                                          int meetingTypeSpinnerSelectedPos,
                                          int submeetingTypeSelectedSpinner,
                                          String valueOfFllowUpOrder,
                                          String orderLostOfFollowupOrder,
                                          int complaintsTypesSpinnerSelectedPos,
                                          int arFollowTypesSpinnerSelectedPos,
                                          String arFollowPaymentNewDate,
                                          String arFollowPaymentProjectissue,
                                          String arFollowPaymentProjectDeley,
                                          String arFollowCformFollowupCollected,
                                          String arFollowCformFollowupNewDate,
                                          String arFollowCformFollowupNotTracable,
                                          String arFollowCformReconcilationMissingInvoice,
                                          String arFollowCformReconcilationShortPayment,
                                          String arFollowCformReconcilationStatement,
                                          ArrayList<ProductsDataModel> productsDataModels_Submitance,
                                          ArrayList<ProductsDataModel> productsDataModels_Presentation,
                                          ArrayList<ProductsDataModel> productsDataModels_Followup,
                                          String notes,
                                          AssignMeetingIntractor.OnMeetingPostDataDownlodeLisner onMeetingPostDataDownlodeLisner,
                                          String location, String meetingSubTypeValue,String customerId,String meetingLocation);

}
