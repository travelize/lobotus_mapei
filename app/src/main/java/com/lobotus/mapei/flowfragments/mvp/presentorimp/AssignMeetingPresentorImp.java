package com.lobotus.mapei.flowfragments.mvp.presentorimp;

import android.content.Context;

import com.lobotus.mapei.flowfragments.model.ComplaintDataTypeModel;
import com.lobotus.mapei.flowfragments.model.CustomerDataModel;
import com.lobotus.mapei.flowfragments.model.ProductsDataModel;
import com.lobotus.mapei.flowfragments.model.ProjectsDataModel;
import com.lobotus.mapei.flowfragments.model.UsersDataModel;
import com.lobotus.mapei.flowfragments.mvp.intrector.AddProjectsIntractor;
import com.lobotus.mapei.flowfragments.mvp.intrector.AssignMeetingIntractor;
import com.lobotus.mapei.flowfragments.mvp.intrector.MeetingInterator;
import com.lobotus.mapei.flowfragments.mvp.intrectorimp.AddProjectsIntractorImp;
import com.lobotus.mapei.flowfragments.mvp.intrectorimp.AssignMeetingIntractorImp;
import com.lobotus.mapei.flowfragments.mvp.intrectorimp.MeetingInteratorImp;
import com.lobotus.mapei.flowfragments.mvp.presentor.AssignMeetingPresentor;
import com.lobotus.mapei.flowfragments.mvp.presentor.MeetingPresentor;
import com.lobotus.mapei.flowfragments.mvp.views.AssignMeetingsView;
import com.lobotus.mapei.utils.NetworkUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 20-03-2018.
 */

public class AssignMeetingPresentorImp implements AssignMeetingPresentor, AssignMeetingIntractor.
        OnUserListDownlodeLisner, MeetingInterator.OnMeetingCustomerDataDownlodeLisner,
        MeetingInterator.OnProjectsListBasedOnCustomerNameLisner,
        AssignMeetingIntractor.OnClomplaintsDataDownlodeLisner,
        AssignMeetingIntractor.OnMeetingAssignLisner, AddProjectsIntractor.OnProductsDataDownlodeLisner, MeetingInterator.OnMeetingPostDataDownlodeLisner,
        AssignMeetingIntractor.OnMeetingPostDataDownlodeLisner {


    private AssignMeetingsView assignMeetingsView = null;
    private AssignMeetingIntractor assignMeetingIntractor = null;
    private MeetingInterator meetingInterator = null;
    private AddProjectsIntractor addProjectsIntractor = null;

    public AssignMeetingPresentorImp(AssignMeetingsView assignMeetingsView) {
        this.assignMeetingsView = assignMeetingsView;
        this.assignMeetingIntractor = new AssignMeetingIntractorImp();
        this.meetingInterator = new MeetingInteratorImp();
        this.addProjectsIntractor = new AddProjectsIntractorImp();
    }

    @Override
    public void reqToGetTheComplaintsDataFromTheServer(Context context) {
        if (NetworkUtils.checkInternetAndOpenDialog(context)) {
            assignMeetingsView.showProgressDialog();
            assignMeetingIntractor.getTheComplaintsDataFromTheServer(context, this);
        }

    }

    @Override
    public void reqToDownlodeUserData(Context context, String TypeID, String UserId) {
        assignMeetingsView.showProgressDialog();
        assignMeetingIntractor.reqToFetchUserListFromServer(context, this, TypeID, UserId);

    }

    @Override
    public void OnUserListDownlodeSuccess(ArrayList<UsersDataModel> usersDataModels) {
        assignMeetingsView.dismissProgressDialog();
        assignMeetingsView.afterUserListDownlodeSuccess(usersDataModels);

    }

    @Override
    public void OnUserListDownlodeFail(String message, String dialogTitle, boolean isInternetError) {
        assignMeetingsView.dismissProgressDialog();
        assignMeetingsView.afterUserListDownlodeFail(message, dialogTitle, isInternetError);

    }

    //////////////////////////////////////////////////////////////////////////////////
    @Override
    public void reqToGetTheCustomerDataFromPresentor(Context context) {
        assignMeetingsView.showProgressDialog();
        meetingInterator.reqToPostCustomerDataReqToServer(context, this);

    }


    @Override
    public void OnMeetingCustomerDataDownlodeSuccess(ArrayList<CustomerDataModel> customerDataModels) {
        assignMeetingsView.dismissProgressDialog();
        assignMeetingsView.afterCustomerNameDownlodeSuccess(customerDataModels);

    }

    @Override
    public void OnMeetingCustomerDataDownlodeFail(String message, String dialogTitle, boolean isInternetError) {
        assignMeetingsView.dismissProgressDialog();
        assignMeetingsView.afterCistomerNameDownlodeFail(message, dialogTitle, isInternetError);
    }

    @Override
    public void reqToGetTheProjectNameBasedOnCustomerName(Context context, String customerId) {
        assignMeetingsView.showProgressDialog();
        meetingInterator.reqToGetTheProjectsListBasedOnCustomerFromIntractor(context, customerId, this);

    }


    @Override
    public void OnProjectsListBasedOnCustomerNameDownlodeSuccess(ArrayList<ProjectsDataModel> projectsDataModels) {
        assignMeetingsView.dismissProgressDialog();
        assignMeetingsView.afterProjectsListDownlodeSuccess(projectsDataModels);
    }

    @Override
    public void OnProjectsListBasedOnCustomerNameDownlodeFail(String message, String dialogTitle, boolean isInternetError) {
        assignMeetingsView.dismissProgressDialog();
        assignMeetingsView.afterProjectsListDownlodeFail(message, dialogTitle, isInternetError);
    }


 /*   @Override
    public void reqToAssignMeetingToUser(String UserId, String MeetingTypeID, String ProjectID, String Status,
                                         String CustomerID, String OnDate, String OnTime, Context context) {
        assignMeetingsView.showProgressDialog();
        assignMeetingIntractor.reqToAssignMeetingTOUser(UserId,MeetingTypeID,ProjectID,Status,
                CustomerID,OnDate,OnTime,context,this);
    }*/

    @Override
    public void reqToGetTheProductsDataFromPresentor(Context context) {
        if (NetworkUtils.checkInternetAndOpenDialog(context)) {
            assignMeetingsView.showProgressDialog();
            addProjectsIntractor.reqToPostProductsDataReqToServer(this, context);
        }

    }

    @Override
    public void OnMeetingAssignSuccess(String message) {
        assignMeetingsView.dismissProgressDialog();
        assignMeetingsView.afterAssignMeetingSuccess(message);

    }

    @Override
    public void OnMeetingAssignFail(String message, String dialogTitle, boolean isInternetError) {
        assignMeetingsView.dismissProgressDialog();
        assignMeetingsView.afterAssignMeetingFail(message, dialogTitle, isInternetError);
    }


    @Override
    public void OnProductsDataDownlodeSuccess(List<ProductsDataModel> productsDataModels) {
        assignMeetingsView.dismissProgressDialog();
        assignMeetingsView.afterProductsModelDownlodeSuccess(productsDataModels);
    }

    @Override
    public void OnProductsDataDownlodeFail(String message, String dialogTitle, boolean isInternetError) {
        assignMeetingsView.dismissProgressDialog();
        assignMeetingsView.afterProductsModelDoenlodeFail(message, dialogTitle, isInternetError);
    }


    @Override
    public void reqToPostTheMeetingToTheServer(String userId, Context context, String projectName,
                                               ArrayList<ProjectsDataModel> projectsDataModels,
                                               String customerName, String customerType,
                                               int meetingTypeSpinnerSelectedPos,
                                               int submeetingTypeSelectedSpinner, String valueOfFllowUpOrder,
                                               String orderLostOfFollowupOrder,
                                               int complaintsTypesSpinnerSelectedPos,
                                               int arFollowTypesSpinnerSelectedPos,
                                               String arFollowPaymentNewDate,
                                               String arFollowPaymentProjectissue,
                                               String arFollowPaymentProjectDeley,
                                               String arFollowCformFollowupCollected,
                                               String arFollowCformFollowupNewDate,
                                               String arFollowCformFollowupNotTracable,
                                               String arFollowCformReconcilationMissingInvoice,
                                               String arFollowCformReconcilationShortPayment,
                                               String arFollowCformReconcilationStatement,
                                               String notes,
                                               ArrayList<ProductsDataModel> productsDataModels_Submitance,
                                               ArrayList<ProductsDataModel> productsDataModels_Presentation,
                                               ArrayList<ProductsDataModel> productsDataModels_Followup,
                                               String location,
                                               String meetingSubTypeValue, String customerId, String meetingLocation) {
        if (NetworkUtils.checkInternetAndOpenDialog(context)) {
            assignMeetingsView.showProgressDialog();
            assignMeetingIntractor.reqToPostTheMeetingsDataToServer(userId, context,
                    projectName,
                    projectsDataModels,
                    customerName,
                    customerType,
                    meetingTypeSpinnerSelectedPos,
                    submeetingTypeSelectedSpinner,
                    valueOfFllowUpOrder,
                    orderLostOfFollowupOrder,
                    complaintsTypesSpinnerSelectedPos,
                    arFollowTypesSpinnerSelectedPos,
                    arFollowPaymentNewDate,
                    arFollowPaymentProjectissue,
                    arFollowPaymentProjectDeley,
                    arFollowCformFollowupCollected,
                    arFollowCformFollowupNewDate,
                    arFollowCformFollowupNotTracable,
                    arFollowCformReconcilationMissingInvoice,
                    arFollowCformReconcilationShortPayment,
                    arFollowCformReconcilationStatement,
                    productsDataModels_Submitance,
                    productsDataModels_Presentation,
                    productsDataModels_Followup,
                    notes,
                    this,
                    location, meetingSubTypeValue, customerId, meetingLocation);

        }


    }


    @Override
    public void OnMeetingPostSuccess(String message, String meetingId) {
        assignMeetingsView.dismissProgressDialog();
        assignMeetingsView.afterMeetingPostSuccess(message, meetingId);
    }

    @Override
    public void OnMeetingPostFail(String message, String dialogTitle, boolean isInternetError) {
        assignMeetingsView.dismissProgressDialog();
        assignMeetingsView.afterMeetingPostFail(message, dialogTitle, isInternetError);
    }

    @Override
    public void OnComplaintsDataDownlodeSuccess(ArrayList<ComplaintDataTypeModel> complaintDataTypeModels) {
        assignMeetingsView.dismissProgressDialog();
        assignMeetingsView.afterComplaintsDownlodeSuccess(complaintDataTypeModels);

    }

    @Override
    public void OnComplaintsDataDownlodeFail(String message, String dialogTitle, boolean isInternetError) {
        assignMeetingsView.dismissProgressDialog();
        assignMeetingsView.afterComplaintsDownloldeFail(message, dialogTitle, isInternetError);

    }
}
