package com.lobotus.mapei.flowfragments.mvp.intrector;

import android.content.Context;

import com.lobotus.mapei.flowfragments.model.ComplaintDataTypeModel;
import com.lobotus.mapei.flowfragments.model.CustomerDataModel;
import com.lobotus.mapei.flowfragments.model.CustomerModel;
import com.lobotus.mapei.flowfragments.model.ProductsDataModel;
import com.lobotus.mapei.flowfragments.model.ProjectsDataModel;

import java.util.ArrayList;

/**
 * Created by user1 on 04-10-2017.
 */

public interface MeetingInterator {
    //////////////////////// customer data
    interface OnMeetingCustomerDataDownlodeLisner
    {
        void OnMeetingCustomerDataDownlodeSuccess(ArrayList<CustomerDataModel> customerDataModels);
        void OnMeetingCustomerDataDownlodeFail(String message,String dialogTitle,boolean isInternetError);
    }
    void reqToPostCustomerDataReqToServer(Context context,OnMeetingCustomerDataDownlodeLisner onMeetingCustomerDataDownlodeLisner);
    //////////////////  get customer name and the customer type
    interface OnCustomerNameAndTypeLisner
    {
        void OnCustomerNameAndTypeGetLisnerSuccess(String customerName,String customerType);
        void OnCustomerNameAndTypeGetLisnerFail(String message,String dialogTitle,boolean isInternetError);
    }
    void getCustomerNamaAndTypeFromTheChoosenProject(ArrayList<ProjectsDataModel> projectsDataModels,String autocompletetext,
                                                     Context context,OnCustomerNameAndTypeLisner onCustomerNameAndTypeLisner);

    /////////////////////////// downlkode the complaints
    interface  OnClomplaintsDataDownlodeLisner
    {
        void OnComplaintsDataDownlodeSuccess(ArrayList<ComplaintDataTypeModel> complaintDataTypeModels);
        void OnComplaintsDataDownlodeFail(String message,String dialogTitle,boolean isInternetError);
    }
    void getTheComplaintsDataFromTheServer(Context context,OnClomplaintsDataDownlodeLisner onClomplaintsDataDownlodeLisner);

    //////////////////////////////////// post the meeting to server
    interface OnMeetingPostDataDownlodeLisner
    {
        void OnMeetingPostSuccess(String message,String meetingId);
        void OnMeetingPostFail(String message,String dialogTitle,boolean isInternetError);
    }
    void reqToPostTheMeetingsDataToServer(Context context,
                                          String projectName,
                                          ArrayList<ProjectsDataModel> projectsDataModels,
                                          String customerName,
                                          String customerType,
                                          int meetingTypeSpinnerSelectedPos,
                                          int submeetingTypeSelectedSpinner,
                                          String valueOfFllowUpOrder,
                                          String orderLostOfFollowupOrder,
                                          int complaintsTypesSpinnerSelectedPos,
                                          int arFollowTypesSpinnerSelectedPos,
                                          String arFollowPaymentNewDate,
                                          String arFollowPaymentProjectissue,
                                          String arFollowPaymentProjectDeley,
                                          String arFollowCformFollowupCollected,
                                          String arFollowCformFollowupNewDate,
                                          String arFollowCformFollowupNotTracable,
                                          String arFollowCformReconcilationMissingInvoice,
                                          String arFollowCformReconcilationShortPayment,
                                          String arFollowCformReconcilationStatement,
                                          ArrayList<ProductsDataModel> productsDataModels_Submitance,
                                          ArrayList<ProductsDataModel> productsDataModels_Presentation,
                                          ArrayList<ProductsDataModel> productsDataModels_Followup,
                                          String notes,
                                          OnMeetingPostDataDownlodeLisner onMeetingPostDataDownlodeLisner,
                                          String location, String meetingSubTypeValue,String customerId);
    ///////////////////////////////////////////////// get the list of  projects from  customer name

    interface OnProjectsListBasedOnCustomerNameLisner
    {
        void OnProjectsListBasedOnCustomerNameDownlodeSuccess(ArrayList<ProjectsDataModel> projectsDataModels);
        void OnProjectsListBasedOnCustomerNameDownlodeFail(String message,String dialogTitle,boolean isInternetError);
    }
    void reqToGetTheProjectsListBasedOnCustomerFromIntractor(Context context,String custId,
                                                             OnProjectsListBasedOnCustomerNameLisner onProjectsListBasedOnCustomerNameLisner);



}
