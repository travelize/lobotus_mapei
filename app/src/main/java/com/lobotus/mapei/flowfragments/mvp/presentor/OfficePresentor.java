package com.lobotus.mapei.flowfragments.mvp.presentor;

import android.content.Context;

/**
 * Created by user1 on 12-10-2017.
 */

public interface OfficePresentor {
    //// post the purpose of the office vist
    void  reqToPostThePurposeOfOfficeVisit(Context context,String purpose,String notes);
    /// post the check in
    void reqToPostTheOfficeCheckIn(Context context,String officeAddress);
    void reqToPostTheOfficeCheckOut(Context context);
}
