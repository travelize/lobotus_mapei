package com.lobotus.mapei.flowfragments.fragments;


import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatAutoCompleteTextView;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.gson.Gson;
import com.lobotus.mapei.R;
import com.lobotus.mapei.baseactivity.BaseActivity;
import com.lobotus.mapei.customwidgits.LabelledSpinner;
import com.lobotus.mapei.flowfragments.model.ComplaintDataTypeModel;
import com.lobotus.mapei.flowfragments.model.CustomerDataModel;
import com.lobotus.mapei.flowfragments.model.ProductsDataModel;
import com.lobotus.mapei.flowfragments.model.ProjectsDataModel;
import com.lobotus.mapei.flowfragments.model.UsersDataModel;
import com.lobotus.mapei.flowfragments.mvp.presentor.AssignMeetingPresentor;
import com.lobotus.mapei.flowfragments.mvp.presentorimp.AssignMeetingPresentorImp;
import com.lobotus.mapei.flowfragments.mvp.views.AssignMeetingsView;
import com.lobotus.mapei.flowfragments.onactivityforresult.CustomerActForResult;
import com.lobotus.mapei.flowfragments.onactivityforresult.MeetingProductOnActiForResult;
import com.lobotus.mapei.flowfragments.onactivityforresult.UserListActiForRes;
import com.lobotus.mapei.utils.CommonFunc;
import com.lobotus.mapei.utils.ConstantsUtils;
import com.lobotus.mapei.utils.NetworkUtils;
import com.lobotus.mapei.utils.PreferenceUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class AssignMeetingFrag extends Fragment implements AssignMeetingsView,
        LabelledSpinner.OnItemChosenListener, View.OnClickListener, View.OnFocusChangeListener {

    // req code to get thecustomer seltion done. . .
    private final int REQ_CODE_TO_GET_THE_CUSTOMR_DATA = 11;
    private final int REQ_CODE_TO_GET_THE_USER_DATA = 12;
    // TEMP CUSTOMER ID. .
    private String TEMP_CUST_ID = null;
    private String TEMP_USER_ID = null;
    private View viewFragment = null;
    private Dialog dialog = null;
    ///
    private AssignMeetingPresentor assignMeetingPresentor = null;
    /////
    private ArrayList<ProjectsDataModel> projectsDataModels = new ArrayList<>();
    /////////////////////
    private boolean alreadyOnCreateExecuted = false;
    private int projuctSpinnerSelectedValueVefireLeavingThisFragmentToHistrayFrag = 0;

    private ArrayList<ComplaintDataTypeModel> complaintDataTypeModels = new ArrayList<>();

    public static ArrayList<ProductsDataModel> productsDataModels_Submitance = new ArrayList<>();
    public static ArrayList<ProductsDataModel> productsDataModels_Presentation = new ArrayList<>();
    public static ArrayList<ProductsDataModel> productsDataModels_Follow_up = new ArrayList<>();

    public static int REQ_CODE_TO_GET_THE_PRODUCTS_SUBMITANCE_ASSIGN = 18;
    public static int REQ_CODE_TO_GET_THE_PRODUCTS_PRESENTATION_ASSIGN = 19;
    public static int REQ_CODE_TO_GET_THE_PRODUCTS_FOLLOW_UP_ASSIGN = 110;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        switch (v.getId()) {
            case R.id.payment_followup_types_new_date_edittext_id:
                myDatePicker(true);
                break;
            case R.id.c_form_flowup_stub_new_date_edittext_id:
                myDatePicker(false);
        }
    }


    private class EnquiryHolder extends RecyclerView.ViewHolder {
        AppCompatImageView imageViewDelete = null;
        AppCompatTextView textViewProductName = null, textViewAmt = null;

        EnquiryHolder(View itemView) {
            super(itemView);
            this.imageViewDelete = itemView.findViewById(R.id.add_enquiry_layout_delete_imageview);
            this.textViewProductName = itemView.findViewById(R.id.add_enquiry_layout_product_name_id);
            this.textViewAmt = itemView.findViewById(R.id.add_enquiry_layout_product_amt_id);
        }
    }


    public class AddProductsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        private Context context = null;
        private ArrayList<ProductsDataModel> productsDataModelsInAdapter = null;

        public AddProductsAdapter(Context context, ArrayList<ProductsDataModel> productsDataModelsInAdapter) {
            this.context = context;
            this.productsDataModelsInAdapter = productsDataModelsInAdapter;
        }


        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(context).inflate(R.layout.add_products_row_layout, parent, false);
            return new AssignMeetingFrag.EnquiryHolder(view);


        }

        @Override
        public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
            AssignMeetingFrag.EnquiryHolder enquiryHolder = (AssignMeetingFrag.EnquiryHolder) holder;
            enquiryHolder.imageViewDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    double price = 0.0;
                    switch (((LabelledSpinner) viewFragment.findViewById(R.id.meeting_stub_meeting_sub_type_id))
                            .getSpinner().getSelectedItemPosition()) {
                        case 1: // submitance
                            for (int i = 0; i < productsDataModels_Submitance.size(); i++) {
                                if (productsDataModelsInAdapter.get(holder.getAdapterPosition()).getProductID().equals(productsDataModels_Submitance
                                        .get(i).getProductID())) {
                                    productsDataModels_Submitance.get(i).setChecked(false);
                                    productsDataModels_Submitance.get(i).setQty("0");
                                    productsDataModels_Submitance.get(i).setPrice("");
                                }
                            }
                            //
                            for (int i = 0; i < productsDataModels_Submitance.size(); i++) {
                                if (productsDataModels_Submitance.get(i).isChecked() && !productsDataModels_Submitance.get(i).getQty().isEmpty() &&
                                        !productsDataModels_Submitance.get(i).getPrice().isEmpty()) {
                                    price = price + (Double.valueOf(productsDataModels_Submitance.get(i).getPrice())) *
                                            (Double.valueOf(productsDataModels_Submitance.get(i).getQty()) > 0 ?
                                                    Double.valueOf(productsDataModels_Submitance.get(i).getQty()) : 1);
                                }
                            }
                            ((AppCompatEditText) viewFragment.findViewById(R.id.submitance_stub__orders_value_edittext_id))
                                    .setText("" + price);
                            break;
                        case 2: // presentation
                            for (int i = 0; i < productsDataModels_Presentation.size(); i++) {
                                if (productsDataModelsInAdapter.get(holder.getAdapterPosition()).getProductID().equals(productsDataModels_Presentation
                                        .get(i).getProductID())) {
                                    productsDataModels_Presentation.get(i).setChecked(false);
                                    productsDataModels_Presentation.get(i).setQty("0");
                                    productsDataModels_Presentation.get(i).setPrice("");
                                }
                            }
                            //
                            for (int i = 0; i < productsDataModels_Presentation.size(); i++) {
                                if (productsDataModels_Presentation.get(i).isChecked() && !productsDataModels_Presentation.get(i).getQty().isEmpty() &&
                                        !productsDataModels_Presentation.get(i).getPrice().isEmpty()) {
                                    price = price + (Double.valueOf(productsDataModels_Presentation.get(i).getPrice())) *
                                            (Double.valueOf(productsDataModels_Presentation.get(i).getQty()) > 0 ?
                                                    Double.valueOf(productsDataModels_Presentation.get(i).getQty()) : 1);
                                }
                            }
                            ((AppCompatEditText) viewFragment.findViewById(R.id.presentation_stub__orders_value_edittext_id))
                                    .setText("" + price);
                            break;
                        case 3: // follow up
                            for (int i = 0; i < productsDataModels_Follow_up.size(); i++) {
                                if (productsDataModelsInAdapter.get(holder.getAdapterPosition()).getProductID().equals(productsDataModels_Follow_up
                                        .get(i).getProductID())) {
                                    productsDataModels_Follow_up.get(i).setChecked(false);
                                    productsDataModels_Follow_up.get(i).setQty("0");
                                    productsDataModels_Follow_up.get(i).setPrice("");
                                }
                            }
                            //
                            for (int i = 0; i < productsDataModels_Follow_up.size(); i++) {
                                if (productsDataModels_Follow_up.get(i).isChecked() && !productsDataModels_Follow_up.get(i).getQty().isEmpty() &&
                                        !productsDataModels_Follow_up.get(i).getPrice().isEmpty()) {
                                    price = price + (Double.valueOf(productsDataModels_Follow_up.get(i).getPrice())) *
                                            (Double.valueOf(productsDataModels_Follow_up.get(i).getQty()) > 0 ?
                                                    Double.valueOf(productsDataModels_Presentation.get(i).getQty()) : 1);
                                }
                            }
                            ((AppCompatEditText) viewFragment.findViewById(R.id.follow_up_for_orders_value_edittext_id))
                                    .setText("" + price);
                    }


                    //
                    productsDataModelsInAdapter.remove(holder.getAdapterPosition());
                    notifyDataSetChanged();
                }
            });
            enquiryHolder.textViewProductName.setText(productsDataModelsInAdapter.get(holder.getAdapterPosition()).getProductName());
            enquiryHolder.textViewAmt.setText(
                    "" + (Double.parseDouble(productsDataModelsInAdapter.get(holder.getAdapterPosition()).getPrice())) *
                            (Double.parseDouble(productsDataModelsInAdapter.get(holder.getAdapterPosition()).getQty()) > 0 ?
                                    Double.parseDouble(productsDataModelsInAdapter.get(holder.getAdapterPosition()).getQty()) : 1));

            Log.e("Quantity Value=", productsDataModelsInAdapter.get(holder.getAdapterPosition()).getQty());

        }

        @Override
        public int getItemCount() {
            return productsDataModelsInAdapter.size();
        }

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        viewFragment = inflater.inflate(R.layout.fragment_assign_meeting, container, false);
        viewFragment.findViewById(R.id.fragment_assign_to_add_customer_id_imageview).setOnClickListener(this);
        viewFragment.findViewById(R.id.fragment_assign_to_add_projects_imageview_button).setOnClickListener(this);
        viewFragment.findViewById(R.id.fragment_assign_to_view_project_history).setOnClickListener(this);
        viewFragment.findViewById(R.id.fragment_assign_to_submit_button_id).setOnClickListener(this);
        viewFragment.findViewById(R.id.fragment_assign_to_followup_stub_date_id).setOnClickListener(this);
        viewFragment.findViewById(R.id.fragment_assign_to_followup_stub_time_id).setOnClickListener(this);
        viewFragment.findViewById(R.id.fragment_assign_to_assign_autocomplete_textview_id).setOnClickListener(this);
        viewFragment.findViewById(R.id.fragment_assign_to_customer_name_edittext_id).setOnClickListener(this);
        viewFragment.findViewById(R.id.fragment_assign_meeting_location_edittext_id).setOnClickListener(this);

        //meetingPresentor = new MeetingPresenterImp(this);

        if (alreadyOnCreateExecuted) {
            String[] strings = new String[projectsDataModels.size()];
            for (int i = 0; i < strings.length; i++) {
                strings[i] = projectsDataModels.get(i).getProjectName();
            }
            ((LabelledSpinner) viewFragment.findViewById(R.id.fragment_assign_to_project_list_dropdown))
                    .setItemsArray(strings);
            ((LabelledSpinner) viewFragment.findViewById(R.id.fragment_assign_to_project_list_dropdown))
                    .setDefaultErrorEnabled(false);
            ((LabelledSpinner) viewFragment.findViewById(R.id.fragment_assign_to_project_list_dropdown))
                    .setDefaultErrorText("Please select project name");
            ((LabelledSpinner) viewFragment.findViewById(R.id.fragment_assign_to_project_list_dropdown)).
                    setSelection(projuctSpinnerSelectedValueVefireLeavingThisFragmentToHistrayFrag);
            ((LabelledSpinner) viewFragment.findViewById(R.id.fragment_assign_to_project_list_dropdown)).setOnItemChosenListener(this);
            if (projuctSpinnerSelectedValueVefireLeavingThisFragmentToHistrayFrag == 0)
                viewFragment.findViewById(R.id.fragment_assign_to_view_project_history).setVisibility(View.GONE);
            else
                viewFragment.findViewById(R.id.fragment_assign_to_view_project_history).setVisibility(View.VISIBLE);

        } else {
            viewFragment.findViewById(R.id.fragment_assign_to_view_project_history).setVisibility(View.GONE);
            setInitialProjectSelectSpinner();
        }
        ///
        setMeetingTypeSpinner();
        assignMeetingPresentor = new AssignMeetingPresentorImp(this);
        assignMeetingPresentor.reqToDownlodeUserData(getActivity(), "2", PreferenceUtils.getUserIdFromPreference(getActivity()));
        alreadyOnCreateExecuted = true;
        return viewFragment;
    }

    private void setMeetingTypeSpinner() {
        ((LabelledSpinner) viewFragment.findViewById(R.id.fragment_assign_to_meeting_type_spinner))
                .setItemsArray(R.array.meeting_type_array);
        ((LabelledSpinner) viewFragment.findViewById(R.id.fragment_assign_to_meeting_type_spinner))
                .setDefaultErrorEnabled(false);
        ((LabelledSpinner) viewFragment.findViewById(R.id.fragment_assign_to_meeting_type_spinner))
                .setDefaultErrorText("Please select meeting type");
        ((LabelledSpinner) viewFragment.findViewById(R.id.fragment_assign_to_meeting_type_spinner))
                .setOnItemChosenListener(this);
    }
    //////////////////////////////////////////////////////////////// mvp stuff. . . . .

    @Override
    public void showProgressDialog() {
        dialog = new Dialog(getActivity());
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.loaging_layout);
        dialog.show();
    }

    @Override
    public void dismissProgressDialog() {
        if (getActivity() != null)
            if (dialog != null) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                dialog = null;
            }
    }

    @Override
    public void afterUserListDownlodeSuccess(ArrayList<UsersDataModel> usersDataModels) {
       /* System.out.println("ccccccccccccc-afterUserListDownlodeSuccess---------------"+new Gson().toJson(usersDataModels));
        usersDataModelsGlobel.clear();
        usersDataModelsGlobel.addAll(usersDataModels);
        String[] strings=new String[usersDataModelsGlobel.size()];
        for (int i=0;i<strings.length;i++)
        {
            strings[i]=usersDataModelsGlobel.get(i).getFullName();
        }
        ArrayAdapter<String> arrayAdapterUser = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_list_item_1, strings);
        ((AppCompatAutoCompleteTextView)viewFragment.findViewById(R.id.fragment_assign_to_assign_autocomplete_textview_id))
                .setAdapter(arrayAdapterUser);
        assignMeetingPresentor.reqToGetTheCustomerDataFromPresentor(getActivity());*/
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        productsDataModels_Submitance.clear();
        productsDataModels_Presentation.clear();
        productsDataModels_Follow_up.clear();
    }

    @Override
    public void afterUserListDownlodeFail(String message, String dialogTitle, boolean isInternetError) {
        CommonFunc.commonDialog(getActivity(), dialogTitle, message, isInternetError);
    }

    @Override
    public void afterCustomerNameDownlodeSuccess(ArrayList<CustomerDataModel> customerDataModels) {
     /*   System.out.println("ccccccccccccc-afterCustomerNameDownlodeSuccess---------------"+new Gson().toJson(customerDataModels));
        customerDataModelsGlobel.clear();
        customerDataModelsGlobel.addAll(customerDataModels);
        String[] strings=new String[customerDataModelsGlobel.size()];
        for (int i=0;i<strings.length;i++)
        {
            strings[i]=customerDataModelsGlobel.get(i).getCustomerName();
        }
        ArrayAdapter<String> arrayAdapterUser = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_list_item_1, strings);
        ((AppCompatAutoCompleteTextView)viewFragment.findViewById(R.id.fragment_assign_to_customer_name_edittext_id))
                .setAdapter(arrayAdapterUser);
        ((AppCompatAutoCompleteTextView)viewFragment.findViewById(R.id.fragment_assign_to_customer_name_edittext_id))
                .setOnItemClickListener(this);*/

    }

    @Override
    public void afterCistomerNameDownlodeFail(String message, String dialogTitle, boolean isInternetError) {
        CommonFunc.commonDialog(getActivity(), dialogTitle, message, isInternetError);
    }

    @Override
    public void afterProjectsListDownlodeSuccess(ArrayList<ProjectsDataModel> projectsDataModels) {
        System.out.println("ccccccccccccc-afterProjectsListDownlodeSuccess---------------" + new Gson().toJson(projectsDataModels));
        this.projectsDataModels.clear();
        this.projectsDataModels.add(new ProjectsDataModel("Please select project"));
        this.projectsDataModels.addAll(projectsDataModels);
        String[] strings = new String[this.projectsDataModels.size()];
        for (int i = 0; i < strings.length; i++) {
            strings[i] = this.projectsDataModels.get(i).getProjectName();
        }
        ((LabelledSpinner) viewFragment.findViewById(R.id.fragment_assign_to_project_list_dropdown))
                .setItemsArray(strings);
        ((LabelledSpinner) viewFragment.findViewById(R.id.fragment_assign_to_project_list_dropdown))
                .setDefaultErrorEnabled(false);
        ((LabelledSpinner) viewFragment.findViewById(R.id.fragment_assign_to_project_list_dropdown))
                .setDefaultErrorText("Please select project name");
        ((LabelledSpinner) viewFragment.findViewById(R.id.fragment_assign_to_project_list_dropdown)).setOnItemChosenListener(this);
    }

    @Override
    public void onItemChosen(View labelledSpinner, AdapterView<?> adapterView, View itemView, int position, long id) {
        if (position == 0)
            viewFragment.findViewById(R.id.fragment_assign_to_view_project_history).setVisibility(View.GONE);
        else
            viewFragment.findViewById(R.id.fragment_assign_to_view_project_history).setVisibility(View.VISIBLE);

        switch (labelledSpinner.getId()) {
            case R.id.fragment_assign_to_meeting_type_spinner:
                //  removeinflatedViewFromViewstubAndShowSelected(position);
                removeinflatedViewFromViewstubAndShowSelected(position);
                break;
            case R.id.spinner_ar_followup_stub_spinner_id:
//                removeinflatedViewARFollow(position);
                break;
            case R.id.fragment_meeting_project_list_dropdown:
                if (position == 0) {
                    viewFragment.findViewById(R.id.fragment_meeting_view_project_history).setVisibility(View.GONE);
                } else {
                    viewFragment.findViewById(R.id.fragment_meeting_view_project_history).setVisibility(View.VISIBLE);
                }
                break;
            case R.id.meeting_stub_meeting_sub_type_id:
//                removeAndInFlateMeetingSubtypes(position);
        }


    }

    private void removeinflatedViewFromViewstubAndShowSelected(int pos) {
        switch (pos) {
            case 0:
                // select
                //meetings
                if (((ViewStub) viewFragment.findViewById(R.id.fragment_assign_to_meeting_viewstub)) == null) {
                    viewFragment.findViewById(R.id.fragment_assign_to_meeting_viewstub_inflateId).setVisibility(View.GONE);
                }
                // complaints
                if (((ViewStub) viewFragment.findViewById(R.id.fragment_assign_to_meeting_complaints_viewstub)) == null) {
                    viewFragment.findViewById(R.id.fragment_assign_to_meeting_complaints_viewstub_inflateId).setVisibility(View.GONE);
                }
                // ar follow up
                if (((ViewStub) viewFragment.findViewById(R.id.fragment_assign_to_meeting_arfollowup_viewstub)) == null) {
                    viewFragment.findViewById(R.id.fragment_assign_to_meeting_arfollowup_viewstub_inflateId).setVisibility(View.GONE);
                }

                break;
            case 1:  //meeting
                /////////////////////
                // complaints
                if (((ViewStub) viewFragment.findViewById(R.id.fragment_assign_to_meeting_complaints_viewstub)) == null) {
                    viewFragment.findViewById(R.id.fragment_assign_to_meeting_complaints_viewstub_inflateId).setVisibility(View.GONE);
                }
                // ar follow up
                if (((ViewStub) viewFragment.findViewById(R.id.fragment_assign_to_meeting_arfollowup_viewstub)) == null) {
                    viewFragment.findViewById(R.id.fragment_assign_to_meeting_arfollowup_viewstub_inflateId).setVisibility(View.GONE);
                }
                ///////////////////////////
                // show meeting. . .
                if (((ViewStub) viewFragment.findViewById(R.id.fragment_assign_to_meeting_viewstub)) != null) {
                    ((ViewStub) viewFragment.findViewById(R.id.fragment_assign_to_meeting_viewstub))
                            .inflate();
                    /*viewFragment.findViewById(R.id.follow_up_for_orders__add_product_button_id).setOnClickListener(this);*/
/*
                    inflate the spinner
*/
                    setTheSubMeetingTypeSpinner();
                } else {
                    viewFragment.findViewById(R.id.fragment_assign_to_meeting_viewstub_inflateId).setVisibility(View.VISIBLE);
                }
                //////////////////////////////// load products
//                if (productsDataModels_Follow_up.isEmpty())
//                    meetingPresentor.reqToGetTheProductsDataFromPresentor(getActivity());
                break;
            case 2:
                //AR follow up
                // hide meeting
                if (((ViewStub) viewFragment.findViewById(R.id.fragment_assign_to_meeting_viewstub)) == null) {
                    viewFragment.findViewById(R.id.fragment_assign_to_meeting_viewstub_inflateId).setVisibility(View.GONE);
                }
                // complaints
                if (((ViewStub) viewFragment.findViewById(R.id.fragment_assign_to_meeting_complaints_viewstub)) == null) {
                    viewFragment.findViewById(R.id.fragment_assign_to_meeting_complaints_viewstub_inflateId).setVisibility(View.GONE);
                }
                ///////////////////////////////////
                // AR follow up
                if (((ViewStub) viewFragment.findViewById(R.id.fragment_assign_to_meeting_arfollowup_viewstub)) != null) {
                    ((ViewStub) viewFragment.findViewById(R.id.fragment_assign_to_meeting_arfollowup_viewstub))
                            .inflate();
                } else {
                    viewFragment.findViewById(R.id.fragment_assign_to_meeting_arfollowup_viewstub_inflateId).setVisibility(View.VISIBLE);
                }
                inflateArfollowUpTypesSpinner();
                break;
            case 3:
                // complaints
                //hide meeting. . .
                if (((ViewStub) viewFragment.findViewById(R.id.fragment_assign_to_meeting_viewstub)) == null) {
                    viewFragment.findViewById(R.id.fragment_assign_to_meeting_viewstub_inflateId).setVisibility(View.GONE);
                }
                // ar follow up
                if (((ViewStub) viewFragment.findViewById(R.id.fragment_assign_to_meeting_arfollowup_viewstub)) == null) {
                    viewFragment.findViewById(R.id.fragment_assign_to_meeting_arfollowup_viewstub_inflateId).setVisibility(View.GONE);
                }
                ///////////////////////////////////
                // complaints
                if (((ViewStub) viewFragment.findViewById(R.id.fragment_assign_to_meeting_complaints_viewstub)) != null) {
                    ((ViewStub) viewFragment.findViewById(R.id.fragment_assign_to_meeting_complaints_viewstub))
                            .inflate();
                } else {
                    viewFragment.findViewById(R.id.fragment_assign_to_meeting_complaints_viewstub_inflateId).setVisibility(View.VISIBLE);
                }
                if (complaintDataTypeModels.isEmpty()) {
                    assignMeetingPresentor.reqToGetTheComplaintsDataFromTheServer(getActivity());
                }

        }
    }

    private void removeinflatedViewARFollow(int pos) {
        switch (pos) {
            case 0: // select
                // payment followup
                if (((ViewStub) viewFragment.findViewById(R.id.ar_flowup_stub_payment_followup_viewstub)) == null) {
                    viewFragment.findViewById(R.id.ar_flowup_stub_payment_followup_viewstub_inflateId).setVisibility(View.GONE);
                }
                // c form follow up
                if (((ViewStub) viewFragment.findViewById(R.id.ar_flowup_stub_c_form_followup_viewstub)) == null) {
                    viewFragment.findViewById(R.id.ar_flowup_stub_c_form_followup_inflateId).setVisibility(View.GONE);
                }
                // reconcilation
                if (((ViewStub) viewFragment.findViewById(R.id.fragment_reconcilation_viewstub)) == null) {
                    viewFragment.findViewById(R.id.fragment_reconcilation_viewstub_inflateId).setVisibility(View.GONE);
                }

                break;
            case 1:  // payment followup
                /////////////////////
                // c form follow up
                if (((ViewStub) viewFragment.findViewById(R.id.ar_flowup_stub_c_form_followup_viewstub)) == null) {
                    viewFragment.findViewById(R.id.ar_flowup_stub_c_form_followup_inflateId).setVisibility(View.GONE);
                }
                // reconcilation
                if (((ViewStub) viewFragment.findViewById(R.id.fragment_reconcilation_viewstub)) == null) {
                    viewFragment.findViewById(R.id.fragment_reconcilation_viewstub_inflateId).setVisibility(View.GONE);
                }
                ///////////////////////////////////
                // followup for Order
                if (((ViewStub) viewFragment.findViewById(R.id.ar_flowup_stub_payment_followup_viewstub)) != null) {
                    ((ViewStub) viewFragment.findViewById(R.id.ar_flowup_stub_payment_followup_viewstub))
                            .inflate();
                } else {
                    viewFragment.findViewById(R.id.ar_flowup_stub_payment_followup_viewstub_inflateId).setVisibility(View.VISIBLE);
                }
                ((AppCompatEditText) viewFragment.findViewById(R.id.payment_followup_types_new_date_edittext_id)).setOnFocusChangeListener(this);
                ((AppCompatEditText) viewFragment.findViewById(R.id.payment_followup_types_new_date_edittext_id)).setOnClickListener(this);
                break;
            case 2: //  c form follow up
                // payment followup
                if (((ViewStub) viewFragment.findViewById(R.id.ar_flowup_stub_payment_followup_viewstub)) == null) {
                    viewFragment.findViewById(R.id.ar_flowup_stub_payment_followup_viewstub_inflateId).setVisibility(View.GONE);
                }
                // reconcilation
                if (((ViewStub) viewFragment.findViewById(R.id.fragment_reconcilation_viewstub)) == null) {
                    viewFragment.findViewById(R.id.fragment_reconcilation_viewstub_inflateId).setVisibility(View.GONE);
                }
                // c form follow up
                if (((ViewStub) viewFragment.findViewById(R.id.ar_flowup_stub_c_form_followup_viewstub)) != null) {
                    ((ViewStub) viewFragment.findViewById(R.id.ar_flowup_stub_c_form_followup_viewstub))
                            .inflate();
                } else {
                    viewFragment.findViewById(R.id.ar_flowup_stub_c_form_followup_inflateId).setVisibility(View.VISIBLE);
                }
                ((AppCompatEditText) viewFragment.findViewById(R.id.c_form_flowup_stub_new_date_edittext_id)).setOnFocusChangeListener(this);
                ((AppCompatEditText) viewFragment.findViewById(R.id.c_form_flowup_stub_new_date_edittext_id)).setOnClickListener(this);
                break;
            case 3: // reconcilation
                // payment followup
                if (((ViewStub) viewFragment.findViewById(R.id.ar_flowup_stub_payment_followup_viewstub)) == null) {
                    viewFragment.findViewById(R.id.ar_flowup_stub_payment_followup_viewstub_inflateId).setVisibility(View.GONE);
                }
                // c form follow up
                if (((ViewStub) viewFragment.findViewById(R.id.ar_flowup_stub_c_form_followup_viewstub)) == null) {
                    viewFragment.findViewById(R.id.ar_flowup_stub_c_form_followup_inflateId).setVisibility(View.GONE);
                }
                // reconcilation
                if (((ViewStub) viewFragment.findViewById(R.id.fragment_reconcilation_viewstub)) != null) {
                    ((ViewStub) viewFragment.findViewById(R.id.fragment_reconcilation_viewstub))
                            .inflate();
                } else {
                    viewFragment.findViewById(R.id.fragment_reconcilation_viewstub_inflateId).setVisibility(View.VISIBLE);
                }


        }
    }

    private void removeAndInFlateMeetingSubtypes(int position) {

        switch (position) {
            case 0: // remove all
                // select
                //submitance
                if (((ViewStub) viewFragment.findViewById(R.id.meeting_stub_submittance_viewstub)) == null) {
                    viewFragment.findViewById(R.id.meeting_stub_submittance_viewstub_inflateId).setVisibility(View.GONE);
                }
                // presentation
                if (((ViewStub) viewFragment.findViewById(R.id.meeting_stub_presentation_viewstub)) == null) {
                    viewFragment.findViewById(R.id.meeting_stub_presentation_viewstub_inflateId).setVisibility(View.GONE);
                }
                // follow up
                if (((ViewStub) viewFragment.findViewById(R.id.meeting_stub_followupForOrders_viewstub)) == null) {
                    viewFragment.findViewById(R.id.meeting_stub_followupForOrders_viewstub_inflateId).setVisibility(View.GONE);
                }
                break;
            case 1: // show submitance and hide others
                // hide
                // presentation
                if (((ViewStub) viewFragment.findViewById(R.id.meeting_stub_presentation_viewstub)) == null) {
                    viewFragment.findViewById(R.id.meeting_stub_presentation_viewstub_inflateId).setVisibility(View.GONE);
                }
                // follow up
                if (((ViewStub) viewFragment.findViewById(R.id.meeting_stub_followupForOrders_viewstub)) == null) {
                    viewFragment.findViewById(R.id.meeting_stub_followupForOrders_viewstub_inflateId).setVisibility(View.GONE);
                }
                // show submitance
                if (((ViewStub) viewFragment.findViewById(R.id.meeting_stub_submittance_viewstub)) != null) {
                    ((ViewStub) viewFragment.findViewById(R.id.meeting_stub_submittance_viewstub))
                            .inflate();
                    viewFragment.findViewById(R.id.submitance_stub__add_product_button_id)
                            .setOnClickListener(this);
                } else {
                    viewFragment.findViewById(R.id.meeting_stub_submittance_viewstub_inflateId).setVisibility(View.VISIBLE);
                }

                break;
            case 2: // show presentation and hide others
                // hide
                //submitance
                if (((ViewStub) viewFragment.findViewById(R.id.meeting_stub_submittance_viewstub)) == null) {
                    viewFragment.findViewById(R.id.meeting_stub_submittance_viewstub_inflateId).setVisibility(View.GONE);
                }
                // follow up
                if (((ViewStub) viewFragment.findViewById(R.id.meeting_stub_followupForOrders_viewstub)) == null) {
                    viewFragment.findViewById(R.id.meeting_stub_followupForOrders_viewstub_inflateId).setVisibility(View.GONE);
                }
                // show presentation
                if (((ViewStub) viewFragment.findViewById(R.id.meeting_stub_presentation_viewstub)) != null) {
                    ((ViewStub) viewFragment.findViewById(R.id.meeting_stub_presentation_viewstub))
                            .inflate();
                    viewFragment.findViewById(R.id.presentation_stub__add_product_button_id)
                            .setOnClickListener(this);
                } else {
                    viewFragment.findViewById(R.id.meeting_stub_presentation_viewstub_inflateId).setVisibility(View.VISIBLE);
                }


                break;
            case 3:// show Follow-up and hide others
                // hide
                //submitance
                if (((ViewStub) viewFragment.findViewById(R.id.meeting_stub_submittance_viewstub)) == null) {
                    viewFragment.findViewById(R.id.meeting_stub_submittance_viewstub_inflateId).setVisibility(View.GONE);
                }
                // presentation
                if (((ViewStub) viewFragment.findViewById(R.id.meeting_stub_presentation_viewstub)) == null) {
                    viewFragment.findViewById(R.id.meeting_stub_presentation_viewstub_inflateId).setVisibility(View.GONE);
                }
                // show follow up
                if (((ViewStub) viewFragment.findViewById(R.id.meeting_stub_followupForOrders_viewstub)) != null) {
                    ((ViewStub) viewFragment.findViewById(R.id.meeting_stub_followupForOrders_viewstub))
                            .inflate();
                    viewFragment.findViewById(R.id.follow_up_for_orders__add_product_button_id)
                            .setOnClickListener(this);
                } else {
                    viewFragment.findViewById(R.id.meeting_stub_followupForOrders_viewstub_inflateId).setVisibility(View.VISIBLE);
                }

        }
    }


    private void setTheSubMeetingTypeSpinner() {
        ((LabelledSpinner) viewFragment.findViewById(R.id.meeting_stub_meeting_sub_type_id))
                .setItemsArray(R.array.meeting_sub_type_array);
        ((LabelledSpinner) viewFragment.findViewById(R.id.meeting_stub_meeting_sub_type_id))
                .setDefaultErrorEnabled(false);
        ((LabelledSpinner) viewFragment.findViewById(R.id.meeting_stub_meeting_sub_type_id))
                .setDefaultErrorText("Please select sub - meeting type");
        ((LabelledSpinner) viewFragment.findViewById(R.id.meeting_stub_meeting_sub_type_id))
                .setOnItemChosenListener(this);
    }

    private void inflateArfollowUpTypesSpinner() {
        ((LabelledSpinner) viewFragment.findViewById(R.id.spinner_ar_followup_stub_spinner_id))
                .setItemsArray(R.array.AR_follow_up);
        ((LabelledSpinner) viewFragment.findViewById(R.id.spinner_ar_followup_stub_spinner_id))
                .setDefaultErrorEnabled(false);
        ((LabelledSpinner) viewFragment.findViewById(R.id.spinner_ar_followup_stub_spinner_id))
                .setDefaultErrorText("Please select AR follow up type");
        ((LabelledSpinner) viewFragment.findViewById(R.id.spinner_ar_followup_stub_spinner_id)).setOnItemChosenListener(this);
    }

    private void inflateComplateTypesSpinner() {
        String[] stringsComplaints = new String[complaintDataTypeModels.size()];
        for (int i = 0; i < complaintDataTypeModels.size(); i++) {
            stringsComplaints[i] = complaintDataTypeModels.get(i).getTypeName();
        }
        ((LabelledSpinner) viewFragment.findViewById(R.id.complaints_stub_complaints_type_spinner_id))
                .setItemsArray(stringsComplaints);
        ((LabelledSpinner) viewFragment.findViewById(R.id.complaints_stub_complaints_type_spinner_id))
                .setDefaultErrorEnabled(false);
        ((LabelledSpinner) viewFragment.findViewById(R.id.complaints_stub_complaints_type_spinner_id))
                .setDefaultErrorText("Please select complaint type");
        ((LabelledSpinner) viewFragment.findViewById(R.id.complaints_stub_complaints_type_spinner_id)).setOnItemChosenListener(this);
    }


    @Override
    public void onNothingChosen(View labelledSpinner, AdapterView<?> adapterView) {

    }

    @Override
    public void afterProjectsListDownlodeFail(String message, String dialogTitle, boolean isInternetError) {
        CommonFunc.commonDialog(getActivity(), dialogTitle, message, isInternetError);
    }

    @Override
    public void afterAssignMeetingSuccess(String message) {
        Bundle bundle = new Bundle();
        bundle.putString(ConstantsUtils.successFragMessageKey, message);
        bundle.putString(ConstantsUtils.successFragAddMoreButtonKey, "Assign more meetings");
        ((BaseActivity) getActivity()).attachFragment(new SuccessAddingMeting(), true, "", bundle);
    }

    @Override
    public void afterAssignMeetingFail(String message, String dialogTitle, boolean isInternetError) {
        CommonFunc.commonDialog(getActivity(), dialogTitle, message, isInternetError);
    }


    private String getProjuctIdId() {
        String customerId = null;
        for (int i = 0; i < projectsDataModels.size(); i++) {
            if (projectsDataModels.get(i).getProjectName().equals
                    (String.valueOf(((LabelledSpinner) viewFragment.findViewById(R.id.fragment_assign_to_project_list_dropdown))
                            .getSpinner().getSelectedItem()))) {
                customerId = projectsDataModels.get(i).getProjectID();
                break;
            }
        }
        return customerId;
    }

    private void setInitialProjectSelectSpinner() {
        projectsDataModels.clear();
        projectsDataModels.add(new ProjectsDataModel("Enter customer name first"));
        String[] strings = new String[projectsDataModels.size()];
        for (int i = 0; i < strings.length; i++) {
            strings[i] = projectsDataModels.get(i).getProjectName();
        }
        ((LabelledSpinner) viewFragment.findViewById(R.id.fragment_assign_to_project_list_dropdown))
                .setItemsArray(strings);
        ((LabelledSpinner) viewFragment.findViewById(R.id.fragment_assign_to_project_list_dropdown))
                .setDefaultErrorEnabled(false);
        ((LabelledSpinner) viewFragment.findViewById(R.id.fragment_assign_to_project_list_dropdown))
                .setDefaultErrorText("Please select customer name first");
    }

    private void placePickerCode(int requestCode) {
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
        try {
            Intent intent = builder.build(getActivity());
            startActivityForResult(intent, requestCode);
            dismissProgressDialog();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.fragment_assign_meeting_location_edittext_id:
                if (NetworkUtils.checkInternetAndOpenDialog(getActivity())) {
                    showProgressDialog();
                    Toast.makeText(getActivity(), "Going to.... select location", Toast.LENGTH_SHORT).show();
                    placePickerCode(101);
                }
                break;
            case R.id.payment_followup_types_new_date_edittext_id:
                myDatePicker(true);
                break;
            case R.id.c_form_flowup_stub_new_date_edittext_id:
                myDatePicker(false);
                break;
            case R.id.fragment_assign_to_assign_autocomplete_textview_id:
                Intent intentUserList = new Intent(getActivity(), UserListActiForRes.class);
                startActivityForResult(intentUserList, REQ_CODE_TO_GET_THE_USER_DATA);
                break;
            case R.id.fragment_assign_to_customer_name_edittext_id:
                Intent intent = new Intent(getActivity(), CustomerActForResult.class);
                startActivityForResult(intent, REQ_CODE_TO_GET_THE_CUSTOMR_DATA);
                break;
            case R.id.fragment_assign_to_view_project_history:
                if (projectsDataModels.isEmpty()) {
                    CommonFunc.commonDialog(getActivity(), getString(R.string.alert), "No products history available", false);
                } else {
                    if (((LabelledSpinner) viewFragment.findViewById(R.id.fragment_assign_to_project_list_dropdown))
                            .getSpinner().getSelectedItemPosition() != 0) {
                        projuctSpinnerSelectedValueVefireLeavingThisFragmentToHistrayFrag = ((LabelledSpinner)
                                viewFragment.findViewById(R.id.fragment_assign_to_project_list_dropdown))
                                .getSpinner().getSelectedItemPosition();


                        Bundle bundle = new Bundle();
                        bundle.putString("projectID", projectsDataModels.get(((LabelledSpinner) viewFragment.findViewById
                                (R.id.fragment_assign_to_project_list_dropdown)).
                                getSpinner().getSelectedItemPosition()).getProjectID());
                        ((BaseActivity) getActivity()).attachFragment(new ProjectHistoryFragment(), true, projectsDataModels
                                .get(((LabelledSpinner) viewFragment.findViewById(R.id.fragment_assign_to_project_list_dropdown)).
                                        getSpinner().getSelectedItemPosition())
                                .getProjectName() + "'s history", bundle);
                    } else {
                        CommonFunc.commonDialog(getActivity(), getString(R.string.alert), "Please select project first", false);
                    }
                }
                break;
            case R.id.fragment_assign_to_add_customer_id_imageview:
                projuctSpinnerSelectedValueVefireLeavingThisFragmentToHistrayFrag = ((LabelledSpinner)
                        viewFragment.findViewById(R.id.fragment_assign_to_project_list_dropdown))
                        .getSpinner().getSelectedItemPosition();
                ((BaseActivity) getActivity()).attachFragment(new AddCustomers(), true, getString(R.string.AddCustomers), null);
                break;
            case R.id.fragment_assign_to_add_projects_imageview_button:
                projuctSpinnerSelectedValueVefireLeavingThisFragmentToHistrayFrag = ((LabelledSpinner)
                        viewFragment.findViewById(R.id.fragment_assign_to_project_list_dropdown))
                        .getSpinner().getSelectedItemPosition();
                ((BaseActivity) getActivity()).attachFragment(new AddProjects(), true, "Add Projects", null);
                break;
            case R.id.fragment_assign_to_submit_button_id:

                if (booleeanValidateAll()) {
               /*     String projectId = "";
                    if (((LabelledSpinner) viewFragment.findViewById(R.id.fragment_assign_to_project_list_dropdown)).getSpinner().getSelectedItemPosition() != 0) {
                        projectId = getProjuctIdId();
                    }
                    assignMeetingPresentor.reqToAssignMeetingToUser(TEMP_USER_ID,
                            String.valueOf(((LabelledSpinner) viewFragment.findViewById(R.id.fragment_assign_to_meeting_type_spinner)).getSpinner().getSelectedItemPosition()),
                            projectId, "Pending", TEMP_CUST_ID,
                            ((AppCompatEditText) viewFragment.findViewById(R.id.fragment_assign_to_followup_stub_date_id)).getText().toString(),
                            ((AppCompatEditText) viewFragment.findViewById(R.id.fragment_assign_to_followup_stub_time_id)).getText().toString(),
                            getActivity());*/
                    afterValidationOfMeetingSuccessfull();
                }
                break;
            case R.id.fragment_assign_to_followup_stub_date_id:
                selectDate(v.getId());
                break;
            case R.id.fragment_assign_to_followup_stub_time_id:
                selectTime(v.getId());
                break;
            case R.id.submitance_stub__add_product_button_id:
            case R.id.presentation_stub__add_product_button_id:
            case R.id.follow_up_for_orders__add_product_button_id:
                assignMeetingPresentor.reqToGetTheProductsDataFromPresentor(getActivity()); // get the products. . . . .
                break;

        }
    }

    @Override
    public void afterComplaintsDownlodeSuccess(ArrayList<ComplaintDataTypeModel> complaintDataTypeModelss) {
        complaintDataTypeModels.clear();
        complaintDataTypeModels.addAll(complaintDataTypeModelss);
        inflateComplateTypesSpinner();
    }

    @Override
    public void afterComplaintsDownloldeFail(String message, String dialogTitle, boolean isInternetError) {
        CommonFunc.commonDialog(getActivity(), dialogTitle, message, isInternetError);
    }

    @Override
    public void afterProductsModelDownlodeSuccess(List<ProductsDataModel> productsDataModelss) {
//        Log.e("value=", ((LabelledSpinner) viewFragment.findViewById(R.id.meeting_stub_meeting_sub_type_id)).getSpinner()
//                .getSelectedItemPosition() + "");
        if (((LabelledSpinner) viewFragment.findViewById(R.id.meeting_stub_meeting_sub_type_id)).getSpinner()
                .getSelectedItemPosition() == 1) // submitance
        {
            Intent intent = new Intent(getActivity(), MeetingProductOnActiForResult.class);
            //////////////////////
            ArrayList<ProductsDataModel> productsDataModelsOld = new ArrayList<>();
            for (int i = 0; i < productsDataModels_Submitance.size(); i++) {
                if (productsDataModels_Submitance.get(i).isChecked()) {
                    productsDataModelsOld.add(productsDataModels_Submitance.get(i));
                }
            }
            //////////////////////////////////////////////////////////////////////////////////////// add new data
            productsDataModels_Submitance.clear();
            for (int i = 0; i < productsDataModelss.size(); i++) {
                productsDataModels_Submitance.add(productsDataModelss.get(i));
            }
            //////////////////////////////////////////////////////////////////////// attach selected one
            if (!productsDataModelsOld.isEmpty()) {
                for (int i = 0; i < productsDataModelsOld.size(); i++) {
                    for (int j = 0; j < productsDataModels_Submitance.size(); j++) {
                        if (productsDataModelsOld.get(i).getProductID().equals(productsDataModels_Submitance.get(j).getProductID())) {
                            productsDataModels_Submitance.get(j).setPrice(productsDataModelsOld.get(i).getPrice());
                            productsDataModels_Submitance.get(j).setQty(productsDataModelsOld.get(i).getQty());
                            productsDataModels_Submitance.get(j).setChecked(productsDataModelsOld.get(i).isChecked());
                        }
                    }
                }
            }
            intent.putExtra("REQ_CODE", REQ_CODE_TO_GET_THE_PRODUCTS_SUBMITANCE_ASSIGN);
            startActivityForResult(intent, REQ_CODE_TO_GET_THE_PRODUCTS_SUBMITANCE_ASSIGN);
            /////////////////////
        } else if (((LabelledSpinner) viewFragment.findViewById(R.id.meeting_stub_meeting_sub_type_id)).getSpinner()
                .getSelectedItemPosition() == 2) // presentation
        {
            Intent intent = new Intent(getActivity(), MeetingProductOnActiForResult.class);
            //////////////////////
            ArrayList<ProductsDataModel> productsDataModelsOld = new ArrayList<>();
            for (int i = 0; i < productsDataModels_Presentation.size(); i++) {
                if (productsDataModels_Presentation.get(i).isChecked()) {
                    productsDataModelsOld.add(productsDataModels_Presentation.get(i));
                }
            }
            //////////////////////////////////////////////////////////////////////////////////////// add new data
            productsDataModels_Presentation.clear();
            for (int i = 0; i < productsDataModelss.size(); i++) {
                productsDataModels_Presentation.add(productsDataModelss.get(i));
            }
            //////////////////////////////////////////////////////////////////////// attach selected one
            if (!productsDataModelsOld.isEmpty()) {
                for (int i = 0; i < productsDataModelsOld.size(); i++) {
                    for (int j = 0; j < productsDataModels_Presentation.size(); j++) {
                        if (productsDataModelsOld.get(i).getProductID().equals(productsDataModels_Presentation.get(j).getProductID())) {
                            productsDataModels_Presentation.get(j).setPrice(productsDataModelsOld.get(i).getPrice());
                            productsDataModels_Presentation.get(j).setQty(productsDataModelsOld.get(i).getQty());
                            productsDataModels_Presentation.get(j).setChecked(productsDataModelsOld.get(i).isChecked());
                        }
                    }
                }
            }
            intent.putExtra("REQ_CODE", REQ_CODE_TO_GET_THE_PRODUCTS_PRESENTATION_ASSIGN);
            startActivityForResult(intent, REQ_CODE_TO_GET_THE_PRODUCTS_PRESENTATION_ASSIGN);
            /////////////////////
        } else if (((LabelledSpinner) viewFragment.findViewById(R.id.meeting_stub_meeting_sub_type_id)).getSpinner()
                .getSelectedItemPosition() == 3)// follow up
        {
            Intent intent = new Intent(getActivity(), MeetingProductOnActiForResult.class);
            //////////////////////
            ArrayList<ProductsDataModel> productsDataModelsOld = new ArrayList<>();
            for (int i = 0; i < productsDataModels_Follow_up.size(); i++) {
                if (productsDataModels_Follow_up.get(i).isChecked()) {
                    productsDataModelsOld.add(productsDataModels_Follow_up.get(i));
                }
            }
            //////////////////////////////////////////////////////////////////////////////////////// add new data
            productsDataModels_Follow_up.clear();
            for (int i = 0; i < productsDataModelss.size(); i++) {
                productsDataModels_Follow_up.add(productsDataModelss.get(i));
            }
            //////////////////////////////////////////////////////////////////////// attach selected one
            if (!productsDataModelsOld.isEmpty()) {
                for (int i = 0; i < productsDataModelsOld.size(); i++) {
                    for (int j = 0; j < productsDataModels_Follow_up.size(); j++) {
                        if (productsDataModelsOld.get(i).getProductID().equals(productsDataModels_Follow_up.get(j).getProductID())) {
                            productsDataModels_Follow_up.get(j).setPrice(productsDataModelsOld.get(i).getPrice());
                            productsDataModels_Follow_up.get(j).setQty(productsDataModelsOld.get(i).getQty());
                            productsDataModels_Follow_up.get(j).setChecked(productsDataModelsOld.get(i).isChecked());
                        }
                    }
                }
            }
            intent.putExtra("REQ_CODE", REQ_CODE_TO_GET_THE_PRODUCTS_FOLLOW_UP_ASSIGN);
            startActivityForResult(intent, REQ_CODE_TO_GET_THE_PRODUCTS_FOLLOW_UP_ASSIGN);
            /////////////////////
        }

    }

    @Override
    public void afterProductsModelDoenlodeFail(String message, String dialogTitle, boolean isInternetError) {
        CommonFunc.commonDialog(getActivity(), dialogTitle, message, isInternetError);
    }

    ////
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data != null) {
            Place placeSelected = PlacePicker.getPlace(data, getActivity());
            if (requestCode == 101 && resultCode == RESULT_OK) {
                ((AppCompatEditText) viewFragment.findViewById(R.id.fragment_assign_meeting_location_edittext_id)).setText(placeSelected.getAddress().toString() == null ? "" : placeSelected.getAddress().toString());
            }
        }

        if (getActivity().RESULT_OK == resultCode) {
            if (requestCode == REQ_CODE_TO_GET_THE_CUSTOMR_DATA) {
                CustomerDataModel customerDataModel = data.getExtras().getParcelable("customer_model");
                setInitialProjectSelectSpinner();
                assignMeetingPresentor.reqToGetTheProjectNameBasedOnCustomerName(getActivity(), customerDataModel.getCustomerID());
                ((AppCompatEditText) viewFragment.findViewById(R.id.fragment_assign_to_customer_type_edittext_id))
                        .setText(customerDataModel.getType());
                ((AppCompatEditText) viewFragment.findViewById(R.id.fragment_assign_to_customer_mobile_no_edittext_id))
                        .setText(customerDataModel.getMobile());
                ((AppCompatEditText) viewFragment.findViewById(R.id.fragment_assign_to_customer_location_edittext_id))
                        .setText(customerDataModel.getLocation());
                ((AppCompatAutoCompleteTextView) viewFragment.findViewById(R.id.fragment_assign_to_customer_name_edittext_id))
                        .setText(customerDataModel.getCustomerName());
                TEMP_CUST_ID = customerDataModel.getCustomerID();

            }
            if (requestCode == REQ_CODE_TO_GET_THE_USER_DATA) {
                UsersDataModel usersDataModel = data.getExtras().getParcelable("user_model");
                TEMP_USER_ID = usersDataModel.getUserId();
                ((AppCompatAutoCompleteTextView) viewFragment.findViewById(R.id.fragment_assign_to_assign_autocomplete_textview_id))
                        .setText("" + usersDataModel.getFullName());
            }

            if (requestCode == REQ_CODE_TO_GET_THE_PRODUCTS_SUBMITANCE_ASSIGN) {
                double value = 0.0;
                ArrayList<ProductsDataModel> productsDataModelsTemp = new ArrayList<>();
                for (int i = 0; i < productsDataModels_Submitance.size(); i++) {
                    if (productsDataModels_Submitance.get(i).isChecked()) {
                        if (!productsDataModels_Submitance.get(i).getPrice().trim().equals(""))
                            value = value + (Double.valueOf(productsDataModels_Submitance.get(i).getQty())) *
                                    (Double.valueOf(productsDataModels_Submitance.get(i).getPrice()));
                        productsDataModelsTemp.add(productsDataModels_Submitance.get(i));
                    }
                }
                ((RecyclerView) viewFragment.findViewById(R.id.submitance_stub__add_product_recycleview_id))
                        .setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
                ((RecyclerView) viewFragment.findViewById(R.id.submitance_stub__add_product_recycleview_id))
                        .setAdapter(new AssignMeetingFrag.AddProductsAdapter(getActivity(), productsDataModelsTemp));
                if (value == 0)
                    ((AppCompatEditText) viewFragment.findViewById(R.id.submitance_stub__orders_value_edittext_id)).setEnabled(true);
                ((AppCompatEditText) viewFragment.findViewById(R.id.submitance_stub__orders_value_edittext_id))
                        .setText("" + value);

            }
            if (requestCode == REQ_CODE_TO_GET_THE_PRODUCTS_PRESENTATION_ASSIGN) {
                double value = 0.0;
                ArrayList<ProductsDataModel> productsDataModelsTemp = new ArrayList<>();
                for (int i = 0; i < productsDataModels_Presentation.size(); i++) {
                    if (productsDataModels_Presentation.get(i).isChecked()) {
                        if (!productsDataModels_Presentation.get(i).getPrice().trim().equals(""))

                            value = value + (Double.valueOf(productsDataModels_Presentation.get(i).getPrice())) *
                                    (Double.valueOf(productsDataModels_Presentation.get(i).getQty()));
                        productsDataModelsTemp.add(productsDataModels_Presentation.get(i));
                    }
                }
                ((RecyclerView) viewFragment.findViewById(R.id.presentation_stub__add_product_recycleview_id))
                        .setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
                ((RecyclerView) viewFragment.findViewById(R.id.presentation_stub__add_product_recycleview_id))
                        .setAdapter(new AssignMeetingFrag.AddProductsAdapter(getActivity(), productsDataModelsTemp));
                if (value == 0)
                    ((AppCompatEditText) viewFragment.findViewById(R.id.presentation_stub__orders_value_edittext_id)).setEnabled(true);
                ((AppCompatEditText) viewFragment.findViewById(R.id.presentation_stub__orders_value_edittext_id))
                        .setText("" + value);

            }
            if (requestCode == REQ_CODE_TO_GET_THE_PRODUCTS_FOLLOW_UP_ASSIGN) {
                double value = 0.0;
                ArrayList<ProductsDataModel> productsDataModelsTemp = new ArrayList<>();
                for (int i = 0; i < productsDataModels_Follow_up.size(); i++) {
                    if (productsDataModels_Follow_up.get(i).isChecked()) {
                        if (!productsDataModels_Follow_up.get(i).getPrice().trim().equals(""))
                            value = value + (Double.valueOf(productsDataModels_Follow_up.get(i).getPrice())) *
                                    (Double.valueOf(productsDataModels_Follow_up.get(i).getQty()));
                        productsDataModelsTemp.add(productsDataModels_Follow_up.get(i));
                    }
                }
                ((RecyclerView) viewFragment.findViewById(R.id.follow_up_for_orders__add_product_recycleview_id))
                        .setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
                ((RecyclerView) viewFragment.findViewById(R.id.follow_up_for_orders__add_product_recycleview_id))
                        .setAdapter(new AssignMeetingFrag.AddProductsAdapter(getActivity(), productsDataModelsTemp));
                if (value == 0)
                    ((AppCompatEditText) viewFragment.findViewById(R.id.follow_up_for_orders_value_edittext_id)).setEnabled(true);
                ((AppCompatEditText) viewFragment.findViewById(R.id.follow_up_for_orders_value_edittext_id))
                        .setText("" + value);
            }

        }
    }

    private boolean booleeanValidateAll() {
        /*if (((AppCompatAutoCompleteTextView) viewFragment.findViewById(R.id.fragment_assign_to_assign_autocomplete_textview_id)).getText().toString().isEmpty()) {
            ((AppCompatAutoCompleteTextView) viewFragment.findViewById(R.id.fragment_assign_to_assign_autocomplete_textview_id)).setError("Enter user name to assign");
            return false;
        } else if (((AppCompatAutoCompleteTextView) viewFragment.findViewById(R.id.fragment_assign_to_customer_name_edittext_id)).getText().toString().isEmpty()) {
            ((AppCompatAutoCompleteTextView) viewFragment.findViewById(R.id.fragment_assign_to_customer_name_edittext_id)).setError("Enter customer name");
            return false;
        } else if (((LabelledSpinner) viewFragment.findViewById(R.id.fragment_assign_to_meeting_type_spinner)).getSpinner().getSelectedItemPosition() == 0) {
            CommonFunc.commonDialog(getActivity(), getString(R.string.alert),
                    "Please select meeting type", false);
            return false;
        } else if (((AppCompatEditText) viewFragment.findViewById(R.id.fragment_assign_to_followup_stub_date_id)).getText().toString().isEmpty()) {
            ((AppCompatEditText) viewFragment.findViewById(R.id.fragment_assign_to_followup_stub_date_id)).setError("Please select date!");
            return false;
        } else if (((AppCompatEditText) viewFragment.findViewById(R.id.fragment_assign_to_followup_stub_time_id)).getText().toString().isEmpty()) {
            ((AppCompatEditText) viewFragment.findViewById(R.id.fragment_assign_to_followup_stub_time_id)).setError("Please select time!");
            return false;
        } else {
            return true;
        }*/


        boolean checkValue = validateMeetingsDataInPresenter(getActivity(),
                ((LabelledSpinner) viewFragment.findViewById(R.id.fragment_assign_to_project_list_dropdown)).getSpinner().getSelectedItemPosition(),
                ((AppCompatAutoCompleteTextView) viewFragment.findViewById(R.id.fragment_assign_to_customer_name_edittext_id)).getText().toString(),
                ((AppCompatEditText) viewFragment.findViewById(R.id.fragment_assign_to_customer_type_edittext_id)).getText().toString()
                , ((LabelledSpinner) viewFragment.findViewById(R.id.fragment_assign_to_meeting_type_spinner)).getSpinner().getSelectedItemPosition()
                , viewFragment.findViewById(R.id.meeting_stub_meeting_sub_type_id)
                        == null ? 0 :
                        ((LabelledSpinner) viewFragment.findViewById(R.id.meeting_stub_meeting_sub_type_id)).getSpinner().getSelectedItemPosition(),
                (viewFragment.findViewById(R.id.follow_up_for_orders_value_edittext_id))
                        == null ? "" : ((AppCompatEditText) viewFragment.findViewById(R.id.follow_up_for_orders_value_edittext_id)).getText().toString(),
                (viewFragment.findViewById(R.id.follow_up_for_orders_lost_edittext_id))
                        == null ? "" : ((AppCompatEditText) viewFragment.findViewById(R.id.follow_up_for_orders_lost_edittext_id)).getText().toString(),
                viewFragment.findViewById(R.id.complaints_stub_complaints_type_spinner_id)
                        == null ? 0 : ((LabelledSpinner) viewFragment.findViewById(R.id.complaints_stub_complaints_type_spinner_id)).getSpinner().getSelectedItemPosition(),
                viewFragment.findViewById(R.id.spinner_ar_followup_stub_spinner_id)
                        == null ? 0 : ((LabelledSpinner) viewFragment.findViewById(R.id.spinner_ar_followup_stub_spinner_id)).getSpinner().getSelectedItemPosition(),
                (viewFragment.findViewById(R.id.payment_followup_types_new_date_edittext_id))
                        == null ? "" : ((AppCompatEditText) viewFragment.findViewById(R.id.payment_followup_types_new_date_edittext_id)).getText().toString(),
                (viewFragment.findViewById(R.id.payment_followup_types_project_issue_edittext_id))
                        == null ? "" : ((AppCompatEditText) viewFragment.findViewById(R.id.payment_followup_types_project_issue_edittext_id)).getText().toString(),
                (viewFragment.findViewById(R.id.payment_followup_types_project_delay_edittext_id))
                        == null ? "" : ((AppCompatEditText) viewFragment.findViewById(R.id.payment_followup_types_project_delay_edittext_id)).getText().toString(),
                (viewFragment.findViewById(R.id.c_form_flowup_stub_collected_edittext_id))
                        == null ? "" : ((AppCompatEditText) viewFragment.findViewById(R.id.c_form_flowup_stub_collected_edittext_id)).getText().toString(),
                (viewFragment.findViewById(R.id.c_form_flowup_stub_new_date_edittext_id))
                        == null ? "" : ((AppCompatEditText) viewFragment.findViewById(R.id.c_form_flowup_stub_new_date_edittext_id)).getText().toString(),
                (viewFragment.findViewById(R.id.c_form_flowup_stub_not_traceble_edittext_id))
                        == null ? "" : ((AppCompatEditText) viewFragment.findViewById(R.id.c_form_flowup_stub_not_traceble_edittext_id)).getText().toString(),
                (viewFragment.findViewById(R.id.reconciliation_missing_invoice_id))
                        == null ? "" : ((AppCompatEditText) viewFragment.findViewById(R.id.reconciliation_missing_invoice_id)).getText().toString(),
                (viewFragment.findViewById(R.id.reconciliation_short_payment_id))
                        == null ? "" : ((AppCompatEditText) viewFragment.findViewById(R.id.reconciliation_short_payment_id)).getText().toString(),
                (viewFragment.findViewById(R.id.reconciliation_statement_id))
                        == null ? "" : ((AppCompatEditText) viewFragment.findViewById(R.id.reconciliation_statement_id)).getText().toString(),
                productsDataModels_Submitance,
                productsDataModels_Presentation,
                productsDataModels_Follow_up,
                ((AppCompatEditText) viewFragment.findViewById(R.id.fragment_assign_to_customer_location_edittext_id)).getText().toString(),
                ((AppCompatEditText) viewFragment.findViewById(R.id.fragment_assign_meeting_location_edittext_id)).getText().toString());
        return checkValue;

    }


    public boolean validateMeetingsDataInPresenter(Context context, int projectNameSpinnerSelectedItemPos,
                                                   String customerName, String customerType,
                                                   int meetingTypeSpinnerSelectedPos, int submeetingTypeSelectedSpinner,
                                                   String valueOfFllowUpOrder, String orderLostOfFollowupOrder,
                                                   int complaintsTypesSpinnerSelectedPos,
                                                   int arFollowTypesSpinnerSelectedPos,
                                                   String arFollowPaymentNewDate, String arFollowPaymentProjectissue,
                                                   String arFollowPaymentProjectDeley,
                                                   String arFollowCformFollowupCollected,
                                                   String arFollowCformFollowupNewDate,
                                                   String arFollowCformFollowupNotTracable,
                                                   String arFollowCformReconcilationMissingInvoice,
                                                   String arFollowCformReconcilationShortPayment,
                                                   String arFollowCformReconcilationStatement,
                                                   ArrayList<ProductsDataModel> productsDataModels_Submitance,
                                                   ArrayList<ProductsDataModel> productsDataModels_Presentation,
                                                   ArrayList<ProductsDataModel> productsDataModels_Followup,
                                                   String locAddress, String meetingLocation) {

        if (((AppCompatAutoCompleteTextView) viewFragment.findViewById(R.id.fragment_assign_to_assign_autocomplete_textview_id)).getText().toString().isEmpty()) {
            CommonFunc.commonDialog(context, context.getString(R.string.alert), "Please enter user name to assign", false);
            return false;
        } else if (customerName.isEmpty()) {
            CommonFunc.commonDialog(context, context.getString(R.string.alert), "Please enter customer name", false);
            return false;
        } else if (customerType.isEmpty()) {
            CommonFunc.commonDialog(context, context.getString(R.string.alert), "Please enter customer name (Customer type will get auto filled)", false);
            return false;
        } else if (checkSpinnersData(context, meetingTypeSpinnerSelectedPos,
                submeetingTypeSelectedSpinner, complaintsTypesSpinnerSelectedPos,
                arFollowTypesSpinnerSelectedPos)) {
            if (((AppCompatEditText) viewFragment.findViewById(R.id.fragment_assign_to_followup_stub_date_id)).getText().toString().isEmpty()) {
                CommonFunc.commonDialog(context, context.getString(R.string.alert), "Please select meeting Date", false);
                return false;
            } else if (((AppCompatEditText) viewFragment.findViewById(R.id.fragment_assign_to_followup_stub_time_id)).getText().toString().isEmpty()) {
                CommonFunc.commonDialog(context, context.getString(R.string.alert), "Please select meeting Time", false);
                return false;
            } else if (meetingLocation.isEmpty()) {
                CommonFunc.commonDialog(context, context.getString(R.string.alert), "Please select meeting Location", false);
                return false;
            } else
                return true;
        } else {
            return false;
        }



           /* ////////////////// clear up to selecting the spinner. . ..  check after the spinner. .
            switch (meetingTypeSpinnerSelectedPos) {
                case 1:
                    // meeting
                    if (submeetingTypeSelectedSpinner == 0) {
                        CommonFunc.commonDialog(context, context.getString(R.string.alert), "Please sub meeting type", false);
                        return false;
                    } else {
                        switch (submeetingTypeSelectedSpinner) {
                            case 1: // submitance
                                if (!checkProductsAdded(productsDataModels_Submitance)) {
                                    CommonFunc.commonDialog(context, context.getString(R.string.alert), "Please add Submittance products", false);
                                    return false;
                                } else if (locAddress.isEmpty()) {
                                    CommonFunc.commonDialog(context, context.getString(R.string.alert),
                                            "Please check your location", false);
                                    return false;
                                } else {
                                    return true;
                                }

                            case 2: // presentation
                                if (!checkProductsAdded(productsDataModels_Presentation)) {
                                    CommonFunc.commonDialog(context, context.getString(R.string.alert), "Please add Presentation products", false);
                                    return false;
                                } else if (locAddress.isEmpty()) {
                                    CommonFunc.commonDialog(context, context.getString(R.string.alert),
                                            "Please check your location", false);
                                    return false;
                                } else {
                                    return true;
                                }

                            case 3: // follow up
                                if (!checkProductsAdded(productsDataModels_Followup)) {
                                    CommonFunc.commonDialog(context, context.getString(R.string.alert), "Please add Followup products", false);
                                    return false;
                                } else if (valueOfFllowUpOrder.isEmpty()) {
                                    CommonFunc.commonDialog(context, context.getString(R.string.alert), "Please enter value", false);
                                    return false;
                                } else if (orderLostOfFollowupOrder.isEmpty()) {
                                    CommonFunc.commonDialog(context, context.getString(R.string.alert), "Please enter order lost", false);
                                    return false;
                                } else if (locAddress.isEmpty()) {
                                    CommonFunc.commonDialog(context, context.getString(R.string.alert),
                                            "Please check your location", false);
                                    return false;
                                } else {
                                    return true;
                                }
                        }
                    }
                    break;
                case 2:
                    // AR follow ups
                    if (arFollowTypesSpinnerSelectedPos == 0) {
                        CommonFunc.commonDialog(context, context.getString(R.string.alert), "Please select AR follow type", false);
                        return false;
                    } else {
                        switch (arFollowTypesSpinnerSelectedPos) {
                            case 1:// payment followup
                                if (arFollowPaymentNewDate.isEmpty()) {
                                    CommonFunc.commonDialog(context, context.getString(R.string.alert), "Please enter New date", false);
                                    return false;
                                } else if (arFollowPaymentProjectissue.isEmpty()) {
                                    CommonFunc.commonDialog(context, context.getString(R.string.alert), "Please enter Project issue", false);
                                    return false;
                                } else if (arFollowPaymentProjectDeley.isEmpty()) {
                                    CommonFunc.commonDialog(context, context.getString(R.string.alert), "Please enter Project delay", false);
                                    return false;
                                } else if (locAddress.isEmpty()) {
                                    CommonFunc.commonDialog(context, context.getString(R.string.alert),
                                            "Please check your location", false);
                                    return false;
                                } else {
                                    return true;
                                }

                            case 2: // c form followup
                                if (arFollowCformFollowupCollected.isEmpty()) {
                                    CommonFunc.commonDialog(context, context.getString(R.string.alert), "Please enter collected", false);
                                    return false;
                                } else if (arFollowCformFollowupNewDate.isEmpty()) {
                                    CommonFunc.commonDialog(context, context.getString(R.string.alert), "Please enter New date", false);
                                    return false;
                                } else if (arFollowCformFollowupNotTracable.isEmpty()) {
                                    CommonFunc.commonDialog(context, context.getString(R.string.alert), "Please enter Not traceable", false);
                                    return false;
                                } else if (locAddress.isEmpty()) {
                                    CommonFunc.commonDialog(context, context.getString(R.string.alert),
                                            "Please check your location", false);
                                    return false;
                                } else {
                                    return true;
                                }

                            case 3: // reconcilation
                                if (arFollowCformReconcilationMissingInvoice.isEmpty()) {
                                    CommonFunc.commonDialog(context, context.getString(R.string.alert), "Please enter missing invoice", false);
                                    return false;
                                } else if (arFollowCformReconcilationShortPayment.isEmpty()) {
                                    CommonFunc.commonDialog(context, context.getString(R.string.alert), "Please enter short payment", false);
                                    return false;
                                } else if (arFollowCformReconcilationStatement.isEmpty()) {
                                    CommonFunc.commonDialog(context, context.getString(R.string.alert), "Please enter statement", false);
                                    return false;
                                } else if (locAddress.isEmpty()) {
                                    CommonFunc.commonDialog(context, context.getString(R.string.alert),
                                            "Please check your location", false);
                                    return false;
                                } else {
                                    return true;
                                }
                        }
                    }
                    break;
                case 3:
                    // complaints
                    if (complaintsTypesSpinnerSelectedPos == 0) {
                        CommonFunc.commonDialog(context, context.getString(R.string.alert), "Please enter complaints type", false);
                        return false;
                    } else if (locAddress.isEmpty()) {
                        CommonFunc.commonDialog(context, context.getString(R.string.alert),
                                "Please check your location", false);
                        return false;
                    } else {
                        return true;
                    }
            }
    }*/
    }

    public boolean checkSpinnersData(Context context, int meetingTypeSpinnerSelectedPos, int submeetingTypeSelectedSpinner,
                                     int complaintsTypesSpinnerSelectedPos,
                                     int arFollowTypesSpinnerSelectedPos) {
        if (meetingTypeSpinnerSelectedPos == 1) {
            if (submeetingTypeSelectedSpinner == 0) {
                CommonFunc.commonDialog(context, context.getString(R.string.alert), "Please sub meeting type", false);
                return false;
            } else return true;
        } else if (meetingTypeSpinnerSelectedPos == 2) {
            if (arFollowTypesSpinnerSelectedPos == 0) {
                CommonFunc.commonDialog(context, context.getString(R.string.alert), "Please select AR follow type", false);
                return false;
            } else return true;
        } else if (meetingTypeSpinnerSelectedPos == 3) {
            if (complaintsTypesSpinnerSelectedPos == 0) {
                CommonFunc.commonDialog(context, context.getString(R.string.alert), "Please enter complaints type", false);
                return false;
            } else return true;
        } else {
            CommonFunc.commonDialog(context, context.getString(R.string.alert), "Please Selectl Meeting type", false);
            return false;
        }
    }

    private boolean checkProductsAdded(ArrayList<ProductsDataModel> productsDataModels) {
        boolean isProductsAdded = false;
        for (int i = 0; i < productsDataModels.size(); i++) {
            if (productsDataModels.get(i).isChecked()) {
                isProductsAdded = true;
                break;
            }
        }
        return isProductsAdded;
    }


    /////////////////////////////////////////////////////////////////////////////////////////////////// from date  and to date picker of the followup


    private void selectDate(final int viewId) {

        SimpleDateFormat mDF = new SimpleDateFormat("yyyy-mm-dd");
        Date today = new Date();
        mDF.format(today);
        final Calendar c = Calendar.getInstance();
        c.setTime(today);
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        String formattedDay = (String.valueOf(dayOfMonth));
                        int m = monthOfYear + 1;


                        String formattedMonth = (String.valueOf(m));

                        if (dayOfMonth < 10) {
                            formattedDay = "0" + dayOfMonth;
                        }

                        if (m < 10) {
                            formattedMonth = "0" + m;

                        }

                      /*  case R.id.fragment_uplode_meet_result_followup_stub_to_date_id:
                        break;
                        case R.id.fragment_uplode_meet_result_followup_stub_from_date_id:*/


                        ((AppCompatEditText) viewFragment.findViewById(viewId)).setText(formattedDay + "/" + (formattedMonth) + "/" + year);
                        ((AppCompatEditText) viewFragment.findViewById(viewId)).clearFocus();
                        ((AppCompatEditText) viewFragment.findViewById(viewId)).setError(null);


                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {


            }
        });
        datePickerDialog.show();
        datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());
    }

    ///////////// time picker. . .
    private void selectTime(final int viewId) {
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

                String s = selectedHour + ":" + selectedMinute;
                DateFormat f1 = new SimpleDateFormat("HH:mm"); //HH for hour of the day (0 - 23)
                Date d = null;
                try {
                    d = f1.parse(s);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                DateFormat f2 = new SimpleDateFormat("h:mma");

                f2.format(d).toLowerCase(); // "12:18am"
                ((AppCompatEditText) viewFragment.findViewById(viewId))
                        .setText(f2.format(d).toUpperCase());
                ((AppCompatEditText) viewFragment.findViewById(viewId)).setError(null);
                ((AppCompatEditText) viewFragment.findViewById(viewId)).clearFocus();
                ((AppCompatEditText) viewFragment.findViewById(viewId)).setError(null);
            }
        }, hour, minute, false);//Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }

    private void myDatePicker(final boolean isPaymentFollowUp) {

        SimpleDateFormat mDF = new SimpleDateFormat("yyyy-mm-dd");
        Date today = new Date();
        mDF.format(today);
        final Calendar c = Calendar.getInstance();
        c.setTime(today);
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        String formattedDay = (String.valueOf(dayOfMonth));
                        int m = monthOfYear + 1;


                        String formattedMonth = (String.valueOf(m));

                        if (dayOfMonth < 10) {
                            formattedDay = "0" + dayOfMonth;
                        }

                        if (m < 10) {
                            formattedMonth = "0" + m;
                            Log.e("month ", " " + m);
                        }
                        if (isPaymentFollowUp)
                            ((AppCompatEditText) viewFragment.findViewById(R.id.payment_followup_types_new_date_edittext_id))
                                    .setText(formattedDay + "/" + (formattedMonth) + "/" + year);
                        else
                            ((AppCompatEditText) viewFragment.findViewById(R.id.c_form_flowup_stub_new_date_edittext_id))
                                    .setText(formattedDay + "/" + (formattedMonth) + "/" + year);


                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                if (isPaymentFollowUp)
                    ((AppCompatEditText) viewFragment.findViewById(R.id.payment_followup_types_new_date_edittext_id)).clearFocus();
                else
                    ((AppCompatEditText) viewFragment.findViewById(R.id.c_form_flowup_stub_new_date_edittext_id)).clearFocus();


            }
        });
        datePickerDialog.show();
        datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());
    }


    @Override
    public void afterValidationOfMeetingSuccessfull() {
        String meetingSubType = "0";
        /*if (((LabelledSpinner) viewFragment.findViewById(R.id.fragment_assign_to_meeting_type_spinner)).getSpinner().getSelectedItemPosition() == 1) {
            switch (((LabelledSpinner) viewFragment.findViewById(R.id.meeting_stub_meeting_sub_type_id)).getSpinner().getSelectedItemPosition()) {
                case 1:// submitance value

                    *//*if (!((AppCompatEditText) viewFragment.findViewById(R.id.submitance_stub__orders_value_edittext_id))
                            .getText().toString().isEmpty())
                        meetingSubType = ((AppCompatEditText) viewFragment.findViewById(R.id.submitance_stub__orders_value_edittext_id))
                                .getText().toString();
                    else
                        meetingSubType = "0";
                    break;*//*
                case 2: // presentation value
                    if (!((AppCompatEditText) viewFragment.findViewById(R.id.presentation_stub__orders_value_edittext_id))
                            .getText().toString().isEmpty())
                        meetingSubType = ((AppCompatEditText) viewFragment.findViewById(R.id.presentation_stub__orders_value_edittext_id))
                                .getText().toString();
                    else
                        meetingSubType = "0";
                    break;
                case 3:
                    // follow up value
                    if (!((AppCompatEditText) viewFragment.findViewById(R.id.follow_up_for_orders_value_edittext_id))
                            .getText().toString().isEmpty())
                        meetingSubType = ((AppCompatEditText) viewFragment.findViewById(R.id.follow_up_for_orders_value_edittext_id))
                                .getText().toString();
                    else
                        meetingSubType = "0";

            }
        }*/
        String projuctName = "";
        if (((LabelledSpinner) viewFragment.findViewById(R.id.fragment_assign_to_project_list_dropdown)).getSpinner().getSelectedItemPosition() > 0)
            projuctName = projectsDataModels.get(((LabelledSpinner) viewFragment.findViewById(R.id.fragment_assign_to_project_list_dropdown)).getSpinner().getSelectedItemPosition())
                    .getProjectName();


        assignMeetingPresentor.reqToPostTheMeetingToTheServer(
                TEMP_USER_ID,
                getActivity(),
                projuctName,
                projectsDataModels,
                ((AppCompatAutoCompleteTextView) viewFragment.findViewById(R.id.fragment_assign_to_customer_name_edittext_id)).getText().toString(),
                ((AppCompatEditText) viewFragment.findViewById(R.id.fragment_assign_to_customer_type_edittext_id)).getText().toString(),
                ((LabelledSpinner) viewFragment.findViewById(R.id.fragment_assign_to_meeting_type_spinner)).getSpinner().getSelectedItemPosition(),
                viewFragment.findViewById(R.id.meeting_stub_meeting_sub_type_id)
                        == null ? 0 :
                        ((LabelledSpinner) viewFragment.findViewById(R.id.meeting_stub_meeting_sub_type_id)).getSpinner().getSelectedItemPosition(),
                (viewFragment.findViewById(R.id.follow_up_for_orders_value_edittext_id))
                        == null ? "" : ((AppCompatEditText) viewFragment.findViewById(R.id.follow_up_for_orders_value_edittext_id)).getText().toString(),
                (viewFragment.findViewById(R.id.follow_up_for_orders_lost_edittext_id))
                        == null ? "" : ((AppCompatEditText) viewFragment.findViewById(R.id.follow_up_for_orders_lost_edittext_id)).getText().toString(),
                viewFragment.findViewById(R.id.complaints_stub_complaints_type_spinner_id)
                        == null ? 0 : ((LabelledSpinner) viewFragment.findViewById(R.id.complaints_stub_complaints_type_spinner_id)).getSpinner().getSelectedItemPosition(),
                viewFragment.findViewById(R.id.spinner_ar_followup_stub_spinner_id)
                        == null ? 0 : ((LabelledSpinner) viewFragment.findViewById(R.id.spinner_ar_followup_stub_spinner_id)).getSpinner().getSelectedItemPosition(),
                (viewFragment.findViewById(R.id.payment_followup_types_new_date_edittext_id))
                        == null ? "" : ((AppCompatEditText) viewFragment.findViewById(R.id.payment_followup_types_new_date_edittext_id)).getText().toString(),
                (viewFragment.findViewById(R.id.payment_followup_types_project_issue_edittext_id))
                        == null ? "" : ((AppCompatEditText) viewFragment.findViewById(R.id.payment_followup_types_project_issue_edittext_id)).getText().toString(),
                (viewFragment.findViewById(R.id.payment_followup_types_project_delay_edittext_id))
                        == null ? "" : ((AppCompatEditText) viewFragment.findViewById(R.id.payment_followup_types_project_delay_edittext_id)).getText().toString(),
                (viewFragment.findViewById(R.id.c_form_flowup_stub_collected_edittext_id))
                        == null ? "" : ((AppCompatEditText) viewFragment.findViewById(R.id.c_form_flowup_stub_collected_edittext_id)).getText().toString(),
                (viewFragment.findViewById(R.id.c_form_flowup_stub_new_date_edittext_id))
                        == null ? "" : ((AppCompatEditText) viewFragment.findViewById(R.id.c_form_flowup_stub_new_date_edittext_id)).getText().toString(),
                (viewFragment.findViewById(R.id.c_form_flowup_stub_not_traceble_edittext_id))
                        == null ? "" : ((AppCompatEditText) viewFragment.findViewById(R.id.c_form_flowup_stub_not_traceble_edittext_id)).getText().toString(),
                (viewFragment.findViewById(R.id.reconciliation_missing_invoice_id))
                        == null ? "" : ((AppCompatEditText) viewFragment.findViewById(R.id.reconciliation_missing_invoice_id)).getText().toString(),
                (viewFragment.findViewById(R.id.reconciliation_short_payment_id))
                        == null ? "" : ((AppCompatEditText) viewFragment.findViewById(R.id.reconciliation_short_payment_id)).getText().toString(),
                (viewFragment.findViewById(R.id.reconciliation_statement_id))
                        == null ? "" : ((AppCompatEditText) viewFragment.findViewById(R.id.reconciliation_statement_id)).getText().toString(),
                ((AppCompatEditText) viewFragment.findViewById(R.id.fragment_assign_to_followup_stub_date_id)).getText().toString(), productsDataModels_Submitance,
                productsDataModels_Presentation,
                productsDataModels_Follow_up,
                ((AppCompatEditText) viewFragment.findViewById(R.id.fragment_assign_to_followup_stub_time_id)).getText().toString(),
                meetingSubType, TEMP_CUST_ID, ((AppCompatEditText) viewFragment.findViewById(R.id.fragment_assign_meeting_location_edittext_id)).getText().toString());


    }

    @Override
    public void afterMeetingPostSuccess(String message, String meetingId) {

        Bundle bundle = new Bundle();
        bundle.putString(ConstantsUtils.successFragMessageKey, message);
        bundle.putString(ConstantsUtils.successFragAddMoreButtonKey, "Assign more meetings");
        ((BaseActivity) getActivity()).attachFragment(new SuccessAddingMeting(), true, "", bundle);
    }

    @Override
    public void afterMeetingPostFail(String message, String dialogTitle, boolean isInternetError) {
        CommonFunc.commonDialog(getActivity(), dialogTitle, message, isInternetError);
    }

    @Override
    public void onStop() {
        super.onStop();
        dismissProgressDialog();
    }
}
