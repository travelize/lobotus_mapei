package com.lobotus.mapei.flowfragments.mvp.presentorimp;

import android.content.Context;

import com.lobotus.mapei.flowfragments.model.UserTravelModel;
import com.lobotus.mapei.flowfragments.mvp.intrector.TravelDataIntractor;
import com.lobotus.mapei.flowfragments.mvp.intrectorimp.TravelDataIntractorImp;
import com.lobotus.mapei.flowfragments.mvp.presentor.TravelDataPresentor;
import com.lobotus.mapei.flowfragments.mvp.views.TravelDataView;

import java.util.ArrayList;

/**
 * Created by User on 21-03-2018.
 */

public class TravelDataPresentorImp implements TravelDataPresentor,TravelDataIntractor.OnTravelDataDownlodeLisner {

    private TravelDataIntractor travelDataIntractor=null;
    private TravelDataView travelDataView=null;

    public TravelDataPresentorImp(TravelDataView travelDataView) {
        this.travelDataIntractor = new TravelDataIntractorImp();
        this.travelDataView = travelDataView;
    }

    @Override
    public void reqToGetTheTravelData(Context context, String fromDate, String toDate, String userId) {
        travelDataView.showProgressDialog();
        travelDataIntractor.reqToGetUserTravelData(context,fromDate,toDate,userId,this);

    }

    @Override
    public void OnTravelDataDownlodeSuccess(ArrayList<UserTravelModel> userTravelModels) {
        travelDataView.dismissProgressDialog();
        travelDataView.afterTravelDataDownlodeSuccess(userTravelModels);

    }

    @Override
    public void OnTravelDataDOwnlodeFail(String message, String dialogTitle, boolean isInternetError) {
        travelDataView.dismissProgressDialog();
        travelDataView.afterTravelDataDOwnlodeFail(message,dialogTitle,isInternetError);
    }
}
