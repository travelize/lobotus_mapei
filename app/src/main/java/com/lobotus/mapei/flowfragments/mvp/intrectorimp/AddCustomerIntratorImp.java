package com.lobotus.mapei.flowfragments.mvp.intrectorimp;

import android.content.Context;
import android.widget.Toast;

import com.lobotus.mapei.R;
import com.lobotus.mapei.flowfragments.model.CustomerTypeDataModel;
import com.lobotus.mapei.flowfragments.model.CustomerTypeModel;
import com.lobotus.mapei.flowfragments.mvp.intrector.AddCustomerIntractor;
import com.lobotus.mapei.retrofitapi.APIService;
import com.lobotus.mapei.retrofitapi.ApiUtils;
import com.lobotus.mapei.utils.ConstantsUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by user1 on 08-11-2017.
 */

public class AddCustomerIntratorImp implements AddCustomerIntractor {

    ////////////////////////////// get customer type. . . .
    @Override
    public void reqToGetTheCustomerTypeFromIntractor(final Context context, final CustomerTypeLisner customerTypeLisner) {
        APIService apiService=ApiUtils.getAPIService(ConstantsUtils.postBaseUrl+ConstantsUtils.postToGetTheCustomerType);
        apiService.postToGetTheCustomerTypeModel("").enqueue(new Callback<CustomerTypeModel>() {
            @Override
            public void onResponse(Call<CustomerTypeModel> call, Response<CustomerTypeModel> response) {
                if (response.isSuccessful())
                {
                    if (response.body().getSuccess()==1)
                    {
                        if (response.body().getData()!=null)
                        {
                            if (response.body().getData().size()>0)
                            {
                                customerTypeLisner.DownlodeCustomerTypeSuccessLisner((ArrayList<CustomerTypeDataModel>) response.body().getData());
                            }else {
                                customerTypeLisner.DownlodeCustomerTypeFailLisner(
                                        context.getString(R.string.justErrorCode)+" 95",context.getString(R.string.internalError),
                                        false);
                            }
                        }else {
                            customerTypeLisner.DownlodeCustomerTypeFailLisner(
                                    context.getString(R.string.justErrorCode)+" 94",context.getString(R.string.internalError),
                                    false);
                        }
                    }else {
                        customerTypeLisner.DownlodeCustomerTypeFailLisner(response.body().getMsg(),
                                context.getString(R.string.alert),false);
                    }
                }else {
                    customerTypeLisner.DownlodeCustomerTypeFailLisner(
                            context.getString(R.string.justErrorCode)+" 93",context.getString(R.string.ServerNotresponding),
                            false);
                }
            }

            @Override
            public void onFailure(Call<CustomerTypeModel> call, Throwable t) {
                customerTypeLisner.DownlodeCustomerTypeFailLisner(context.getString(R.string.noInternetMessage)+" 92",
                        context.getString(R.string.noInternetAlert),
                        true);
            }
        });
    }

    /////////////////////////////// add customer . . .
    @Override
    public void reqToAddCustomerFromIntractor(final Context context, String CustTypeId, String FullName,
                                              String Location, String Email, String Phone, String Status,
                                              final AddCustomerLisner addCustomerLisner) {
        APIService apiService= ApiUtils.getAPIService(ConstantsUtils.postBaseUrl+ConstantsUtils.postToSaveTheCustomerDetails);
        apiService.postToSaveCustDetails(CustTypeId,FullName,Location,Email,Phone,Status).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful())
                {
                    try {
                        JSONObject jsonObject=new JSONObject(response.body().string());
                        if (jsonObject.getInt("Success")==1)
                            addCustomerLisner.AddCustomerSuccessLisner(jsonObject.getString("Msg"));
                        else
                            addCustomerLisner.AddCustomerFailLisner(jsonObject.getString("Msg"),
                                    context.getString(R.string.alert),false);
                    } catch (JSONException | IOException e) {
                        addCustomerLisner.AddCustomerFailLisner(   context.getString(R.string.justErrorCode)+" 90",
                                context.getString(R.string.internalError),
                                false);
                    }

                }else {
                    addCustomerLisner.AddCustomerFailLisner(
                            context.getString(R.string.justErrorCode)+" 89",context.getString(R.string.ServerNotresponding),
                            false);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                addCustomerLisner.AddCustomerFailLisner(context.getString(R.string.noInternetMessage)+" 91",
                        context.getString(R.string.noInternetAlert),
                        true);
            }
        });
    }
}
