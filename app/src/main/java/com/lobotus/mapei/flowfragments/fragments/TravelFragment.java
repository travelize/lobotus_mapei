package com.lobotus.mapei.flowfragments.fragments;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatAutoCompleteTextView;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.lobotus.mapei.R;
import com.lobotus.mapei.baseactivity.BaseActivity;
import com.lobotus.mapei.customwidgits.LabelledSpinner;
import com.lobotus.mapei.flowfragments.model.CustomerDataModel;
import com.lobotus.mapei.flowfragments.model.ModeOfTravelModel;
import com.lobotus.mapei.flowfragments.model.MotByUidModel;
import com.lobotus.mapei.flowfragments.model.PurposeOfTravelModel;
import com.lobotus.mapei.flowfragments.model.TravelImageModel;
import com.lobotus.mapei.flowfragments.mvp.presentor.TravelPresentor;
import com.lobotus.mapei.flowfragments.mvp.presentorimp.TravelPresentorImp;
import com.lobotus.mapei.flowfragments.mvp.views.TravelView;
import com.lobotus.mapei.flowfragments.onactivityforresult.AddImageCaption;
import com.lobotus.mapei.flowfragments.onactivityforresult.CustomerActForResult;
import com.lobotus.mapei.retrofitapi.APIService;
import com.lobotus.mapei.retrofitapi.ApiUtils;
import com.lobotus.mapei.utils.CommonFunc;
import com.lobotus.mapei.utils.ConstantsUtils;
import com.lobotus.mapei.utils.ImageUtility;
import com.lobotus.mapei.utils.NetworkUtils;
import com.lobotus.mapei.utils.PermissionUtils;
import com.lobotus.mapei.utils.PreferenceUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HttpsURLConnection;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class TravelFragment extends Fragment  implements LabelledSpinner.OnItemChosenListener,
        View.OnClickListener,TravelView{
    // req code to get thecustomer seltion done. . .
    private final int REQ_CODE_TO_GET_THE_CUSTOMR_DATA=11;
    // TEMP CUSTOMER ID. .
    private String TEMP_CUST_ID=null;
    private String TEMP_CUST_LOC="";
    //
    private  View fragmentView=null;
    private boolean isUserDontHaveOwnvehical=false;
    /* String[] stringsfourwheeler;
     String[] stringstwowheeler;*/
    //
    private TravelPresentor travelPresentor=null;
    ////// temp data's
    private ArrayList<ModeOfTravelModel> modeOfTravelModels=new ArrayList<>();
    private ArrayList<PurposeOfTravelModel> purposeOfTravelModels=new ArrayList<>();

    private LinkedList<TravelImageModel> travelImageModels=new LinkedList<>();
    /// travel images adapter
    private TravelImageAdapter travelImageAdapter=null;
    ///// request codes
    private  final int SELECT_FILE = 20;
    private final  int REQUEST_CAMERA=21;
    //
    private final int SELECT_FILE_CAPTION=22;
    private final int SELECT_CAMERA_CAPTION=23;
    //
    private Uri uriCachedBuffer=null;
    //
    private int locationAutoFillNeededButtonId=0;


    ////////////////////////////////////////////////////////////// life cycle stuff
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fragmentView= inflater.inflate(R.layout.fragment_travel, container, false);
        fragmentView.findViewById(R.id.fragment_travel_submit_button_id).setOnClickListener(this);
        fragmentView.findViewById(R.id.fragment_travel_add_customer_id).setOnClickListener(this);
        fragmentView.findViewById(R.id.fragment_travel_start_location_button_id).setOnClickListener(this);
        fragmentView.findViewById(R.id.fragment_travel_client_location_button_id).setOnClickListener(this);
        fragmentView.findViewById(R.id.fragment_travel_autocompletetextview_id).setOnClickListener(this);

        ((AppCompatAutoCompleteTextView)fragmentView.findViewById(R.id.fragment_travel_autocompletetextview_id))
                .addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                ((LabelledSpinner)fragmentView.findViewById(R.id.fragment_travel_mode_of_travel_spinner_id)).setSelection(0);
                if (((ViewStub)fragmentView.findViewById(R.id.fragment_travel_mode_of_travel_own_vehical_type))==null)
                {
                    fragmentView.findViewById(R.id.fragment_travel_mode_of_travel_own_vehical_type_viewstub_inflateId).setVisibility(View.GONE);
                }
                if (((ViewStub)fragmentView.findViewById(R.id.fragment_travel_foure_wheeler_vehical_type))==null)
                {
                    fragmentView.findViewById(R.id.fragment_travel_foure_wheeler_viewstub_inflateId).setVisibility(View.GONE);
                }

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {




            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        //// travel images stuff
        setTheInitialPlaceholderImage();
        travelPresentor=new TravelPresentorImp(this);
        travelPresentor.reqToGetMOTandPOTFromPresentor(getActivity());
        return fragmentView;
    }



    @Override
    public void onDetach() {
        super.onDetach();
        CommonFunc.hideKeyboard(getActivity());
    }
    /////////////////////////////////////////////////////////////////////// setting the data
    /////// image uplode stuff
    private void setTheInitialPlaceholderImage()
    {
        travelImageModels.clear();
        travelImageModels.add(new TravelImageModel(null,null,true,null,null));
        travelImageAdapter=new TravelImageAdapter();
        ((RecyclerView)fragmentView.findViewById(R.id.fragment_travel_add_imege_recycleview)).setLayoutManager(new
                LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,false));
        ((RecyclerView)fragmentView.findViewById(R.id.fragment_travel_add_imege_recycleview)).setAdapter(travelImageAdapter);
    }
    private void addattachmentImageToLinkedList(Bitmap bitmap, String caption,String amount)
    {

        String b64=ImageUtility.convertImageToBase64(bitmap,getActivity());
        travelImageModels.addFirst(new TravelImageModel(b64,bitmap,false,caption,amount));
        travelImageAdapter.notifyDataSetChanged();
    }



    ///////////////////////////////////////// image adapter
    private class TravelImageAdapter extends RecyclerView.Adapter<TravelImageAdapter.TravelImageHolder>
    {
        @Override
        public TravelImageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view=LayoutInflater.from(getActivity()).inflate(R.layout.fragment_travel_image_row,parent,false);
            return new TravelImageHolder(view);
        }

        @Override
        public void onBindViewHolder(final TravelImageHolder holder, int position) {
            if (travelImageModels.get(holder.getAdapterPosition()).isAddImageButton())
            {
                holder.imageViewDelete.setVisibility(View.GONE);
                holder.imageViewTravelImageView.setImageDrawable(ContextCompat.getDrawable(getActivity(),R.drawable.ic_add_image));
                holder.textViewImageCaption.setVisibility(View.GONE);
                holder.textViewAmount.setVisibility(View.GONE);

            }else {
                holder.imageViewDelete.setVisibility(View.VISIBLE);
                holder.textViewImageCaption.setVisibility(View.VISIBLE);
                holder.textViewAmount.setVisibility(View.VISIBLE);
                holder.imageViewTravelImageView.setImageBitmap(travelImageModels.get(holder.getAdapterPosition()).getBitmap());
                if (!travelImageModels.get(holder.getAdapterPosition()).getImageCaption().equals(""))
                {
                    holder.textViewImageCaption.setText(travelImageModels.get(holder.getAdapterPosition()).getImageCaption());
                }else {
                    holder.textViewImageCaption.setText("No Caption");
                }
                //
                if (!travelImageModels.get(holder.getAdapterPosition()).getAmount().equals(""))
                {

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                        holder.textViewAmount.setText(Html.fromHtml("&#8377; "+travelImageModels.get(holder.getAdapterPosition()).getAmount(),0));
                    else
                        holder.textViewAmount.setText(Html.fromHtml("&#8377; "+travelImageModels.get(holder.getAdapterPosition()).getAmount()));
                }else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                        holder.textViewAmount.setText(Html.fromHtml("No "+"&#8377; ",0));
                    else
                        holder.textViewAmount.setText(Html.fromHtml("No "+"&#8377; "));
                }

            }
            holder.imageViewTravelImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (travelImageModels.get(holder.getAdapterPosition()).isAddImageButton())
                    {
                        if (travelImageModels.size()<6)
                            selectUploadImage();
                        else
                            Toast.makeText(getActivity(), "Maximum image upload limit is 5.", Toast.LENGTH_SHORT).show();
                    }

                }
            });
            holder.imageViewDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    travelImageModels.remove(holder.getAdapterPosition());
                    notifyDataSetChanged();
                }
            });

        }

        @Override
        public int getItemCount() {
            return travelImageModels.size();
        }

        class TravelImageHolder extends RecyclerView.ViewHolder
        {
            AppCompatImageView imageViewTravelImageView=null,imageViewDelete=null;
            AppCompatTextView textViewImageCaption=null;
            AppCompatTextView textViewAmount=null;
            TravelImageHolder(View itemView) {
                super(itemView);
                this.imageViewTravelImageView=itemView.findViewById(R.id.fragment_travel_image_row_imageview_id);
                this.imageViewDelete=itemView.findViewById(R.id.fragment_travel_image_row_delete_imageview);
                this.textViewImageCaption=itemView.findViewById(R.id.fragment_travel_image_row_image_caption_id);
                this.textViewAmount=itemView.findViewById(R.id.fragment_travel_image_row_amount_id);
            }
        }
    }

    private void selectUploadImage() {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getActivity());
        builder.setTitle("Upload Pictures Option");
        builder.setMessage("How do you want to set your picture?");
        builder.setPositiveButton("Gallery", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        if (PermissionUtils.checkStoragePermission(getActivity())) {
                            openGalary();
                        }else {
                            PermissionUtils.reqForStoragePermission(getActivity());

                        }
                    }
                }
        );
        builder.setNegativeButton("Camera", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        if (PermissionUtils.checkCameraPermissionAndStoragePermission(getActivity())) {
                            openCamera();
                        }else {
                            PermissionUtils.reqForCameraPermissionWithStorage(getActivity());
                        }
                    }
                }
        );
        android.app.AlertDialog dialog = builder.create();
        dialog.show();

/*        if (PermissionUtils.checkCameraPermissionAndStoragePermission(getActivity())) {
            openCamera();
        }else {
            PermissionUtils.reqForCameraPermissionWithStorage(getActivity());
        }*/
    }
    private void openGalary()
    {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }
    private void openCamera()
    {
        uriCachedBuffer=null;
        uriCachedBuffer= ImageUtility.saveImageInCachaFolder(TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis())+"",getActivity());
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.putExtra(MediaStore.EXTRA_OUTPUT,uriCachedBuffer);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    public void OnCameraPermissionResult(boolean isGranted) {
        if (isGranted)
            openCamera();
        else
            PermissionUtils.reqForCameraPermissionWithStorage(getActivity());


    }
    public void OnStoragePermissionResult(boolean isGranted){
        if (isGranted)
            openGalary();
        else
            PermissionUtils.reqForStoragePermission(getActivity());

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode==getActivity().RESULT_OK)
        {
            if (requestCode==SELECT_FILE)
            {
                Intent intent=new Intent(getActivity(), AddImageCaption.class);
                intent.putExtra("File_uri",data.getData().toString());
                intent.putExtra("isTypeFile",true);
                startActivityForResult(intent,SELECT_FILE_CAPTION);
            }
            if (requestCode==REQUEST_CAMERA)
            {

                Intent intent=new Intent(getActivity(), AddImageCaption.class);
                intent.putExtra("File_uri",uriCachedBuffer.toString());
                intent.putExtra("isTypeFile",false);
                startActivityForResult(intent,SELECT_CAMERA_CAPTION);
            }
            if (requestCode==SELECT_FILE_CAPTION)
            {
                OnFileResult(data) ;
            }
            if (requestCode==SELECT_CAMERA_CAPTION)
            {
                OnCameraResult(data);
            }
            if (requestCode==REQ_CODE_TO_GET_THE_CUSTOMR_DATA)
            {
                CustomerDataModel customerDataModel=data.getExtras().getParcelable("customer_model");
                ((AppCompatAutoCompleteTextView)fragmentView.findViewById(R.id.fragment_travel_autocompletetextview_id))
                        .setText(""+customerDataModel.getCustomerName());
                TEMP_CUST_ID=customerDataModel.getCustomerID();
                TEMP_CUST_LOC=customerDataModel.getLocation();
                //////////////////////////////////////////////////////
                String userId=PreferenceUtils.getUserIdFromPreference(getActivity());
                if (userId!=null)
                    downlodeMotByUid(userId);
                else
                    CommonFunc.commonDialog(getActivity(),getString(R.string.alert),"Data not loaded",false);

            }
        }

    }

    private void OnFileResult(Intent intent)
    {
        Bitmap bm=null;
        if (intent != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getActivity().getApplicationContext()
                        .getContentResolver(), Uri.parse(intent.getStringExtra("File_uri")));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (bm!=null)
        {
//            Bitmap bitmap=Bitmap.createScaledBitmap(bm,1080,600,false);
            Bitmap bitmap=ImageUtility.getResizedBitmap(bm,400);
            addattachmentImageToLinkedList(bitmap,intent.getStringExtra("caption"),intent.getStringExtra("amount"));
        }

    }
    private void OnCameraResult(final Intent intent)
    {
      /*  Bitmap thumbnail = (Bitmap) intent.getExtras().get("data");
        addattachmentImageToLinkedList(thumbnail, intent.getStringExtra("caption"));*/

        Glide.with(getActivity()).load(intent.getStringExtra("File_uri")).asBitmap()
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                        if (resource!=null)
                        {
                            Bitmap bitmap=ImageUtility.getResizedBitmap(resource,400);
                            addattachmentImageToLinkedList(bitmap, intent.getStringExtra("caption"),intent.getStringExtra("amount"));
                        }
                    }
                });
    }

    ////////////////////// image uplode stuff end

    private void inflatefourewheelerType(String[] stringsfourwheeler)
    {

        ((LabelledSpinner)fragmentView.findViewById(R.id.fore_wheeler_vehical_type_spinner_id))
                .setItemsArray(stringsfourwheeler);
        ((LabelledSpinner)fragmentView.findViewById(R.id.fore_wheeler_vehical_type_spinner_id))
                .setDefaultErrorEnabled(false);
        ((LabelledSpinner)fragmentView.findViewById(R.id.fore_wheeler_vehical_type_spinner_id))
                .setDefaultErrorText("Select from Dropdown");
    }
    private void inflateVehicalTypeSpinner(final String[] stringstwowheeler, final boolean isTypeDeselFor4vehiler)
    {

        ((LabelledSpinner)fragmentView.findViewById(R.id.own_vehical_type_spinner_id))
                .setItemsArray(stringstwowheeler);
        ((LabelledSpinner)fragmentView.findViewById(R.id.own_vehical_type_spinner_id))
                .setDefaultErrorEnabled(false);
        ((LabelledSpinner)fragmentView.findViewById(R.id.own_vehical_type_spinner_id))
                .setDefaultErrorText("Select from Dropdown");
        ((LabelledSpinner)fragmentView.findViewById(R.id.own_vehical_type_spinner_id))
                .setOnItemChosenListener(new LabelledSpinner.OnItemChosenListener() {
            @Override
            public void onItemChosen(View labelledSpinner, AdapterView<?> adapterView, View itemView, int position, long id) {
                if (isUserDontHaveOwnvehical)
                {
                    switch (position)
                    {
                        case 1: if (((ViewStub)fragmentView.findViewById(R.id.fragment_travel_foure_wheeler_vehical_type))!=null)
                        {
                            ((ViewStub)fragmentView.findViewById(R.id.fragment_travel_foure_wheeler_vehical_type))
                                    .inflate();

                        }else {
                            fragmentView.findViewById(R.id.fragment_travel_foure_wheeler_viewstub_inflateId).setVisibility(View.VISIBLE);
                        }

                            inflatefourewheelerType(new String[]{"Diesel","Petrol"});
                            break;
                        default:
                            if (((ViewStub)fragmentView.findViewById(R.id.fragment_travel_foure_wheeler_vehical_type))==null)
                            {
                                fragmentView.findViewById(R.id.fragment_travel_foure_wheeler_viewstub_inflateId).setVisibility(View.GONE);
                            }
                    }
                }else {
                    if (stringstwowheeler.length==2)
                        switch (position)
                        {
                            case 1: if (((ViewStub)fragmentView.findViewById(R.id.fragment_travel_foure_wheeler_vehical_type))!=null)
                            {
                                ((ViewStub)fragmentView.findViewById(R.id.fragment_travel_foure_wheeler_vehical_type))
                                        .inflate();

                            }else {
                                fragmentView.findViewById(R.id.fragment_travel_foure_wheeler_viewstub_inflateId).setVisibility(View.VISIBLE);
                            }
                                if (isTypeDeselFor4vehiler)
                                    inflatefourewheelerType(new String[]{"Diesel"});
                                else
                                    inflatefourewheelerType(new String[]{"Petrol"});
                                break;
                            default:
                                if (((ViewStub)fragmentView.findViewById(R.id.fragment_travel_foure_wheeler_vehical_type))==null)
                                {
                                    fragmentView.findViewById(R.id.fragment_travel_foure_wheeler_viewstub_inflateId).setVisibility(View.GONE);
                                }
                        }
                }


            }

            @Override
            public void onNothingChosen(View labelledSpinner, AdapterView<?> adapterView) {

            }
        });

    }

    private void inflatemodeOfTravelSpinner(List<ModeOfTravelModel> modeOfTravelModelss)
    {
        modeOfTravelModels.clear();
        modeOfTravelModels.addAll(modeOfTravelModelss);
        String[] strings=new String[modeOfTravelModels.size()];
        for (int i=0;i<strings.length;i++)
        {
            strings[i]=modeOfTravelModels.get(i).getMOT();
        }
        ((LabelledSpinner)fragmentView.findViewById(R.id.fragment_travel_mode_of_travel_spinner_id))
                .setItemsArray(strings);
        ((LabelledSpinner)fragmentView.findViewById(R.id.fragment_travel_mode_of_travel_spinner_id))
                .setDefaultErrorEnabled(false);
        ((LabelledSpinner)fragmentView.findViewById(R.id.fragment_travel_mode_of_travel_spinner_id))
                .setDefaultErrorText("Select from Dropdown");
        ((LabelledSpinner)fragmentView.findViewById(R.id.fragment_travel_mode_of_travel_spinner_id)).setOnItemChosenListener(this);
    }
    private void inflatemeetingPurposeSpinner(List<PurposeOfTravelModel> purposeOfTravelModelss)
    {
        purposeOfTravelModels.clear();
        purposeOfTravelModels.addAll(purposeOfTravelModelss);
        String[] strings=new String[purposeOfTravelModels.size()];
        for (int i=0;i<strings.length;i++)
        {
            strings[i]=purposeOfTravelModels.get(i).getPurpose();
        }
        ((LabelledSpinner)fragmentView.findViewById(R.id.fragment_travel_meeting_purpose_spinner_id))
                .setItemsArray(strings);
        ((LabelledSpinner)fragmentView.findViewById(R.id.fragment_travel_meeting_purpose_spinner_id))
                .setDefaultErrorEnabled(false);
        ((LabelledSpinner)fragmentView.findViewById(R.id.fragment_travel_meeting_purpose_spinner_id))
                .setDefaultErrorText("Select from Dropdown");
        ((LabelledSpinner)fragmentView.findViewById(R.id.fragment_travel_meeting_purpose_spinner_id)).setOnItemChosenListener(this);
    }
    @Override
    public void onItemChosen(View labelledSpinner, AdapterView<?> adapterView, View itemView, int position, long id) {

        if (((AppCompatAutoCompleteTextView)fragmentView.findViewById(R.id.fragment_travel_autocompletetextview_id)).getText().toString().isEmpty())
        {
            ((LabelledSpinner)fragmentView.findViewById(R.id.fragment_travel_mode_of_travel_spinner_id)).getSpinner().setSelection(0);
            ((LabelledSpinner)fragmentView.findViewById(R.id.fragment_travel_meeting_purpose_spinner_id)).getSpinner().setSelection(0);
            if (id!=0)
                Toast.makeText(getActivity(), "Please start with entering customer name", Toast.LENGTH_SHORT).show();
        }else {
            switch (labelledSpinner.getId())
                {
                    case R.id.fragment_travel_mode_of_travel_spinner_id:
                        System.out.println("vvvvvvvvvvvv---fragment_travel_mode_of_travel_spinner_id----------------"+position);
                        switch (position)
                        {

                            case 4:
                            if (isUserDontHaveOwnvehical)
                            {
                                if (((ViewStub)fragmentView.findViewById(R.id.fragment_travel_mode_of_travel_own_vehical_type))!=null)
                                {
                                    ((ViewStub)fragmentView.findViewById(R.id.fragment_travel_mode_of_travel_own_vehical_type))
                                            .inflate();
                                    inflateVehicalTypeSpinner(new String[]{"2 Wheeler","4 Wheeler"},false);
                                }else {
                                    fragmentView.findViewById(R.id.fragment_travel_mode_of_travel_own_vehical_type_viewstub_inflateId).setVisibility(View.VISIBLE);
                                    if (((LabelledSpinner)fragmentView.findViewById(R.id.own_vehical_type_spinner_id)).getSpinner().getSelectedItemPosition()==2)
                                    {
                                        if (((ViewStub)fragmentView.findViewById(R.id.fragment_travel_foure_wheeler_vehical_type))!=null)
                                        {
                                            ((ViewStub)fragmentView.findViewById(R.id.fragment_travel_foure_wheeler_vehical_type))
                                                    .inflate();
                                            inflatefourewheelerType(new String[]{"Diesel","Petrol"});
                                        }else {
                                            fragmentView.findViewById(R.id.fragment_travel_foure_wheeler_viewstub_inflateId).setVisibility(View.VISIBLE);
                                        }
                                    }
                                }
                            }else {
                                downlodeMotByUid(PreferenceUtils.getUserIdFromPreference(getActivity()));
                            }


                                break;
                            default:
                                if (((ViewStub)fragmentView.findViewById(R.id.fragment_travel_mode_of_travel_own_vehical_type))==null)
                                {
                                    fragmentView.findViewById(R.id.fragment_travel_mode_of_travel_own_vehical_type_viewstub_inflateId).setVisibility(View.GONE);
                                }
                                if (((ViewStub)fragmentView.findViewById(R.id.fragment_travel_foure_wheeler_vehical_type))==null)
                                {
                                    fragmentView.findViewById(R.id.fragment_travel_foure_wheeler_viewstub_inflateId).setVisibility(View.GONE);
                                }
                        }
                        break;
                    case R.id.fragment_travel_meeting_purpose_spinner_id:
                        switch (position)
                        {
                            case 4:
                                ((TextInputLayout)fragmentView.findViewById(R.id.fragment_travel_start_location_inputlayout_id)).setHint("Start location");
                                ((TextInputLayout)fragmentView.findViewById(R.id.fragment_travel_client_location_inputlayout_id)).setHint("End location");
                                //
                                ((AppCompatEditText)fragmentView.findViewById(R.id.fragment_travel_start_location_edittext_id)).setText("");
                                ((AppCompatEditText)fragmentView.findViewById(R.id.fragment_travel_client_location_edittext_id)).setText("");
                                break;
                            default:
                                ((TextInputLayout)fragmentView.findViewById(R.id.fragment_travel_start_location_inputlayout_id)).setHint("Start location");
                                ((TextInputLayout)fragmentView.findViewById(R.id.fragment_travel_client_location_inputlayout_id)).setHint("Client location");
                                //
                                loadStartLocWithMyLocAndEndLocWithClientLoc();
                        }
                }


        }

    }

    private boolean checkCustomerNameIsMatchingTheList(ArrayList<CustomerDataModel> customerDataModels,
                                                       String customerNameInAutoCompletetextView)
    {
        boolean flag=false;
        for (int i=0;i<customerDataModels.size();i++)
        {
            if (customerDataModels.get(i).getCustomerName().equals(customerNameInAutoCompletetextView))
            {
                flag=true;
                break;
            }
        }
        return flag;
    }

    private void loadStartLocWithMyLocAndEndLocWithClientLoc()
    {
        fragmentView.findViewById(R.id.fragment_travel_start_location_button_id).performClick();
        ((AppCompatEditText)fragmentView.findViewById(R.id.fragment_travel_client_location_edittext_id)).setText(
                TEMP_CUST_LOC);
    }

    @Override
    public void onNothingChosen(View labelledSpinner, AdapterView<?> adapterView) {

    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {

            case R.id.fragment_travel_autocompletetextview_id:
                Intent intent =new Intent(getActivity(), CustomerActForResult.class);
                startActivityForResult(intent,REQ_CODE_TO_GET_THE_CUSTOMR_DATA);
                break;

            case R.id.fragment_travel_submit_button_id:
                if (PreferenceUtils.getIsUserCheckedIn(getActivity()))
                    travelPresentor.reqToValidateAllInPresentorItself(fragmentView,
                            getActivity(),((AppCompatAutoCompleteTextView)fragmentView.findViewById(R.id.fragment_travel_autocompletetextview_id)).getText().toString(),
                         ((LabelledSpinner)fragmentView.findViewById(R.id.fragment_travel_mode_of_travel_spinner_id)).getSpinner().getSelectedItemPosition(),
                            modeOfTravelModels.isEmpty(),
                            ((LabelledSpinner)fragmentView.findViewById(R.id.fragment_travel_meeting_purpose_spinner_id)).getSpinner().getSelectedItemPosition(),
                            purposeOfTravelModels.isEmpty(),
                            fragmentView.findViewById(R.id.own_vehical_type_spinner_id)
                                    ==null?0:((LabelledSpinner)fragmentView.findViewById(R.id.own_vehical_type_spinner_id)).getSpinner().getSelectedItemPosition(),
                            fragmentView.findViewById(R.id.fore_wheeler_vehical_type_spinner_id)
                                    ==null?0:((LabelledSpinner)fragmentView.findViewById(R.id.fore_wheeler_vehical_type_spinner_id)).getSpinner().getSelectedItemPosition());
                else
                    CommonFunc.commonDialog(getActivity(),getString(R.string.alert),getString(R.string.chekInAlert),false);
                break;
            case R.id.fragment_travel_add_customer_id:
                ((BaseActivity)getActivity()).attachFragment(new AddCustomers(),true,
                        getString(R.string.AddCustomers),null);
                break;
            case R.id.fragment_travel_start_location_button_id:
                locationAutoFillNeededButtonId=R.id.fragment_travel_start_location_edittext_id;
                connectToGoogleApiClientAndGetTheAddress();
                break;
            case R.id.fragment_travel_client_location_button_id:
                locationAutoFillNeededButtonId=R.id.fragment_travel_client_location_edittext_id;
                connectToGoogleApiClientAndGetTheAddress();


        }
    }


    /////////////////////////////////////////////////////////////////////////////////////// permission

    /////////////////////////////////////////////////////////////////////////////////////// mvp stuff
    private Dialog dialog=null;
    @Override
    public void showProgressDialog() {
        dialog=new Dialog(getActivity());
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.loaging_layout);
        dialog.show();
    }

    @Override
    public void dismissProgressDialog() {
        if (getActivity()!=null)
            if (dialog!=null)
            {
                if (dialog.isShowing())
                {
                    dialog.dismiss();
                }
                dialog=null;
            }
    }

    @Override
    public void afterCustomerModelDownlodeSuccess(ArrayList<CustomerDataModel> customerDataModelss) {

     /*   customerDataModels.clear();
        customerDataModels.addAll(customerDataModelss);
        String[] strings=new String[customerDataModels.size()];
        for (int i=0;i<strings.length;i++)
        {
            strings[i]=customerDataModels.get(i).getCustomerName();
        }
        ArrayAdapter<String> arrayAdapterUser = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_list_item_1, strings);
        ((AppCompatAutoCompleteTextView)fragmentView.findViewById(R.id.fragment_travel_autocompletetextview_id))
                .setAdapter(arrayAdapterUser);*/


    }

    @Override
    public void afterCustomerModelDownlodeFail(String message, String dialogTitle, boolean isInternetError) {
        CommonFunc.commonDialog(getActivity(),dialogTitle,message,isInternetError);
    }
    ////// MOT and POT stuff (mode of travel and the purpose of the travel)
    @Override
    public void afterMOTandPOTDownlodeSuccess(List<ModeOfTravelModel> modeOfTravelModels, List<PurposeOfTravelModel> purposeOfTravelModels) {
        inflatemodeOfTravelSpinner(modeOfTravelModels);
        inflatemeetingPurposeSpinner(purposeOfTravelModels);
    }

    @Override
    public void afterMOTandPOTDownlodeFail(String message, String dialogTitle, boolean isInternetError) {
        CommonFunc.commonDialog(getActivity(),dialogTitle,message,isInternetError);
    }
    //// validating
    @Override
    public void afterValidationSuccess() {
        //ontext context, String remark, String custId, String modeOfTravel, String purpose, String attachmentB6
        String image="";
        if (travelImageModels.size()>1)
        {
            ArrayList<TravelImageModel> models=new ArrayList<>();
            for (int i=0;i<travelImageModels.size();i++)
            {
                if (!travelImageModels.get(i).isAddImageButton())
                {
                    models.add(travelImageModels.get(i));
                }
            }
            for (int i=0;i<models.size();i++)
            {
                if (models.size()==1)
                {
                    image=models.get(i).get_b64String();
                }else {
                    if (!image.isEmpty())
                        image=image+","+models.get(i).get_b64String();
                    else
                        image=models.get(i).get_b64String();

                }
            }
        }
        String imageCaption="";
        if (travelImageModels.size()>1)
        {
            ArrayList<TravelImageModel> models=new ArrayList<>();
            for (int i=0;i<travelImageModels.size();i++)
            {
                if (!travelImageModels.get(i).isAddImageButton())
                {
                    models.add(travelImageModels.get(i));
                }
            }
            for (int i=0;i<models.size();i++)
            {
                if (models.size()==1)
                {
                    imageCaption=models.get(i).getImageCaption();
                }else {
                    if (!imageCaption.isEmpty())
                        imageCaption=imageCaption+","+models.get(i).getImageCaption();
                    else
                        imageCaption=models.get(i).getImageCaption();

                }
            }
        }
        /////// image amount

        String imageAmount="";
        if (travelImageModels.size()>1)
        {
            ArrayList<TravelImageModel> models=new ArrayList<>();
            for (int i=0;i<travelImageModels.size();i++)
            {
                if (!travelImageModels.get(i).isAddImageButton())
                {
                    models.add(travelImageModels.get(i));
                }
            }
            for (int i=0;i<models.size();i++)
            {
                if (models.size()==1)
                {
                    imageAmount=models.get(i).getAmount();
                }else {
                    if (!imageAmount.isEmpty())
                        imageAmount=imageAmount+","+models.get(i).getAmount();
                    else
                        imageAmount=models.get(i).getAmount();

                }
            }
        }


//        ImageUtility.writeToFile(image);
        travelPresentor.reqToSaveTheTravelDetails(getActivity(),
                ((AppCompatEditText)fragmentView.findViewById(R.id.fragment_travel_notes_textview_id)).getText().toString(),TEMP_CUST_ID,
                modeOfTravelModels.get(((LabelledSpinner)fragmentView.findViewById(R.id.fragment_travel_mode_of_travel_spinner_id)).getSpinner().getSelectedItemPosition()).getMOT(),
                purposeOfTravelModels.get(((LabelledSpinner)fragmentView.findViewById(R.id.fragment_travel_meeting_purpose_spinner_id)).getSpinner().getSelectedItemPosition()).getPurpose(),
                image,((AppCompatEditText)fragmentView.findViewById(R.id.fragment_travel_start_location_edittext_id)).getText().toString(),
                ((AppCompatEditText)fragmentView.findViewById(R.id.fragment_travel_client_location_edittext_id)).getText().toString() ,imageCaption,
                fragmentView.findViewById(R.id.own_vehical_type_spinner_id)
                        ==null?"":String.valueOf(((LabelledSpinner)fragmentView.findViewById(R.id.own_vehical_type_spinner_id)).getSpinner().getSelectedItem()),
                fragmentView.findViewById(R.id.fore_wheeler_vehical_type_spinner_id)
                        ==null?"":String.valueOf(((LabelledSpinner)fragmentView.findViewById(R.id.fore_wheeler_vehical_type_spinner_id)).getSpinner().getSelectedItem()),
                imageAmount
        );
    }

    ///// after travel post success
    @Override
    public void afterTravelPostSuccess(String message) {
        setTheInitialPlaceholderImage();
        ((AppCompatAutoCompleteTextView)fragmentView.findViewById(R.id.fragment_travel_autocompletetextview_id)).setText("");
        ((AppCompatEditText)fragmentView.findViewById(R.id.fragment_travel_start_location_edittext_id)).setText("");
        ((AppCompatEditText)fragmentView.findViewById(R.id.fragment_travel_client_location_edittext_id)).setText("");
        ((BaseActivity)getActivity()).attachSuccessFragment(message,"Add more Travel details");
    }

    @Override
    public void afterTravelPostFail(String message, String dialogTitle, boolean isInternetError) {
        CommonFunc.commonDialog(getActivity(),dialogTitle,message,isInternetError);
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////// location fetch stuff. . . .
    private GoogleApiClient googleApiClient = null;
    private LocationRequest locationRequest = null;
    private FusedLocationProviderClient mFusedLocationClient = null;
    private AsyncTaskToGetTheAddress asyncTaskToGetTheAddress=null;
    private void connectToGoogleApiClientAndGetTheAddress() {
        if (NetworkUtils.checkInternetAndOpenDialog(getActivity())) {
            if (CommonFunc.isGooglePlayServicesAvailable(getActivity()))
            {
                if (dialog == null)
                {
                    showProgressDialog();
                    connectGoogleApiClient();
                }
            }
        }
    }

    private void connectGoogleApiClient()
    {

        googleApiClient = new GoogleApiClient.Builder(getActivity())
                .addApi(LocationServices.API)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(@Nullable Bundle bundle) {
                        checkForLocationSettings();
                    }
                    @Override
                    public void onConnectionSuspended(int i) {
                    }
                })
                .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                        Toast.makeText(getActivity(), "Google play service not responding... please refresh your mobile",
                                Toast.LENGTH_LONG).show();
                        googleApiClient=null;
                        dismissProgressDialog();

                    }
                }).build();
        googleApiClient.connect();


    }

    private void checkForLocationSettings()
    {
        ////////////////////////////////////////////////////////

        locationRequest =new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);

        //**************************
        builder.setAlwaysShow(true); //this is the key ingredient
        //**************************

        Task<LocationSettingsResponse> locationSettingsResponseTask =
                LocationServices.getSettingsClient(getActivity()).checkLocationSettings(builder.build());
        //
        locationSettingsResponseTask.addOnCompleteListener(new OnCompleteListener<LocationSettingsResponse>() {
            @Override
            public void onComplete(@NonNull Task<LocationSettingsResponse> task) {
                try {
                    LocationSettingsResponse response = task.getResult(ApiException.class);
                    // All location settings are satisfied. The client can initialize location
                    // requests here.
                    if (PermissionUtils.checkLoactionPermission(getActivity()) && NetworkUtils.isConnected(getActivity())) {
                        getLastLocation();
                    }else {
                        dismissProgressDialog();
                        if (!PermissionUtils.checkLoactionPermission(getActivity()))
                        {
                            PermissionUtils.openAppSettings(getActivity());

                        }else {
                            NetworkUtils.checkInternetAndOpenDialog(getActivity());
                        }
                    }

                } catch (ApiException exception) {
                    dismissProgressDialog();

                    switch (exception.getStatusCode()) {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // Location settings are not satisfied. But could be fixed by showing the
                            // user a dialog.
                            try {
                                NetworkUtils.openGpsSettings(getActivity());
                            } catch (ClassCastException e) {
                                CommonFunc.commonDialog(getActivity(),getString(R.string.alert),
                                        getString(R.string.justErrorCode)+" 86",false);
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            CommonFunc.commonDialog(getActivity(),"GPS not working",
                                    getString(R.string.justErrorCode)+" 87",false);
                            break;
                    }
                }
            }
        });
        //

    }

    @SuppressWarnings("MissingPermission")
    private void getLastLocation() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());
        mFusedLocationClient.requestLocationUpdates(locationRequest,locationCallback,null);

    }

    private LocationCallback locationCallback=new LocationCallback()
    {
        @Override
        public void onLocationResult(LocationResult locationResult) {

            for (Location location : locationResult.getLocations()) {
                decodeAddress(new LatLng(location.getLatitude(),
                        location.getLongitude()));
                stopLocationUpdates();
            }
        }
    };

    private void decodeAddress(final LatLng latLng)
    {
        //https://maps.googleapis.com/maps/api/geocode/json?latlng=40.714224,-73.961452&key=YOUR_API_KEY
        String req="https://maps.googleapis.com/maps/api/geocode/json?latlng="+
                latLng.latitude+","+latLng.longitude+"&key="+getString(R.string.google_key);


        if (asyncTaskToGetTheAddress!=null)
        {
            if (asyncTaskToGetTheAddress.getStatus()!= AsyncTask.Status.RUNNING)
            {
                asyncTaskToGetTheAddress=new AsyncTaskToGetTheAddress();
                asyncTaskToGetTheAddress.execute(req);
            }
        }else {
            asyncTaskToGetTheAddress=new AsyncTaskToGetTheAddress();
            asyncTaskToGetTheAddress.execute(req);
        }
    }

    private class  AsyncTaskToGetTheAddress extends AsyncTask<String, Void, String>
    {

        @Override
        protected String doInBackground(String... strings) {
            return makeserverConnection(strings[0]);
        }
        @Override
        protected void onPostExecute(String s) {
            dismissProgressDialog();
            if (s!=null)
            {
                try {
                    JSONObject jsonObject=new JSONObject(s);
                    JSONArray jsonArray=jsonObject.getJSONArray("results");
                    if (jsonArray.length()>0)
                    {
                        String formatedAddress=jsonArray.getJSONObject(0).getString("formatted_address");
                        if (formatedAddress!=null && !formatedAddress.isEmpty()) {
                            ((AppCompatEditText) fragmentView.findViewById(locationAutoFillNeededButtonId)).setText(formatedAddress);
                        }
                        else {
                            CommonFunc.commonDialog(getActivity(),"Alert!", "Unable to detect your location",false);
                        }

                    }else {
                        CommonFunc.commonDialog(getActivity(),"Alert!", "Unable to detect your location",false);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }else {
                CommonFunc.commonDialog(getActivity(),"Alert!", "Unable to detect your location",false);
            }
        }


    }

    public String makeserverConnection(String requestURL) {
        URL url;
        String response = "";
        try {
            url = new URL(requestURL);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded");
            conn.setDoOutput(true);
            int responseCode = conn.getResponseCode();
            if (responseCode == HttpsURLConnection.HTTP_OK) {

                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line = br.readLine()) != null) {
                    response += line;
                }
            } else {
                response = "";
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }
    private void stopLocationUpdates() {
        if (mFusedLocationClient!=null)
            mFusedLocationClient.removeLocationUpdates(locationCallback);
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////// mvp tress pass for auto travel by . . .



    private void downlodeMotByUid(String uid)
    {

        showProgressDialog();
        APIService apiService= ApiUtils.getAPIService(ConstantsUtils.postBaseUrl+ConstantsUtils.POST_TO_GET_CUSTOMER_MOT_BY_ID);
        apiService.postToGetMotByUid(uid).enqueue(new Callback<MotByUidModel>() {
            @SuppressLint("NewApi")
            @Override
            public void onResponse(Call<MotByUidModel> call, Response<MotByUidModel> response) {
                if (response.isSuccessful())
                {
                    if (response.body().getSuccess()==1)
                    {
                        if (response.body().getData().get(0).getMode().equals("Own Vehicle"))
                        {
                            isUserDontHaveOwnvehical=false;
                            ((LabelledSpinner)fragmentView.findViewById(R.id.fragment_travel_mode_of_travel_spinner_id)).setSelection(4);

                            String[] Type=response.body().getData().get(0).getType().split(",");
                            if (Type.length!=2)
                            {
                                if (Type[0].equals("2 Wheeler")) {

                                    InFlateOnvehicalTypeBasedOnSelection(new String[]{"2 Wheeler"},false);
                                }
                                else {
                                    InFlateOnvehicalTypeBasedOnSelection(new String[]{"4 Wheeler"},response.body().getData().get(0).getVehicle().equals("Diesel"));


                                    if (response.body().getData().get(0).getVehicle().equals("Diesel")) {
                                        Inflate4vehilerTypeBasedOnSelection(new String[]{"Diesel"});
                                    }
                                    else {
                                        Inflate4vehilerTypeBasedOnSelection(new String[]{"Petrol"});

                                    }
                                }
                            }else {
                                InFlateOnvehicalTypeBasedOnSelection(new String[]{"2 Wheeler","4 Wheeler"},response.body().getData().get(0).getVehicle().equals("Diesel"));
                            }
                        }else {
                            // no vehical. . . .
                            isUserDontHaveOwnvehical=true;
                        }
                    }else {

                        CommonFunc.commonDialog(getActivity(),getString(R.string.internalError),response.body().getErrorMessage(),false);
                    }
                }else {

                    CommonFunc.commonDialog(getActivity(),getString(R.string.internalError),getString(R.string.justErrorCode)+" 118",false);
                }
                dismissProgressDialog();
            }

            @Override
            public void onFailure(Call<MotByUidModel> call, Throwable t) {
                dismissProgressDialog();
                CommonFunc.commonDialog(getActivity(),getString(R.string.noInternetAlert),getString(R.string.noInternetMessage)+" 119",true);

            }
        });
    }

    private void InFlateOnvehicalTypeBasedOnSelection(String[] stringsOwnVegicalType,boolean isTypeDeselFor4vehiler)
    {
        if (((ViewStub)fragmentView.findViewById(R.id.fragment_travel_mode_of_travel_own_vehical_type))!=null)
        {
            ((ViewStub)fragmentView.findViewById(R.id.fragment_travel_mode_of_travel_own_vehical_type))
                    .inflate();
            inflateVehicalTypeSpinner(stringsOwnVegicalType,isTypeDeselFor4vehiler);
        }else
        {
            fragmentView.findViewById(R.id.fragment_travel_mode_of_travel_own_vehical_type_viewstub_inflateId).setVisibility(View.VISIBLE);
            inflateVehicalTypeSpinner(stringsOwnVegicalType,isTypeDeselFor4vehiler);
        }
    }


    private void Inflate4vehilerTypeBasedOnSelection(String[] strings4Vehiler)
    {
        if (((ViewStub)fragmentView.findViewById(R.id.fragment_travel_foure_wheeler_vehical_type))!=null)
        {
            ((ViewStub)fragmentView.findViewById(R.id.fragment_travel_foure_wheeler_vehical_type))
                    .inflate();
            inflatefourewheelerType(strings4Vehiler);
        }else {
            fragmentView.findViewById(R.id.fragment_travel_foure_wheeler_viewstub_inflateId).setVisibility(View.VISIBLE);
            inflatefourewheelerType(strings4Vehiler);
        }
    }



}
