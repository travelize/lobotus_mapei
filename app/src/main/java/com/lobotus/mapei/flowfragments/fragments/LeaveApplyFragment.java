package com.lobotus.mapei.flowfragments.fragments;


import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatEditText;
import android.text.method.KeyListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;

import com.lobotus.mapei.R;
import com.lobotus.mapei.baseactivity.BaseActivity;
import com.lobotus.mapei.customwidgits.LabelledSpinner;
import com.lobotus.mapei.flowfragments.model.LeaveTypeDataModel;
import com.lobotus.mapei.flowfragments.mvp.presentor.LeaveApplyPresentor;
import com.lobotus.mapei.flowfragments.mvp.presentorimp.LeaveApplyPresentorImp;
import com.lobotus.mapei.flowfragments.mvp.views.LeaveApplyView;
import com.lobotus.mapei.utils.CommonFunc;
import com.lobotus.mapei.utils.PreferenceUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * A simple {@link Fragment} subclass.
 */
public class LeaveApplyFragment extends Fragment implements LeaveApplyView,View.OnClickListener {

    private   View fragmentView=null;
    private Dialog dialog=null;
    //
    private LeaveApplyPresentor leaveApplyPresentor=null;
    // temp data
    private ArrayList<LeaveTypeDataModel> leaveTypeDataModels=new ArrayList<>();

    //////////////////////////////////////////////////////////// life cycle stuff
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fragmentView= inflater.inflate(R.layout.fragment_leave_apply, container, false);

        fragmentView.findViewById(R.id.fragment_leave_submit_button_id).setOnClickListener(this);
        ((LabelledSpinner)fragmentView.findViewById(R.id.fragment_leave_type_for_same_day_spinner_id))
                .setItemsArray(getResources().getStringArray(R.array.same_day_leave_type)); // set the same day types. . .
        //
        KeyListener keyListenerstart;
        assert  ((AppCompatEditText)fragmentView.findViewById(R.id.fragment_leave_type_from_date_edittext)) != null;
        keyListenerstart= ((AppCompatEditText)fragmentView.findViewById(R.id.fragment_leave_type_from_date_edittext)).getKeyListener();
        ((AppCompatEditText)fragmentView.findViewById(R.id.fragment_leave_type_from_date_edittext)).setKeyListener(null);

        //
        KeyListener keyListenerend;
        assert  ((AppCompatEditText)fragmentView.findViewById(R.id.fragment_leave_type_to_date_edittext)) != null;
        keyListenerend= ((AppCompatEditText)fragmentView.findViewById(R.id.fragment_leave_type_to_date_edittext)).getKeyListener();
        ((AppCompatEditText)fragmentView.findViewById(R.id.fragment_leave_type_to_date_edittext)).setKeyListener(null);
        //
        leaveApplyPresentor=new LeaveApplyPresentorImp(this);
        //
        fragmentView.findViewById(R.id.fragment_leave_type_from_date_edittext).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myDatePicker(true);

            }
        });
        fragmentView.findViewById(R.id.fragment_leave_type_from_date_edittext).setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                myDatePicker(true);
            }
        });
        fragmentView.findViewById(R.id.fragment_leave_type_to_date_edittext).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myDatePicker(false);
            }
        });
        fragmentView.findViewById(R.id.fragment_leave_type_to_date_edittext).setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                myDatePicker(false);
            }
        });
        //
        leaveApplyPresentor.reqToPostLeaveType(getActivity());
        return fragmentView;
    }
    @Override
    public void onPause() {
        super.onPause();
        clearFields();
    }
    @Override
    public void onDetach() {
        super.onDetach();
        CommonFunc.hideKeyboard(getActivity());
    }
    ///////////////////////////////////////////////////////////////////////////////// life cycle stuff end
    //////////////////////////////////////// data setting stuff
    private void clearFields()
    {
        ((LabelledSpinner)fragmentView.findViewById(R.id.fragment_leave_type_spinner_id)).getSpinner().setSelection(0);
        ((AppCompatEditText)fragmentView.findViewById(R.id.fragment_leave_type_from_date_edittext)).setText("");
        ((AppCompatEditText)fragmentView.findViewById(R.id.fragment_leave_type_to_date_edittext)).setText("");
        ((AppCompatEditText)fragmentView.findViewById(R.id.fragment_leave_type_notes_id)).setText("");
    }



    private void myDatePicker(final boolean isFromDate) {

        SimpleDateFormat mDF = new SimpleDateFormat("yyyy-mm-dd");
        Date today = new Date();
        mDF.format(today);
        final Calendar c = Calendar.getInstance();
        c.setTime(today);
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        String formattedDay = (String.valueOf(dayOfMonth));
                        int m = monthOfYear + 1;


                        String formattedMonth = (String.valueOf(m));

                        if (dayOfMonth < 10) {
                            formattedDay = "0" + dayOfMonth;
                        }

                        if (m < 10) {
                            formattedMonth = "0" + m;
                            Log.e("month ", " " + m);
                        }
                        if (isFromDate)
                        {
                            ((AppCompatEditText)fragmentView.findViewById(R.id.fragment_leave_type_from_date_edittext)).setText(formattedDay + "/" + (formattedMonth) + "/" + year);
                            ((AppCompatEditText)fragmentView.findViewById(R.id.fragment_leave_type_from_date_edittext)).clearFocus();
                            /////////////////////////
                            ////////////////////////
                        }else {
                            ((AppCompatEditText)fragmentView.findViewById(R.id.fragment_leave_type_to_date_edittext)).setText(formattedDay + "/" + (formattedMonth) + "/" + year);
                            ((AppCompatEditText)fragmentView.findViewById(R.id.fragment_leave_type_to_date_edittext)).clearFocus();
                            ////////////////////////
                            ////////////////////////
                        }
                        if (((AppCompatEditText)fragmentView.findViewById(R.id.fragment_leave_type_from_date_edittext)).getText().toString()
                                .equals(
                                        ((AppCompatEditText)fragmentView.findViewById(R.id.fragment_leave_type_to_date_edittext)).getText().toString()))

                        {
                            ((LabelledSpinner)fragmentView.findViewById(R.id.fragment_leave_type_for_same_day_spinner_id)).setVisibility(View.GONE);
                        }else {
                            ((LabelledSpinner)fragmentView.findViewById(R.id.fragment_leave_type_for_same_day_spinner_id)).setVisibility(View.GONE);
                        }

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                if (isFromDate)
                    ((AppCompatEditText)fragmentView.findViewById(R.id.fragment_leave_type_from_date_edittext)).clearFocus();
                else
                    ((AppCompatEditText)fragmentView.findViewById(R.id.fragment_leave_type_to_date_edittext)).clearFocus();

            }
        });
        datePickerDialog.show();
        datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());
    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.fragment_leave_submit_button_id:
                if (PreferenceUtils.getIsUserCheckedIn(getActivity()))
                {
                    try {
                        if (fragmentView.findViewById(R.id.fragment_leave_type_for_same_day_spinner_id).getVisibility() ==View.VISIBLE) // same day leave type
                            leaveApplyPresentor.reqToValidateLeaveApplyInPresentorOnly(getActivity(),fragmentView,
                                    ((LabelledSpinner)fragmentView.findViewById(R.id.fragment_leave_type_spinner_id)).getSpinner().getSelectedItemPosition(),
                                    ((AppCompatEditText)fragmentView.findViewById(R.id.fragment_leave_type_from_date_edittext)).getText().toString(),
                                    ((AppCompatEditText)fragmentView.findViewById(R.id.fragment_leave_type_to_date_edittext)).getText().toString(),
                                    ((AppCompatEditText)fragmentView.findViewById(R.id.fragment_leave_type_notes_id)).getText().toString());
                        else
                            leaveApplyPresentor.reqToValidateLeaveApplyInPresentorOnly(getActivity(),fragmentView,
                                    ((LabelledSpinner)fragmentView.findViewById(R.id.fragment_leave_type_spinner_id)).getSpinner().getSelectedItemPosition(),
                                    ((AppCompatEditText)fragmentView.findViewById(R.id.fragment_leave_type_from_date_edittext)).getText().toString(),
                                    ((AppCompatEditText)fragmentView.findViewById(R.id.fragment_leave_type_to_date_edittext)).getText().toString(),
                                    ((AppCompatEditText)fragmentView.findViewById(R.id.fragment_leave_type_notes_id)).getText().toString());
                    } catch (ParseException e) {
                        CommonFunc.commonDialog(getActivity(),getString(R.string.justErrorCode)+" 23",getString(R.string.internalError),false);
                    }
                }else {
                    CommonFunc.commonDialog(getActivity(),getString(R.string.alert),getString(R.string.chekInAlert),false);
                }

        }
    }


    ///////////////////////////// data setting stuff end
    /////////////////////////////////////////////////////////////////////////////////////////// mvp stuff
    @SuppressWarnings("ConstantConditions")
    @Override
    public void showProgressDialog() {
        dialog=new Dialog(getActivity());
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.loaging_layout);
        dialog.show();
    }

    @Override
    public void dismissProgressDialog() {
        if (getActivity()!=null)
            if (dialog!=null)
            {
                if (dialog.isShowing())
                {
                    dialog.dismiss();
                }
                dialog=null;
            }
    }


    @Override
    public void afterLeaveApplySuccess(String message, String dialogTitle, boolean isInternetError) {
        ((BaseActivity)getActivity()).attachSuccessFragment(message,"Request more leave");
    }

    @Override
    public void afterLeaveApplyFail(String message, String dialogTitle, boolean isInternetError) {
        CommonFunc.commonDialog(getActivity(),dialogTitle,message,isInternetError);
    }

    @Override
    public void afterLeaveApplyValidationSuccess() {
        //ontext context, String leaveType, String fromDate, String toDate, String remark
//        String[] stringsLeaves=getResources().getStringArray(R.array.leaveType);
        if (fragmentView.findViewById(R.id.fragment_leave_type_for_same_day_spinner_id).getVisibility() ==View.VISIBLE) // same day leave type
            leaveApplyPresentor.reqToPostLeaveDataFromPresentor(getActivity(),
                    String.valueOf(leaveTypeDataModels.get( ((LabelledSpinner)fragmentView.findViewById(R.id.fragment_leave_type_spinner_id)).getSpinner().getSelectedItemPosition())
                            .getLeaveTypeId()),
                    ((AppCompatEditText)fragmentView.findViewById(R.id.fragment_leave_type_from_date_edittext)).getText().toString(),
                    ((AppCompatEditText)fragmentView.findViewById(R.id.fragment_leave_type_to_date_edittext)).getText().toString(),
                    ((AppCompatEditText)fragmentView.findViewById(R.id.fragment_leave_type_notes_id)).getText().toString());
        else // not same day
            leaveApplyPresentor.reqToPostLeaveDataFromPresentor(getActivity(),
                    String.valueOf(leaveTypeDataModels.get( ((LabelledSpinner)fragmentView.findViewById(R.id.fragment_leave_type_spinner_id)).getSpinner().getSelectedItemPosition())
                            .getLeaveTypeId()),
                    ((AppCompatEditText)fragmentView.findViewById(R.id.fragment_leave_type_from_date_edittext)).getText().toString(),
                    ((AppCompatEditText)fragmentView.findViewById(R.id.fragment_leave_type_to_date_edittext)).getText().toString(),
                    ((AppCompatEditText)fragmentView.findViewById(R.id.fragment_leave_type_notes_id)).getText().toString());

    }
    @Override
    public void afterLeaveTypesDownlodeSuccess(ArrayList<LeaveTypeDataModel> leaveTypeDataModelss) {
        leaveTypeDataModels.clear();
        leaveTypeDataModels.add(new LeaveTypeDataModel(0,"Please select leave type"));
        for (int i=0;i<leaveTypeDataModelss.size();i++)
        {
            leaveTypeDataModels.add(leaveTypeDataModelss.get(i));
        }
        String[] stringsLeaveType=new String[leaveTypeDataModels.size()];
        for (int i=0;i<leaveTypeDataModels.size();i++)
        {
            stringsLeaveType[i]=leaveTypeDataModels.get(i).getLeaveType();
        }
        ((LabelledSpinner)fragmentView.findViewById(R.id.fragment_leave_type_spinner_id))
                .setItemsArray(stringsLeaveType);
        ((LabelledSpinner)fragmentView.findViewById(R.id.fragment_leave_type_spinner_id)).setDefaultErrorEnabled(false);
    }

    @Override
    public void afterLeaveTypeDownlodeFail(String message, String dialogTitle, boolean isInternetError) {
        CommonFunc.commonDialog(getActivity(),dialogTitle,message,isInternetError);
    }


}
