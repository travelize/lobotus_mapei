package com.lobotus.mapei.flowfragments.mvp.presentor;

import android.content.Context;

/**
 * Created by user1 on 25-09-2017.
 */

public interface DemoPresentor {
    void reqToGetTheCustomerDataFromPresentor(Context context);
    void reqToGetDemoListFromPresentor(Context context);
    void reqToPostDemoDetails(Context context,
                              int demoTypeId,
                              String remark,
                              int demosubtypeId,String customerId);
}
