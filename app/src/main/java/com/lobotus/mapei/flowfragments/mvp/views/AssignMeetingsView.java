package com.lobotus.mapei.flowfragments.mvp.views;

import android.content.Context;

import com.lobotus.mapei.flowfragments.model.ComplaintDataTypeModel;
import com.lobotus.mapei.flowfragments.model.CustomerDataModel;
import com.lobotus.mapei.flowfragments.model.ProductsDataModel;
import com.lobotus.mapei.flowfragments.model.ProjectsDataModel;
import com.lobotus.mapei.flowfragments.model.UsersDataModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 20-03-2018.
 */

public interface AssignMeetingsView {
    void showProgressDialog();
    void dismissProgressDialog();
    ///////////////////////////////////////////////
    void afterUserListDownlodeSuccess(ArrayList<UsersDataModel> usersDataModels);
    void afterUserListDownlodeFail(String message, String dialogTitle, boolean isInternetError);
    ////////////////////////////////////////////////
    void afterCustomerNameDownlodeSuccess(ArrayList<CustomerDataModel> customerDataModels);
    void afterCistomerNameDownlodeFail(String message, String dialogTitle, boolean isInternetError);
    ////////////////////////////////////////////////////////////// post to get the list of projects
    void afterProjectsListDownlodeSuccess(ArrayList<ProjectsDataModel> projectsDataModels);
    void afterProjectsListDownlodeFail(String message,String dialogTitle,boolean isInternetError);
    /////////////////////////////////////////////////////////////
    void afterAssignMeetingSuccess(String message);
    void afterAssignMeetingFail(String message,String dialogTitle,boolean isInternetError);

    void afterComplaintsDownlodeSuccess(ArrayList<ComplaintDataTypeModel> complaintDataTypeModels);
    void afterComplaintsDownloldeFail(String message, String dialogTitle, boolean isInternetError);

    void afterProductsModelDownlodeSuccess(List<ProductsDataModel> productsDataModels);
    void afterProductsModelDoenlodeFail(String message, String dialogTitle, boolean isInternetError);

    void afterValidationOfMeetingSuccessfull();

    void afterMeetingPostSuccess(String message,String meetingId);
    void afterMeetingPostFail(String message,String dialogTitle,boolean isInternetError);


}
