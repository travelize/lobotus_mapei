package com.lobotus.mapei.flowfragments.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.lobotus.mapei.R;
import com.lobotus.mapei.flowfragments.fragments.ViewDemoMeetings;
import com.lobotus.mapei.flowfragments.model.DemoMeetingsModel;
import com.lobotus.mapei.utils.PreferenceUtils;

import java.util.ArrayList;

/**
 * Created by user1 on 20-11-2017.
 */

public class DemoMeetingsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context = null;
    private ArrayList<DemoMeetingsModel> meetingModels = null;
    private ViewDemoMeetings meetingsFragment = null;
    ///////////////////////////
    private int VIEW_TYPE_TITLE = 1;
    private int VIEW_TYPE_DATA = 2;


    public DemoMeetingsAdapter(Context context, ArrayList<DemoMeetingsModel> meetingModels, ViewDemoMeetings viewDemoMeetings) {
        this.context = context;
        this.meetingModels = meetingModels;
        this.meetingsFragment = viewDemoMeetings;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == VIEW_TYPE_TITLE) {
            View view = LayoutInflater.from(context).inflate(R.layout.demo_meetings_top_header, parent, false);
            return new HolderMeetingTopRow(view);
        } else {
            View view = LayoutInflater.from(context).inflate(R.layout.demo_meeting_list_row, parent, false);
            return new HolderMeetingDataRow(view);
        }

    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof HolderMeetingDataRow) {
            ((HolderMeetingDataRow) holder).textViewClientName.setText("" + meetingModels.get(holder.getAdapterPosition()).getCustomerName().trim());
            Log.e("customer Name=" + meetingModels.get(holder.getAdapterPosition()).getCustomerName(), "hai");
            ((HolderMeetingDataRow) holder).textViewDemo.setText(meetingModels.get(holder.getAdapterPosition()).getDemoType());
            ((HolderMeetingDataRow) holder).textViewDemoType.setText("" + meetingModels.get(holder.getAdapterPosition()).getOnDate());
            ((HolderMeetingDataRow) holder).layoutCompatClick.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    meetingsFragment.onMeetingListClick(meetingModels.get(holder.getAdapterPosition()));
                }
            });
            if (PreferenceUtils.getMeetingIdFromStartRoutePreference(context) != null) {
                if (PreferenceUtils.getMeetingIdFromStartRoutePreference(context)
                        .equals(meetingModels.get(holder.getAdapterPosition()).getMeetingID())) {
                    ((HolderMeetingDataRow) holder).textViewClientName.setTypeface(null, Typeface.BOLD);
                    ((HolderMeetingDataRow) holder).textViewDemo.setTypeface(null, Typeface.BOLD);
                    ((HolderMeetingDataRow) holder).textViewDemoType.setTypeface(null, Typeface.BOLD);
                } else {
                    ((HolderMeetingDataRow) holder).textViewClientName.setTypeface(null, Typeface.NORMAL);
                    ((HolderMeetingDataRow) holder).textViewDemo.setTypeface(null, Typeface.NORMAL);
                    ((HolderMeetingDataRow) holder).textViewDemoType.setTypeface(null, Typeface.NORMAL);
                }
            }
        }


    }

    @Override
    public int getItemCount() {
        return meetingModels.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (meetingModels.get(position) == null) {
            return VIEW_TYPE_TITLE;
        } else {
            return VIEW_TYPE_DATA;
        }
    }

    /////////////////////////////////////////////////////////////////////////////// meeting to row layout

    private class HolderMeetingTopRow extends RecyclerView.ViewHolder {

        HolderMeetingTopRow(View itemView) {
            super(itemView);
        }
    }

    private class HolderMeetingDataRow extends RecyclerView.ViewHolder {
        AppCompatTextView textViewClientName = null, textViewDemo = null, textViewDemoType = null;
        LinearLayoutCompat layoutCompatClick = null;

        public HolderMeetingDataRow(View itemView) {
            super(itemView);
            this.textViewClientName = itemView.findViewById(R.id.demo_meeting_list_data_row_client_id);
            this.textViewDemo = itemView.findViewById(R.id.demo_meeting_list_data_row_demo_id);
            this.textViewDemoType = itemView.findViewById(R.id.demo_meeting_list_data_row_demo_date_id);
            this.layoutCompatClick = itemView.findViewById(R.id.demo_meeting_list_linear_layout);
        }
    }
}
