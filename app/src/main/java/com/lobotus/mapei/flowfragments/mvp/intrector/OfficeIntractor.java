package com.lobotus.mapei.flowfragments.mvp.intrector;

import android.content.Context;

/**
 * Created by user1 on 12-10-2017.
 */

public interface OfficeIntractor {
    /// post the office vist purpose
    interface OnOfficeVisitPostLisner
    {
        void OnOfficeVisitPostSuccess(String message);
        void OnOfficeVisitPostFail(String message, String dialogTitle, boolean isInternetError);
    }
    void reqToPostThePurposeOfOfficeVisit(Context context,String purpose,String notes,OnOfficeVisitPostLisner onOfficeVisitPostLisner);
    /// post the office check in
    interface OnOfficeCheckInLisner
    {
        void OnofficeCheckInSuccess(String message);
        void OnofficeCheckInFail(String message, String dialogTitle, boolean isInternetError);
    }
    void reqToPostTheOfficeCheckIn(Context context, OnOfficeCheckInLisner onOfficeCheckInLisner, String officeLoc);
    /// post the office check out
    interface OnOfficeCheckOutLisner
    {
        void OnofficeCheckOutSuccess(String message);
        void OnofficeCheckOutFail(String message, String dialogTitle, boolean isInternetError);
    }
    void reqToPostTheOfficeCheckOut(Context context,OnOfficeCheckOutLisner onOfficeCheckOutLisner);

}
