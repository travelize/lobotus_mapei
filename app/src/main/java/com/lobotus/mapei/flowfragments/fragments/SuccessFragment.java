package com.lobotus.mapei.flowfragments.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.lobotus.mapei.R;
import com.lobotus.mapei.baseactivity.BaseActivity;
import com.lobotus.mapei.utils.ConstantsUtils;

import static com.lobotus.mapei.utils.ConstantsUtils.successFragMessageKey;


public class SuccessFragment extends Fragment {

    private View viewFragment=null;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        viewFragment= inflater.inflate(R.layout.fragment_success, container, false);

        ((AppCompatTextView)viewFragment.findViewById(R.id.fragment_success_textview_id))
                .setText(getArguments().getString(ConstantsUtils.successFragMessageKey));
        ((AppCompatButton)viewFragment.findViewById(R.id.fragment_success_add_more_button_id))
                .setText(getArguments().getString(ConstantsUtils.successFragAddMoreButtonKey));
        viewFragment.findViewById(R.id.fragment_success_add_more_button_id).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               getActivity().getSupportFragmentManager().popBackStack();
            }
        });
        viewFragment.findViewById(R.id.fragment_success_go_to_dashboard_button_id).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((BaseActivity)getActivity()).clearFragments();
            }
        });
        return viewFragment;
    }

}
