package com.lobotus.mapei.flowfragments.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by user1 on 10-10-2017.
 */

public class ComplaintDataTypeModel {


    @SerializedName("TypeID")
    @Expose
    private Integer typeID;
    @SerializedName("TypeName")
    @Expose
    private String typeName;

    public Integer getTypeID() {
        return typeID;
    }

    public void setTypeID(Integer typeID) {
        this.typeID = typeID;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

}
