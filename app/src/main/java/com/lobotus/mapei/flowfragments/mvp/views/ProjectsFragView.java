package com.lobotus.mapei.flowfragments.mvp.views;

import com.lobotus.mapei.flowfragments.model.ProductsDataModel;
import com.lobotus.mapei.flowfragments.model.ProjectsDataModel;

import java.util.ArrayList;

/**
 * Created by user1 on 06-10-2017.
 */

public interface ProjectsFragView {
    void showProgressDialog();
    void dismissProgressDialog();
    ////////////////////////////////////////////////////////////// post to get the list of projects
    void afterProjectsListDownlodeSuccess(ArrayList<ProjectsDataModel> projectsDataModels);
    void afterProjectsListDownlodeFail(String message,String dialogTitle,boolean isInternetError);
    ///////////////////////////////////////////////////////////////

}
