package com.lobotus.mapei.flowfragments.mvp.presentor;

import android.content.Context;

/**
 * Created by User on 21-03-2018.
 */

public interface TravelDataPresentor {

    void reqToGetTheTravelData(Context context,String fromDate,String toDate,String userId);
}
