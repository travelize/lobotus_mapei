package com.lobotus.mapei.flowfragments.mvp.intrectorimp;

import android.content.Context;

import com.lobotus.mapei.R;
import com.lobotus.mapei.flowfragments.mvp.intrector.DemoIntractor;
import com.lobotus.mapei.retrofitapi.APIService;
import com.lobotus.mapei.retrofitapi.ApiUtils;
import com.lobotus.mapei.utils.CommonFunc;
import com.lobotus.mapei.utils.ConstantsUtils;
import com.lobotus.mapei.utils.NetworkUtils;
import com.lobotus.mapei.utils.PreferenceUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by user1 on 25-09-2017.
 */

public class DemoIntrectorImp implements DemoIntractor {

    ///////////////////////////////////////////////////////////////////////// load  demo spinner from strings array
    @Override
    public void reqToGetDemoListFromIntrector(Context context, DemoTypesLoadLisner demoTypesLoadLisner) {
        demoTypesLoadLisner.onDemotypesLoadedLisner(context.getResources().getStringArray(R.array.demoTypes));
    }

    ///////////////////////////////////////////////////////////////////////// post the demo data to the server
    @Override
    public void reqToPostTheDemoToServer(Context context,
                                         int demoTypeId,
                                         String remark,
                                         int demosubtype,
                                         DemoPostResponseLisner demoPostResponseLisner, String customerId) {

        if (PreferenceUtils.checkUserisLogedin(context)) {
            postDemoStuffToServer(context, demoTypeId, remark, demosubtype, demoPostResponseLisner, customerId);
        } else {
            CommonFunc.commonDialog(context, context.getString(R.string.alert), context.getString(R.string.SessionLost), false);
        }


    }

    private void postDemoStuffToServer(final Context context, int demoTypeId, final String remark, int demosubtype, final DemoPostResponseLisner demoPostResponseLisner,
                                       String customerId) {
        int demoSubtypeId = 0;
        switch (demoTypeId) {
            case 1:
                switch (demosubtype) {
                    case 1:
                        demoSubtypeId = 1;
                        break;
                    case 2:
                        demoSubtypeId = 2;
                        break;
                    case 3:
                        demoSubtypeId = 7;
                        break;
                    case 4:
                        demoSubtypeId = 3;
                }
                break;
            case 2:
                switch (demosubtype) {
                    case 1:
                        demoSubtypeId = 4;
                        break;
                    case 2:
                        demoSubtypeId = 5;
                        break;
                    case 3:
                        demoSubtypeId = 8;
                        break;
                    case 4:
                        demoSubtypeId = 6;
                }
        }

        APIService apiService = ApiUtils.getAPIService(ConstantsUtils.postBaseUrl + ConstantsUtils.postAppendToBaseUrlForDemo);
        apiService.postToSaveDemo(PreferenceUtils.getUserIdFromPreference(context), String.valueOf(demoTypeId),
                remark, String.valueOf(demoSubtypeId), customerId).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    String Json = null;
                    try {
                        Json = response.body().string();
                    } catch (IOException e) {
                        demoPostResponseLisner.onDemoPostFailLisner(
                                context.getString(R.string.contact_support) + " 6", context.getString(R.string.internalError),
                                false);

                    }
                    try {
                        JSONObject jsonObject = new JSONObject(Json);
                        if (jsonObject.getInt("Success") == 1) {
                            demoPostResponseLisner.onDemoPostSuccessLisner(jsonObject.getString("Msg"));
                        } else {
                            demoPostResponseLisner.onDemoPostFailLisner(
                                    jsonObject.getString("Msg"), context.getString(R.string.alert),
                                    false);
                        }
                    } catch (JSONException e) {
                        demoPostResponseLisner.onDemoPostFailLisner(
                                context.getString(R.string.contact_support) + " 6", context.getString(R.string.internalError),
                                false);
                    }

                } else {
                    demoPostResponseLisner.onDemoPostFailLisner(
                            context.getString(R.string.ServerNotresponding), context.getString(R.string.justErrorCode) + " 4",
                            false);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                demoPostResponseLisner.onDemoPostFailLisner(
                        context.getString(R.string.noInternetMessage) + " 5",
                        context.getString(R.string.noInternetAlert),
                        true);
            }
        });

    }


}
