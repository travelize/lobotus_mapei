package com.lobotus.mapei.flowfragments.mvp.presentorimp;

import android.content.Context;
import android.view.View;

import com.lobotus.mapei.R;
import com.lobotus.mapei.flowfragments.model.LeaveTypeDataModel;
import com.lobotus.mapei.flowfragments.mvp.intrector.LeaveApplyInteractor;
import com.lobotus.mapei.flowfragments.mvp.intrectorimp.LeaveApplyInteractorImp;
import com.lobotus.mapei.flowfragments.mvp.presentor.LeaveApplyPresentor;
import com.lobotus.mapei.flowfragments.mvp.views.LeaveApplyView;
import com.lobotus.mapei.utils.CommonFunc;
import com.lobotus.mapei.utils.NetworkUtils;

import java.text.ParseException;
import java.util.ArrayList;

/**
 * Created by user1 on 04-10-2017.
 */

public class LeaveApplyPresentorImp implements LeaveApplyPresentor,
        LeaveApplyInteractor.OnLeaveApplyLisner,LeaveApplyInteractor.OnLeaveTypePostLisner {

    private LeaveApplyInteractor leaveApplyInteractor=null;
    private LeaveApplyView leaveApplyView=null;

    public LeaveApplyPresentorImp( LeaveApplyView leaveApplyView) {
        this.leaveApplyInteractor = new LeaveApplyInteractorImp();
        this.leaveApplyView = leaveApplyView;
    }
///////////////////////////////////////////////////// post to get the leave type
    @Override
    public void reqToPostLeaveType(Context context) {
        if (NetworkUtils.isConnected(context))
        {
            leaveApplyView.showProgressDialog();
            leaveApplyInteractor.reqToPostDownlodeLeaveTypesFromIntractor(context,this);
        }else {
            CommonFunc.commonDialog(context,context.getString(R.string.noInternetAlert),
                    context.getString(R.string.please_check_internet),true);
        }
    }
    @Override
    public void OnLeaveTypePostSuccess(ArrayList<LeaveTypeDataModel> leaveTypeDataModels) {
        leaveApplyView.dismissProgressDialog();
        leaveApplyView.afterLeaveTypesDownlodeSuccess(leaveTypeDataModels);
    }

    @Override
    public void OnLeaveTypePostFail(String message, String dialogTitle, boolean isInternetError) {
        leaveApplyView.dismissProgressDialog();
        leaveApplyView.afterLeaveTypeDownlodeFail(message,dialogTitle,isInternetError);
    }
    //////////////////////////////////////////////////////////// leave apply post stuff
    @Override
    public void reqToPostLeaveDataFromPresentor(Context context, String leaveType, String fromDate, String toDate, String remark) {
        if (NetworkUtils.checkInternetAndOpenDialog(context))
        {
            leaveApplyView.showProgressDialog();
            leaveApplyInteractor.reqToPostLeaveApplyFromInteractor(context,
                    leaveType,fromDate,toDate,remark,this);
        }

    }



    @Override
    public void OnLeaveApplySuccess(String message, String dialogTitle, boolean isInternetError) {
        leaveApplyView.dismissProgressDialog();
        leaveApplyView.afterLeaveApplySuccess(message,dialogTitle,isInternetError);
    }

    @Override
    public void OnLeaveApplyFail(String message, String dialogTitle, boolean isInternetError) {
        leaveApplyView.dismissProgressDialog();
        leaveApplyView.afterLeaveApplyFail(message,dialogTitle,isInternetError);
    }
    /////////////////////////////////////////////////////// validate apply for leave

    @Override
    public void reqToValidateLeaveApplyInPresentorOnly(Context context, View view,
                                                       int leaveTypeSelected,
                                                       String fromDate,
                                                       String toDate,
                                                       String remark) throws ParseException {
        if (leaveTypeSelected==0)
        {
            CommonFunc.commonDialog(context,context.getString(R.string.alert),"Please select leave type",false);
        }else if (fromDate==null)
        {
            CommonFunc.commonDialog(context,context.getString(R.string.alert),"Please select From date",false);
        }else if (fromDate.isEmpty())
        {
            CommonFunc.commonDialog(context,context.getString(R.string.alert),"Please select From date",false);

        }
        else if (CommonFunc.IsToDateIsLowThanFrom(fromDate,toDate))
        {
            CommonFunc.commonDialog(context,context.getString(R.string.alert),"To Date is less than from date",false);
        }
        else {
            leaveApplyView.afterLeaveApplyValidationSuccess();
        }

    }


}
