package com.lobotus.mapei.flowfragments.fragments;


import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatEditText;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.lobotus.mapei.R;
import com.lobotus.mapei.customwidgits.LabelledSpinner;
import com.lobotus.mapei.flowfragments.mapdistance.DirectionsJSONParser;
import com.lobotus.mapei.flowfragments.mvp.presentor.OfficePresentor;
import com.lobotus.mapei.flowfragments.mvp.presentorimp.OfficePresentorImp;
import com.lobotus.mapei.flowfragments.mvp.views.OfficeView;
import com.lobotus.mapei.utils.CommonFunc;
import com.lobotus.mapei.utils.ConstantsUtils;
import com.lobotus.mapei.utils.NetworkUtils;
import com.lobotus.mapei.utils.PermissionUtils;
import com.lobotus.mapei.utils.PreferenceUtils;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class OfficeFragment extends Fragment implements OfficeView,View.OnClickListener {
    private View fragmentView=null;
    private Dialog dialog=null;
    // mvp
    private OfficePresentor officePresentor=null;
    /////////// location stuff
    private GoogleApiClient googleApiClient = null;
    private LocationRequest locationRequest = null;
    private FusedLocationProviderClient mFusedLocationClient = null;
    /////////////////////////////////////////////////////////////////////
    private String DISTANCE_FROM_BLORE_OFFICE="";
    private String DISTANCE_FROM_MUMBAI_OFFICE="";
    private String DISTANCE_FROM_DELHI_OFFICE="";
    /////////////////////////////////////////////////////
    private LatLng latLngMyLoc=null;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        fragmentView= inflater.inflate(R.layout.fragment_office, container, false);
        officePresentor=new OfficePresentorImp(this);
        fragmentView.findViewById(R.id.fragment_office_submit_id).setOnClickListener(this);
        setTheOfficeTypeSpinner();
        setTheFloatingActionMenuButton();
        return fragmentView;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        CommonFunc.hideKeyboard(getActivity());
    }

    private void setTheFloatingActionMenuButton()
    {
        FloatingActionButton floatingActionButtonCheckIn=new FloatingActionButton(getActivity());
        floatingActionButtonCheckIn.setButtonSize(com.github.clans.fab.FloatingActionButton.SIZE_MINI);
        floatingActionButtonCheckIn.setColorNormalResId(R.color.colorPrimary);
        floatingActionButtonCheckIn.setColorPressedResId(R.color.colorPrimary);
        floatingActionButtonCheckIn.setColorRippleResId(R.color.colorPrimary);
        floatingActionButtonCheckIn.setImageResource(R.drawable.ic_checkin);
        floatingActionButtonCheckIn.setLabelText("Check-In");
        floatingActionButtonCheckIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (PreferenceUtils.getIsUserCheckedIn(getActivity()))
                {
                    if (PreferenceUtils.getOfficeVisitId(getActivity())!=null)
                    {

                        CommonFunc.commonDialog(getActivity(),getString(R.string.alert),"You have already checked-In"
                                ,false);
                    }else {
                       /* if (PreferenceUtils.getTheUserRoleNameFromPreference(getActivity()).equals("Manager"))
                        {
                            officePresentor.reqToPostTheOfficeCheckIn(getActivity(),ConstantsUtils.BANGLORE_ADDRESS);
                        }else {
                            connectToGoogleApiClientAndGetTheAddress();
                        }*/
                        officePresentor.reqToPostTheOfficeCheckIn(getActivity(),ConstantsUtils.BANGLORE_ADDRESS);

                    }
                }else {
                    CommonFunc.commonDialog(getActivity(),getString(R.string.alert),getString(R.string.chekInAlert),false);
                }

                if ( ((FloatingActionMenu)fragmentView.findViewById(R.id.fragment_office_floating_menu)).isOpened())
                    ((FloatingActionMenu)fragmentView.findViewById(R.id.fragment_office_floating_menu)).close(true);
                else
                    ((FloatingActionMenu)fragmentView.findViewById(R.id.fragment_office_floating_menu)).open(true);
            }
        });
        ///////////////////////////////////////////////////////
        FloatingActionButton floatingActionButtonCheckOut=new FloatingActionButton(getActivity());
        floatingActionButtonCheckOut.setButtonSize(com.github.clans.fab.FloatingActionButton.SIZE_MINI);
        floatingActionButtonCheckOut.setColorNormalResId(R.color.colorPrimary);
        floatingActionButtonCheckOut.setColorPressedResId(R.color.colorPrimary);
        floatingActionButtonCheckOut.setColorRippleResId(R.color.colorPrimary);
        floatingActionButtonCheckOut.setImageResource(R.drawable.ic_checkout);
        floatingActionButtonCheckOut.setLabelText("Check-Out");
        floatingActionButtonCheckOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (PreferenceUtils.getIsUserCheckedIn(getActivity()))
                {
                    if (PreferenceUtils.getOfficeVisitId(getActivity())==null)
                    {
                        CommonFunc.commonDialog(getActivity(),getString(R.string.alert),"You have already checked-out"
                                ,false);
                    }else {
                        officePresentor.reqToPostTheOfficeCheckOut(getActivity());
                    }
                }else {
                    CommonFunc.commonDialog(getActivity(),getString(R.string.alert),getString(R.string.chekInAlert),false);
                }

                if ( ((FloatingActionMenu)fragmentView.findViewById(R.id.fragment_office_floating_menu)).isOpened())
                    ((FloatingActionMenu)fragmentView.findViewById(R.id.fragment_office_floating_menu)).close(true);
                else
                    ((FloatingActionMenu)fragmentView.findViewById(R.id.fragment_office_floating_menu)).open(true);
            }
        });
        /////////////////////////////////
        ((FloatingActionMenu)fragmentView.findViewById(R.id.fragment_office_floating_menu))
                .addMenuButton(floatingActionButtonCheckIn);
        ((FloatingActionMenu)fragmentView.findViewById(R.id.fragment_office_floating_menu))
                .addMenuButton(floatingActionButtonCheckOut);
        ((FloatingActionMenu)fragmentView.findViewById(R.id.fragment_office_floating_menu)).
        getMenuIconView().setImageResource(R.drawable.ic_office);
        ((FloatingActionMenu)fragmentView.findViewById(R.id.fragment_office_floating_menu)).
                setClosedOnTouchOutside(true);
        //
        ((FloatingActionMenu)fragmentView.findViewById(R.id.fragment_office_floating_menu)).setOnMenuButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ( ((FloatingActionMenu)fragmentView.findViewById(R.id.fragment_office_floating_menu)).isOpened())
                    ((FloatingActionMenu)fragmentView.findViewById(R.id.fragment_office_floating_menu)).close(true);
                else
                    ((FloatingActionMenu)fragmentView.findViewById(R.id.fragment_office_floating_menu)).open(true);
            }
        });
    }


    private void setTheOfficeTypeSpinner()
    {
        ((LabelledSpinner)fragmentView.findViewById(R.id.fragment_office_work_done_spinner_id))
                .setItemsArray(R.array.officeWork);
        ((LabelledSpinner)fragmentView.findViewById(R.id.fragment_office_work_done_spinner_id)).setDefaultErrorEnabled(false);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.fragment_office_submit_id:
                if (((LabelledSpinner)fragmentView.
                        findViewById(R.id.fragment_office_work_done_spinner_id))
                        .getSpinner().getSelectedItemPosition()>0)
                {
                    List<String> stringsPurpose = Arrays.asList(getResources().getStringArray(R.array.officeWork));
                    officePresentor.reqToPostThePurposeOfOfficeVisit(getActivity(),
                            stringsPurpose.get(((LabelledSpinner)fragmentView.
                                    findViewById(R.id.fragment_office_work_done_spinner_id)).getSpinner().getSelectedItemPosition()),
                            ((AppCompatEditText)fragmentView.findViewById(R.id.fragment_office_notes_id)).getText().toString());
                }else {
                    CommonFunc.commonDialog(getActivity(),getString(R.string.alert),"Please select purpose of visit",false);
                }

        }
    }
    //////////////////////////////////////////////////////////////////////////////// mvp stuff

    @Override
    public void showProgressDialog() {
        dialog=new Dialog(getActivity());
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.loaging_layout);
        dialog.show();
    }

    @Override
    public void dismissProgressDialog() {
        if (getActivity()!=null)
        {
            if (dialog!=null)
            {
                if (dialog.isShowing())
                {
                    dialog.dismiss();
                }
                dialog=null;
            }
        }

    }

    @Override
    public void afterRecordingTheOfficeVisitSuccess(String message) {
        ((LabelledSpinner)fragmentView.findViewById(R.id.fragment_office_work_done_spinner_id)).setSelection(0);
        CommonFunc.commonDialog(getActivity(),getString(R.string.alert),message,false);
    }


    @Override
    public void afterRecordingTheOfficeVisitFail(String message, String dialogTitle, boolean isInternetError) {
        CommonFunc.commonDialog(getActivity(),dialogTitle,message,isInternetError);
    }
    /////////////// office check out  and the office check out
    @Override
    public void afterOfficeCheckInSuccess(String message) {
        CommonFunc.commonDialog(getActivity(),getString(R.string.alert),message,false);
    }

    @Override
    public void afterOfficeCheckInFail(String message, String dialogTitle, boolean isInternetError) {
        CommonFunc.commonDialog(getActivity(),dialogTitle,message,isInternetError);
    }

    @Override
    public void afterOfficeCheckOutSuccess(String message) {
        CommonFunc.commonDialog(getActivity(),getString(R.string.alert),message,false);
    }

    @Override
    public void afterOfficeCheckOutFail(String message, String dialogTitle, boolean isInternetError) {
        CommonFunc.commonDialog(getActivity(),dialogTitle,message,isInternetError);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////// location matching stuff
    private void connectToGoogleApiClientAndGetTheAddress() {
        if (NetworkUtils.checkInternetAndOpenDialog(getActivity())) {
            if (isGooglePlayServicesAvailable(getActivity())) {
                if (dialog == null) {
                    resetTheDistanceData();
                    showProgressDialog();
                    connectGoogleApiClient();
                }

            }
        }
    }
    public boolean isGooglePlayServicesAvailable(Activity activity) {
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int status = googleApiAvailability.isGooglePlayServicesAvailable(activity);
        if(status != ConnectionResult.SUCCESS) {
            if(googleApiAvailability.isUserResolvableError(status)) {
                googleApiAvailability.getErrorDialog(activity, status, 2404).show();
            }
            return false;
        }
        return true;
    }

    private void connectGoogleApiClient()
    {

        googleApiClient = new GoogleApiClient.Builder(getActivity())
                .addApi(LocationServices.API)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(@Nullable Bundle bundle) {
                        checkForLocationSettings();
                    }
                    @Override
                    public void onConnectionSuspended(int i) {
                    }
                })
                .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                        CommonFunc.commonDialog(getActivity(),"Alert!","Google play service not responding... please refresh your mobile",
                                false);
                        googleApiClient=null;
                        dismissProgressDialog();

                    }
                }).build();
        googleApiClient.connect();


    }

    private void checkForLocationSettings()
    {
        ////////////////////////////////////////////////////////

        locationRequest =new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);

        //**************************
        builder.setAlwaysShow(true); //this is the key ingredient
        //**************************

        Task<LocationSettingsResponse> locationSettingsResponseTask =
                LocationServices.getSettingsClient(getActivity()).checkLocationSettings(builder.build());
        //
        locationSettingsResponseTask.addOnCompleteListener(new OnCompleteListener<LocationSettingsResponse>() {
            @Override
            public void onComplete(@NonNull Task<LocationSettingsResponse> task) {
                try {
                    LocationSettingsResponse response = task.getResult(ApiException.class);
                    // All location settings are satisfied. The client can initialize location
                    // requests here.
                    if (PermissionUtils.checkLoactionPermission(getActivity()) && NetworkUtils.isConnected(getActivity())) {
                        getLastLocation();
                    }else {
                        dismissProgressDialog();
                        if (!PermissionUtils.checkLoactionPermission(getActivity()))
                        {
                            openAppSettings();

                        }else {
                            NetworkUtils.checkInternetAndOpenDialog(getActivity());
                        }
                    }

                } catch (ApiException exception) {
                    dismissProgressDialog();

                    switch (exception.getStatusCode()) {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // Location settings are not satisfied. But could be fixed by showing the
                            // user a dialog.
                            try {
                                openGpsSettings();
                            } catch (ClassCastException e) {
                                CommonFunc.commonDialog(getActivity(),getString(R.string.alert),
                                        getString(R.string.justErrorCode)+" 86",false);
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            CommonFunc.commonDialog(getActivity(),"GPS not working",
                                    getString(R.string.justErrorCode)+" 87",false);
                            break;
                    }
                }
            }
        });
        //

    }
    private void openAppSettings()
    {
        new AlertDialog.Builder(getActivity())
                .setTitle("Alert!!")
                .setMessage("Please grant location permission")
                .setCancelable(false)
                .setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        Intent intent = new Intent();
                        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
                        intent.setData(uri);
                        startActivity(intent);


                    }

                })
                .create().show();
    }
    private void openGpsSettings()
    {
        new AlertDialog.Builder(getActivity())
                .setTitle("Alert!!")
                .setMessage("Please grant gps access")
                .setCancelable(false)
                .setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        startActivity(new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS));
                    }

                })
                .create().show();
    }
    @SuppressWarnings("MissingPermission")
    private void getLastLocation() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());
        mFusedLocationClient.requestLocationUpdates(locationRequest,locationCallback,null);
    }
    private LocationCallback locationCallback=new LocationCallback()
    {
        @Override
        public void onLocationResult(LocationResult locationResult) {

            for (Location location : locationResult.getLocations()) {
                latLngMyLoc=new LatLng(location.getLatitude(), location.getLongitude());
                String url = getDirectionsUrl(latLngMyLoc,
                        new LatLng(ConstantsUtils.LAT_MAPIE_BANGLORE_OFFICE,ConstantsUtils.LNG_MAPIE_BANGLORE_OFFICE));
                DownloadTask downloadTask = new DownloadTask();
                downloadTask.execute(url);
                mFusedLocationClient.removeLocationUpdates(locationCallback);

            }
        }
    };

    /////////////////////////////////// distance fetching
    public String getDirectionsUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor;

        // Output format
        String output = "json";

        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;

        return url;
    }

    // Fetches data from url passed
    private class DownloadTask extends AsyncTask<String, Void, String> {
        @Override
        public void onPreExecute() {
            super.onPreExecute();

        } //

        // Downloading data in non-ui thread
        @Override
        public String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
                Toast.makeText(getActivity(),"Network error... check internet",Toast.LENGTH_LONG).show();
            }
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        public void onPostExecute(String result) {
            super.onPostExecute(result);
            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);
        }
    }

    public String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            dismissProgressDialog();
            Toast.makeText(getActivity(),"Network error... check internet",Toast.LENGTH_LONG).show();
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }


    public class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        public List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        public void onPostExecute(List<List<HashMap<String, String>>> result) {

            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;

            String distance = "";
            String duriation = "";

            // Traversing through all the routes
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);
                    if (j == 0) {    // Get distance from the list
                        distance = (String) point.get("distance");
                        continue;
                    } else if (j == 1) { // Get duration from the list
                        duriation = (String) point.get("duration");
                        continue;
                    }
                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }
                if (!distance.equals(""))
                {
                    if (distance.toLowerCase().contains("km"))
                    {
                        checkNextAddressAndThrowWrongCheckInIfAllThreeAddressNoMatched(distance);

                    }else {
                        if (Integer.parseInt(distance.replaceAll("[^0-9]", ""))<=80)
                        {
                            if (DISTANCE_FROM_BLORE_OFFICE.equals(""))
                            {
                                officePresentor.reqToPostTheOfficeCheckIn(getActivity(),ConstantsUtils.BANGLORE_ADDRESS);
                            }else if (DISTANCE_FROM_MUMBAI_OFFICE.equals(""))
                            {
                                officePresentor.reqToPostTheOfficeCheckIn(getActivity(),ConstantsUtils.MUMBAI_ADDRESS);
                            }else if (DISTANCE_FROM_DELHI_OFFICE.equals(""))
                            {
                                officePresentor.reqToPostTheOfficeCheckIn(getActivity(),ConstantsUtils.DELHI_ADDRESS);
                            }

                        }else {
                            checkNextAddressAndThrowWrongCheckInIfAllThreeAddressNoMatched(distance);

                        }
                    }
                }else {
                    dismissProgressDialog();
                    CommonFunc.commonDialog(getActivity(),getString(R.string.alert),"Location not available",false);
                }
            }


        }
    }

    private void checkNextAddressAndThrowWrongCheckInIfAllThreeAddressNoMatched(String distance)
    {
        if (DISTANCE_FROM_BLORE_OFFICE.equals(""))
        {
            DISTANCE_FROM_BLORE_OFFICE=distance; // store blore and see mumbai
            String url = getDirectionsUrl(latLngMyLoc,
                    new LatLng(ConstantsUtils.LAT_MAPEI_MUMBAI_OFFICE,ConstantsUtils.LNG_MAPEI_MUMBAI_OFFICE));
            DownloadTask downloadTask = new DownloadTask();
            downloadTask.execute(url);
        }else if (DISTANCE_FROM_MUMBAI_OFFICE.equals(""))
        {
            DISTANCE_FROM_MUMBAI_OFFICE=distance; // store mumbai and see delhi
            String url = getDirectionsUrl(latLngMyLoc,
                    new LatLng(ConstantsUtils.LAT_MAPEI_DELHI_OFFICE,ConstantsUtils.LNG_MAPEI_DELHI_OFFICE));
            DownloadTask downloadTask = new DownloadTask();
            downloadTask.execute(url);
        }else if (DISTANCE_FROM_DELHI_OFFICE.equals("")){
            DISTANCE_FROM_DELHI_OFFICE=distance; // store delhi office and stop and show dialog and clear all
            dismissProgressDialog();
            CommonFunc.commonDialog(getActivity(),getString(R.string.alert),
                    "Please check-in when you are inside the nearest Mapei office.. currently you are "+DISTANCE_FROM_BLORE_OFFICE+" far from the Bengaluru office, " +
                            ""+DISTANCE_FROM_MUMBAI_OFFICE+" far from the Mumbai office, "+
                            ""+DISTANCE_FROM_DELHI_OFFICE+" far from the Delhi/Faridabad office." ,false);
            resetTheDistanceData();

        }
    }
    private void resetTheDistanceData()
    {
        DISTANCE_FROM_BLORE_OFFICE="";DISTANCE_FROM_MUMBAI_OFFICE="";DISTANCE_FROM_DELHI_OFFICE="";

    }


}


