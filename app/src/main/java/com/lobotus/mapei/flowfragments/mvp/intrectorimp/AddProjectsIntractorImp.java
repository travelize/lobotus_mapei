package com.lobotus.mapei.flowfragments.mvp.intrectorimp;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;

import com.lobotus.mapei.R;
import com.lobotus.mapei.flowfragments.model.CustomerDataModel;
import com.lobotus.mapei.flowfragments.model.CustomerModel;
import com.lobotus.mapei.flowfragments.model.ProductsModel;
import com.lobotus.mapei.flowfragments.model.ProjectsModel;
import com.lobotus.mapei.flowfragments.mvp.intrector.AddProjectsIntractor;
import com.lobotus.mapei.retrofitapi.APIService;
import com.lobotus.mapei.retrofitapi.ApiUtils;
import com.lobotus.mapei.utils.CommonFunc;
import com.lobotus.mapei.utils.ConstantsUtils;
import com.lobotus.mapei.utils.PreferenceUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by user1 on 06-10-2017.
 */

public class AddProjectsIntractorImp implements AddProjectsIntractor {

    //////////////////////////////////////////////////////// req To Load The project stages
    @Override
    public void reqToLoadTheProjectStages(Context context, OnProjectStagesLoadLisner onProjectStagesLoadLisner) {
        onProjectStagesLoadLisner.OnProjectDataLoadSuccess(context
                .getResources().getStringArray(R.array.projectStages));
    }

    ///////////////////////////////////////////////////////// customer details
    @Override
    public void reqToPostCustomerDataReqToServer(final Context context,
                                                 final OnTravelCustomerDataDownlodeLisner onTravelCustomerDataDownlodeLisner) {

        APIService apiService = ApiUtils.getAPIService(ConstantsUtils.postBaseUrl + ConstantsUtils.postToDownlodeTheCustomersData);
        apiService.postReqTogetTheCustomerData("Active").enqueue(new Callback<CustomerModel>() {
            @Override
            public void onResponse(Call<CustomerModel> call, Response<CustomerModel> response) {
                if (response.isSuccessful()) {
                    CustomerModel customerModel = response.body();
                    if (customerModel == null) {
                        onTravelCustomerDataDownlodeLisner.OnTravelCustomerDataDownlodeFail(
                                context.getString(R.string.justErrorCode) + " 8", context.getString(R.string.ServerNotresponding),
                                false);
                    } else {
                        if (customerModel.getSuccess() == 1) {
                            onTravelCustomerDataDownlodeLisner
                                    .OnTravelCustomerDataDownlodeSuccess((ArrayList<CustomerDataModel>) customerModel.getCustomerDataModels());
                        } else {
                            onTravelCustomerDataDownlodeLisner.OnTravelCustomerDataDownlodeFail(
                                    customerModel.getMsg(), context.getString(R.string.alert),
                                    false);
                        }

                    }

                } else {
                    onTravelCustomerDataDownlodeLisner.OnTravelCustomerDataDownlodeFail(
                            context.getString(R.string.justErrorCode) + " 7", context.getString(R.string.ServerNotresponding),
                            false);
                }
            }

            @Override
            public void onFailure(Call<CustomerModel> call, Throwable t) {
                onTravelCustomerDataDownlodeLisner.OnTravelCustomerDataDownlodeFail(
                        context.getString(R.string.noInternetMessage) + " 9",
                        context.getString(R.string.noInternetAlert),
                        true);
            }
        });

    }

    /////////////////////////////////////////////////////////////////// products data downlode stuff
    @Override
    public void reqToPostProductsDataReqToServer(final OnProductsDataDownlodeLisner onProductsDataDownlodeLisner,
                                                 final Context context) {
        APIService apiService = ApiUtils.getAPIService(ConstantsUtils.postBaseUrl + ConstantsUtils.postToGetTheListOfProducts);
        apiService.postToGetTheListofProducts("")
                .enqueue(new Callback<ProductsModel>() {
                    @Override
                    public void onResponse(Call<ProductsModel> call, Response<ProductsModel> response) {
                        Log.e("varsha=", "enter retrofit");
                        if (response.isSuccessful()) {
                            ProductsModel productsModel = response.body();
                            if (productsModel == null) {
                                onProductsDataDownlodeLisner.OnProductsDataDownlodeFail(context.getString(R.string.ServerNotresponding), context.getString(R.string.justErrorCode) + " 29"
                                        , false);
                                Log.e("varsha=", "enter product null");
                            } else {
                                Log.e("varsha=", "enter product success");
                                if (productsModel.getSuccess() == 1) {
                                    Log.e("data success list=", productsModel.getData().size() + "");
                                    onProductsDataDownlodeLisner.OnProductsDataDownlodeSuccess(productsModel.getData());
                                } else {
                                    Log.e("data empty list=", productsModel.getData().size() + "");
                                    onProductsDataDownlodeLisner.OnProductsDataDownlodeFail(productsModel.getErrorMsg(), context.getString(R.string.alert), false);
                                }

                            }
                        } else {
                            onProductsDataDownlodeLisner.OnProductsDataDownlodeFail(context.getString(R.string.ServerNotresponding), context.getString(R.string.justErrorCode) + " 28",

                                    false);
                            Log.e("varsha=", "enter product error");
                        }
                    }

                    @Override
                    public void onFailure(Call<ProductsModel> call, Throwable t) {
                        onProductsDataDownlodeLisner.OnProductsDataDownlodeFail(context.getString(R.string.noInternetMessage) + " 27",
                                context.getString(R.string.noInternetAlert),
                                true);
                        Log.e("varsha=", "enter product network error");
                    }
                });

    }

    //////////////////////////////////////////////////////////// req to post the projects
    @Override
    public void reqToPostTheNewProductsData(Context context, String projectName,
                                            String customerName, String customerType, String productList,
                                            String projectPeriod, String projectStage, String notes,
                                            OnProductsPostLisner onProductsPostLisner) {
        if (PreferenceUtils.checkUserisLogedin(context)) {
            postTheNewCustomerData(context, projectName, customerName,
                    customerType, productList, projectPeriod, projectStage,
                    notes, onProductsPostLisner);
        } else {
            CommonFunc.commonDialog(context, context.getString(R.string.alert), context.getString(R.string.SessionLost), false);
        }
    }

    private void postTheNewCustomerData(final Context context, String projectName,
                                        String customerId, String customerType, String productList,
                                        String projectPeriod, String projectStage, String notes,
                                        final OnProductsPostLisner onProductsPostLisner) {
        System.out.println("bbbbbbbbbbbbb-----projectStage--------" + projectStage);
        System.out.println("bbbbbbbbbbbbb-----productList--------" + productList);
        APIService apiService = ApiUtils.getAPIService(ConstantsUtils.postBaseUrl + ConstantsUtils.postToAddTheProjectsToServer);
        apiService.postToAddtheProjuectsToServer(PreferenceUtils.getUserIdFromPreference(context),
                customerId, productList, projectPeriod, notes, "Created", projectStage, projectName)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()) {
                            try {
                                JSONObject jsonObject = new JSONObject(response.body().string());
                                if (jsonObject.getInt("Success") == 1)
                                    onProductsPostLisner.OnProductsPostSuccess(jsonObject.getString("Msg"));
                                else
                                    onProductsPostLisner.OnProductsPostFail(jsonObject.getString("Msg"),
                                            context.getString(R.string.alert), false);
                            } catch (JSONException | IOException e) {
                                onProductsPostLisner.OnProductsPostFail(context.getString(R.string.justErrorCode) + " 32",
                                        context.getString(R.string.internalError),
                                        false);
                            }
                        } else {
                            onProductsPostLisner.OnProductsPostFail(context.getString(R.string.justErrorCode) + " 31",
                                    context.getString(R.string.ServerNotresponding),
                                    false);
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        onProductsPostLisner.OnProductsPostFail(context.getString(R.string.noInternetMessage) + " 30",
                                context.getString(R.string.noInternetAlert),
                                true);
                    }
                });
    }

}
