package com.lobotus.mapei.flowfragments.mvp.presentor;

import android.content.Context;
import android.view.View;

import com.lobotus.mapei.flowfragments.model.CustomerDataModel;
import com.lobotus.mapei.flowfragments.model.ProductsDataModel;
import com.lobotus.mapei.flowfragments.model.ProjectsDataModel;

import java.util.ArrayList;

/**
 * Created by user1 on 04-10-2017.
 */

public interface MeetingPresentor {
    //////////////////////////// downlode the customer data
    void reqToGetTheCustomerDataFromPresentor(Context context);
    /////////////////////////// validate meetings data in presentor only
    void validateMeetingsDataInPresenter(View view, Context context, int projectNameSpinnerSelectedItemPos,
                                         String customerName, String customerType,
                                         int meetingTypeSpinnerSelectedPos,int submeetingTypeSelectedSpinner,
                                         String valueOfFllowUpOrder, String orderLostOfFollowupOrder,
                                         int complaintsTypesSpinnerSelectedPos,
                                         int arFollowTypesSpinnerSelectedPos,
                                         String arFollowPaymentNewDate, String arFollowPaymentProjectissue,
                                         String arFollowPaymentProjectDeley,
                                         String arFollowCformFollowupCollected,
                                         String arFollowCformFollowupNewDate,
                                         String arFollowCformFollowupNotTracable,
                                         String arFollowCformReconcilationMissingInvoice,
                                         String arFollowCformReconcilationShortPayment,
                                         String arFollowCformReconcilationStatement,
                                         ArrayList<ProductsDataModel> productsDataModels_Submitance,
                                         ArrayList<ProductsDataModel> productsDataModels_Presentation,
                                         ArrayList<ProductsDataModel> productsDataModels_Followup,
                                         String locAddress);
    //////// post to downlode the list of projects
    void reqToDownlodeTheListOfProjectsFromPresentor(Context context);
    /////////// post to get the customer anme and the customer typew from the choosen project
    void reqToGetTheCustomerNameAndTypeFromChoosenProject(ArrayList<ProjectsDataModel> projectsDataModels,
                                                          String autocompleteText,Context context);
    /////////////////////////// downlode the products data
    void reqToGetTheProductsDataFromPresentor(Context context);
    /////////////////////////// downlode complaint types
    void reqToGetTheComplaintsDataFromTheServer(Context context);
    /////////////////////////// post the meeting to server
    void reqToPostTheMeetingToTheServer(Context context, String projectName,
                                        ArrayList<ProjectsDataModel> projectsDataModels,
                                        String customerName,String customerType,
                                        int meetingTypeSpinnerSelectedPos,int submeetingTypeSelectedSpinner,String valueOfFllowUpOrder,
                                        String orderLostOfFollowupOrder,
                                        int complaintsTypesSpinnerSelectedPos,
                                        int arFollowTypesSpinnerSelectedPos,
                                        String arFollowPaymentNewDate,
                                        String arFollowPaymentProjectissue,
                                        String arFollowPaymentProjectDeley,
                                        String arFollowCformFollowupCollected,
                                        String arFollowCformFollowupNewDate,
                                        String arFollowCformFollowupNotTracable,
                                        String arFollowCformReconcilationMissingInvoice,
                                        String arFollowCformReconcilationShortPayment,
                                        String arFollowCformReconcilationStatement,
                                        String notes,
                                        ArrayList<ProductsDataModel> productsDataModels_Submitance,
                                        ArrayList<ProductsDataModel> productsDataModels_Presentation,
                                        ArrayList<ProductsDataModel> productsDataModels_Followup,
                                        String location,
                                        String meetingSubTypeValue,String customerId);
    /////////////////////////////////////////////// req to get the projects based on customer name
    void reqToGetTheProjectNameBasedOnCustomerName(Context context,String CustID);
}
