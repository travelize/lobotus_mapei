package com.lobotus.mapei.flowfragments.mvp.intrectorimp;

import android.content.Context;

import com.lobotus.mapei.R;
import com.lobotus.mapei.flowfragments.model.UserTravelModel;
import com.lobotus.mapei.flowfragments.mvp.intrector.TravelDataIntractor;
import com.lobotus.mapei.retrofitapi.APIService;
import com.lobotus.mapei.retrofitapi.ApiUtils;
import com.lobotus.mapei.utils.ConstantsUtils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by User on 21-03-2018.
 */

public class TravelDataIntractorImp implements TravelDataIntractor {
    private Call<List<UserTravelModel>> userTravelModelCall=null;
    
    @Override
    public void reqToGetUserTravelData(final Context context, String fromDate, String toDate, String userId,
                                       final OnTravelDataDownlodeLisner onTravelDataDownlodeLisner) {

        APIService apiService= ApiUtils.getAPIService
                (ConstantsUtils.postBaseUrl+ConstantsUtils.POST_TO_GET_THE_USER_TRAVEL_LIST);
        userTravelModelCall=apiService.postToGetTheUserTravelList(fromDate,toDate,userId);
        userTravelModelCall.enqueue(new Callback<List<UserTravelModel>>() {
            @Override
            public void onResponse(Call<List<UserTravelModel>> call, Response<List<UserTravelModel>> response) {
                if (response.isSuccessful())
                {
                    if (response.body().size()==0)
                    {
                        onTravelDataDownlodeLisner.OnTravelDataDOwnlodeFail(
                                "No Travel Data found",context.getString(R.string.alert),
                                false);
                    }else {
                        onTravelDataDownlodeLisner.OnTravelDataDownlodeSuccess((ArrayList<UserTravelModel>) response.body());
                    }
                }else {
                    onTravelDataDownlodeLisner.OnTravelDataDOwnlodeFail(
                            context.getString(R.string.justErrorCode)+" 125",context.getString(R.string.ServerNotresponding),
                            false);  
                }
            }

            @Override
            public void onFailure(Call<List<UserTravelModel>> call, Throwable t) {
                onTravelDataDownlodeLisner.OnTravelDataDOwnlodeFail(
                        context.getString(R.string.noInternetMessage)+" 124",
                        context.getString(R.string.noInternetAlert),
                        true);
            }
        });
        
        
        
        
    }
}
