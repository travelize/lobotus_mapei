package com.lobotus.mapei.flowfragments.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.lobotus.mapei.R;
import com.lobotus.mapei.flowfragments.model.UserTravelModel;
import com.lobotus.mapei.flowfragments.model.UsersDataModel;

import java.util.ArrayList;

/**
 * Created by User on 21-03-2018.
 */

public class TravelDataAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context=null;
    private ArrayList<UserTravelModel> usersDataModels=null;

    public TravelDataAdapter(Context context, ArrayList<UserTravelModel> usersDataModels) {
        this.context = context;
        this.usersDataModels = usersDataModels;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.travel_data_row,
                parent,false);
        return new TravelDataHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        UserTravelModel usersDataModel=usersDataModels.get(holder.getAdapterPosition());
        TravelDataHolder travelDataHolder= (TravelDataHolder) holder;
        //
        travelDataHolder.textViewDate.setText(""+usersDataModel.getTraveldate());
        travelDataHolder.textViewKms.setText(""+usersDataModel.getDistance());
        //
    }

    @Override
    public int getItemCount() {
        return usersDataModels.size();
    }

    private class TravelDataHolder extends RecyclerView.ViewHolder
    {
        private AppCompatTextView textViewDate=null,textViewKms=null;
         TravelDataHolder(View itemView) {
            super(itemView);
            this.textViewDate=itemView.findViewById(R.id.travel_data_row_date_id);
            this.textViewKms=itemView.findViewById(R.id.travel_data_row_kms_id);
        }
    }
}
