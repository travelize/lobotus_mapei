package com.lobotus.mapei.flowfragments.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by user1 on 06-10-2017.
 */

public class ProjectsDataModel {

    @SerializedName("ProjectID")
    @Expose
    private String projectID;
    @SerializedName("ProjectName")
    @Expose
    private String projectName;
    @SerializedName("CustomerID")
    @Expose
    private String customerID;
    @SerializedName("CustomerName")
    @Expose
    private String customerName;
    @SerializedName("UserID")
    @Expose
    private String userID;
    @SerializedName("AddedBy")
    @Expose
    private String addedBy;
    @SerializedName("Status")
    @Expose
    private String status;
    @SerializedName("Products")
    @Expose
    private String products;
    @SerializedName("Period")
    @Expose
    private String period;
    @SerializedName("ProductStage")
    @Expose
    private String productStage;
    @SerializedName("Remarks")
    @Expose
    private String remarks;
    @SerializedName("CustomerType")
    @Expose
    private String customerType;
    private boolean isExpand=false;

    public ProjectsDataModel(String projectName) {
        this.projectName = projectName;
    }

    public boolean isExpand() {
        return isExpand;
    }

    public void setExpand(boolean expand) {
        isExpand = expand;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    public String getProjectID() {
        return projectID;
    }

    public void setProjectID(String projectID) {
        this.projectID = projectID;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getCustomerID() {
        return customerID;
    }

    public void setCustomerID(String customerID) {
        this.customerID = customerID;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getAddedBy() {
        return addedBy;
    }

    public void setAddedBy(String addedBy) {
        this.addedBy = addedBy;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getProducts() {
        return products;
    }

    public void setProducts(String products) {
        this.products = products;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getProductStage() {
        return productStage;
    }

    public void setProductStage(String productStage) {
        this.productStage = productStage;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }


}
