package com.lobotus.mapei.flowfragments.mvp.intrectorimp;

import android.content.Context;

import com.lobotus.mapei.R;
import com.lobotus.mapei.flowfragments.mvp.intrector.OfficeIntractor;
import com.lobotus.mapei.retrofitapi.APIService;
import com.lobotus.mapei.retrofitapi.ApiUtils;
import com.lobotus.mapei.utils.ConstantsUtils;
import com.lobotus.mapei.utils.PreferenceUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by user1 on 12-10-2017.
 */

public class OfficeIntractorImp implements OfficeIntractor {
    @Override
    public void reqToPostThePurposeOfOfficeVisit(Context context, String purpose, String notes,OnOfficeVisitPostLisner onOfficeVisitPostLisner) {
        if (PreferenceUtils.getOfficeVisitId(context)!=null)
        {
            if (!PreferenceUtils.getOfficeVisitId(context).equals(""))
            {
                postThePurposeVisit(context,purpose,notes,onOfficeVisitPostLisner);
            }else {
                onOfficeVisitPostLisner.OnOfficeVisitPostFail("You have not visited the office",
                        context.getString(R.string.alert),false);
            }
        }else {
            onOfficeVisitPostLisner.OnOfficeVisitPostFail("You have not visited the office",
                    context.getString(R.string.alert),false);
        }
    }




    private void postThePurposeVisit(final Context context, String purpose, String notes, final OnOfficeVisitPostLisner onOfficeVisitPostLisner)
    {
        if (notes==null)
            notes="";
        else if (notes.equals(""))
            notes="";
        else if (notes.isEmpty())
            notes="";
        APIService apiService= ApiUtils.getAPIService(ConstantsUtils.postBaseUrl+ConstantsUtils.postToUpdatePurposeOfOfficeVisit);
        apiService.postToUpdatePurposeOfOfficeVisit(PreferenceUtils.getOfficeVisitId(context),purpose,notes)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful())
                        {
                            try {
                                JSONObject jsonObject=new JSONObject(response.body().string());
                                if (jsonObject.getInt("Success")==1)
                                {
                                    onOfficeVisitPostLisner.OnOfficeVisitPostSuccess(jsonObject.getString("Msg"));
                                }else {
                                    onOfficeVisitPostLisner.OnOfficeVisitPostFail(jsonObject.getString("Msg"),
                                            context.getString(R.string.alert),false);
                                }

                            } catch (JSONException | IOException e) {
                                onOfficeVisitPostLisner.OnOfficeVisitPostFail(
                                        context.getString(R.string.justErrorCode)+" 61",context.getString(R.string.internalError),
                                        false);
                            }
                        }else {
                            onOfficeVisitPostLisner.OnOfficeVisitPostFail(
                                    context.getString(R.string.justErrorCode)+" 60",context.getString(R.string.ServerNotresponding),
                                    false);
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        onOfficeVisitPostLisner.OnOfficeVisitPostFail(
                                context.getString(R.string.noInternetMessage)+" 59",
                                context.getString(R.string.noInternetAlert),
                                true);
                    }
                });
    }


    @Override
    public void reqToPostTheOfficeCheckIn(final Context context, final OnOfficeCheckInLisner onOfficeCheckInLisner, final String officeLoc) {
        if (PreferenceUtils.getOfficeVisitId(context)!=null)
        {
            onOfficeCheckInLisner.OnofficeCheckInFail("You have already checked-In",context
                    .getString(R.string.alert),false);
        }else {
            APIService apiService= ApiUtils.getAPIService(ConstantsUtils.postBaseUrl+ConstantsUtils.postEntryOffice);
            apiService.postToSendOfficeEntryToServer(PreferenceUtils.getUserIdFromPreference(context),officeLoc)
                    .enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            if (response.isSuccessful())
                            {
                                try {
                                    JSONObject jsonObject=new JSONObject(response.body().string());
                                    if (jsonObject.getInt("Success")==1)
                                    {
                                        PreferenceUtils.addOfficeVisitId(jsonObject.getString("Msg"),context);
                                        switch (officeLoc) {
                                            case ConstantsUtils.BANGLORE_ADDRESS:
                                                onOfficeCheckInLisner.OnofficeCheckInSuccess("You have successfully check-In to Mapei Bengaluru HO");
                                                break;
                                            case ConstantsUtils.MUMBAI_ADDRESS:
                                                onOfficeCheckInLisner.OnofficeCheckInSuccess("You have successfully check-In to Mapei Mumbai office");
                                                break;
                                            case ConstantsUtils.DELHI_ADDRESS:
                                                onOfficeCheckInLisner.OnofficeCheckInSuccess("You have successfully check-In to Delhi/Faridabad office");

                                        }

                                    }else {
                                        onOfficeCheckInLisner.OnofficeCheckInFail(jsonObject.getString("Msg"),
                                                context.getString(R.string.alert),
                                                false)  ;
                                    }
                                } catch (JSONException | IOException e) {
                                    onOfficeCheckInLisner.OnofficeCheckInFail(context.getString(R.string.noInternetMessage)+" 64",
                                            context.getString(R.string.internalError),
                                            false);                                        }
                            }else {
                                onOfficeCheckInLisner.OnofficeCheckInFail(context.getString(R.string.noInternetMessage)+" 63",
                                        context.getString(R.string.ServerNotresponding),
                                        false);
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            onOfficeCheckInLisner.OnofficeCheckInFail(context.getString(R.string.noInternetMessage)+" 62",
                                    context.getString(R.string.noInternetAlert),
                                    true);
                        }
                    });
        }
    }

    @Override
    public void reqToPostTheOfficeCheckOut(final Context context, final OnOfficeCheckOutLisner onOfficeCheckOutLisner) {
        if (PreferenceUtils.getOfficeVisitId(context)==null)
        {
            onOfficeCheckOutLisner.OnofficeCheckOutFail("You have already checked-out",
                    context.getString(R.string.alert),false);
        }else {
            APIService apiService= ApiUtils.getAPIService(ConstantsUtils.postBaseUrl+ConstantsUtils.postExitOffice);
            apiService.postToSendOfficeExitToServer(PreferenceUtils.getOfficeVisitId(context))
                    .enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            if (response.isSuccessful())
                            {
                                try {
                                    JSONObject jsonObject=new JSONObject(response.body().string());
                                    if (jsonObject.getInt("Success")==1)
                                    {
                                        PreferenceUtils.clearTheOfficeVisitId(context);
                                        onOfficeCheckOutLisner.OnofficeCheckOutSuccess("You have successfully checked-out.");
                                    }else {
                                        onOfficeCheckOutLisner.OnofficeCheckOutFail(jsonObject.getString("Msg"),
                                                context.getString(R.string.alert),false);
                                    }
                                } catch (JSONException | IOException e) {
                                    onOfficeCheckOutLisner.OnofficeCheckOutFail(context.getString(R.string.noInternetMessage)+" 67",
                                            context.getString(R.string.internalError),
                                            false);
                                }
                            }else {
                                onOfficeCheckOutLisner.OnofficeCheckOutFail(context.getString(R.string.noInternetMessage)+" 66",
                                        context.getString(R.string.ServerNotresponding),
                                        false);
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            onOfficeCheckOutLisner.OnofficeCheckOutFail(context.getString(R.string.noInternetMessage)+" 65",
                                    context.getString(R.string.noInternetAlert),
                                    true);
                        }
                    });
        }
    }
}
