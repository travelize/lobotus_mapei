package com.lobotus.mapei.flowfragments.mvp.intrectorimp;

import android.content.Context;

import com.lobotus.mapei.R;
import com.lobotus.mapei.flowfragments.model.ComplaintDataTypeModel;
import com.lobotus.mapei.flowfragments.model.ComplaintTypeModel;
import com.lobotus.mapei.flowfragments.model.CustomerDataModel;
import com.lobotus.mapei.flowfragments.model.CustomerModel;
import com.lobotus.mapei.flowfragments.model.ProductsDataModel;
import com.lobotus.mapei.flowfragments.model.ProjectsDataModel;
import com.lobotus.mapei.flowfragments.model.ProjectsModel;
import com.lobotus.mapei.flowfragments.mvp.intrector.MeetingInterator;
import com.lobotus.mapei.retrofitapi.APIService;
import com.lobotus.mapei.retrofitapi.ApiUtils;
import com.lobotus.mapei.utils.CommonFunc;
import com.lobotus.mapei.utils.ConstantsUtils;
import com.lobotus.mapei.utils.PreferenceUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by user1 on 04-10-2017.
 */

public class MeetingInteratorImp implements MeetingInterator {
    ///////////////////////////////////////////////////////// customer details
    @Override
    public void reqToPostCustomerDataReqToServer(final Context context,
                                                 final OnMeetingCustomerDataDownlodeLisner onMeetingCustomerDataDownlodeLisner) {

        APIService apiService = ApiUtils.getAPIService(ConstantsUtils.postBaseUrl + ConstantsUtils.postToDownlodeTheCustomersData);
        apiService.postReqTogetTheCustomerData("Active").enqueue(new Callback<CustomerModel>() {
            @Override
            public void onResponse(Call<CustomerModel> call, Response<CustomerModel> response) {
                if (response.isSuccessful()) {
                    CustomerModel customerModel = response.body();
                    if (customerModel == null) {
                        onMeetingCustomerDataDownlodeLisner.OnMeetingCustomerDataDownlodeFail(
                                context.getString(R.string.justErrorCode) + " 8", context.getString(R.string.ServerNotresponding),
                                false);
                    } else {
                        if (customerModel.getSuccess() == 1) {
                            onMeetingCustomerDataDownlodeLisner
                                    .OnMeetingCustomerDataDownlodeSuccess((ArrayList<CustomerDataModel>) customerModel.getCustomerDataModels());
                        } else {
                            onMeetingCustomerDataDownlodeLisner.OnMeetingCustomerDataDownlodeFail(
                                    customerModel.getMsg(), context.getString(R.string.alert),
                                    false);
                        }

                    }

                } else {
                    onMeetingCustomerDataDownlodeLisner.OnMeetingCustomerDataDownlodeFail(
                            context.getString(R.string.justErrorCode) + " 7", context.getString(R.string.ServerNotresponding),
                            false);
                }
            }

            @Override
            public void onFailure(Call<CustomerModel> call, Throwable t) {
                onMeetingCustomerDataDownlodeLisner.OnMeetingCustomerDataDownlodeFail(
                        context.getString(R.string.noInternetMessage) + " 9",
                        context.getString(R.string.noInternetAlert),
                        true);
            }
        });

    }

    ///////////////////////////////////////////////////////////////////////////////  get name and type
    @Override
    public void getCustomerNamaAndTypeFromTheChoosenProject(ArrayList<ProjectsDataModel> projectsDataModels,
                                                            String autocompletetext, Context context,
                                                            OnCustomerNameAndTypeLisner onCustomerNameAndTypeLisner) {


        if (projectsDataModels.isEmpty()) {
            onCustomerNameAndTypeLisner.OnCustomerNameAndTypeGetLisnerFail("No Product data found", context.getString(R.string.alert), false);
        } else {
            for (int i = 0; i < projectsDataModels.size(); i++) {
                if (projectsDataModels.get(i).getProjectName().equalsIgnoreCase(autocompletetext)) {
                    onCustomerNameAndTypeLisner.OnCustomerNameAndTypeGetLisnerSuccess(projectsDataModels.get(i)
                            .getCustomerName(), projectsDataModels.get(i).getCustomerType());
                    break;
                }
            }
        }


    }

    ////////////////////////////////// complaints data downlode lisner
    @Override
    public void getTheComplaintsDataFromTheServer(final Context context, final OnClomplaintsDataDownlodeLisner
            onClomplaintsDataDownlodeLisner) {
        APIService apiService = ApiUtils.getAPIService(ConstantsUtils.postBaseUrl + ConstantsUtils.postToGetTheComplaintsData);
        apiService.postTogetComplaintsFromServer("")
                .enqueue(new Callback<ComplaintTypeModel>() {
                    @Override
                    public void onResponse(Call<ComplaintTypeModel> call, Response<ComplaintTypeModel> response) {
                        if (response.isSuccessful()) {
                            if (response.body() == null) {
                                onClomplaintsDataDownlodeLisner.OnComplaintsDataDownlodeFail(
                                        context.getString(R.string.justErrorCode) + " 36", context.getString(R.string.ServerNotresponding),
                                        false);
                            } else {
                                if (response.body().getSuccess() == 1)
                                    onClomplaintsDataDownlodeLisner.OnComplaintsDataDownlodeSuccess((ArrayList<ComplaintDataTypeModel>) response.body().getData());
                                else
                                    onClomplaintsDataDownlodeLisner.OnComplaintsDataDownlodeFail(response.body().getErrorMsg(),
                                            context.getString(R.string.alert), false);
                            }
                        } else {
                            onClomplaintsDataDownlodeLisner.OnComplaintsDataDownlodeFail(
                                    context.getString(R.string.justErrorCode) + " 35", context.getString(R.string.ServerNotresponding),
                                    false);
                        }
                    }

                    @Override
                    public void onFailure(Call<ComplaintTypeModel> call, Throwable t) {
                        onClomplaintsDataDownlodeLisner.OnComplaintsDataDownlodeFail(
                                context.getString(R.string.noInternetMessage) + " 34",
                                context.getString(R.string.noInternetAlert),
                                true);
                    }
                });
    }

    /////////////////////////////////////////////////////////// posting the meeting data to the server
    @Override
    public void reqToPostTheMeetingsDataToServer(Context context,
                                                 String projectName,
                                                 ArrayList<ProjectsDataModel> projectsDataModels,
                                                 String customerName,
                                                 String customerType,
                                                 int meetingTypeSpinnerSelectedPos,
                                                 int submeetingTypeSelectedSpinner,
                                                 String valueOfFllowUpOrder,
                                                 String orderLostOfFollowupOrder,
                                                 int complaintsTypesSpinnerSelectedPos,
                                                 int arFollowTypesSpinnerSelectedPos,
                                                 String arFollowPaymentNewDate,
                                                 String arFollowPaymentProjectissue,
                                                 String arFollowPaymentProjectDeley,
                                                 String arFollowCformFollowupCollected,
                                                 String arFollowCformFollowupNewDate,
                                                 String arFollowCformFollowupNotTracable,
                                                 String arFollowCformReconcilationMissingInvoice,
                                                 String arFollowCformReconcilationShortPayment,
                                                 String arFollowCformReconcilationStatement,
                                                 ArrayList<ProductsDataModel> productsDataModels_Submitance,
                                                 ArrayList<ProductsDataModel> productsDataModels_Presentation,
                                                 ArrayList<ProductsDataModel> productsDataModels_Followup,
                                                 String notes,
                                                 OnMeetingPostDataDownlodeLisner onMeetingPostDataDownlodeLisner,
                                                 String location, String meetingSubTypeValue, String customerId) {

        if (PreferenceUtils.checkUserisLogedin(context)) {
            postTheMeeeting(context,
                    projectName, projectsDataModels,
                    customerName, customerType,
                    meetingTypeSpinnerSelectedPos, submeetingTypeSelectedSpinner, valueOfFllowUpOrder,
                    orderLostOfFollowupOrder, complaintsTypesSpinnerSelectedPos,
                    arFollowTypesSpinnerSelectedPos, arFollowPaymentNewDate,
                    arFollowPaymentProjectissue, arFollowPaymentProjectDeley,
                    arFollowCformFollowupCollected, arFollowCformFollowupNewDate,
                    arFollowCformFollowupNotTracable, arFollowCformReconcilationMissingInvoice,
                    arFollowCformReconcilationShortPayment, arFollowCformReconcilationStatement,
                    productsDataModels_Submitance,
                    productsDataModels_Presentation,
                    productsDataModels_Followup, notes, onMeetingPostDataDownlodeLisner, location, meetingSubTypeValue, customerId);
        } else {
            CommonFunc.commonDialog(context, "Alert!", "Session Lost Login back", false);
        }
    }


    private void postTheMeeeting(Context context,
                                 String projectName,
                                 ArrayList<ProjectsDataModel> projectsDataModels,
                                 String customerName,
                                 String customerType,
                                 int meetingTypeSpinnerSelectedPos,
                                 int submeetingTypeSelectedSpinner,
                                 String valueOfFllowUpOrder,
                                 String orderLostOfFollowupOrder,
                                 int complaintsTypesSpinnerSelectedPos,
                                 int arFollowTypesSpinnerSelectedPos,
                                 String arFollowPaymentNewDate,
                                 String arFollowPaymentProjectissue,
                                 String arFollowPaymentProjectDeley,
                                 String arFollowCformFollowupCollected,
                                 String arFollowCformFollowupNewDate,
                                 String arFollowCformFollowupNotTracable,
                                 String arFollowCformReconcilationMissingInvoice,
                                 String arFollowCformReconcilationShortPayment,
                                 String arFollowCformReconcilationStatement,
                                 ArrayList<ProductsDataModel> productsDataModels_Submitance,
                                 ArrayList<ProductsDataModel> productsDataModels_Presentation,
                                 ArrayList<ProductsDataModel> productsDataModels_Followup,
                                 String notes,
                                 OnMeetingPostDataDownlodeLisner onMeetingPostDataDownlodeLisner, String location,
                                 String meetingSubTypeValue, String customerId) {


        switch (meetingTypeSpinnerSelectedPos) {
            case 1:
                // meeting
                postMeetingTypeFollowupForOrders(projectsDataModels, projectName,
                        meetingTypeSpinnerSelectedPos, submeetingTypeSelectedSpinner, productsDataModels_Submitance,
                        productsDataModels_Presentation, productsDataModels_Followup, valueOfFllowUpOrder, orderLostOfFollowupOrder, notes,
                        context, onMeetingPostDataDownlodeLisner, location, meetingSubTypeValue, customerId);
                break;
            case 2:
                // Ar follow up
                switch (arFollowTypesSpinnerSelectedPos) {
                    case 1:// payment
                        postArFollowUpTypePayment(getProjectId(projectsDataModels, projectName),
                                meetingTypeSpinnerSelectedPos, arFollowTypesSpinnerSelectedPos,
                                arFollowPaymentNewDate, arFollowPaymentProjectissue, arFollowPaymentProjectDeley,
                                notes, context, onMeetingPostDataDownlodeLisner, location, customerId);

                        break;
                    case 2: // c form
                        postTheArFollowupCoforms(getProjectId(projectsDataModels, projectName),
                                String.valueOf(meetingTypeSpinnerSelectedPos), String.valueOf(arFollowTypesSpinnerSelectedPos),
                                arFollowCformFollowupNewDate, arFollowCformFollowupCollected,
                                arFollowCformFollowupNotTracable, notes, context,
                                onMeetingPostDataDownlodeLisner, location, customerId);
                        break;
                    case 3: // reconcilation
                        postTheArFollowupReconciliation(getProjectId(projectsDataModels, projectName),
                                String.valueOf(meetingTypeSpinnerSelectedPos), String.valueOf(arFollowTypesSpinnerSelectedPos),
                                arFollowCformReconcilationShortPayment, arFollowCformReconcilationMissingInvoice,
                                arFollowCformReconcilationStatement, notes, context,
                                onMeetingPostDataDownlodeLisner, location, customerId);
                }
                break;
            // complaints
            case 3:
                postingTheMeetingTypeComplaints(getProjectId(projectsDataModels, projectName),
                        String.valueOf(meetingTypeSpinnerSelectedPos), String.valueOf(complaintsTypesSpinnerSelectedPos), notes,
                        context, onMeetingPostDataDownlodeLisner, location, customerId);
        }

    }

    /// posting  follow up for order
    private void postMeetingTypeFollowupForOrders(ArrayList<ProjectsDataModel> projectsDataModels,
                                                  String projectName, int meetingTypeSpinnerSelectedPos, int submeetingTypeSelectedSpinner,
                                                  ArrayList<ProductsDataModel> productsDataModels_Submitance,
                                                  ArrayList<ProductsDataModel> productsDataModels_Presentation,
                                                  ArrayList<ProductsDataModel> productsDataModels_Followup,
                                                  String value, String OrderLost, String Notes, final Context context,
                                                  final OnMeetingPostDataDownlodeLisner onMeetingPostDataDownlodeLisner, String location, String meetingSubTypeValue, String customerId) {
        String productsJsonArray = "";
        switch (submeetingTypeSelectedSpinner) {
            case 1:
                productsJsonArray = getProductsJsonArray(productsDataModels_Submitance, context);
                break;
            case 2:
                productsJsonArray = getProductsJsonArray(productsDataModels_Presentation, context);
                break;
            case 3:
                productsJsonArray = getProductsJsonArray(productsDataModels_Followup, context);
        }

        APIService apiService = ApiUtils.getAPIService(ConstantsUtils.postBaseUrl + ConstantsUtils.postToAddTheMeetings);
        System.out.println("vvvvvvvvvvvvv-------ProjectID---------" + getProjectId(projectsDataModels, projectName));
        System.out.println("vvvvvvvvvvvvv-------UserId---------" + PreferenceUtils.getUserIdFromPreference(context));
        System.out.println("vvvvvvvvvvvvv-------MeetingTypeID---------" + String.valueOf(meetingTypeSpinnerSelectedPos));
        System.out.println("vvvvvvvvvvvvv-------MeetingSubTypeID---------" + String.valueOf(submeetingTypeSelectedSpinner));
        System.out.println("vvvvvvvvvvvvv-------ProductList---------" + productsJsonArray);
        System.out.println("vvvvvvvvvvvvv-------Status---------" + "Pending");
        System.out.println("vvvvvvvvvvvvv-------OrderValue---------" + value);
        System.out.println("vvvvvvvvvvvvv-------OrderLost---------" + OrderLost);
        System.out.println("vvvvvvvvvvvvv-------Remarks---------" + Notes);
        System.out.println("vvvvvvvvvvvvv-------Location---------" + location);
        System.out.println("vvvvvvvvvvvvv-------TotalValue---------" + meetingSubTypeValue);
        System.out.println("vvvvvvvvvvvvv-------CustomerID---------" + customerId);

        apiService.postTheMeetingTypeMeeting(getProjectId(projectsDataModels, projectName),
                PreferenceUtils.getUserIdFromPreference(context), String.valueOf(meetingTypeSpinnerSelectedPos),
                String.valueOf(submeetingTypeSelectedSpinner),
                productsJsonArray, "Pending",
                value, OrderLost, Notes, location, meetingSubTypeValue, customerId).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    if (response.body() == null) {
                        onMeetingPostDataDownlodeLisner.OnMeetingPostFail(context.getString(R.string.justErrorCode) + " 39",
                                context.getString(R.string.alert), false);
                    } else {
                        try {
                            JSONObject jsonObject = new JSONObject(response.body().string());
                            if (jsonObject.getInt("Success") == 1) {
                                onMeetingPostDataDownlodeLisner.OnMeetingPostSuccess(jsonObject.getString("Msg"),
                                        jsonObject.getString("MeetingID"));
                            } else {
                                onMeetingPostDataDownlodeLisner.OnMeetingPostFail(jsonObject.getString("Msg"),
                                        context.getString(R.string.alert), false);
                            }
                        } catch (JSONException | IOException e) {
                            onMeetingPostDataDownlodeLisner.OnMeetingPostFail(context.getString(R.string.justErrorCode) + " 38",
                                    context.getString(R.string.internalError), false);
                        }
                    }
                } else {
                    onMeetingPostDataDownlodeLisner.OnMeetingPostFail(context.getString(R.string.justErrorCode) + " 40",
                            context.getString(R.string.alert), false);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                onMeetingPostDataDownlodeLisner.OnMeetingPostFail(context.getString(R.string.noInternetMessage) + " 41",
                        context.getString(R.string.noInternetAlert),
                        true);
            }
        });
    }

    private String getProjectId(ArrayList<ProjectsDataModel> projectsDataModels,
                                String projectName) {
        String projectId = "";
        for (int i = 0; i < projectsDataModels.size(); i++) {
            if (projectName.equals(projectsDataModels.get(i).getProjectName())) {
                projectId = projectsDataModels.get(i).getProjectID();
                break;
            }
        }
        return projectId;
    }

    private String getProductsJsonArray(ArrayList<ProductsDataModel> productsDataModels, Context context) {

        JSONArray jsonArrayProjucts = new JSONArray();
        for (int i = 0; i < productsDataModels.size(); i++) {
            if (productsDataModels.get(i).isChecked()) {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("ProductID", productsDataModels.get(i).getProductID());
                    jsonObject.put("Amount", productsDataModels.get(i).getPrice());
                    jsonObject.put("Qty", productsDataModels.get(i).getQty());
                    jsonArrayProjucts.put(jsonObject);
                } catch (JSONException e) {
                    CommonFunc.commonDialog(context, context.getString(R.string.alert),
                            context.getString(R.string.justErrorCode) + " 37", false);
                }
            }
        }
        if (jsonArrayProjucts.length() != 0)
            return jsonArrayProjucts.toString();
        else
            return "";
    }

    ////// posting   Ar follow up
    // posting   Ar follow up type  payment
    private void postArFollowUpTypePayment(String projectId, int meetingTypeSpinnerSelectedPos, int arFollowTypesSpinnerSelectedPos,
                                           String ondate, String ProductIssues,
                                           String ProductDelay, String notes, final Context context, final OnMeetingPostDataDownlodeLisner onMeetingPostDataDownlodeLisner, String location, String customerId) {
        APIService apiService = ApiUtils.getAPIService(ConstantsUtils.postBaseUrl + ConstantsUtils.postToAddTheMeetings);

        System.out.println("vvvvvvvvvvvvv-------MeetingTypeID---------" + String.valueOf(meetingTypeSpinnerSelectedPos));
        System.out.println("vvvvvvvvvvvvv-------FollowupTypeID---------" + String.valueOf(arFollowTypesSpinnerSelectedPos));


        apiService.postTheMeetingTypeArFollowPayment(projectId, PreferenceUtils.getUserIdFromPreference(context),
                String.valueOf(meetingTypeSpinnerSelectedPos), String.valueOf(arFollowTypesSpinnerSelectedPos),
                ondate, ProductIssues, ProductDelay, "Pending", notes, location, customerId)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()) {
                            if (response.body() == null) {
                                onMeetingPostDataDownlodeLisner.OnMeetingPostFail(context.getString(R.string.justErrorCode) + " 43",
                                        context.getString(R.string.alert), false);
                            } else {
                                try {
                                    JSONObject jsonObject = new JSONObject(response.body().string());
                                    if (jsonObject.getInt("Success") == 1) {
                                        onMeetingPostDataDownlodeLisner.OnMeetingPostSuccess(jsonObject.getString("Msg"),
                                                jsonObject.getString("MeetingID"));
                                    } else {
                                        onMeetingPostDataDownlodeLisner.OnMeetingPostFail(jsonObject.getString("Msg"),
                                                context.getString(R.string.alert), false);
                                    }
                                } catch (JSONException | IOException e) {
                                    onMeetingPostDataDownlodeLisner.OnMeetingPostFail(context.getString(R.string.justErrorCode) + " 44",
                                            context.getString(R.string.internalError), false);
                                }
                            }
                        } else {
                            onMeetingPostDataDownlodeLisner.OnMeetingPostFail(context.getString(R.string.justErrorCode) + " 40",
                                    context.getString(R.string.alert), false);
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        onMeetingPostDataDownlodeLisner.OnMeetingPostFail(context.getString(R.string.noInternetMessage) + " 45",
                                context.getString(R.string.noInternetAlert),
                                true);
                    }
                });
    }
    // posting   Ar follow up type  C forms

    private void postTheArFollowupCoforms(String projectID, String MeetingTypeID, String FollowupTypeID,
                                          String OnDate, String CFCollected, String CFNotTraceable,
                                          String notes, final Context context,
                                          final OnMeetingPostDataDownlodeLisner onMeetingPostDataDownlodeLisner,
                                          String location, String customerId) {
        APIService apiService = ApiUtils.getAPIService(ConstantsUtils.postBaseUrl + ConstantsUtils.postToAddTheMeetings);

        System.out.println("vvvvvvvvvvvvv-------MeetingTypeID---------" + MeetingTypeID);
        System.out.println("vvvvvvvvvvvvv-------FollowupTypeID---------" + FollowupTypeID);
        apiService.postTheMeetingTypeArFollowCform(projectID, PreferenceUtils.getUserIdFromPreference(context),
                MeetingTypeID, FollowupTypeID, OnDate, CFCollected, CFNotTraceable, "Pending", notes, location, customerId).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    if (response.body() == null) {
                        onMeetingPostDataDownlodeLisner.OnMeetingPostFail(context.getString(R.string.justErrorCode) + " 49",
                                context.getString(R.string.alert), false);
                    } else {
                        try {
                            JSONObject jsonObject = new JSONObject(response.body().string());
                            if (jsonObject.getInt("Success") == 1) {
                                onMeetingPostDataDownlodeLisner.OnMeetingPostSuccess(jsonObject.getString("Msg"),
                                        jsonObject.getString("MeetingID"));
                            } else {
                                onMeetingPostDataDownlodeLisner.OnMeetingPostFail(jsonObject.getString("Msg"),
                                        context.getString(R.string.alert), false);
                            }
                        } catch (JSONException | IOException e) {
                            onMeetingPostDataDownlodeLisner.OnMeetingPostFail(context.getString(R.string.justErrorCode) + " 48",
                                    context.getString(R.string.internalError), false);
                        }
                    }
                } else {
                    onMeetingPostDataDownlodeLisner.OnMeetingPostFail(context.getString(R.string.justErrorCode) + " 47",
                            context.getString(R.string.alert), false);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                onMeetingPostDataDownlodeLisner.OnMeetingPostFail(context.getString(R.string.noInternetMessage) + " 46",
                        context.getString(R.string.noInternetAlert),
                        true);
            }
        });
    }

    // posting   Ar follow up type  Reconciliation
    private void postTheArFollowupReconciliation(String ProjectID, String MeetingTypeID, String FollowupTypeID,
                                                 String ShortPayments, String MissingInvoices, String Statement,
                                                 String notes, final Context context,
                                                 final OnMeetingPostDataDownlodeLisner onMeetingPostDataDownlodeLisner,
                                                 String location, String customerId) {
        APIService apiService = ApiUtils.getAPIService(ConstantsUtils.postBaseUrl + ConstantsUtils.postToAddTheMeetings);

        System.out.println("vvvvvvvvvvvvv-------MeetingTypeID---------" + MeetingTypeID);
        System.out.println("vvvvvvvvvvvvv-------FollowupTypeID---------" + FollowupTypeID);

        apiService.postTheMeetingTypeArFollowReconciliation(ProjectID, PreferenceUtils.getUserIdFromPreference(context),
                MeetingTypeID, FollowupTypeID, ShortPayments, "Pending", MissingInvoices, Statement, notes, location, customerId).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    if (response.body() == null) {
                        onMeetingPostDataDownlodeLisner.OnMeetingPostFail(context.getString(R.string.justErrorCode) + " 53",
                                context.getString(R.string.alert), false);
                    } else {
                        try {
                            JSONObject jsonObject = new JSONObject(response.body().string());
                            if (jsonObject.getInt("Success") == 1) {
                                onMeetingPostDataDownlodeLisner.OnMeetingPostSuccess(jsonObject.getString("Msg"),
                                        jsonObject.getString("MeetingID"));
                            } else {
                                onMeetingPostDataDownlodeLisner.OnMeetingPostFail(jsonObject.getString("Msg"),
                                        context.getString(R.string.alert), false);
                            }
                        } catch (JSONException | IOException e) {
                            onMeetingPostDataDownlodeLisner.OnMeetingPostFail(context.getString(R.string.justErrorCode) + " 52",
                                    context.getString(R.string.internalError), false);
                        }
                    }
                } else {
                    onMeetingPostDataDownlodeLisner.OnMeetingPostFail(context.getString(R.string.justErrorCode) + " 51",
                            context.getString(R.string.alert), false);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                onMeetingPostDataDownlodeLisner.OnMeetingPostFail(context.getString(R.string.noInternetMessage) + " 50",
                        context.getString(R.string.noInternetAlert),
                        true);
            }
        });
    }

    ////// posting   meeting type complaints
    private void postingTheMeetingTypeComplaints(String ProjectID, String MeetingTypeID,
                                                 String ComplaintTypeID, String notes, final Context context,
                                                 final OnMeetingPostDataDownlodeLisner onMeetingPostDataDownlodeLisner, String location, String customerId) {
        APIService apiService = ApiUtils.getAPIService(ConstantsUtils.postBaseUrl + ConstantsUtils.postToAddTheMeetings);

        System.out.println("vvvvvvvvvvvvv-------MeetingTypeID---------" + MeetingTypeID);
        System.out.println("vvvvvvvvvvvvv-------ComplaintTypeID---------" + ComplaintTypeID);

        apiService.postTheMeetingTypeComplaints(ProjectID, PreferenceUtils.getUserIdFromPreference(context), MeetingTypeID,
                ComplaintTypeID, "Pending", notes, location, customerId).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    if (response.body() == null) {
                        onMeetingPostDataDownlodeLisner.OnMeetingPostFail(context.getString(R.string.justErrorCode) + " 57",
                                context.getString(R.string.alert), false);
                    } else {
                        try {
                            JSONObject jsonObject = new JSONObject(response.body().string());
                            if (jsonObject.getInt("Success") == 1) {
                                onMeetingPostDataDownlodeLisner.OnMeetingPostSuccess(jsonObject.getString("Msg"),
                                        jsonObject.getString("MeetingID"));
                            } else {
                                onMeetingPostDataDownlodeLisner.OnMeetingPostFail(jsonObject.getString("Msg"),
                                        context.getString(R.string.alert), false);
                            }
                        } catch (JSONException | IOException e) {
                            onMeetingPostDataDownlodeLisner.OnMeetingPostFail(context.getString(R.string.justErrorCode) + " 56",
                                    context.getString(R.string.internalError), false);
                        }
                    }
                } else {
                    onMeetingPostDataDownlodeLisner.OnMeetingPostFail(context.getString(R.string.justErrorCode) + " 55",
                            context.getString(R.string.alert), false);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                onMeetingPostDataDownlodeLisner.OnMeetingPostFail(context.getString(R.string.noInternetMessage) + " 54",
                        context.getString(R.string.noInternetAlert),
                        true);
            }
        });

    }

    ///////////////////////////////////////////////// get the project list based on customer name
    @Override
    public void reqToGetTheProjectsListBasedOnCustomerFromIntractor(final Context context, String custId,
                                                                    final OnProjectsListBasedOnCustomerNameLisner onProjectsListBasedOnCustomerNameLisner) {
        APIService apiService = ApiUtils.getAPIService(ConstantsUtils.postBaseUrl + ConstantsUtils.PostingToGetTheProjectListBasedOnCustomerName);
        apiService.postToGetTheListOfProjectBasedOnCustomerName(custId)
                .enqueue(new Callback<ProjectsModel>() {
                    @Override
                    public void onResponse(Call<ProjectsModel> call, Response<ProjectsModel> response) {

                        if (response.isSuccessful()) {
                            if (response.body() != null) {

                                if (response.body().getSuccess() == 1) {
                                    if (response.body().getData() != null) {
                                        if (!response.body().getData().isEmpty()) {
                                            onProjectsListBasedOnCustomerNameLisner.OnProjectsListBasedOnCustomerNameDownlodeSuccess((ArrayList<ProjectsDataModel>) response.body().getData());
                                        } else {
                                            onProjectsListBasedOnCustomerNameLisner.OnProjectsListBasedOnCustomerNameDownlodeFail(context.getString(R.string.justErrorCode) + " 111",
                                                    context.getString(R.string.internalError), false);
                                        }
                                    } else {
                                        onProjectsListBasedOnCustomerNameLisner.OnProjectsListBasedOnCustomerNameDownlodeFail(context.getString(R.string.justErrorCode) + " 110",
                                                context.getString(R.string.internalError), false);
                                    }
                                } else {
                                    onProjectsListBasedOnCustomerNameLisner.OnProjectsListBasedOnCustomerNameDownlodeFail(response.body().getErrorMsg(),
                                            context.getString(R.string.alert), false);
                                }

                            } else {
                                onProjectsListBasedOnCustomerNameLisner.OnProjectsListBasedOnCustomerNameDownlodeFail(context.getString(R.string.justErrorCode) + " 109",
                                        context.getString(R.string.internalError), false);
                            }
                        } else {
                            onProjectsListBasedOnCustomerNameLisner.OnProjectsListBasedOnCustomerNameDownlodeFail(context.getString(R.string.justErrorCode) + " 108",
                                    context.getString(R.string.alert), false);
                        }
                    }

                    @Override
                    public void onFailure(Call<ProjectsModel> call, Throwable t) {
                        onProjectsListBasedOnCustomerNameLisner.OnProjectsListBasedOnCustomerNameDownlodeFail(context.getString(R.string.noInternetMessage) + " 107",
                                context.getString(R.string.noInternetAlert),
                                true);
                    }
                });
    }


}
