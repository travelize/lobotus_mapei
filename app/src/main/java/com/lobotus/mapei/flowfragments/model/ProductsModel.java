package com.lobotus.mapei.flowfragments.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by user1 on 06-10-2017.
 */

public class ProductsModel {

    @SerializedName("Success")
    @Expose
    private Integer success;
    @SerializedName("data")
    @Expose
    private List<ProductsDataModel> data = null;
    @SerializedName("Msg")
    @Expose
    private String errorMsg=null;

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public List<ProductsDataModel> getData() {
        return data;
    }

    public void setData(List<ProductsDataModel> data) {
        this.data = data;
    }
}
