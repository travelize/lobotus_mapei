package com.lobotus.mapei.flowfragments.mvp.presentor;

import android.content.Context;
import android.view.View;

import com.lobotus.mapei.flowfragments.model.CustomerDataModel;

import java.util.ArrayList;

/**
 * Created by user1 on 04-10-2017.
 */

public interface TravelPresentor {
    //////////////////////////// downlode the customer data
    void reqToGetTheCustomerDataFromPresentor(Context context);
    /////////////////////////// downlode MOT and POT
    void reqToGetMOTandPOTFromPresentor(Context context);
    ////////////////////////// validate credential in the presentor
    void reqToValidateAllInPresentorItself(View view, Context context,
                                           String customerNameInAutoCompletetextView,
                                           int modeOfTravelSelectedPos,
                                           boolean isModeOfTravelArraylistEmpty,
                                           int meeetingPurposeSelectedPos,
                                           boolean isMeetingPurposeArraylistEmpty,
                                           int ownvehicalspinnerselectedpos,
                                           int fuletypespinnerselectedpos);
    /////////////////////////// post to save the travel details
    void reqToSaveTheTravelDetails(Context context,String remark,
                                   String custId,String modeOfTravel,
                                   String purpose,String attachmentB64,
                                   String startLoc,String endLoc,String imagecaption,String ownvehicalspinnerselectedpos,
                                   String fuletypespinnerselectedpos,String imageAmount);
}
