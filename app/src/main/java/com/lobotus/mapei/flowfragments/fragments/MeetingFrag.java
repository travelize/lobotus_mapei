package com.lobotus.mapei.flowfragments.fragments;


import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatAutoCompleteTextView;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.lobotus.mapei.R;
import com.lobotus.mapei.baseactivity.BaseActivity;
import com.lobotus.mapei.customwidgits.LabelledSpinner;
import com.lobotus.mapei.flowfragments.model.ComplaintDataTypeModel;
import com.lobotus.mapei.flowfragments.model.CustomerDataModel;
import com.lobotus.mapei.flowfragments.model.ProductsDataModel;
import com.lobotus.mapei.flowfragments.model.ProjectsDataModel;
import com.lobotus.mapei.flowfragments.mvp.presentor.MeetingPresentor;
import com.lobotus.mapei.flowfragments.mvp.presentorimp.MeetingPresenterImp;
import com.lobotus.mapei.flowfragments.mvp.views.MeetingView;
import com.lobotus.mapei.flowfragments.onactivityforresult.CustomerActForResult;
import com.lobotus.mapei.flowfragments.onactivityforresult.MeetingProductOnActiForResult;
import com.lobotus.mapei.utils.CommonFunc;
import com.lobotus.mapei.utils.ConstantsUtils;
import com.lobotus.mapei.utils.NetworkUtils;
import com.lobotus.mapei.utils.PermissionUtils;
import com.lobotus.mapei.utils.PreferenceUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class MeetingFrag extends Fragment implements
        LabelledSpinner.OnItemChosenListener,
        View.OnClickListener, View.OnFocusChangeListener, MeetingView {

    // req code to get thecustomer seltion done. . .
    private final int REQ_CODE_TO_GET_THE_CUSTOMR_DATA = 11;
    // TEMP CUSTOMER ID. .
    private String TEMP_CUST_ID = null;
    private View fragmentView = null;
    //
    private MeetingPresentor meetingPresentor = null;
    ////// temp data's
    private ArrayList<ProjectsDataModel> projectsDataModels = new ArrayList<>();
    // productsDataModel for different meeting types
    public static ArrayList<ProductsDataModel> productsDataModels_Submitance = new ArrayList<>();
    public static ArrayList<ProductsDataModel> productsDataModels_Presentation = new ArrayList<>();
    public static ArrayList<ProductsDataModel> productsDataModels_Follow_up = new ArrayList<>();
    ///// get products req code  for different meeting types
    public static int REQ_CODE_TO_GET_THE_PRODUCTS_SUBMITANCE = 8;
    public static int REQ_CODE_TO_GET_THE_PRODUCTS_PRESENTATION = 9;
    public static int REQ_CODE_TO_GET_THE_PRODUCTS_FOLLOW_UP = 10;
    //
    private ArrayList<ComplaintDataTypeModel> complaintDataTypeModels = new ArrayList<>();
    ////////////////////////////////////////////////////////////// life cycle stuff

    private boolean alreadyOnCreateExecuted = false;
    private int projuctSpinnerSelectedValueVefireLeavingThisFragmentToHistrayFrag = 0;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        alreadyOnCreateExecuted = false;
        System.out.println("yyyyyyyyyyyyyy------------onCreate------------------------" + alreadyOnCreateExecuted);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fragmentView = inflater.inflate(R.layout.fragment_meeting, container, false);
        fragmentView.findViewById(R.id.fragment_meeting_submit_button_id).setOnClickListener(this);
        fragmentView.findViewById(R.id.fragment_meeting_customer_type_edittext_id).setOnClickListener(this);
        fragmentView.findViewById(R.id.fragment_meeting_customer_mobile_no_edittext_id).setOnClickListener(this);
        fragmentView.findViewById(R.id.fragment_meeting_customer_location_edittext_id).setOnClickListener(this);
        fragmentView.findViewById(R.id.fragment_meeting_add_customer_id_imageview).setOnClickListener(this);
        fragmentView.findViewById(R.id.fragment_meeting_view_project_history).setOnClickListener(this);
        fragmentView.findViewById(R.id.fragment_meeting_loc_button_id).setOnClickListener(this);
        fragmentView.findViewById(R.id.fragment_meeting_customer_name_edittext_id).setOnClickListener(this);
        fragmentView.findViewById(R.id.fragment_meeting_location_edittext_id).setOnClickListener(this);
        ((AppCompatImageView) fragmentView.findViewById(R.id.fragment_meeting_add_projects_imageview_button))
                .setOnClickListener(this);
        ((AppCompatImageView) fragmentView.findViewById(R.id.fragment_meeting_add_projects_imageview_button))
                .setOnClickListener(this);
        setMeetingTypeSpinner();

        meetingPresentor = new MeetingPresenterImp(this);
        connectToGoogleApiClientAndGetTheAddress();
        if (alreadyOnCreateExecuted) {
            String[] strings = new String[projectsDataModels.size()];
            for (int i = 0; i < strings.length; i++) {
                strings[i] = projectsDataModels.get(i).getProjectName();
            }
            ((LabelledSpinner) fragmentView.findViewById(R.id.fragment_meeting_project_list_dropdown))
                    .setItemsArray(strings);
            ((LabelledSpinner) fragmentView.findViewById(R.id.fragment_meeting_project_list_dropdown))
                    .setDefaultErrorEnabled(false);
            ((LabelledSpinner) fragmentView.findViewById(R.id.fragment_meeting_project_list_dropdown))
                    .setDefaultErrorText("Please select project name");
            ((LabelledSpinner) fragmentView.findViewById(R.id.fragment_meeting_project_list_dropdown)).setSelection(projuctSpinnerSelectedValueVefireLeavingThisFragmentToHistrayFrag);
            ((LabelledSpinner) fragmentView.findViewById(R.id.fragment_meeting_project_list_dropdown)).setOnItemChosenListener(this);

        } else {
            setInitialProjectSelectSpinner();
        }
        ///
        ((AppCompatAutoCompleteTextView) fragmentView.findViewById(R.id.fragment_meeting_customer_name_edittext_id))
                .addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                       /* setInitialProjectSelectSpinner();
                        ((AppCompatEditText)fragmentView.findViewById(R.id.fragment_meeting_customer_type_edittext_id)).setText("");*/
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });
        ///
        alreadyOnCreateExecuted = true;
        return fragmentView;
    }


    @Override
    public void onDetach() {
        super.onDetach();
        CommonFunc.hideKeyboard(getActivity());
        productsDataModels_Follow_up.clear();
    }


    //////////////////////////////////////////////////////////////  products adapter
    public class AddProductsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        private Context context = null;
        private ArrayList<ProductsDataModel> productsDataModelsInAdapter = null;

        public AddProductsAdapter(Context context, ArrayList<ProductsDataModel> productsDataModelsInAdapter) {
            this.context = context;
            this.productsDataModelsInAdapter = productsDataModelsInAdapter;
        }


        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(context).inflate(R.layout.add_products_row_layout, parent, false);
            return new EnquiryHolder(view);


        }

        @Override
        public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
            EnquiryHolder enquiryHolder = (EnquiryHolder) holder;
            enquiryHolder.imageViewDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    double price = 0.0;
                    switch (((LabelledSpinner) fragmentView.findViewById(R.id.meeting_stub_meeting_sub_type_id))
                            .getSpinner().getSelectedItemPosition()) {
                        case 1: // submitance
                            for (int i = 0; i < productsDataModels_Submitance.size(); i++) {
                                if (productsDataModelsInAdapter.get(holder.getAdapterPosition()).getProductID().equals(productsDataModels_Submitance
                                        .get(i).getProductID())) {
                                    productsDataModels_Submitance.get(i).setChecked(false);
                                    productsDataModels_Submitance.get(i).setQty("0");
                                    productsDataModels_Submitance.get(i).setPrice("");
                                }
                            }
                            //
                            for (int i = 0; i < productsDataModels_Submitance.size(); i++) {
                                if (productsDataModels_Submitance.get(i).isChecked() && !productsDataModels_Submitance.get(i).getQty().isEmpty() &&
                                        !productsDataModels_Submitance.get(i).getPrice().isEmpty()) {
                                    price = price + (Double.valueOf(productsDataModels_Submitance.get(i).getPrice())) *
                                            (Double.valueOf(productsDataModels_Submitance.get(i).getQty()) > 0 ?
                                                    Double.valueOf(productsDataModels_Submitance.get(i).getQty()) : 1);
                                }
                            }
                            ((AppCompatEditText) fragmentView.findViewById(R.id.submitance_stub__orders_value_edittext_id))
                                    .setText("" + price);
                            break;
                        case 2: // presentation
                            for (int i = 0; i < productsDataModels_Presentation.size(); i++) {
                                if (productsDataModelsInAdapter.get(holder.getAdapterPosition()).getProductID().equals(productsDataModels_Presentation
                                        .get(i).getProductID())) {
                                    productsDataModels_Presentation.get(i).setChecked(false);
                                    productsDataModels_Presentation.get(i).setQty("0");
                                    productsDataModels_Presentation.get(i).setPrice("");
                                }
                            }
                            //
                            for (int i = 0; i < productsDataModels_Presentation.size(); i++) {
                                if (productsDataModels_Presentation.get(i).isChecked() && !productsDataModels_Presentation.get(i).getQty().isEmpty() &&
                                        !productsDataModels_Presentation.get(i).getPrice().isEmpty()) {
                                    price = price + (Double.valueOf(productsDataModels_Presentation.get(i).getPrice())) *
                                            (Double.valueOf(productsDataModels_Presentation.get(i).getQty()) > 0 ?
                                                    Double.valueOf(productsDataModels_Presentation.get(i).getQty()) : 1);
                                }
                            }
                            ((AppCompatEditText) fragmentView.findViewById(R.id.presentation_stub__orders_value_edittext_id))
                                    .setText("" + price);
                            break;
                        case 3: // follow up
                            for (int i = 0; i < productsDataModels_Follow_up.size(); i++) {
                                if (productsDataModelsInAdapter.get(holder.getAdapterPosition()).getProductID().equals(productsDataModels_Follow_up
                                        .get(i).getProductID())) {
                                    productsDataModels_Follow_up.get(i).setChecked(false);
                                    productsDataModels_Follow_up.get(i).setQty("0");
                                    productsDataModels_Follow_up.get(i).setPrice("");
                                }
                            }
                            //
                            for (int i = 0; i < productsDataModels_Follow_up.size(); i++) {
                                if (productsDataModels_Follow_up.get(i).isChecked() && !productsDataModels_Follow_up.get(i).getQty().isEmpty() &&
                                        !productsDataModels_Follow_up.get(i).getPrice().isEmpty()) {
                                    price = price + (Double.valueOf(productsDataModels_Follow_up.get(i).getPrice())) *
                                            (Double.valueOf(productsDataModels_Follow_up.get(i).getQty()) > 0 ?
                                                    Double.valueOf(productsDataModels_Presentation.get(i).getQty()) : 1);
                                }
                            }
                            ((AppCompatEditText) fragmentView.findViewById(R.id.follow_up_for_orders_value_edittext_id))
                                    .setText("" + price);
                            break;
                    }

                    //
                    productsDataModelsInAdapter.remove(holder.getAdapterPosition());
                    notifyDataSetChanged();
                }
            });
            enquiryHolder.textViewProductName.setText(productsDataModelsInAdapter.get(holder.getAdapterPosition()).getProductName());
            enquiryHolder.textViewAmt.setText(
                    "" + (Double.parseDouble(productsDataModelsInAdapter.get(holder.getAdapterPosition()).getPrice())) *
                            (Double.parseDouble(productsDataModelsInAdapter.get(holder.getAdapterPosition()).getQty()) > 0 ?
                                    Double.parseDouble(productsDataModelsInAdapter.get(holder.getAdapterPosition()).getQty()) : 1));

        }

        @Override
        public int getItemCount() {
            return productsDataModelsInAdapter.size();
        }


    }

    private class EnquiryHolder extends RecyclerView.ViewHolder {
        AppCompatImageView imageViewDelete = null;
        AppCompatTextView textViewProductName = null, textViewAmt = null;

        EnquiryHolder(View itemView) {
            super(itemView);
            this.imageViewDelete = itemView.findViewById(R.id.add_enquiry_layout_delete_imageview);
            this.textViewProductName = itemView.findViewById(R.id.add_enquiry_layout_product_name_id);
            this.textViewAmt = itemView.findViewById(R.id.add_enquiry_layout_product_amt_id);
        }
    }
    //////////////////////////////////////////////////////////////  data setting stuff

    @Override
    public void onItemChosen(View labelledSpinner, AdapterView<?> adapterView, View itemView, int position, long id) {
        switch (labelledSpinner.getId()) {
            case R.id.fragment_meeting_meeting_type_spinner:
                removeinflatedViewFromViewstubAndShowSelected(position);

                break;
            case R.id.spinner_ar_followup_stub_spinner_id:
                removeinflatedViewARFollow(position);
                break;
            case R.id.fragment_meeting_project_list_dropdown:
                if (position == 0) {
                    fragmentView.findViewById(R.id.fragment_meeting_view_project_history).setVisibility(View.GONE);
                } else {
                    fragmentView.findViewById(R.id.fragment_meeting_view_project_history).setVisibility(View.VISIBLE);
                }
                break;
            case R.id.meeting_stub_meeting_sub_type_id:
                removeAndInFlateMeetingSubtypes(position);
        }
    }

    @Override
    public void onNothingChosen(View labelledSpinner, AdapterView<?> adapterView) {
        // Do something here
    }


    ////////////////////
    private void removeinflatedViewFromViewstubAndShowSelected(int pos) {
        switch (pos) {
            case 0:
                // select
                //meetings
                if (((ViewStub) fragmentView.findViewById(R.id.fragment_meeting_meeting_viewstub)) == null) {
                    fragmentView.findViewById(R.id.fragment_meeting_meeting_viewstub_inflateId).setVisibility(View.GONE);
                }
                // complaints
                if (((ViewStub) fragmentView.findViewById(R.id.fragment_meeting_complaints_viewstub)) == null) {
                    fragmentView.findViewById(R.id.fragment_meeting_complaints_viewstub_inflateId).setVisibility(View.GONE);
                }
                // ar follow up
                if (((ViewStub) fragmentView.findViewById(R.id.fragment_meeting_arfollowup_viewstub)) == null) {
                    fragmentView.findViewById(R.id.fragment_meeting_arfollowup_viewstub_inflateId).setVisibility(View.GONE);
                }

                break;
            case 1:  //meeting
                /////////////////////
                // complaints
                if (((ViewStub) fragmentView.findViewById(R.id.fragment_meeting_complaints_viewstub)) == null) {
                    fragmentView.findViewById(R.id.fragment_meeting_complaints_viewstub_inflateId).setVisibility(View.GONE);
                }
                // ar follow up
                if (((ViewStub) fragmentView.findViewById(R.id.fragment_meeting_arfollowup_viewstub)) == null) {
                    fragmentView.findViewById(R.id.fragment_meeting_arfollowup_viewstub_inflateId).setVisibility(View.GONE);
                }
                ///////////////////////////
                // show meeting. . .
                if (((ViewStub) fragmentView.findViewById(R.id.fragment_meeting_meeting_viewstub)) != null) {
                    ((ViewStub) fragmentView.findViewById(R.id.fragment_meeting_meeting_viewstub))
                            .inflate();
                    /*fragmentView.findViewById(R.id.follow_up_for_orders__add_product_button_id).setOnClickListener(this);*/
/*
                    inflate the spinner
*/
                    setTheSubMeetingTypeSpinner();
                } else {
                    fragmentView.findViewById(R.id.fragment_meeting_meeting_viewstub_inflateId).setVisibility(View.VISIBLE);
                }
                //////////////////////////////// load products
//                if (productsDataModels_Follow_up.isEmpty())
//                    meetingPresentor.reqToGetTheProductsDataFromPresentor(getActivity());
                break;
            case 2:
                //AR follow up
                // hide meeting
                if (((ViewStub) fragmentView.findViewById(R.id.fragment_meeting_meeting_viewstub)) == null) {
                    fragmentView.findViewById(R.id.fragment_meeting_meeting_viewstub_inflateId).setVisibility(View.GONE);
                }
                // complaints
                if (((ViewStub) fragmentView.findViewById(R.id.fragment_meeting_complaints_viewstub)) == null) {
                    fragmentView.findViewById(R.id.fragment_meeting_complaints_viewstub_inflateId).setVisibility(View.GONE);
                }
                ///////////////////////////////////
                // AR follow up
                if (((ViewStub) fragmentView.findViewById(R.id.fragment_meeting_arfollowup_viewstub)) != null) {
                    ((ViewStub) fragmentView.findViewById(R.id.fragment_meeting_arfollowup_viewstub))
                            .inflate();
                } else {
                    fragmentView.findViewById(R.id.fragment_meeting_arfollowup_viewstub_inflateId).setVisibility(View.VISIBLE);
                }
                inflateArfollowUpTypesSpinner();
                break;
            case 3:
                // complaints
                //hide meeting. . .
                if (((ViewStub) fragmentView.findViewById(R.id.fragment_meeting_meeting_viewstub)) == null) {
                    fragmentView.findViewById(R.id.fragment_meeting_meeting_viewstub_inflateId).setVisibility(View.GONE);
                }
                // ar follow up
                if (((ViewStub) fragmentView.findViewById(R.id.fragment_meeting_arfollowup_viewstub)) == null) {
                    fragmentView.findViewById(R.id.fragment_meeting_arfollowup_viewstub_inflateId).setVisibility(View.GONE);
                }
                ///////////////////////////////////
                // complaints
                if (((ViewStub) fragmentView.findViewById(R.id.fragment_meeting_complaints_viewstub)) != null) {
                    ((ViewStub) fragmentView.findViewById(R.id.fragment_meeting_complaints_viewstub))
                            .inflate();
                } else {
                    fragmentView.findViewById(R.id.fragment_meeting_complaints_viewstub_inflateId).setVisibility(View.VISIBLE);
                }
                if (complaintDataTypeModels.isEmpty()) {
                    meetingPresentor.reqToGetTheComplaintsDataFromTheServer(getActivity());
                }

        }
    }

    private void removeAndInFlateMeetingSubtypes(int position) {

        switch (position) {
            case 0: // remove all
                // select
                //submitance
                if (((ViewStub) fragmentView.findViewById(R.id.meeting_stub_submittance_viewstub)) == null) {
                    fragmentView.findViewById(R.id.meeting_stub_submittance_viewstub_inflateId).setVisibility(View.GONE);
                }
                // presentation
                if (((ViewStub) fragmentView.findViewById(R.id.meeting_stub_presentation_viewstub)) == null) {
                    fragmentView.findViewById(R.id.meeting_stub_presentation_viewstub_inflateId).setVisibility(View.GONE);
                }
                // follow up
                if (((ViewStub) fragmentView.findViewById(R.id.meeting_stub_followupForOrders_viewstub)) == null) {
                    fragmentView.findViewById(R.id.meeting_stub_followupForOrders_viewstub_inflateId).setVisibility(View.GONE);
                }
                break;
            case 1: // show submitance and hide others
                // hide
                // presentation
                if (((ViewStub) fragmentView.findViewById(R.id.meeting_stub_presentation_viewstub)) == null) {
                    fragmentView.findViewById(R.id.meeting_stub_presentation_viewstub_inflateId).setVisibility(View.GONE);
                }
                // follow up
                if (((ViewStub) fragmentView.findViewById(R.id.meeting_stub_followupForOrders_viewstub)) == null) {
                    fragmentView.findViewById(R.id.meeting_stub_followupForOrders_viewstub_inflateId).setVisibility(View.GONE);
                }
                // show submitance
                if (((ViewStub) fragmentView.findViewById(R.id.meeting_stub_submittance_viewstub)) != null) {
                    ((ViewStub) fragmentView.findViewById(R.id.meeting_stub_submittance_viewstub))
                            .inflate();
                    fragmentView.findViewById(R.id.submitance_stub__add_product_button_id)
                            .setOnClickListener(this);
                } else {
                    fragmentView.findViewById(R.id.meeting_stub_submittance_viewstub_inflateId).setVisibility(View.VISIBLE);
                }

                break;
            case 2: // show presentation and hide others
                // hide
                //submitance
                if (((ViewStub) fragmentView.findViewById(R.id.meeting_stub_submittance_viewstub)) == null) {
                    fragmentView.findViewById(R.id.meeting_stub_submittance_viewstub_inflateId).setVisibility(View.GONE);
                }
                // follow up
                if (((ViewStub) fragmentView.findViewById(R.id.meeting_stub_followupForOrders_viewstub)) == null) {
                    fragmentView.findViewById(R.id.meeting_stub_followupForOrders_viewstub_inflateId).setVisibility(View.GONE);
                }
                // show presentation
                if (((ViewStub) fragmentView.findViewById(R.id.meeting_stub_presentation_viewstub)) != null) {
                    ((ViewStub) fragmentView.findViewById(R.id.meeting_stub_presentation_viewstub))
                            .inflate();
                    fragmentView.findViewById(R.id.presentation_stub__add_product_button_id)
                            .setOnClickListener(this);
                } else {
                    fragmentView.findViewById(R.id.meeting_stub_presentation_viewstub_inflateId).setVisibility(View.VISIBLE);
                }


                break;
            case 3:// show Follow-up and hide others
                // hide
                //submitance
                if (((ViewStub) fragmentView.findViewById(R.id.meeting_stub_submittance_viewstub)) == null) {
                    fragmentView.findViewById(R.id.meeting_stub_submittance_viewstub_inflateId).setVisibility(View.GONE);
                }
                // presentation
                if (((ViewStub) fragmentView.findViewById(R.id.meeting_stub_presentation_viewstub)) == null) {
                    fragmentView.findViewById(R.id.meeting_stub_presentation_viewstub_inflateId).setVisibility(View.GONE);
                }
                // show follow up
                if (((ViewStub) fragmentView.findViewById(R.id.meeting_stub_followupForOrders_viewstub)) != null) {
                    ((ViewStub) fragmentView.findViewById(R.id.meeting_stub_followupForOrders_viewstub))
                            .inflate();
                    fragmentView.findViewById(R.id.follow_up_for_orders__add_product_button_id)
                            .setOnClickListener(this);
                } else {
                    fragmentView.findViewById(R.id.meeting_stub_followupForOrders_viewstub_inflateId).setVisibility(View.VISIBLE);
                }

        }
    }

    ///////
    ////////////////////
    private void removeinflatedViewARFollow(int pos) {
        switch (pos) {
            case 0: // select
                // payment followup
                if (((ViewStub) fragmentView.findViewById(R.id.ar_flowup_stub_payment_followup_viewstub)) == null) {
                    fragmentView.findViewById(R.id.ar_flowup_stub_payment_followup_viewstub_inflateId).setVisibility(View.GONE);
                }
                // c form follow up
                if (((ViewStub) fragmentView.findViewById(R.id.ar_flowup_stub_c_form_followup_viewstub)) == null) {
                    fragmentView.findViewById(R.id.ar_flowup_stub_c_form_followup_inflateId).setVisibility(View.GONE);
                }
                // reconcilation
                if (((ViewStub) fragmentView.findViewById(R.id.fragment_reconcilation_viewstub)) == null) {
                    fragmentView.findViewById(R.id.fragment_reconcilation_viewstub_inflateId).setVisibility(View.GONE);
                }

                break;
            case 1:  // payment followup
                /////////////////////
                // c form follow up
                if (((ViewStub) fragmentView.findViewById(R.id.ar_flowup_stub_c_form_followup_viewstub)) == null) {
                    fragmentView.findViewById(R.id.ar_flowup_stub_c_form_followup_inflateId).setVisibility(View.GONE);
                }
                // reconcilation
                if (((ViewStub) fragmentView.findViewById(R.id.fragment_reconcilation_viewstub)) == null) {
                    fragmentView.findViewById(R.id.fragment_reconcilation_viewstub_inflateId).setVisibility(View.GONE);
                }
                ///////////////////////////////////
                // followup for Order
                if (((ViewStub) fragmentView.findViewById(R.id.ar_flowup_stub_payment_followup_viewstub)) != null) {
                    ((ViewStub) fragmentView.findViewById(R.id.ar_flowup_stub_payment_followup_viewstub))
                            .inflate();
                } else {
                    fragmentView.findViewById(R.id.ar_flowup_stub_payment_followup_viewstub_inflateId).setVisibility(View.VISIBLE);
                }
                ((AppCompatEditText) fragmentView.findViewById(R.id.payment_followup_types_new_date_edittext_id)).setOnFocusChangeListener(this);
                ((AppCompatEditText) fragmentView.findViewById(R.id.payment_followup_types_new_date_edittext_id)).setOnClickListener(this);
                break;
            case 2: //  c form follow up
                // payment followup
                if (((ViewStub) fragmentView.findViewById(R.id.ar_flowup_stub_payment_followup_viewstub)) == null) {
                    fragmentView.findViewById(R.id.ar_flowup_stub_payment_followup_viewstub_inflateId).setVisibility(View.GONE);
                }
                // reconcilation
                if (((ViewStub) fragmentView.findViewById(R.id.fragment_reconcilation_viewstub)) == null) {
                    fragmentView.findViewById(R.id.fragment_reconcilation_viewstub_inflateId).setVisibility(View.GONE);
                }
                // c form follow up
                if (((ViewStub) fragmentView.findViewById(R.id.ar_flowup_stub_c_form_followup_viewstub)) != null) {
                    ((ViewStub) fragmentView.findViewById(R.id.ar_flowup_stub_c_form_followup_viewstub))
                            .inflate();
                } else {
                    fragmentView.findViewById(R.id.ar_flowup_stub_c_form_followup_inflateId).setVisibility(View.VISIBLE);
                }
                ((AppCompatEditText) fragmentView.findViewById(R.id.c_form_flowup_stub_new_date_edittext_id)).setOnFocusChangeListener(this);
                ((AppCompatEditText) fragmentView.findViewById(R.id.c_form_flowup_stub_new_date_edittext_id)).setOnClickListener(this);
                break;
            case 3: // reconcilation
                // payment followup
                if (((ViewStub) fragmentView.findViewById(R.id.ar_flowup_stub_payment_followup_viewstub)) == null) {
                    fragmentView.findViewById(R.id.ar_flowup_stub_payment_followup_viewstub_inflateId).setVisibility(View.GONE);
                }
                // c form follow up
                if (((ViewStub) fragmentView.findViewById(R.id.ar_flowup_stub_c_form_followup_viewstub)) == null) {
                    fragmentView.findViewById(R.id.ar_flowup_stub_c_form_followup_inflateId).setVisibility(View.GONE);
                }
                // reconcilation
                if (((ViewStub) fragmentView.findViewById(R.id.fragment_reconcilation_viewstub)) != null) {
                    ((ViewStub) fragmentView.findViewById(R.id.fragment_reconcilation_viewstub))
                            .inflate();
                } else {
                    fragmentView.findViewById(R.id.fragment_reconcilation_viewstub_inflateId).setVisibility(View.VISIBLE);
                }


        }
    }

    private void setMeetingTypeSpinner() {
        ((LabelledSpinner) fragmentView.findViewById(R.id.fragment_meeting_meeting_type_spinner))
                .setItemsArray(R.array.meeting_type_array);
        ((LabelledSpinner) fragmentView.findViewById(R.id.fragment_meeting_meeting_type_spinner))
                .setDefaultErrorEnabled(false);
        ((LabelledSpinner) fragmentView.findViewById(R.id.fragment_meeting_meeting_type_spinner))
                .setDefaultErrorText("Please select meeting type");
        ((LabelledSpinner) fragmentView.findViewById(R.id.fragment_meeting_meeting_type_spinner))
                .setOnItemChosenListener(this);
    }

    private void setTheSubMeetingTypeSpinner() {
        ((LabelledSpinner) fragmentView.findViewById(R.id.meeting_stub_meeting_sub_type_id))
                .setItemsArray(R.array.meeting_sub_type_array);
        ((LabelledSpinner) fragmentView.findViewById(R.id.meeting_stub_meeting_sub_type_id))
                .setDefaultErrorEnabled(false);
        ((LabelledSpinner) fragmentView.findViewById(R.id.meeting_stub_meeting_sub_type_id))
                .setDefaultErrorText("Please select sub - meeting type");
        ((LabelledSpinner) fragmentView.findViewById(R.id.meeting_stub_meeting_sub_type_id))
                .setOnItemChosenListener(this);
    }

    private void setInitialProjectSelectSpinner() {

        projectsDataModels.clear();
        projectsDataModels.add(new ProjectsDataModel("Enter customer name first"));
        String[] strings = new String[projectsDataModels.size()];
        for (int i = 0; i < strings.length; i++) {
            strings[i] = projectsDataModels.get(i).getProjectName();
        }
        ((LabelledSpinner) fragmentView.findViewById(R.id.fragment_meeting_project_list_dropdown))
                .setItemsArray(strings);
        ((LabelledSpinner) fragmentView.findViewById(R.id.fragment_meeting_project_list_dropdown))
                .setDefaultErrorEnabled(false);
        ((LabelledSpinner) fragmentView.findViewById(R.id.fragment_meeting_project_list_dropdown))
                .setDefaultErrorText("Please select customer name first");
        ((LabelledSpinner) fragmentView.findViewById(R.id.fragment_meeting_project_list_dropdown)).setOnItemChosenListener(this);

    }

    private void inflateComplateTypesSpinner() {
        String[] stringsComplaints = new String[complaintDataTypeModels.size()];
        for (int i = 0; i < complaintDataTypeModels.size(); i++) {
            stringsComplaints[i] = complaintDataTypeModels.get(i).getTypeName();
        }
        ((LabelledSpinner) fragmentView.findViewById(R.id.complaints_stub_complaints_type_spinner_id))
                .setItemsArray(stringsComplaints);
        ((LabelledSpinner) fragmentView.findViewById(R.id.complaints_stub_complaints_type_spinner_id))
                .setDefaultErrorEnabled(false);
        ((LabelledSpinner) fragmentView.findViewById(R.id.complaints_stub_complaints_type_spinner_id))
                .setDefaultErrorText("Please select complaint type");
        ((LabelledSpinner) fragmentView.findViewById(R.id.complaints_stub_complaints_type_spinner_id)).setOnItemChosenListener(this);
    }

    private void inflateArfollowUpTypesSpinner() {
        ((LabelledSpinner) fragmentView.findViewById(R.id.spinner_ar_followup_stub_spinner_id))
                .setItemsArray(R.array.AR_follow_up);
        ((LabelledSpinner) fragmentView.findViewById(R.id.spinner_ar_followup_stub_spinner_id))
                .setDefaultErrorEnabled(false);
        ((LabelledSpinner) fragmentView.findViewById(R.id.spinner_ar_followup_stub_spinner_id))
                .setDefaultErrorText("Please select AR follow up type");
        ((LabelledSpinner) fragmentView.findViewById(R.id.spinner_ar_followup_stub_spinner_id)).setOnItemChosenListener(this);
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        switch (v.getId()) {
            case R.id.payment_followup_types_new_date_edittext_id:
                myDatePicker(true);
                break;
            case R.id.c_form_flowup_stub_new_date_edittext_id:
                myDatePicker(false);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.fragment_meeting_customer_name_edittext_id:
                Intent intent = new Intent(getActivity(), CustomerActForResult.class);
                startActivityForResult(intent, REQ_CODE_TO_GET_THE_CUSTOMR_DATA);
                break;
            case R.id.fragment_meeting_submit_button_id:

                if (PreferenceUtils.getIsUserCheckedIn(getActivity()))
                    meetingPresentor.validateMeetingsDataInPresenter(fragmentView,
                            getActivity(),
                            ((LabelledSpinner) fragmentView.findViewById(R.id.fragment_meeting_project_list_dropdown)).getSpinner().getSelectedItemPosition(),
                            ((AppCompatAutoCompleteTextView) fragmentView.findViewById(R.id.fragment_meeting_customer_name_edittext_id)).getText().toString(),
                            ((AppCompatEditText) fragmentView.findViewById(R.id.fragment_meeting_customer_type_edittext_id)).getText().toString(),
                            ((LabelledSpinner) fragmentView.findViewById(R.id.fragment_meeting_meeting_type_spinner)).getSpinner().getSelectedItemPosition(),
                            fragmentView.findViewById(R.id.meeting_stub_meeting_sub_type_id)
                                    == null ? 0 :
                                    ((LabelledSpinner) fragmentView.findViewById(R.id.meeting_stub_meeting_sub_type_id)).getSpinner().getSelectedItemPosition(),
                            (fragmentView.findViewById(R.id.follow_up_for_orders_value_edittext_id))
                                    == null ? "" : ((AppCompatEditText) fragmentView.findViewById(R.id.follow_up_for_orders_value_edittext_id)).getText().toString(),
                            (fragmentView.findViewById(R.id.follow_up_for_orders_lost_edittext_id))
                                    == null ? "" : ((AppCompatEditText) fragmentView.findViewById(R.id.follow_up_for_orders_lost_edittext_id)).getText().toString(),
                            fragmentView.findViewById(R.id.complaints_stub_complaints_type_spinner_id)
                                    == null ? 0 : ((LabelledSpinner) fragmentView.findViewById(R.id.complaints_stub_complaints_type_spinner_id)).getSpinner().getSelectedItemPosition(),
                            fragmentView.findViewById(R.id.spinner_ar_followup_stub_spinner_id)
                                    == null ? 0 : ((LabelledSpinner) fragmentView.findViewById(R.id.spinner_ar_followup_stub_spinner_id)).getSpinner().getSelectedItemPosition(),
                            (fragmentView.findViewById(R.id.payment_followup_types_new_date_edittext_id))
                                    == null ? "" : ((AppCompatEditText) fragmentView.findViewById(R.id.payment_followup_types_new_date_edittext_id)).getText().toString(),
                            (fragmentView.findViewById(R.id.payment_followup_types_project_issue_edittext_id))
                                    == null ? "" : ((AppCompatEditText) fragmentView.findViewById(R.id.payment_followup_types_project_issue_edittext_id)).getText().toString(),
                            (fragmentView.findViewById(R.id.payment_followup_types_project_delay_edittext_id))
                                    == null ? "" : ((AppCompatEditText) fragmentView.findViewById(R.id.payment_followup_types_project_delay_edittext_id)).getText().toString(),
                            (fragmentView.findViewById(R.id.c_form_flowup_stub_collected_edittext_id))
                                    == null ? "" : ((AppCompatEditText) fragmentView.findViewById(R.id.c_form_flowup_stub_collected_edittext_id)).getText().toString(),
                            (fragmentView.findViewById(R.id.c_form_flowup_stub_new_date_edittext_id))
                                    == null ? "" : ((AppCompatEditText) fragmentView.findViewById(R.id.c_form_flowup_stub_new_date_edittext_id)).getText().toString(),
                            (fragmentView.findViewById(R.id.c_form_flowup_stub_not_traceble_edittext_id))
                                    == null ? "" : ((AppCompatEditText) fragmentView.findViewById(R.id.c_form_flowup_stub_not_traceble_edittext_id)).getText().toString(),
                            (fragmentView.findViewById(R.id.reconciliation_missing_invoice_id))
                                    == null ? "" : ((AppCompatEditText) fragmentView.findViewById(R.id.reconciliation_missing_invoice_id)).getText().toString(),
                            (fragmentView.findViewById(R.id.reconciliation_short_payment_id))
                                    == null ? "" : ((AppCompatEditText) fragmentView.findViewById(R.id.reconciliation_short_payment_id)).getText().toString(),
                            (fragmentView.findViewById(R.id.reconciliation_statement_id))
                                    == null ? "" : ((AppCompatEditText) fragmentView.findViewById(R.id.reconciliation_statement_id)).getText().toString(),
                            productsDataModels_Submitance,
                            productsDataModels_Presentation,
                            productsDataModels_Follow_up,
                            ((AppCompatEditText) fragmentView.findViewById(R.id.fragment_meeting_location_edittext_id)).getText().toString());
                else
                    CommonFunc.commonDialog(getActivity(), getString(R.string.alert), getString(R.string.chekInAlert), false);
                break;
            case R.id.payment_followup_types_new_date_edittext_id:
                myDatePicker(true);
                break;
            case R.id.c_form_flowup_stub_new_date_edittext_id:
                myDatePicker(false);
                break;
            case R.id.fragment_meeting_add_projects_imageview_button:
                ((BaseActivity) getActivity()).attachFragment(new AddProjects(), true, "Add Projects", null);
                break;
            case R.id.fragment_meeting_customer_type_edittext_id:
                Toast.makeText(getActivity(), "Customer type will get auto-filled based on customer name", Toast.LENGTH_SHORT).show();
                break;
            case R.id.fragment_meeting_add_customer_id_imageview:
                ((BaseActivity) getActivity()).attachFragment(new AddCustomers(), true, getString(R.string.AddCustomers), null);
                break;
            case R.id.fragment_meeting_view_project_history:
                if (projectsDataModels.isEmpty()) {
                    CommonFunc.commonDialog(getActivity(), getString(R.string.alert), "No products history available", false);
                } else {
                    if (((LabelledSpinner) fragmentView.findViewById(R.id.fragment_meeting_project_list_dropdown))
                            .getSpinner().getSelectedItemPosition() != 0) {
                        projuctSpinnerSelectedValueVefireLeavingThisFragmentToHistrayFrag = ((LabelledSpinner) fragmentView.findViewById(R.id.fragment_meeting_project_list_dropdown))
                                .getSpinner().getSelectedItemPosition();
                        Bundle bundle = new Bundle();
                        bundle.putString("projectID", projectsDataModels.get(((LabelledSpinner) fragmentView.findViewById(R.id.fragment_meeting_project_list_dropdown)).
                                getSpinner().getSelectedItemPosition()).getProjectID());
                        ((BaseActivity) getActivity()).attachFragment(new ProjectHistoryFragment(), true, projectsDataModels
                                .get(((LabelledSpinner) fragmentView.findViewById(R.id.fragment_meeting_project_list_dropdown)).
                                        getSpinner().getSelectedItemPosition())
                                .getProjectName() + "'s history", bundle);
                    } else {
                        CommonFunc.commonDialog(getActivity(), getString(R.string.alert), "Please select project first", false);
                    }

                }
                break;
            // meeting sub types add products button. . ..
            case R.id.submitance_stub__add_product_button_id:
            case R.id.presentation_stub__add_product_button_id:
            case R.id.follow_up_for_orders__add_product_button_id:
                meetingPresentor.reqToGetTheProductsDataFromPresentor(getActivity()); // get the products. . . . .
                break;
            case R.id.fragment_meeting_loc_button_id:
                Toast.makeText(getActivity(), "Current Location", Toast.LENGTH_SHORT).show();
                connectToGoogleApiClientAndGetTheAddress();
                break;
            case R.id.fragment_meeting_location_edittext_id:
                if (NetworkUtils.checkInternetAndOpenDialog(getActivity())) {
                    showProgressDialog();
                    placePickerCode(101);
                    Toast.makeText(getActivity(), "Going to.... select location", Toast.LENGTH_SHORT).show();
                }
        }
    }

    private void placePickerCode(int requestCode) {
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
        try {
            Intent intent = builder.build(getActivity());
            startActivityForResult(intent, requestCode);
            dismissProgressDialog();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void myDatePicker(final boolean isPaymentFollowUp) {

        SimpleDateFormat mDF = new SimpleDateFormat("yyyy-mm-dd");
        Date today = new Date();
        mDF.format(today);
        final Calendar c = Calendar.getInstance();
        c.setTime(today);
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        String formattedDay = (String.valueOf(dayOfMonth));
                        int m = monthOfYear + 1;


                        String formattedMonth = (String.valueOf(m));

                        if (dayOfMonth < 10) {
                            formattedDay = "0" + dayOfMonth;
                        }

                        if (m < 10) {
                            formattedMonth = "0" + m;
                            Log.e("month ", " " + m);
                        }
                        if (isPaymentFollowUp)
                            ((AppCompatEditText) fragmentView.findViewById(R.id.payment_followup_types_new_date_edittext_id))
                                    .setText(formattedDay + "/" + (formattedMonth) + "/" + year);
                        else
                            ((AppCompatEditText) fragmentView.findViewById(R.id.c_form_flowup_stub_new_date_edittext_id))
                                    .setText(formattedDay + "/" + (formattedMonth) + "/" + year);


                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                if (isPaymentFollowUp)
                    ((AppCompatEditText) fragmentView.findViewById(R.id.payment_followup_types_new_date_edittext_id)).clearFocus();
                else
                    ((AppCompatEditText) fragmentView.findViewById(R.id.c_form_flowup_stub_new_date_edittext_id)).clearFocus();


            }
        });
        datePickerDialog.show();
        datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (getActivity().RESULT_OK == resultCode) {

            if (data != null) {
                Place placeSelected = PlacePicker.getPlace(data, getActivity());
                if (requestCode == 101 && resultCode == RESULT_OK) {
                    ((AppCompatEditText) fragmentView.findViewById(R.id.fragment_meeting_location_edittext_id)).setText(placeSelected.getAddress().toString() == null ? "" : placeSelected.getAddress().toString());
                }
            }

            if (requestCode == REQ_CODE_TO_GET_THE_PRODUCTS_SUBMITANCE) {
                double value = 0.0;
                ArrayList<ProductsDataModel> productsDataModelsTemp = new ArrayList<>();
                for (int i = 0; i < productsDataModels_Submitance.size(); i++) {
                    if (productsDataModels_Submitance.get(i).isChecked()) {
                        if (!productsDataModels_Submitance.get(i).getPrice().trim().equals(""))
                            value = value + (Double.valueOf(productsDataModels_Submitance.get(i).getQty())) *
                                    (Double.valueOf(productsDataModels_Submitance.get(i).getPrice()));
                        productsDataModelsTemp.add(productsDataModels_Submitance.get(i));
                    }
                }
                ((RecyclerView) fragmentView.findViewById(R.id.submitance_stub__add_product_recycleview_id))
                        .setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
                ((RecyclerView) fragmentView.findViewById(R.id.submitance_stub__add_product_recycleview_id))
                        .setAdapter(new AddProductsAdapter(getActivity(), productsDataModelsTemp));
                if (value == 0)
                    ((AppCompatEditText) fragmentView.findViewById(R.id.submitance_stub__orders_value_edittext_id)).setEnabled(true);
                ((AppCompatEditText) fragmentView.findViewById(R.id.submitance_stub__orders_value_edittext_id))
                        .setText("" + value);

            }
            if (requestCode == REQ_CODE_TO_GET_THE_PRODUCTS_PRESENTATION) {
                double value = 0.0;
                ArrayList<ProductsDataModel> productsDataModelsTemp = new ArrayList<>();
                for (int i = 0; i < productsDataModels_Presentation.size(); i++) {
                    if (productsDataModels_Presentation.get(i).isChecked()) {
                        if (!productsDataModels_Presentation.get(i).getPrice().trim().equals(""))

                            value = value + (Double.valueOf(productsDataModels_Presentation.get(i).getPrice())) *
                                    (Double.valueOf(productsDataModels_Presentation.get(i).getQty()));
                        productsDataModelsTemp.add(productsDataModels_Presentation.get(i));
                    }
                }
                ((RecyclerView) fragmentView.findViewById(R.id.presentation_stub__add_product_recycleview_id))
                        .setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
                ((RecyclerView) fragmentView.findViewById(R.id.presentation_stub__add_product_recycleview_id))
                        .setAdapter(new AddProductsAdapter(getActivity(), productsDataModelsTemp));
                if (value == 0)
                    ((AppCompatEditText) fragmentView.findViewById(R.id.presentation_stub__orders_value_edittext_id)).setEnabled(true);
                ((AppCompatEditText) fragmentView.findViewById(R.id.presentation_stub__orders_value_edittext_id))
                        .setText("" + value);

            }
            if (requestCode == REQ_CODE_TO_GET_THE_PRODUCTS_FOLLOW_UP) {
                double value = 0.0;
                ArrayList<ProductsDataModel> productsDataModelsTemp = new ArrayList<>();
                for (int i = 0; i < productsDataModels_Follow_up.size(); i++) {
                    if (productsDataModels_Follow_up.get(i).isChecked()) {
                        if (!productsDataModels_Follow_up.get(i).getPrice().trim().equals(""))
                            value = value + (Double.valueOf(productsDataModels_Follow_up.get(i).getPrice())) *
                                    (Double.valueOf(productsDataModels_Follow_up.get(i).getQty()));
                        productsDataModelsTemp.add(productsDataModels_Follow_up.get(i));
                    }
                }
                ((RecyclerView) fragmentView.findViewById(R.id.follow_up_for_orders__add_product_recycleview_id))
                        .setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
                ((RecyclerView) fragmentView.findViewById(R.id.follow_up_for_orders__add_product_recycleview_id))
                        .setAdapter(new AddProductsAdapter(getActivity(), productsDataModelsTemp));
                if (value == 0)
                    ((AppCompatEditText) fragmentView.findViewById(R.id.follow_up_for_orders_value_edittext_id)).setEnabled(true);
                ((AppCompatEditText) fragmentView.findViewById(R.id.follow_up_for_orders_value_edittext_id))
                        .setText("" + value);
            }

            if (requestCode == REQ_CODE_TO_GET_THE_CUSTOMR_DATA) {
                CustomerDataModel customerDataModel = data.getExtras().getParcelable("customer_model");
                setInitialProjectSelectSpinner();
                meetingPresentor.reqToGetTheProjectNameBasedOnCustomerName(getActivity(), customerDataModel.getCustomerID());
                ((AppCompatEditText) fragmentView.findViewById(R.id.fragment_meeting_customer_type_edittext_id))
                        .setText(customerDataModel.getType());
                ((AppCompatEditText) fragmentView.findViewById(R.id.fragment_meeting_customer_mobile_no_edittext_id))
                        .setText(customerDataModel.getMobile());
                ((AppCompatEditText) fragmentView.findViewById(R.id.fragment_meeting_customer_location_edittext_id))
                        .setText(customerDataModel.getLocation());
                ((AppCompatAutoCompleteTextView) fragmentView.findViewById(R.id.fragment_meeting_customer_name_edittext_id))
                        .setText(customerDataModel.getCustomerName());
                TEMP_CUST_ID = customerDataModel.getCustomerID();

            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////// mvp stuff
    private Dialog dialog = null;

    @Override
    public void showProgressDialog() {
        dialog = new Dialog(getActivity());
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.loaging_layout);
        dialog.show();
    }

    @Override
    public void dismissProgressDialog() {
        if (getActivity() != null)
            if (dialog != null) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                dialog = null;
            }
    }

    @Override
    public void afterValidationOfMeetingSuccessfull() {

        String meetingSubType = "NA";
        if (((LabelledSpinner) fragmentView.findViewById(R.id.fragment_meeting_meeting_type_spinner)).getSpinner().getSelectedItemPosition() == 1) {
            switch (((LabelledSpinner) fragmentView.findViewById(R.id.meeting_stub_meeting_sub_type_id)).getSpinner().getSelectedItemPosition()) {
                case 1:// submitance value
                    if (!((AppCompatEditText) fragmentView.findViewById(R.id.submitance_stub__orders_value_edittext_id))
                            .getText().toString().isEmpty())
                        meetingSubType = ((AppCompatEditText) fragmentView.findViewById(R.id.submitance_stub__orders_value_edittext_id))
                                .getText().toString();
                    else
                        meetingSubType = "0";
                    break;
                case 2: // presentation value
                    if (!((AppCompatEditText) fragmentView.findViewById(R.id.presentation_stub__orders_value_edittext_id))
                            .getText().toString().isEmpty())
                        meetingSubType = ((AppCompatEditText) fragmentView.findViewById(R.id.presentation_stub__orders_value_edittext_id))
                                .getText().toString();
                    else
                        meetingSubType = "0";
                    break;
                case 3:
                    // follow up value
                    if (!((AppCompatEditText) fragmentView.findViewById(R.id.follow_up_for_orders_value_edittext_id))
                            .getText().toString().isEmpty())
                        meetingSubType = ((AppCompatEditText) fragmentView.findViewById(R.id.follow_up_for_orders_value_edittext_id))
                                .getText().toString();
                    else
                        meetingSubType = "0";

            }
        }
        String projuctName = "";
        if (((LabelledSpinner) fragmentView.findViewById(R.id.fragment_meeting_project_list_dropdown)).getSpinner().getSelectedItemPosition() > 0)
            projuctName = projectsDataModels.get(((LabelledSpinner) fragmentView.findViewById(R.id.fragment_meeting_project_list_dropdown)).getSpinner().getSelectedItemPosition())
                    .getProjectName();


        meetingPresentor.reqToPostTheMeetingToTheServer(
                getActivity(),
                projuctName,
                projectsDataModels,
                ((AppCompatAutoCompleteTextView) fragmentView.findViewById(R.id.fragment_meeting_customer_name_edittext_id)).getText().toString(),
                ((AppCompatEditText) fragmentView.findViewById(R.id.fragment_meeting_customer_type_edittext_id)).getText().toString(),
                ((LabelledSpinner) fragmentView.findViewById(R.id.fragment_meeting_meeting_type_spinner)).getSpinner().getSelectedItemPosition(),
                fragmentView.findViewById(R.id.meeting_stub_meeting_sub_type_id)
                        == null ? 0 :
                        ((LabelledSpinner) fragmentView.findViewById(R.id.meeting_stub_meeting_sub_type_id)).getSpinner().getSelectedItemPosition(),
                (fragmentView.findViewById(R.id.follow_up_for_orders_value_edittext_id))
                        == null ? "" : ((AppCompatEditText) fragmentView.findViewById(R.id.follow_up_for_orders_value_edittext_id)).getText().toString(),
                (fragmentView.findViewById(R.id.follow_up_for_orders_lost_edittext_id))
                        == null ? "" : ((AppCompatEditText) fragmentView.findViewById(R.id.follow_up_for_orders_lost_edittext_id)).getText().toString(),
                fragmentView.findViewById(R.id.complaints_stub_complaints_type_spinner_id)
                        == null ? 0 : ((LabelledSpinner) fragmentView.findViewById(R.id.complaints_stub_complaints_type_spinner_id)).getSpinner().getSelectedItemPosition(),
                fragmentView.findViewById(R.id.spinner_ar_followup_stub_spinner_id)
                        == null ? 0 : ((LabelledSpinner) fragmentView.findViewById(R.id.spinner_ar_followup_stub_spinner_id)).getSpinner().getSelectedItemPosition(),
                (fragmentView.findViewById(R.id.payment_followup_types_new_date_edittext_id))
                        == null ? "" : ((AppCompatEditText) fragmentView.findViewById(R.id.payment_followup_types_new_date_edittext_id)).getText().toString(),
                (fragmentView.findViewById(R.id.payment_followup_types_project_issue_edittext_id))
                        == null ? "" : ((AppCompatEditText) fragmentView.findViewById(R.id.payment_followup_types_project_issue_edittext_id)).getText().toString(),
                (fragmentView.findViewById(R.id.payment_followup_types_project_delay_edittext_id))
                        == null ? "" : ((AppCompatEditText) fragmentView.findViewById(R.id.payment_followup_types_project_delay_edittext_id)).getText().toString(),
                (fragmentView.findViewById(R.id.c_form_flowup_stub_collected_edittext_id))
                        == null ? "" : ((AppCompatEditText) fragmentView.findViewById(R.id.c_form_flowup_stub_collected_edittext_id)).getText().toString(),
                (fragmentView.findViewById(R.id.c_form_flowup_stub_new_date_edittext_id))
                        == null ? "" : ((AppCompatEditText) fragmentView.findViewById(R.id.c_form_flowup_stub_new_date_edittext_id)).getText().toString(),
                (fragmentView.findViewById(R.id.c_form_flowup_stub_not_traceble_edittext_id))
                        == null ? "" : ((AppCompatEditText) fragmentView.findViewById(R.id.c_form_flowup_stub_not_traceble_edittext_id)).getText().toString(),
                (fragmentView.findViewById(R.id.reconciliation_missing_invoice_id))
                        == null ? "" : ((AppCompatEditText) fragmentView.findViewById(R.id.reconciliation_missing_invoice_id)).getText().toString(),
                (fragmentView.findViewById(R.id.reconciliation_short_payment_id))
                        == null ? "" : ((AppCompatEditText) fragmentView.findViewById(R.id.reconciliation_short_payment_id)).getText().toString(),
                (fragmentView.findViewById(R.id.reconciliation_statement_id))
                        == null ? "" : ((AppCompatEditText) fragmentView.findViewById(R.id.reconciliation_statement_id)).getText().toString(),
                ((AppCompatEditText) fragmentView.findViewById(R.id.fragment_meeting_notes_edittext)).getText().toString(), productsDataModels_Submitance,
                productsDataModels_Presentation,
                productsDataModels_Follow_up,
                ((AppCompatEditText) fragmentView.findViewById(R.id.fragment_meeting_location_edittext_id)).getText().toString(),
                meetingSubType, TEMP_CUST_ID);
    }

    @Override
    public void afterCustomerModelDownlodeSuccess(ArrayList<CustomerDataModel> customerDataModelss) {


    }

    @Override
    public void afterCustomerModelDownlodeFail(String message, String dialogTitle, boolean isInternetError) {
        CommonFunc.commonDialog(getActivity(), dialogTitle, message, isInternetError);
    }

    @Override
    public void afterProjectsListDownlodeSuccess(ArrayList<ProjectsDataModel> projectsDataModelss) {
        projectsDataModels.clear();
        projectsDataModels.add(new ProjectsDataModel("Please select project"));
        projectsDataModels.addAll(projectsDataModelss);
        String[] strings = new String[projectsDataModels.size()];
        for (int i = 0; i < strings.length; i++) {
            strings[i] = projectsDataModels.get(i).getProjectName();
        }
        ((LabelledSpinner) fragmentView.findViewById(R.id.fragment_meeting_project_list_dropdown))
                .setItemsArray(strings);
        ((LabelledSpinner) fragmentView.findViewById(R.id.fragment_meeting_project_list_dropdown))
                .setDefaultErrorEnabled(false);
        ((LabelledSpinner) fragmentView.findViewById(R.id.fragment_meeting_project_list_dropdown))
                .setDefaultErrorText("Please select project name");
    }

    @Override
    public void afterProjectsListDownlodeFail(String message, String dialogTitle, boolean isInternetError) {
        CommonFunc.commonDialog(getActivity(), dialogTitle, message, isInternetError);
    }

    @Override
    public void afterCustomerNameChoosenFromAutoSuccess(String customerName, String customerType) {
        ((AppCompatEditText) fragmentView.findViewById(R.id.fragment_meeting_customer_name_edittext_id)).setText(customerName);
        ((AppCompatEditText) fragmentView.findViewById(R.id.fragment_meeting_customer_type_edittext_id)).setText(customerType);

    }

    @Override
    public void afterCustomerNameChoosenFromAutoFail(String message, String dialogTitle, boolean isInternetError) {
        CommonFunc.commonDialog(getActivity(), dialogTitle, message, isInternetError);
    }

    ///////////////////// after products downlode lisner. . . .
    @Override
    public void afterProductsModelDownlodeSuccess(List<ProductsDataModel> productsDataModelss) {

        Log.e("value=", ((LabelledSpinner) fragmentView.findViewById(R.id.meeting_stub_meeting_sub_type_id)).getSpinner()
                .getSelectedItemPosition() + "");
        if (((LabelledSpinner) fragmentView.findViewById(R.id.meeting_stub_meeting_sub_type_id)).getSpinner()
                .getSelectedItemPosition() == 1) // submitance
        {
            Intent intent = new Intent(getActivity(), MeetingProductOnActiForResult.class);
            //////////////////////
            ArrayList<ProductsDataModel> productsDataModelsOld = new ArrayList<>();
            for (int i = 0; i < productsDataModels_Submitance.size(); i++) {
                if (productsDataModels_Submitance.get(i).isChecked()) {
                    productsDataModelsOld.add(productsDataModels_Submitance.get(i));
                }
            }
            //////////////////////////////////////////////////////////////////////////////////////// add new data
            productsDataModels_Submitance.clear();
            for (int i = 0; i < productsDataModelss.size(); i++) {
                productsDataModels_Submitance.add(productsDataModelss.get(i));
            }
            //////////////////////////////////////////////////////////////////////// attach selected one
            if (!productsDataModelsOld.isEmpty()) {
                for (int i = 0; i < productsDataModelsOld.size(); i++) {
                    for (int j = 0; j < productsDataModels_Submitance.size(); j++) {
                        if (productsDataModelsOld.get(i).getProductID().equals(productsDataModels_Submitance.get(j).getProductID())) {
                            productsDataModels_Submitance.get(j).setPrice(productsDataModelsOld.get(i).getPrice());
                            productsDataModels_Submitance.get(j).setQty(productsDataModelsOld.get(i).getQty());
                            productsDataModels_Submitance.get(j).setChecked(productsDataModelsOld.get(i).isChecked());
                        }
                    }
                }
            }
            intent.putExtra("REQ_CODE", REQ_CODE_TO_GET_THE_PRODUCTS_SUBMITANCE);
            startActivityForResult(intent, REQ_CODE_TO_GET_THE_PRODUCTS_SUBMITANCE);
            /////////////////////
        } else if (((LabelledSpinner) fragmentView.findViewById(R.id.meeting_stub_meeting_sub_type_id)).getSpinner()
                .getSelectedItemPosition() == 2) // presentation
        {
            Intent intent = new Intent(getActivity(), MeetingProductOnActiForResult.class);
            //////////////////////
            ArrayList<ProductsDataModel> productsDataModelsOld = new ArrayList<>();
            for (int i = 0; i < productsDataModels_Presentation.size(); i++) {
                if (productsDataModels_Presentation.get(i).isChecked()) {
                    productsDataModelsOld.add(productsDataModels_Presentation.get(i));
                }
            }
            //////////////////////////////////////////////////////////////////////////////////////// add new data
            productsDataModels_Presentation.clear();
            for (int i = 0; i < productsDataModelss.size(); i++) {
                productsDataModels_Presentation.add(productsDataModelss.get(i));
            }
            //////////////////////////////////////////////////////////////////////// attach selected one
            if (!productsDataModelsOld.isEmpty()) {
                for (int i = 0; i < productsDataModelsOld.size(); i++) {
                    for (int j = 0; j < productsDataModels_Presentation.size(); j++) {
                        if (productsDataModelsOld.get(i).getProductID().equals(productsDataModels_Presentation.get(j).getProductID())) {
                            productsDataModels_Presentation.get(j).setPrice(productsDataModelsOld.get(i).getPrice());
                            productsDataModels_Presentation.get(j).setQty(productsDataModelsOld.get(i).getQty());
                            productsDataModels_Presentation.get(j).setChecked(productsDataModelsOld.get(i).isChecked());
                        }
                    }
                }
            }
            intent.putExtra("REQ_CODE", REQ_CODE_TO_GET_THE_PRODUCTS_PRESENTATION);
            startActivityForResult(intent, REQ_CODE_TO_GET_THE_PRODUCTS_PRESENTATION);
            /////////////////////
        } else if (((LabelledSpinner) fragmentView.findViewById(R.id.meeting_stub_meeting_sub_type_id)).getSpinner()
                .getSelectedItemPosition() == 3)// follow up
        {
            Intent intent = new Intent(getActivity(), MeetingProductOnActiForResult.class);
            //////////////////////
            ArrayList<ProductsDataModel> productsDataModelsOld = new ArrayList<>();
            for (int i = 0; i < productsDataModels_Follow_up.size(); i++) {
                if (productsDataModels_Follow_up.get(i).isChecked()) {
                    productsDataModelsOld.add(productsDataModels_Follow_up.get(i));
                }
            }
            //////////////////////////////////////////////////////////////////////////////////////// add new data
            productsDataModels_Follow_up.clear();
            for (int i = 0; i < productsDataModelss.size(); i++) {
                productsDataModels_Follow_up.add(productsDataModelss.get(i));
            }
            //////////////////////////////////////////////////////////////////////// attach selected one
            if (!productsDataModelsOld.isEmpty()) {
                for (int i = 0; i < productsDataModelsOld.size(); i++) {
                    for (int j = 0; j < productsDataModels_Follow_up.size(); j++) {
                        if (productsDataModelsOld.get(i).getProductID().equals(productsDataModels_Follow_up.get(j).getProductID())) {
                            productsDataModels_Follow_up.get(j).setPrice(productsDataModelsOld.get(i).getPrice());
                            productsDataModels_Follow_up.get(j).setQty(productsDataModelsOld.get(i).getQty());
                            productsDataModels_Follow_up.get(j).setChecked(productsDataModelsOld.get(i).isChecked());
                        }
                    }
                }
            }
            intent.putExtra("REQ_CODE", REQ_CODE_TO_GET_THE_PRODUCTS_FOLLOW_UP);
            startActivityForResult(intent, REQ_CODE_TO_GET_THE_PRODUCTS_FOLLOW_UP);
            /////////////////////
        }


    }

    @Override
    public void afterProductsModelDoenlodeFail(String message, String dialogTitle, boolean isInternetError) {
        CommonFunc.commonDialog(getActivity(), dialogTitle, message, isInternetError);
    }

    @Override
    public void afterComplaintsDownlodeSuccess(ArrayList<ComplaintDataTypeModel> complaintDataTypeModelss) {
        complaintDataTypeModels.clear();
        complaintDataTypeModels.addAll(complaintDataTypeModelss);
        inflateComplateTypesSpinner();
    }

    @Override
    public void onStop() {
        super.onStop();
        dismissProgressDialog();
    }

    @Override
    public void afterComplaintsDownloldeFail(String message, String dialogTitle, boolean isInternetError) {
        CommonFunc.commonDialog(getActivity(), dialogTitle, message, isInternetError);
    }

    @Override
    public void afterMeetingPostSuccess(String message, String meetingId) {

        Bundle bundle = new Bundle();
        bundle.putString(ConstantsUtils.successFragMessageKey, message);
        bundle.putString(ConstantsUtils.successFragAddMoreButtonKey, "Add more meetings");
        ((BaseActivity) getActivity()).attachFragment(new SuccessAddingMeting(), true, "", bundle);
    }

    @Override
    public void afterMeetingPostFail(String message, String dialogTitle, boolean isInternetError) {
        CommonFunc.commonDialog(getActivity(), dialogTitle, message, isInternetError);
    }


    ////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////// google api client establishment to access address
    ////
    private GoogleApiClient googleApiClient = null;
    private LocationRequest locationRequest = null;
    private FusedLocationProviderClient mFusedLocationClient = null;
    private AsyncTaskToGetTheAddress asyncTaskToGetTheAddress = null;

    ///
    private void connectToGoogleApiClientAndGetTheAddress() {
        if (NetworkUtils.checkInternetAndOpenDialog(getActivity())) {
            if (CommonFunc.isGooglePlayServicesAvailable(getActivity())) {
                if (dialog == null) {
                    showProgressDialog();
                    connectGoogleApiClient();
                }
            }
        }
    }

    private void connectGoogleApiClient() {

        googleApiClient = new GoogleApiClient.Builder(getActivity())
                .addApi(LocationServices.API)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(@Nullable Bundle bundle) {
                        try {
                            checkForLocationSettings();
                        } catch (Exception e) {
                            System.out.println("Location error---------eeeeee---" + e.getMessage());
                            dismissProgressDialog();
                        }

                    }

                    @Override
                    public void onConnectionSuspended(int i) {
                    }
                })
                .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                        Toast.makeText(getActivity(), "Google play service not responding... please refresh your mobile",
                                Toast.LENGTH_LONG).show();
                        googleApiClient = null;
                        dismissProgressDialog();

                    }
                }).build();
        googleApiClient.connect();


    }

    private void checkForLocationSettings() {
        ////////////////////////////////////////////////////////

        locationRequest = new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);

        //**************************
        builder.setAlwaysShow(true); //this is the key ingredient
        //**************************

        Task<LocationSettingsResponse> locationSettingsResponseTask =
                LocationServices.getSettingsClient(getActivity()).checkLocationSettings(builder.build());
        //
        locationSettingsResponseTask.addOnCompleteListener(new OnCompleteListener<LocationSettingsResponse>() {
            @Override
            public void onComplete(@NonNull Task<LocationSettingsResponse> task) {
                try {
                    LocationSettingsResponse response = task.getResult(ApiException.class);
                    // All location settings are satisfied. The client can initialize location
                    // requests here.
                    if (PermissionUtils.checkLoactionPermission(getActivity()) && NetworkUtils.isConnected(getActivity())) {
                        getLastLocation();
                    } else {
                        dismissProgressDialog();
                        if (!PermissionUtils.checkLoactionPermission(getActivity())) {
                            PermissionUtils.openAppSettings(getActivity());

                        } else {
                            NetworkUtils.checkInternetAndOpenDialog(getActivity());
                        }
                    }

                } catch (ApiException exception) {
                    dismissProgressDialog();

                    switch (exception.getStatusCode()) {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // Location settings are not satisfied. But could be fixed by showing the
                            // user a dialog.
                            try {
                                NetworkUtils.openGpsSettings(getActivity());
                            } catch (ClassCastException e) {
                                CommonFunc.commonDialog(getActivity(), getString(R.string.alert),
                                        getString(R.string.justErrorCode) + " 86", false);
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            CommonFunc.commonDialog(getActivity(), "GPS not working",
                                    getString(R.string.justErrorCode) + " 87", false);
                            break;
                    }
                }
            }
        });
        //

    }

    @SuppressWarnings("MissingPermission")
    private void getLastLocation() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());
        mFusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);

    }

    private LocationCallback locationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {

            for (Location location : locationResult.getLocations()) {
                decodeAddress(new LatLng(location.getLatitude(),
                        location.getLongitude()));
                stopLocationUpdates();
            }
        }
    };

    private void decodeAddress(final LatLng latLng) {
        //https://maps.googleapis.com/maps/api/geocode/json?latlng=40.714224,-73.961452&key=YOUR_API_KEY
        String req = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" +
                latLng.latitude + "," + latLng.longitude + "&key=" + getString(R.string.google_key);


        if (asyncTaskToGetTheAddress != null) {
            if (asyncTaskToGetTheAddress.getStatus() != AsyncTask.Status.RUNNING) {
                asyncTaskToGetTheAddress = new AsyncTaskToGetTheAddress();
                asyncTaskToGetTheAddress.execute(req);
            }
        } else {
            asyncTaskToGetTheAddress = new AsyncTaskToGetTheAddress();
            asyncTaskToGetTheAddress.execute(req);
        }
    }

    private class AsyncTaskToGetTheAddress extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... strings) {
            return makeserverConnection(strings[0]);
        }

        @Override
        protected void onPostExecute(String s) {
            dismissProgressDialog();
            if (s != null) {
                try {
                    JSONObject jsonObject = new JSONObject(s);
                    JSONArray jsonArray = jsonObject.getJSONArray("results");
                    if (jsonArray.length() > 0) {
                        String formatedAddress = jsonArray.getJSONObject(0).getString("formatted_address");
                        if (formatedAddress != null && !formatedAddress.isEmpty()) {
                            ((AppCompatEditText) fragmentView.findViewById(R.id.fragment_meeting_location_edittext_id)).setText(formatedAddress);
                        } else {
                            CommonFunc.commonDialog(getActivity(), "Alert!", "Unable to detect your location", false);
                        }

                    } else {
                        CommonFunc.commonDialog(getActivity(), "Alert!", "Unable to detect your location", false);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                CommonFunc.commonDialog(getActivity(), "Alert!", "Unable to detect your location", false);
            }
        }


    }

    public String makeserverConnection(String requestURL) {
        URL url;
        String response = "";
        try {
            url = new URL(requestURL);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded");
            conn.setDoOutput(true);
            int responseCode = conn.getResponseCode();
            if (responseCode == HttpsURLConnection.HTTP_OK) {

                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line = br.readLine()) != null) {
                    response += line;
                }
            } else {
                response = "";
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    private void stopLocationUpdates() {
        if (mFusedLocationClient != null)
            mFusedLocationClient.removeLocationUpdates(locationCallback);
    }

    /////////////////////////////////////////////////////////////////////////
    @Override
    public void onDestroy() {
        super.onDestroy();
        productsDataModels_Submitance.clear();
        productsDataModels_Presentation.clear();
        productsDataModels_Follow_up.clear();
    }
}