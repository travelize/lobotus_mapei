package com.lobotus.mapei.flowfragments.mvp.intrector;

import android.content.Context;

import com.lobotus.mapei.flowfragments.model.DemoMeetingsModel;
import com.lobotus.mapei.fromlite.mvp.intractor.MeetingIntractor;

import java.util.ArrayList;

public interface ViewDemoMeetingsInteractor {

    // interface uses after data is downloaded in ViewDemoMeetingsInteractorImp
    interface DemoMeetingListDownlodeLisner {

        void OnDemoMeetingListDownlodeSuccess(ArrayList<DemoMeetingsModel> meetingModels);

        void OnDemoMeetingListDownlodeFail(String message, String dialogTitle, boolean isInternetError);
    }

    void reqToServerToGetTheListOfDemoMeetings(Context context, DemoMeetingListDownlodeLisner demoMeetingListDownlodeLisner);

    //
    void onStopMvpIntractor();
}
