package com.lobotus.mapei.flowfragments.mvp.views;

import com.lobotus.mapei.flowfragments.model.LeaveTypeDataModel;

import java.util.ArrayList;

/**
 * Created by user1 on 04-10-2017.
 */

public interface LeaveApplyView {

    void showProgressDialog();
    void dismissProgressDialog();
    ////  downlode leave list
    void afterLeaveTypesDownlodeSuccess(ArrayList<LeaveTypeDataModel> leaveTypeDataModels);
    void afterLeaveTypeDownlodeFail(String message,String dialogTitle,boolean isInternetError);
    /////////////////////////////////////// post to apply for leaves
    void afterLeaveApplySuccess(String message,String dialogTitle,boolean isInternetError);
    void afterLeaveApplyFail(String message,String dialogTitle,boolean isInternetError);
    //
    /////////////////////////////////////////// after leave apply validation success
    void afterLeaveApplyValidationSuccess();

}
