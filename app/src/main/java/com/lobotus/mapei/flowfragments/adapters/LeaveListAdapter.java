package com.lobotus.mapei.flowfragments.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.lobotus.mapei.R;
import com.lobotus.mapei.baseactivity.BaseActivity;
import com.lobotus.mapei.flowfragments.fragments.LeaveFragment;
import com.lobotus.mapei.flowfragments.model.LeaveDataModel;
import com.lobotus.mapei.utils.CommonFunc;
import com.lobotus.mapei.utils.PreferenceUtils;

import java.util.ArrayList;

/**
 * Created by user1 on 06-10-2017.
 */

public class LeaveListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context=null;
    private ArrayList<LeaveDataModel> leaveDataModels=null;
    private LeaveFragment leaveFragment=null;

    public LeaveListAdapter(Context context, ArrayList<LeaveDataModel> leaveDataModels, LeaveFragment leaveFragment) {
        this.context = context;
        this.leaveDataModels = leaveDataModels;
        this.leaveFragment = leaveFragment;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.leave_fragment_row,parent,false);
        return new LeaveListHolder(view);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        if (leaveDataModels.get(holder.getAdapterPosition()).getDaysOnLeave()>1)
        {
            ((LeaveListHolder)holder).linearLayoutCompatSingleDayLayout.setVisibility(View.GONE);
            ((LeaveListHolder)holder).linearLayoutCompatFromAndToLayout.setVisibility(View.VISIBLE);
            //
            ((LeaveListHolder)holder).textViewLeaveFromDate.setText(leaveDataModels.get(holder.getAdapterPosition()).getFromDate());
            ((LeaveListHolder)holder).textViewToDate.setText(leaveDataModels.get(holder.getAdapterPosition()).getToDate());

        }else {
            ((LeaveListHolder)holder).linearLayoutCompatFromAndToLayout.setVisibility(View.GONE);
            ((LeaveListHolder)holder).linearLayoutCompatSingleDayLayout.setVisibility(View.VISIBLE);
            //
            ((LeaveListHolder)holder).textViewSingleDayDate.setText(leaveDataModels.get(holder.getAdapterPosition()).getFromDate());
        }
        ///////////////
        ((LeaveListHolder)holder).textViewLeaveTypeTextview.setText(leaveDataModels.get(holder.getAdapterPosition()).getType());
        ((LeaveListHolder)holder).textViewNoOfDays.setText(""+leaveDataModels.get(holder.getAdapterPosition()).getDaysOnLeave());
        ((LeaveListHolder)holder).textViewRemark.setText(leaveDataModels.get(holder.getAdapterPosition()).getRemarks());
        //
        switch (leaveDataModels.get(holder.getAdapterPosition()).getStatus().trim())
        {
            case "Requested":
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    ((LeaveListHolder)holder).textViewStatus.setTextColor(context.getResources().getColor(R.color.orangered,null));
                }else {
                    ((LeaveListHolder)holder).textViewStatus.setTextColor(context.getResources().getColor(R.color.orangered));
                }
                ((LeaveListHolder)holder).buttonCancel.setVisibility(View.VISIBLE);
                break;
            case "Approved":
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    ((LeaveListHolder)holder).textViewStatus.setTextColor(context.getResources().getColor(R.color.logo_colour,null));
                }else {
                    ((LeaveListHolder)holder).textViewStatus.setTextColor(context.getResources().getColor(R.color.logo_colour));
                }
                ((LeaveListHolder)holder).buttonCancel.setVisibility(View.GONE);
                break;
            case "Cancelled":
            case "Rejected":
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    ((LeaveListHolder)holder).textViewStatus.setTextColor(context.getResources().getColor(R.color.deadRead,null));
                }else {
                    ((LeaveListHolder)holder).textViewStatus.setTextColor(context.getResources().getColor(R.color.deadRead));
                }
                ((LeaveListHolder)holder).buttonCancel.setVisibility(View.GONE);
        }
        //
        ((LeaveListHolder)holder).textViewStatus.setText(leaveDataModels.get(holder.getAdapterPosition()).getStatus());
        ///////
        ((LeaveListHolder)holder).buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (PreferenceUtils.getIsUserCheckedIn(context))
                {
                    new AlertDialog.Builder(context)
                            .setTitle("Alert!!")
                            .setMessage("Do you want to cancel the requested leave ?")
                            .setCancelable(false)
                            .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    leaveFragment.OnCancelButtonClick(leaveDataModels.get(holder.getAdapterPosition()).getLeaveID());

                                }
                            }).setNegativeButton("NO", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    }).create().show();
                }else {
                    CommonFunc.commonDialog(context,context.getString(R.string.alert),context.getString(R.string.chekInAlert),false);
                }




            }
        });




    }

    @Override
    public int getItemCount() {
        return leaveDataModels.size();
    }

     private class LeaveListHolder extends RecyclerView.ViewHolder
    {
        LinearLayoutCompat linearLayoutCompatFromAndToLayout=null,linearLayoutCompatSingleDayLayout=null;
        AppCompatTextView textViewLeaveTypeTextview=null,textViewNoOfDays=null,textViewRemark=null,textViewStatus=null,
        textViewLeaveFromDate=null,textViewToDate=null,textViewSingleDayDate=null;
        AppCompatButton buttonCancel=null;
        LeaveListHolder(View itemView) {
            super(itemView);
            this.linearLayoutCompatFromAndToLayout=itemView.findViewById(R.id.leave_fragment_row_more_days_layout);
            this.linearLayoutCompatSingleDayLayout=itemView.findViewById(R.id.leave_fragment_row_single_day_layout);
            //
            this.textViewLeaveTypeTextview=itemView.findViewById(R.id.leave_fragment_row_leave_type_id);
            this.textViewNoOfDays=itemView.findViewById(R.id.leave_fragment_row_no_of_days_id);
            this.textViewRemark=itemView.findViewById(R.id.leave_fragment_row_remark_id);
            this.textViewStatus=itemView.findViewById(R.id.leave_fragment_row_status_id);
            //
            this.textViewLeaveFromDate=itemView.findViewById(R.id.leave_fragment_row_from_textview_id);
            this.textViewToDate=itemView.findViewById(R.id.leave_fragment_row_to_textview_id);
            this.textViewSingleDayDate=itemView.findViewById(R.id.leave_fragment_row_single_day_date_textview_id);
            //
            this.buttonCancel=itemView.findViewById(R.id.leave_fragment_row_cancel_button_id);

        }
    }
}
