package com.lobotus.mapei.flowfragments.mvp.presentorimp;

import android.content.Context;

import com.lobotus.mapei.flowfragments.model.LeaveDataModel;
import com.lobotus.mapei.flowfragments.mvp.intrector.LeaveFragIntrector;
import com.lobotus.mapei.flowfragments.mvp.intrectorimp.LeaveFragmentIntrImp;
import com.lobotus.mapei.flowfragments.mvp.presentor.LeaveFragPresentor;
import com.lobotus.mapei.flowfragments.mvp.views.LeaveFragmentView;
import com.lobotus.mapei.registration.mvp.LoginIntrector;
import com.lobotus.mapei.utils.NetworkUtils;

import java.util.ArrayList;

/**
 * Created by user1 on 06-10-2017.
 */

public class LeaveFragPresentorImp implements LeaveFragPresentor,
        LeaveFragIntrector.LeaveListLoadLisner,LeaveFragIntrector.LeaveCancelLisner {

    private LeaveFragmentView leaveFragmentView=null;
    private LeaveFragIntrector leaveFragIntrector=null;

    public LeaveFragPresentorImp(LeaveFragmentView leaveFragmentView) {
        this.leaveFragmentView = leaveFragmentView;
        this.leaveFragIntrector = new LeaveFragmentIntrImp();
    }
    //////////////////////////////////////////// leave list downlode
    @Override
    public void reqToLodeTheLeaveList(Context context) {
        leaveFragmentView.showProgressDialog();
        leaveFragIntrector.postToDOwnlodeTheLeaveListFromServer(context,this);
    }



    @Override
    public void OnLeaveListLoadSuccess(ArrayList<LeaveDataModel> leaveDataModels) {
        leaveFragmentView.dismissProgressDialog();
        leaveFragmentView.afterLeavesListDownlodeSuccess(leaveDataModels);
    }

    @Override
    public void OnLeaveListLoadfail(String message, String dialogTitle, boolean isInternetError) {
        leaveFragmentView.dismissProgressDialog();
        leaveFragmentView.afterLeavesListDownlodeFail(message,dialogTitle,isInternetError);
    }
    ///////////////////////////////////////////////////// leave cancel
    @Override
    public void OnLeaveCancelSuccess(String message) {
        leaveFragmentView.dismissProgressDialog();
        leaveFragmentView.afterLeaveCancelSuccess(message);
    }

    @Override
    public void OnLeaveCancelFail(String message, String dialogTitle, boolean isInternetError) {
        leaveFragmentView.dismissProgressDialog();
        leaveFragmentView.afterLeaveCancelFail(message,dialogTitle,isInternetError);
    }
    @Override
    public void reqToCancelTheLeave(String leaveId,Context context) {
        if (NetworkUtils.checkInternetAndOpenDialog(context))
        {
            leaveFragmentView.showProgressDialog();
            leaveFragIntrector.postReqToCancelLeaveFromServer(leaveId,this,context);

        }

    }
}
