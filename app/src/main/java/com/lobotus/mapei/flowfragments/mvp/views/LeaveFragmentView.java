package com.lobotus.mapei.flowfragments.mvp.views;

import com.lobotus.mapei.flowfragments.model.LeaveDataModel;

import java.util.ArrayList;

/**
 * Created by user1 on 05-10-2017.
 */

public interface LeaveFragmentView {
    void showProgressDialog();
    void dismissProgressDialog();
    ///////////////////////
    void afterLeavesListDownlodeSuccess(ArrayList<LeaveDataModel> leaveDataModels);
    void afterLeavesListDownlodeFail(String message,String dialogTitle,boolean isInternetError);
    //////////////////// cancel the leave
    void afterLeaveCancelSuccess(String  message);
    void afterLeaveCancelFail(String message,String dialogTitle,boolean isInternetError);
}
