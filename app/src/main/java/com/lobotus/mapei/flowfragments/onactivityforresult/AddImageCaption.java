package com.lobotus.mapei.flowfragments.onactivityforresult;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.view.View;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.lobotus.mapei.R;
import com.lobotus.mapei.customwidgits.TouchImageView;
import com.lobotus.mapei.utils.ImageUtility;

import java.io.IOException;

public class AddImageCaption extends Activity {
    private final int SELECT_FILE_CAPTION=22;
    private final int SELECT_CAMERA_CAPTION=23;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_image_caption);
        if (getIntent().getExtras().getBoolean("isTypeFile"))
        {
            OnFileResult(getIntent());
        }else {
            OnCameraResult(getIntent());
        }
        //
        findViewById(R.id.activity_add_image_attach_button_id)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent=new Intent();
                        intent.putExtras(getIntent());
                        if (((AppCompatEditText)findViewById(R.id.activity_add_image_caption_id)).getText().toString().isEmpty())
                            intent.putExtra("caption","NA");
                        else
                            intent.putExtra("caption",((AppCompatEditText)findViewById(R.id.activity_add_image_caption_id)).getText().toString());
                        if (((AppCompatEditText)findViewById(R.id.activity_add_image_amount_id)).getText().toString().isEmpty())
                            intent.putExtra("amount","NA");
                        else
                            intent.putExtra("amount",((AppCompatEditText)findViewById(R.id.activity_add_image_amount_id)).getText().toString());
                        setResult(RESULT_OK,intent);
                        finish();
                    }
                });

    }

    private void OnFileResult(Intent intent)
    {
        Bitmap bm=null;
        if (intent != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), Uri.parse(intent.getStringExtra("File_uri")));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (bm!=null)
        {
//            Bitmap bitmap=Bitmap.createScaledBitmap(bm,1080,600,false);
            /*Bitmap bitmap= ImageUtility.getResizedBitmap(bm,400);*/
            ((AppCompatImageView)findViewById(R.id.activity_add_image_imageview_id)).setImageBitmap(bm);

        }

    }

    private void OnCameraResult(Intent intent)
    {
//        Bitmap thumbnail = (Bitmap) intent.getExtras().get("data");
//        ((AppCompatImageView)findViewById(R.id.activity_add_image_imageview_id)).setImageBitmap(thumbnail);
        showProgressDialog();
        Glide.with(AddImageCaption.this).load(intent.getStringExtra("File_uri")).asBitmap()
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                        dismissProgressDialog();
                        if (resource!=null)
                        {
                            ((AppCompatImageView)findViewById(R.id.activity_add_image_imageview_id)).setImageBitmap(resource);
                        }
                    }
                });

    }

    @Override
    public void onBackPressed() {
        Intent intent=new Intent();
        setResult(RESULT_CANCELED,intent);
        finish();

    }


    /////////////////////////////////////////////////////////////////////////////////////// mvp stuff
    private Dialog dialog=null;
    public void showProgressDialog() {
        dialog=new Dialog(AddImageCaption.this);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.loaging_layout);
        dialog.show();
    }

    public void dismissProgressDialog() {
            if (dialog!=null)
            {
                if (dialog.isShowing())
                {
                    dialog.dismiss();
                }
                dialog=null;
            }
    }
}
