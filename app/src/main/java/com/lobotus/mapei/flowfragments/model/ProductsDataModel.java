package com.lobotus.mapei.flowfragments.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by user1 on 06-10-2017.
 */

public class ProductsDataModel {

    @SerializedName("ProductID")
    @Expose
    private String productID;
    @SerializedName("ProductName")
    @Expose
    private String productName;
    @SerializedName("Amount")
    @Expose
    private String price="";
    @SerializedName("Unit")
    @Expose
    private String unit;
    @SerializedName("Quantity")
    @Expose
    private String Quantity=null;

    private boolean isChecked=false;
    private String qty="";

    /*private String amount="";*/

    public String getQuantity() {
        return Quantity;
    }

    public void setQuantity(String quantity) {
        Quantity = quantity;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

  /*  public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }*/

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public String getProductID() {
        return productID;
    }

    public void setProductID(String productID) {
        this.productID = productID;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }


}
