package com.lobotus.mapei.flowfragments.mvp.intrector;

import android.content.Context;

import com.lobotus.mapei.flowfragments.model.ProductsDataModel;

import java.util.ArrayList;

/**
 * Created by user1 on 16-10-2017.
 */

public interface ProjectHisProductIntractor {

    interface OnProjectHisProductsDownlodeLisner
    {
        void OnProjectHisProductsDownlodeSuccess(ArrayList<ProductsDataModel> productsDataModels);
        void OnProjectHisProductsDownlodeFail(String message, String dialogTitle, boolean isInternetError);
    }
    void reqToGetTheHisProducts(Context context,String MeetingID,OnProjectHisProductsDownlodeLisner onProjectHisProductsDownlodeLisner);
}
