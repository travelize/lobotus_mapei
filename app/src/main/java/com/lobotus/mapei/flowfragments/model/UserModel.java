package com.lobotus.mapei.flowfragments.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by User on 20-03-2018.
 */

public class UserModel {

    @SerializedName("Success")
    @Expose
    private Integer success;
    @SerializedName("data")
    @Expose
    private List<UsersDataModel> data = null;
    @SerializedName("Msg")
    @Expose
    private String errorMsg;

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public List<UsersDataModel> getData() {
        return data;
    }

    public void setData(List<UsersDataModel> data) {
        this.data = data;
    }
}
