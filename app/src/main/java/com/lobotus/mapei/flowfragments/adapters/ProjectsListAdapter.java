package com.lobotus.mapei.flowfragments.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.lobotus.mapei.R;
import com.lobotus.mapei.baseactivity.BaseActivity;
import com.lobotus.mapei.customwidgits.ExpandableLayout;
import com.lobotus.mapei.flowfragments.fragments.ProjectHistoryFragment;
import com.lobotus.mapei.flowfragments.fragments.UpdateProject;
import com.lobotus.mapei.flowfragments.model.ProjectsDataModel;

import java.util.ArrayList;

/**
 * Created by user1 on 06-10-2017.
 */

public class ProjectsListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context = null;
    private ArrayList<ProjectsDataModel> projectsDataModels = null;

    public ProjectsListAdapter(Context context, ArrayList<ProjectsDataModel> projectsDataModels) {
        this.context = context;
        this.projectsDataModels = projectsDataModels;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.projects_list_row, parent, false);
        return new ProjectsHolder(view);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        ((ProjectsHolder) holder).textViewProjuctName.setText(projectsDataModels.get(holder.getAdapterPosition()).getProjectName());
        ((ProjectsHolder) holder).textViewCustomerName.setText(projectsDataModels.get(holder.getAdapterPosition()).getCustomerName());
        ((ProjectsHolder) holder).textViewAddedBy.setText(projectsDataModels.get(holder.getAdapterPosition()).getAddedBy());
        ((ProjectsHolder) holder).textViewStatus.setText(projectsDataModels.get(holder.getAdapterPosition()).getStatus());
        ((ProjectsHolder) holder).textViewProducts.setText(projectsDataModels.get(holder.getAdapterPosition()).getProducts());
        ((ProjectsHolder) holder).textViewPeriod.setText(projectsDataModels.get(holder.getAdapterPosition()).getPeriod());
        ((ProjectsHolder) holder).textViewProjuctStage.setText(projectsDataModels.get(holder.getAdapterPosition()).getProductStage());
        ((ProjectsHolder) holder).textViewRemarks.setText(projectsDataModels.get(holder.getAdapterPosition()).getRemarks());

        /////// view project history
        ((ProjectsHolder) holder).textViewProjecthistoryTextview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("projectID", projectsDataModels.get(holder.getAdapterPosition()).getProjectID());
                ((BaseActivity) context).attachFragment(new ProjectHistoryFragment(), true, projectsDataModels.get(holder.getAdapterPosition())
                        .getProjectName() + "'s history", bundle);
            }
        });
        /// update the project
        ((ProjectsHolder) holder).textViewUpdateProject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("projectID", projectsDataModels.get(holder.getAdapterPosition()).getProjectID());
                bundle.putString("projectName", projectsDataModels.get(holder.getAdapterPosition()).getProjectName());
                bundle.putString("projectStatus", projectsDataModels.get(holder.getAdapterPosition()).getStatus());
                bundle.putString("projectStage", projectsDataModels.get(holder.getAdapterPosition()).getProductStage().replace("%", "").trim());
                bundle.putString("projectNotes", projectsDataModels.get(holder.getAdapterPosition()).getRemarks());
                ((BaseActivity) context).attachFragment(new UpdateProject(), true, "Update " + projectsDataModels.get(holder.getAdapterPosition())
                        .getProjectName(), bundle);

            }
        });

        if (projectsDataModels.get(holder.getAdapterPosition()).isExpand())
            ((ProjectsHolder) holder).expandableLayout.setExpanded(true);
        else
            ((ProjectsHolder) holder).expandableLayout.setExpanded(false);

        //
        ((ProjectsHolder) holder).layoutCompatExpandClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (projectsDataModels.get(holder.getAdapterPosition()).isExpand()) {
                    projectsDataModels.get(holder.getAdapterPosition()).setExpand(false);
                    ((ProjectsHolder) holder).expandableLayout.setExpanded(false, true);
                } else {
                    projectsDataModels.get(holder.getAdapterPosition()).setExpand(true);
                    ((ProjectsHolder) holder).expandableLayout.setExpanded(true, true);
                }

            }
        });


    }

    @Override
    public int getItemCount() {
        return projectsDataModels.size();
    }

    private class ProjectsHolder extends RecyclerView.ViewHolder {
        private AppCompatTextView textViewProjuctName = null, textViewCustomerName = null, textViewAddedBy = null, textViewStatus = null,
                textViewProducts = null, textViewPeriod = null, textViewProjuctStage = null, textViewRemarks = null;
        private AppCompatTextView textViewProjecthistoryTextview = null, textViewUpdateProject = null;
        private ExpandableLayout expandableLayout = null;
        private LinearLayoutCompat layoutCompatExpandClick = null;

        ProjectsHolder(View itemView) {
            super(itemView);
            this.textViewProjuctName = itemView.findViewById(R.id.project_list_row_projectName);
            this.textViewCustomerName = itemView.findViewById(R.id.project_list_row_customerName);
            this.textViewAddedBy = itemView.findViewById(R.id.project_list_row_addedBy);
            this.textViewStatus = itemView.findViewById(R.id.project_list_row_status);
            this.textViewProducts = itemView.findViewById(R.id.project_list_row_products);
            this.textViewPeriod = itemView.findViewById(R.id.project_list_row_period);
            this.textViewProjuctStage = itemView.findViewById(R.id.project_list_row_projectStage);
            this.textViewRemarks = itemView.findViewById(R.id.project_list_row_remarks);
            this.textViewProjecthistoryTextview = itemView.findViewById(R.id.project_list_row_view_project_history);
            this.textViewUpdateProject = itemView.findViewById(R.id.project_list_row_update_project);
            this.expandableLayout = itemView.findViewById(R.id.project_list_row_expandable_layout);
            this.layoutCompatExpandClick = itemView.findViewById(R.id.project_list_row_layout_expand_click);
        }
    }
}
