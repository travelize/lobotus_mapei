package com.lobotus.mapei.flowfragments.mvp.intrectorimp;

import android.content.Context;

import com.google.gson.Gson;
import com.lobotus.mapei.R;
import com.lobotus.mapei.flowfragments.model.ProductsDataModel;
import com.lobotus.mapei.flowfragments.model.ProductsModel;
import com.lobotus.mapei.flowfragments.mvp.intrector.ProjectHisProductIntractor;
import com.lobotus.mapei.retrofitapi.APIService;
import com.lobotus.mapei.retrofitapi.ApiUtils;
import com.lobotus.mapei.utils.ConstantsUtils;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by user1 on 16-10-2017.
 */

public class ProjectHisProductIntractorImp implements ProjectHisProductIntractor {
    @Override
    public void reqToGetTheHisProducts(final Context context, String MeetingID,
                                       final OnProjectHisProductsDownlodeLisner onProjectHisProductsDownlodeLisner) {

        APIService apiService = ApiUtils.getAPIService(ConstantsUtils.postBaseUrl + ConstantsUtils.postToGetTheProductsInHistory);
        apiService.postToGetTheProductsInHistory(MeetingID)
                .enqueue(new Callback<ProductsModel>() {
                    @Override
                    public void onResponse(Call<ProductsModel> call, Response<ProductsModel> response) {


                        if (response.isSuccessful()) {
                            System.out.println("gggggggggggggg----------product list---" + new Gson().toJson(response.body()));
                            if (response.body() != null) {
                                if (response.body().getSuccess() == 1) {

                                    onProjectHisProductsDownlodeLisner.OnProjectHisProductsDownlodeSuccess((ArrayList<ProductsDataModel>) response.body().getData());
                                } else {
                                    onProjectHisProductsDownlodeLisner
                                            .OnProjectHisProductsDownlodeFail(response.body().getErrorMsg(),
                                                    context.getString(R.string.alert), false);
                                }
                            } else {
                                onProjectHisProductsDownlodeLisner.OnProjectHisProductsDownlodeFail(context.getString(R.string.noInternetMessage) + " 82",
                                        context.getString(R.string.internalError),
                                        false);
                            }
                        } else {
                            onProjectHisProductsDownlodeLisner.OnProjectHisProductsDownlodeFail(context.getString
                                            (R.string.noInternetMessage) + " 81",
                                    context.getString(R.string.ServerNotresponding),
                                    false);
                        }
                    }

                    @Override
                    public void onFailure(Call<ProductsModel> call, Throwable t) {
                        onProjectHisProductsDownlodeLisner.OnProjectHisProductsDownlodeFail(
                                context.getString(R.string.noInternetMessage) + " 80",
                                context.getString(R.string.noInternetAlert),
                                true);
                    }
                });
    }
}
