package com.lobotus.mapei.flowfragments.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by user1 on 05-10-2017.
 */

public class MOTandPOTdataModel {

    @SerializedName("Success")
    @Expose
    private Integer success;
    @SerializedName("dataMOT")
    @Expose
    private List<ModeOfTravelModel> ModeOfTravelModel = null;
    @SerializedName("dataPurpose")
    @Expose

    private List<PurposeOfTravelModel> PurposeOfTravelModel = null;
    @SerializedName("Msg")
    @Expose
    private String erroeMsg=null;

    public String getErroeMsg() {
        return erroeMsg;
    }

    public void setErroeMsg(String erroeMsg) {
        this.erroeMsg = erroeMsg;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public List<ModeOfTravelModel> getModeOfTravelModel() {
        return ModeOfTravelModel;
    }

    public void setModeOfTravelModel(List<ModeOfTravelModel> ModeOfTravelModel) {
        this.ModeOfTravelModel = ModeOfTravelModel;
    }

    public List<PurposeOfTravelModel> getPurposeOfTravelModel() {
        return PurposeOfTravelModel;
    }

    public void setPurposeOfTravelModel(List<PurposeOfTravelModel> PurposeOfTravelModel) {
        this.PurposeOfTravelModel = PurposeOfTravelModel;
    }
}
