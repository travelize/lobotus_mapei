package com.lobotus.mapei.flowfragments.fragments;


import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatAutoCompleteTextView;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.ContentFrameLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.lobotus.mapei.R;
import com.lobotus.mapei.baseactivity.BaseActivity;
import com.lobotus.mapei.customwidgits.LabelledSpinner;
import com.lobotus.mapei.flowfragments.model.CustomerDataModel;
import com.lobotus.mapei.flowfragments.mvp.presentor.DemoPresentor;
import com.lobotus.mapei.flowfragments.mvp.presentorimp.DemoPresentorImp;
import com.lobotus.mapei.flowfragments.mvp.views.DemoView;
import com.lobotus.mapei.flowfragments.onactivityforresult.CustomerActForResult;
import com.lobotus.mapei.utils.CommonFunc;
import com.lobotus.mapei.utils.PreferenceUtils;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class DemoFragment extends Fragment implements LabelledSpinner.OnItemChosenListener, DemoView, View.OnClickListener {
    private View fragmentView = null;
    private Dialog dialog = null;
    //
    private DemoPresentor demoPresentor = null;
    // req code to get thecustomer seltion done. . .
    private final int REQ_CODE_TO_GET_THE_CUSTOMR_DATA = 11;
    // TEMP CUSTOMER ID. .
    private String TEMP_CUST_ID = null;


    /////////////////////////////////////////////////////////////////////////////////////////////////// life cycle stuff
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        fragmentView = inflater.inflate(R.layout.fragment_demo, container, false);
        fragmentView.findViewById(R.id.fragment_demo_submit_button).setOnClickListener(this);
        fragmentView.findViewById(R.id.fragment_demo_customer_name_id).setOnClickListener(this);
        fragmentView.findViewById(R.id.fragment_demo_add_customer_id_imageview).setOnClickListener(this);
        setTheFloatingActionMenuButton();
        demoPresentor = new DemoPresentorImp(this);
        demoPresentor.reqToGetDemoListFromPresentor(getActivity());
        return fragmentView;
    }

    @Override
    public void onPause() {
        super.onPause();
        resetFields();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        CommonFunc.hideKeyboard(getActivity());
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////// ui setting

    private void setTheFloatingActionMenuButton() {
        FloatingActionButton floatingActionButton = fragmentView.findViewById(R.id.fragment_demo_floating_menu);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((BaseActivity) getActivity()).attachFragment(new ViewDemoMeetings(), true, "View Demo Meeting", null);
            }
        });
        //
    }


    private void setDemoTypeSpinner(String[] demotypes) {
        ((LabelledSpinner) fragmentView.findViewById(R.id.fragment_demo_demotype_spinner_id))
                .setItemsArray(demotypes);

        ((LabelledSpinner) fragmentView.findViewById(R.id.fragment_demo_demotype_spinner_id)).setOnItemChosenListener(this);
    }

    @Override
    public void onItemChosen(View labelledSpinner, AdapterView<?> adapterView, View itemView, int position, long id) {
        switch (labelledSpinner.getId()) {
            case R.id.fragment_demo_demotype_spinner_id:
                hideAndShowViewStub(position);
        }

    }

    @Override
    public void onNothingChosen(View labelledSpinner, AdapterView<?> adapterView) {

    }

    private void hideAndShowViewStub(int position) {
        switch (position) {
            case 0:
                //trial
                if (((ViewStub) fragmentView.findViewById(R.id.fragment_demo_trial_viewstub)) == null) {
                    fragmentView.findViewById(R.id.fragment_demo_trial_viewstub_inflateId).setVisibility(View.GONE);
                }
                // supervising
                if (((ViewStub) fragmentView.findViewById(R.id.fragment_demo_supervisining_viewstub)) == null) {
                    fragmentView.findViewById(R.id.fragment_demo_supervisining_viewstub_inflateId).setVisibility(View.GONE);
                }
                break;
            case 1:
                // supervising
                if (((ViewStub) fragmentView.findViewById(R.id.fragment_demo_supervisining_viewstub)) == null) {
                    fragmentView.findViewById(R.id.fragment_demo_supervisining_viewstub_inflateId).setVisibility(View.GONE);
                }
                // trial
                if (((ViewStub) fragmentView.findViewById(R.id.fragment_demo_trial_viewstub)) != null) {
                    ((ViewStub) fragmentView.findViewById(R.id.fragment_demo_trial_viewstub))
                            .inflate();
                    loadDemoTypeTrial();
                } else {
                    fragmentView.findViewById(R.id.fragment_demo_trial_viewstub_inflateId).setVisibility(View.VISIBLE);
                }
                break;
            case 2:
                //trial
                if (((ViewStub) fragmentView.findViewById(R.id.fragment_demo_trial_viewstub)) == null) {
                    fragmentView.findViewById(R.id.fragment_demo_trial_viewstub_inflateId).setVisibility(View.GONE);
                }
                // supervising
                if (((ViewStub) fragmentView.findViewById(R.id.fragment_demo_supervisining_viewstub)) != null) {
                    ((ViewStub) fragmentView.findViewById(R.id.fragment_demo_supervisining_viewstub))
                            .inflate();
                    loadDemoTypeSupervising();
                } else {
                    fragmentView.findViewById(R.id.fragment_demo_supervisining_viewstub_inflateId).setVisibility(View.VISIBLE);
                }
        }
    }

    private void resetFields() {
        ((LabelledSpinner) fragmentView.findViewById(R.id.fragment_demo_demotype_spinner_id)).getSpinner().setSelection(0);
        //trial
        if (((ViewStub) fragmentView.findViewById(R.id.fragment_demo_trial_viewstub)) == null) {
            ((LabelledSpinner) fragmentView.findViewById(R.id.fragment_demo_trial_stub_spinner_id)).getSpinner().setSelection(0);
        }
        // supervising
        if (((ViewStub) fragmentView.findViewById(R.id.fragment_demo_supervisining_viewstub)) == null) {
            ((LabelledSpinner) fragmentView.findViewById(R.id.fragment_demo_supervising_stub_spinner_id)).getSpinner().setSelection(0);
        }
        // remark
        ((AppCompatEditText) fragmentView.findViewById(R.id.fragment_demo_notes_editext)).setText("");

    }


    private boolean validateDetails() {

        if (((AppCompatAutoCompleteTextView) fragmentView.findViewById(R.id.fragment_demo_customer_name_id)).getText().toString().isEmpty()) {
            CommonFunc.commonDialog(getActivity(), getString(R.string.alert), "Please enter customer name", false);
        } else if (((LabelledSpinner) fragmentView.findViewById(R.id.fragment_demo_demotype_spinner_id)).
                getSpinner().getSelectedItemPosition() == 0) {
            CommonFunc.commonDialog(getActivity(), "Alert!", "Please choose demo type", false);
            return false;
        } else if (((LabelledSpinner) fragmentView.findViewById(R.id.fragment_demo_demotype_spinner_id)).
                getSpinner().getSelectedItemPosition() == 1) {
            if (((LabelledSpinner) fragmentView.findViewById(R.id.fragment_demo_trial_stub_spinner_id)).
                    getSpinner().getSelectedItemPosition() == 0) {
                CommonFunc.commonDialog(getActivity(), "Alert!", "Please choose trial type", false);
                return false;
            } else {
                return true;
            }
        } else if (((LabelledSpinner) fragmentView.findViewById(R.id.fragment_demo_demotype_spinner_id)).
                getSpinner().getSelectedItemPosition() == 2) {

            if (((LabelledSpinner) fragmentView.findViewById(R.id.fragment_demo_supervising_stub_spinner_id)).
                    getSpinner().getSelectedItemPosition() == 0) {
                CommonFunc.commonDialog(getActivity(), "Alert!", "Please choose supervising type", false);
                return false;
            } else {
                return true;
            }
        }
        return false;

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.fragment_demo_customer_name_id:
                Intent intent = new Intent(getActivity(), CustomerActForResult.class);
                startActivityForResult(intent, REQ_CODE_TO_GET_THE_CUSTOMR_DATA);
                break;
            case R.id.fragment_demo_submit_button:
                if (PreferenceUtils.getIsUserCheckedIn(getActivity())) {
                    if (validateDetails())
                        if (((LabelledSpinner) fragmentView.findViewById(R.id.fragment_demo_demotype_spinner_id)).getSpinner().getSelectedItemPosition() == 1)
                            demoPresentor.reqToPostDemoDetails(getActivity(),
                                    ((LabelledSpinner) fragmentView.findViewById(R.id.fragment_demo_demotype_spinner_id)).getSpinner().getSelectedItemPosition(),
                                    ((AppCompatEditText) fragmentView.findViewById(R.id.fragment_demo_notes_editext)).getText().toString(),
                                    ((LabelledSpinner) fragmentView.findViewById(R.id.fragment_demo_trial_stub_spinner_id)).
                                            getSpinner().getSelectedItemPosition(), TEMP_CUST_ID);
                        else
                            demoPresentor.reqToPostDemoDetails(getActivity(),
                                    ((LabelledSpinner) fragmentView.findViewById(R.id.fragment_demo_demotype_spinner_id)).getSpinner().getSelectedItemPosition(),
                                    ((AppCompatEditText) fragmentView.findViewById(R.id.fragment_demo_notes_editext)).getText().toString(),
                                    ((LabelledSpinner) fragmentView.findViewById(R.id.fragment_demo_supervising_stub_spinner_id)).
                                            getSpinner().getSelectedItemPosition(), TEMP_CUST_ID);
                } else {
                    CommonFunc.commonDialog(getActivity(), getString(R.string.alert), getString(R.string.chekInAlert), false);
                }

                break;
            case R.id.fragment_demo_add_customer_id_imageview:
                ((BaseActivity) getActivity()).attachFragment(new AddCustomers(), true,
                        getString(R.string.AddCustomers), null);


        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (getActivity().RESULT_OK == resultCode) {
            CustomerDataModel customerDataModel = data.getExtras().getParcelable("customer_model");
            TEMP_CUST_ID = customerDataModel.getCustomerID();
            ((AppCompatAutoCompleteTextView) fragmentView.findViewById(R.id.fragment_demo_customer_name_id))
                    .setText("" + customerDataModel.getCustomerName());
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////// mvp stuff
    @Override
    public void showProgressDialog() {
        dialog = new Dialog(getActivity());
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.loaging_layout);
        dialog.show();
    }

    @Override
    public void dismissProgressDialog() {
        if (getActivity() != null)
            if (dialog != null) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                dialog = null;
            }
    }


    ///////////////////////////////////////////////////////////////////// load spinner
    @Override
    public void afterDemoTypeListLoaded(String[] stringsDemos) {
        setDemoTypeSpinner(stringsDemos);
    }

    /////////////////////////////////////////////////////////////////////// demo post
    @Override
    public void afterDemoPostSuccess(String message) {

        ((BaseActivity) getActivity()).attachSuccessFragment(message, "Add more demo");
    }

    @Override
    public void afterDemoPostFail(String message, String dialogTitle, boolean isInternetError) {
        CommonFunc.commonDialog(getActivity(), dialogTitle, message, isInternetError);
    }
    ///////////////////////////////////////////////////////////////////////////// mvp tress pass

    private void loadDemoTypeTrial() {
        ((LabelledSpinner) fragmentView.findViewById(R.id.fragment_demo_trial_stub_spinner_id)).setItemsArray(getResources()
                .getStringArray(R.array.demoTypes_trial));
    }

    private void loadDemoTypeSupervising() {
        ((LabelledSpinner) fragmentView.findViewById(R.id.fragment_demo_supervising_stub_spinner_id)).setItemsArray(getResources()
                .getStringArray(R.array.demoTypes_trial));
    }

    @Override
    public void afterCustomerModelDownlodeSuccess(ArrayList<CustomerDataModel> customerDataModelss) {
      /*  customerDataModels.clear();
        customerDataModels.addAll(customerDataModelss);
        String[] strings=new String[customerDataModels.size()];
        for (int i=0;i<strings.length;i++)
        {
            strings[i]=customerDataModels.get(i).getCustomerName();
        }
        ArrayAdapter<String> arrayAdapterUser = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_list_item_1, strings);
        ((AppCompatAutoCompleteTextView)fragmentView.findViewById(R.id.fragment_demo_customer_name_id))
                .setAdapter(arrayAdapterUser);*/

    }

    @Override
    public void afterCustomerModelDownlodeFail(String message, String dialogTitle, boolean isInternetError) {
        CommonFunc.commonDialog(getActivity(), dialogTitle, message, isInternetError);
    }

}
