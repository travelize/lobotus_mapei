package com.lobotus.mapei.flowfragments.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DemoMeetingsModel {

    @SerializedName("DemoID")
    @Expose
    private String meetingID;

    @SerializedName("UserId")
    @Expose
    private String userId;

    @SerializedName("FullName")
    @Expose
    private String userFullName;

    @SerializedName("CustomerName")
    @Expose
    private String customerName;

    @SerializedName("DemoType")
    @Expose
    private String demoType;

    @SerializedName("DemoSubType")
    @Expose
    private String subDemoType;

    @SerializedName("OnDate")
    @Expose
    private String onDate;

    @SerializedName("Location")
    @Expose
    private String meetingLocation;

    @SerializedName("Success")
    @Expose
    private Integer success;

    @SerializedName("Msg")
    @Expose
    private String msg;


    public String getMeetingID() {
        return meetingID;
    }

    public void setMeetingID(String meetingID) {
        this.meetingID = meetingID;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserFullName() {
        return userFullName;
    }

    public void setUserFullName(String userFullName) {
        this.userFullName = userFullName;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getDemoType() {
        return demoType;
    }

    public void setDemoType(String demoType) {
        this.demoType = demoType;
    }

    public String getSubDemoType() {
        return subDemoType;
    }

    public void setSubDemoType(String subDemoType) {
        this.subDemoType = subDemoType;
    }

    public String getOnDate() {
        return onDate;
    }

    public void setOnDate(String onDate) {
        this.onDate = onDate;
    }

    public String getMeetingLocation() {
        return meetingLocation;
    }

    public void setMeetingLocation(String meetingLocation) {
        this.meetingLocation = meetingLocation;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
