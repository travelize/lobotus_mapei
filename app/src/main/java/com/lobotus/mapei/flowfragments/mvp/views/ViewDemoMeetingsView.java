package com.lobotus.mapei.flowfragments.mvp.views;

import com.lobotus.mapei.flowfragments.model.DemoMeetingsModel;
import com.lobotus.mapei.fromlite.model.MeetingModel;

import java.util.ArrayList;

public interface ViewDemoMeetingsView {

    void showProgressDialog();

    void dismissProgressDialog();

    void afterDemoListDownlodeSuccess(ArrayList<DemoMeetingsModel> demoMeetingsModelArrayList);
    void afterDemoMeetingsListDownlodeFail(String message, String dialogTitle, boolean isInternetError);
}
