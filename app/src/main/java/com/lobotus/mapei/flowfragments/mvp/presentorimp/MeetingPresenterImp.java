package com.lobotus.mapei.flowfragments.mvp.presentorimp;

import android.content.Context;
import android.view.View;

import com.google.gson.Gson;
import com.lobotus.mapei.R;
import com.lobotus.mapei.flowfragments.model.ComplaintDataTypeModel;
import com.lobotus.mapei.flowfragments.model.CustomerDataModel;
import com.lobotus.mapei.flowfragments.model.ProductsDataModel;
import com.lobotus.mapei.flowfragments.model.ProjectsDataModel;
import com.lobotus.mapei.flowfragments.mvp.intrector.AddProjectsIntractor;
import com.lobotus.mapei.flowfragments.mvp.intrector.MeetingInterator;
import com.lobotus.mapei.flowfragments.mvp.intrector.ProjectsFragIntrector;
import com.lobotus.mapei.flowfragments.mvp.intrectorimp.AddProjectsIntractorImp;
import com.lobotus.mapei.flowfragments.mvp.intrectorimp.MeetingInteratorImp;
import com.lobotus.mapei.flowfragments.mvp.intrectorimp.ProjectsFragIntrectorImp;
import com.lobotus.mapei.flowfragments.mvp.presentor.MeetingPresentor;
import com.lobotus.mapei.flowfragments.mvp.views.MeetingView;
import com.lobotus.mapei.utils.CommonFunc;
import com.lobotus.mapei.utils.NetworkUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user1 on 04-10-2017.
 */

public class MeetingPresenterImp implements MeetingPresentor,MeetingInterator.OnMeetingCustomerDataDownlodeLisner,
        ProjectsFragIntrector.ProjectsListDownlodeLisner,MeetingInterator.OnCustomerNameAndTypeLisner,
        AddProjectsIntractor.OnProductsDataDownlodeLisner,
        MeetingInterator.OnClomplaintsDataDownlodeLisner,
        MeetingInterator.OnMeetingPostDataDownlodeLisner,
        MeetingInterator.OnProjectsListBasedOnCustomerNameLisner
{

    private MeetingView meetingView=null;
    private MeetingInterator meetingInterator=null;
    // to downlode projects data. . .
    private ProjectsFragIntrector projectsFragIntrector=null;
    // to downlode the products
    private AddProjectsIntractor addProjectsIntractor=null;

    public MeetingPresenterImp(MeetingView meetingView) {
        this.meetingView = meetingView;
        this.meetingInterator = new MeetingInteratorImp();
        this.projectsFragIntrector=new ProjectsFragIntrectorImp();
        this.addProjectsIntractor=new AddProjectsIntractorImp();
    }
    /////////////////////////////////////////// validate meetings in presentor
    @Override
    public void validateMeetingsDataInPresenter(View view, Context context, int projectNameSpinnerSelectedItemPos,
                                                String customerName, String customerType,
                                                int meetingTypeSpinnerSelectedPos,int submeetingTypeSelectedSpinner,
                                                String valueOfFllowUpOrder, String orderLostOfFollowupOrder,
                                                int complaintsTypesSpinnerSelectedPos,
                                                int arFollowTypesSpinnerSelectedPos,
                                                String arFollowPaymentNewDate, String arFollowPaymentProjectissue,
                                                String arFollowPaymentProjectDeley,
                                                String arFollowCformFollowupCollected,
                                                String arFollowCformFollowupNewDate,
                                                String arFollowCformFollowupNotTracable,
                                                String arFollowCformReconcilationMissingInvoice,
                                                String arFollowCformReconcilationShortPayment,
                                                String arFollowCformReconcilationStatement,
                                                ArrayList<ProductsDataModel> productsDataModels_Submitance,
                                                ArrayList<ProductsDataModel> productsDataModels_Presentation,
                                                ArrayList<ProductsDataModel> productsDataModels_Followup,
                                                String locAddress) {
       if (customerName.isEmpty())
        {
            CommonFunc.commonDialog(context,context.getString(R.string.alert),"Please enter customer name",false);
        }
        else if (customerType.isEmpty())
        {
            CommonFunc.commonDialog(context,context.getString(R.string.alert),"Please enter customer name (Customer type will get auto filled)",false);
        }
        else if (meetingTypeSpinnerSelectedPos==0)
        {
            CommonFunc.commonDialog(context,context.getString(R.string.alert),"Please select meeting type",false);
        }else {
            ////////////////// clear up to selecting the spinner. . ..  check after the spinner. .
            switch (meetingTypeSpinnerSelectedPos)
            {
                case 1:
                    // meeting

                    if (submeetingTypeSelectedSpinner==0)
                    {
                        CommonFunc.commonDialog(context,context.getString(R.string.alert),"Please sub meeting type",false);
                    }else {
                        switch (submeetingTypeSelectedSpinner)
                        {
                            case 1: // submitance
                                if (!checkProductsAdded(productsDataModels_Submitance))
                                {
                                    CommonFunc.commonDialog(context,context.getString(R.string.alert),"Please add Submittance products",false);
                                }else if (locAddress.isEmpty())
                                {
                                    CommonFunc.commonDialog(context,context.getString(R.string.alert),
                                            "Please check your location",false);
                                }
                                else {
                                    meetingView.afterValidationOfMeetingSuccessfull();
                                }
                                break;
                            case 2: // presentation
                                if (!checkProductsAdded(productsDataModels_Presentation))
                                {
                                    CommonFunc.commonDialog(context,context.getString(R.string.alert),"Please add Presentation products",false);
                                }else if (locAddress.isEmpty())
                                {
                                    CommonFunc.commonDialog(context,context.getString(R.string.alert),
                                            "Please check your location",false);
                                }
                                else {
                                    meetingView.afterValidationOfMeetingSuccessfull();
                                }
                                break;
                            case 3: // follow up
                                if (!checkProductsAdded(productsDataModels_Followup))
                                {
                                    CommonFunc.commonDialog(context,context.getString(R.string.alert),"Please add Followup products",false);
                                }
                                else if (valueOfFllowUpOrder.isEmpty())
                                {
                                    CommonFunc.commonDialog(context,context.getString(R.string.alert),"Please enter value",false);
                                }else if (orderLostOfFollowupOrder.isEmpty()){
                                    CommonFunc.commonDialog(context,context.getString(R.string.alert),"Please enter order lost",false);
                                }else if (locAddress.isEmpty())
                                {
                                    CommonFunc.commonDialog(context,context.getString(R.string.alert),
                                            "Please check your location",false);
                                }
                                else {
                                    meetingView.afterValidationOfMeetingSuccessfull();
                                }
                        }
                    }
                    break;
                case 2:
                    // AR follow ups
                    if (arFollowTypesSpinnerSelectedPos==0)
                    {
                        CommonFunc.commonDialog(context,context.getString(R.string.alert),"Please select AR follow type",false);
                    }else {
                        switch (arFollowTypesSpinnerSelectedPos)
                        {
                            case 1:// payment followup
                                if (arFollowPaymentNewDate.isEmpty())
                                {
                                    CommonFunc.commonDialog(context,context.getString(R.string.alert),"Please enter New date",false);
                                }else if (arFollowPaymentProjectissue.isEmpty())
                                {
                                    CommonFunc.commonDialog(context,context.getString(R.string.alert),"Please enter Project issue",false);
                                }else if (arFollowPaymentProjectDeley.isEmpty())
                                {
                                    CommonFunc.commonDialog(context,context.getString(R.string.alert),"Please enter Project delay",false);
                                }else if (locAddress.isEmpty())
                                {
                                    CommonFunc.commonDialog(context,context.getString(R.string.alert),
                                            "Please check your location",false);
                                }
                                else {
                                    meetingView.afterValidationOfMeetingSuccessfull();
                                }
                                break;
                            case 2: // c form followup
                                if (arFollowCformFollowupCollected.isEmpty())
                                {
                                    CommonFunc.commonDialog(context,context.getString(R.string.alert),"Please enter collected",false);
                                }else if (arFollowCformFollowupNewDate.isEmpty())
                                {
                                    CommonFunc.commonDialog(context,context.getString(R.string.alert),"Please enter New date",false);
                                }else if (arFollowCformFollowupNotTracable.isEmpty())
                                {
                                    CommonFunc.commonDialog(context,context.getString(R.string.alert),"Please enter Not traceable",false);
                                }else if (locAddress.isEmpty())
                                {
                                    CommonFunc.commonDialog(context,context.getString(R.string.alert),
                                            "Please check your location",false);
                                }
                                else {
                                    meetingView.afterValidationOfMeetingSuccessfull();
                                }
                                break;
                            case 3: // reconcilation
                                if (arFollowCformReconcilationMissingInvoice.isEmpty())
                                {
                                    CommonFunc.commonDialog(context,context.getString(R.string.alert),"Please enter missing invoice",false);
                                }else if (arFollowCformReconcilationShortPayment.isEmpty()){
                                    CommonFunc.commonDialog(context,context.getString(R.string.alert),"Please enter short payment",false);
                                }else if (arFollowCformReconcilationStatement.isEmpty())
                                {
                                    CommonFunc.commonDialog(context,context.getString(R.string.alert),"Please enter statement",false);
                                }else if (locAddress.isEmpty())
                                {
                                    CommonFunc.commonDialog(context,context.getString(R.string.alert),
                                            "Please check your location",false);
                                }
                                else {
                                    meetingView.afterValidationOfMeetingSuccessfull();
                                }
                        }
                    }
                    break;
                case 3:
                    // complaints
                    if (complaintsTypesSpinnerSelectedPos==0)
                    {
                        CommonFunc.commonDialog(context,context.getString(R.string.alert),"Please enter complaints type",false);
                    }else if (locAddress.isEmpty())
                    {
                        CommonFunc.commonDialog(context,context.getString(R.string.alert),
                                "Please check your location",false);
                    }
                    else {
                        meetingView.afterValidationOfMeetingSuccessfull();
                    }


            }
        }

    }

    private boolean checkProductsAdded(ArrayList<ProductsDataModel> productsDataModels)
    {
        boolean isProductsAdded=false;
        for (int i=0;i<productsDataModels.size();i++)
        {
            if (productsDataModels.get(i).isChecked())
            {
                isProductsAdded=true;
                break;
            }
        }
        return isProductsAdded;
    }

    private boolean checkForcustomerNameIsCorrectFromList(ArrayList<CustomerDataModel> customerDataModels,String entered)
    {
        boolean res=false;
        for (int i=0;i<customerDataModels.size();i++)
        {
            if (entered.equals(customerDataModels.get(i).getCustomerName()))
            {
                res=true;
                break;
            }
        }
        return res;
    }


    ////////////////////////////////////////////// customer data
    @Override
    public void reqToGetTheCustomerDataFromPresentor(Context context) {
        if (NetworkUtils.checkInternetAndOpenDialog(context))
        {
            meetingView.showProgressDialog();
            meetingInterator.reqToPostCustomerDataReqToServer(context,this);
        }

    }



    ////////////////////////////////////////////// customer data lisners
    @Override
    public void OnMeetingCustomerDataDownlodeSuccess(ArrayList<CustomerDataModel> customerDataModels) {
        System.out.println("ggggggggggggg----5------------"+new Gson().toJson(customerDataModels));
        meetingView.dismissProgressDialog();
        meetingView.afterCustomerModelDownlodeSuccess(customerDataModels);
    }

    @Override
    public void OnMeetingCustomerDataDownlodeFail(String message, String dialogTitle, boolean isInternetError) {
        meetingView.dismissProgressDialog();
        meetingView.afterCustomerModelDownlodeFail(message,dialogTitle,isInternetError);
    }
    /////////////////////////////////////////////// downlode projects
    @Override
    public void reqToDownlodeTheListOfProjectsFromPresentor(Context context) {
        meetingView.showProgressDialog();
        projectsFragIntrector.reqToFetchProjectsListFromServer(context,this);
    }



    @Override
    public void OnProjectsListDownlodeSuccess(ArrayList<ProjectsDataModel> projectsDataModels) {
        meetingView.dismissProgressDialog();
        meetingView.afterProjectsListDownlodeSuccess(projectsDataModels);
    }

    @Override
    public void OnProjectsListDownlodeFail(String message, String dialogTitle, boolean isInternetError) {
        meetingView.dismissProgressDialog();
        meetingView.afterProjectsListDownlodeFail(message,dialogTitle,isInternetError);

    }
    /////////////////////////////////////////////////////// getting customer name and type form the choosen project
    @Override
    public void reqToGetTheCustomerNameAndTypeFromChoosenProject(ArrayList<ProjectsDataModel> projectsDataModels,
                                                                 String autocompleteText, Context context) {
        meetingInterator.getCustomerNamaAndTypeFromTheChoosenProject(projectsDataModels,autocompleteText,context,this);

    }


    @Override
    public void OnCustomerNameAndTypeGetLisnerSuccess(String customerName, String customerType) {
        meetingView.afterCustomerNameChoosenFromAutoSuccess(customerName,customerType);
    }

    @Override
    public void OnCustomerNameAndTypeGetLisnerFail(String message, String dialogTitle, boolean isInternetError) {
        meetingView.afterCustomerNameChoosenFromAutoFail(message,dialogTitle,isInternetError);
    }
    /////////////////////////////////////////////////////// post ot get the products

    @Override
    public void reqToGetTheProductsDataFromPresentor(Context context) {
        if (NetworkUtils.checkInternetAndOpenDialog(context))
        {
            meetingView.showProgressDialog();
            addProjectsIntractor.reqToPostProductsDataReqToServer(this,context);
        }

    }

    @Override
    public void OnProductsDataDownlodeSuccess(List<ProductsDataModel> productsDataModels) {
        meetingView.dismissProgressDialog();
        meetingView.afterProductsModelDownlodeSuccess(productsDataModels);
    }

    @Override
    public void OnProductsDataDownlodeFail(String message, String dialogTitle, boolean isInternetError) {
        meetingView.dismissProgressDialog();
        meetingView.afterProductsModelDoenlodeFail(message,dialogTitle,isInternetError);
    }

    ///////////////////////////////////////////////// downlode the complaints data
    @Override
    public void reqToGetTheComplaintsDataFromTheServer(Context context) {
        if (NetworkUtils.checkInternetAndOpenDialog(context))
        {
            meetingView.showProgressDialog();
            meetingInterator.getTheComplaintsDataFromTheServer(context,this);
        }

    }


    @Override
    public void OnComplaintsDataDownlodeSuccess(ArrayList<ComplaintDataTypeModel> complaintDataTypeModels) {
        meetingView.dismissProgressDialog();
        meetingView.afterComplaintsDownlodeSuccess(complaintDataTypeModels);

    }

    @Override
    public void OnComplaintsDataDownlodeFail(String message, String dialogTitle, boolean isInternetError) {
        meetingView.dismissProgressDialog();
        meetingView.afterComplaintsDownloldeFail(message,dialogTitle,isInternetError);

    }

    ////////////////////////////////////////////////// posting the meeting
    @Override
    public void reqToPostTheMeetingToTheServer(Context context, String projectName,
                                               ArrayList<ProjectsDataModel> projectsDataModels,
                                               String customerName,String customerType,
                                               int meetingTypeSpinnerSelectedPos,
                                               int submeetingTypeSelectedSpinner,String valueOfFllowUpOrder,
                                               String orderLostOfFollowupOrder,
                                               int complaintsTypesSpinnerSelectedPos,
                                               int arFollowTypesSpinnerSelectedPos,
                                               String arFollowPaymentNewDate,
                                               String arFollowPaymentProjectissue,
                                               String arFollowPaymentProjectDeley,
                                               String arFollowCformFollowupCollected,
                                               String arFollowCformFollowupNewDate,
                                               String arFollowCformFollowupNotTracable,
                                               String arFollowCformReconcilationMissingInvoice,
                                               String arFollowCformReconcilationShortPayment,
                                               String arFollowCformReconcilationStatement,
                                               String notes,
                                               ArrayList<ProductsDataModel> productsDataModels_Submitance,
                                               ArrayList<ProductsDataModel> productsDataModels_Presentation,
                                               ArrayList<ProductsDataModel> productsDataModels_Followup,
                                               String location,
                                               String meetingSubTypeValue,String customerId) {
        if (NetworkUtils.checkInternetAndOpenDialog(context))
        {
            meetingView.showProgressDialog();
            meetingInterator.reqToPostTheMeetingsDataToServer(context,
                    projectName,
                    projectsDataModels,
                    customerName,
                    customerType,
                    meetingTypeSpinnerSelectedPos,
                    submeetingTypeSelectedSpinner,
                    valueOfFllowUpOrder,
                    orderLostOfFollowupOrder,
                    complaintsTypesSpinnerSelectedPos,
                    arFollowTypesSpinnerSelectedPos,
                    arFollowPaymentNewDate,
                    arFollowPaymentProjectissue,
                    arFollowPaymentProjectDeley,
                    arFollowCformFollowupCollected,
                    arFollowCformFollowupNewDate,
                    arFollowCformFollowupNotTracable,
                    arFollowCformReconcilationMissingInvoice,
                    arFollowCformReconcilationShortPayment,
                    arFollowCformReconcilationStatement,
                    productsDataModels_Submitance,
                    productsDataModels_Presentation,
                    productsDataModels_Followup,
                    notes,
                    this,
                    location,meetingSubTypeValue,customerId);

        }


    }

    @Override
    public void OnMeetingPostSuccess(String message, String meetingId) {
        meetingView.dismissProgressDialog();
        meetingView.afterMeetingPostSuccess(message,meetingId);
    }

    @Override
    public void OnMeetingPostFail(String message, String dialogTitle, boolean isInternetError) {
        meetingView.dismissProgressDialog();
        meetingView.afterMeetingPostFail(message,dialogTitle,isInternetError);
    }

    /////////////////////////////////////////////// req to get the projucts lsit  from choosen cust name
    @Override
    public void reqToGetTheProjectNameBasedOnCustomerName(Context context, String CustID) {
        if (NetworkUtils.checkInternetAndOpenDialog(context))
        {
            meetingView.showProgressDialog();
            meetingInterator.reqToGetTheProjectsListBasedOnCustomerFromIntractor(context,CustID,this);
        }
    }

    @Override
    public void OnProjectsListBasedOnCustomerNameDownlodeSuccess(ArrayList<ProjectsDataModel> projectsDataModels) {
        meetingView.dismissProgressDialog();
        meetingView.afterProjectsListDownlodeSuccess(projectsDataModels);

    }

    @Override
    public void OnProjectsListBasedOnCustomerNameDownlodeFail(String message, String dialogTitle, boolean isInternetError) {
        meetingView.dismissProgressDialog();
        meetingView.afterProjectsListDownlodeFail(message,dialogTitle,isInternetError);
    }
}
