package com.lobotus.mapei.flowfragments.mvp.presentorimp;

import android.content.Context;
import android.support.design.widget.TextInputLayout;
import android.view.View;

import com.lobotus.mapei.R;
import com.lobotus.mapei.flowfragments.model.CustomerDataModel;
import com.lobotus.mapei.flowfragments.model.ProductsDataModel;
import com.lobotus.mapei.flowfragments.mvp.intrector.AddProjectsIntractor;
import com.lobotus.mapei.flowfragments.mvp.intrectorimp.AddProjectsIntractorImp;
import com.lobotus.mapei.flowfragments.mvp.presentor.AddProjectsPresentor;
import com.lobotus.mapei.flowfragments.mvp.views.AddProjectsView;
import com.lobotus.mapei.utils.CommonFunc;
import com.lobotus.mapei.utils.NetworkUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user1 on 06-10-2017.
 */

public class AddProjectsPresentorImp implements AddProjectsPresentor,
        AddProjectsIntractor.OnTravelCustomerDataDownlodeLisner,AddProjectsIntractor.OnProductsDataDownlodeLisner,
        AddProjectsIntractor.OnProductsPostLisner,
        AddProjectsIntractor.OnProjectStagesLoadLisner{

    private AddProjectsView addProjectsView=null;
    private AddProjectsIntractor addProjectsIntractor=null;

    public AddProjectsPresentorImp(AddProjectsView addProjectsView) {
        this.addProjectsView = addProjectsView;
        this.addProjectsIntractor = new AddProjectsIntractorImp();
    }

    ///////////////////////////////////////////////////////////////////////////// validation
    private void attachErrorMessage(int id,String errorMsg,View viewParent)
    {
        ((TextInputLayout)viewParent.findViewById(id)).setError(errorMsg);

    }
    @Override
    public void validateAddProjectsInPresentorOnly(String projectName, String customerName,
                                                   String customerType,String productsList,
                                                   ArrayList<ProductsDataModel> productsDataModels,
                                                   String projectPeriod,int projectStageSelectedPos,
                                                   Context context, View view) {
       if (projectName==null)
        {
            attachErrorMessage(view.findViewById(R.id.fragment_add_projucts_projucts_name_id_input_layout).getId(),
                    "Please enter project name",view);
            CommonFunc.fullscroolToTop(view,R.id.fragment_add_projucts_scrollview);

        }else if (projectName.isEmpty())
        {
            attachErrorMessage(view.findViewById(R.id.fragment_add_projucts_projucts_name_id_input_layout).getId(),
                    "Please enter project name",view);
            CommonFunc.fullscroolToTop(view,R.id.fragment_add_projucts_scrollview);
        }else if (customerName==null)
        {
            attachErrorMessage(view.findViewById(R.id.fragment_add_projucts_customer_name_id_input_layout).getId(),
                    "Please enter customer name",view);
            CommonFunc.fullscroolToTop(view,R.id.fragment_add_projucts_scrollview);
        }else if (customerType==null)
        {
            attachErrorMessage(view.findViewById(R.id.fragment_add_projucts_customer_name_id_input_layout).getId(),
                    "Please enter proper customer name (Type will get auto fill)",view);
            CommonFunc.fullscroolToTop(view,R.id.fragment_add_projucts_scrollview);
        }else if (customerType.isEmpty())
        {
            attachErrorMessage(view.findViewById(R.id.fragment_add_projucts_customer_name_id_input_layout).getId(),
                    "Please enter proper customer name (Type will get auto fill)",view);
            CommonFunc.fullscroolToTop(view,R.id.fragment_add_projucts_scrollview);
        }else if (projectPeriod==null)
        {
            attachErrorMessage(view.findViewById(R.id.fragment_add_projucts_period_start_id_input_layout).getId(),
                    "Please enter project period!",view);
        }else if (projectPeriod.isEmpty())
        {
            attachErrorMessage(view.findViewById(R.id.fragment_add_projucts_period_start_id_input_layout).getId(),
                    "Please enter project period!",view);
        }else if (projectStageSelectedPos==0)
        {
            CommonFunc.commonDialog(context,context.getString(R.string.alert),"Please select project stage",false);
        }else {
            addProjectsView.afterValidatingAddProjects();
        }

    }



    private boolean checkCustomerNameIsMatchingTheList(ArrayList<CustomerDataModel> customerDataModels,
                                                       String customerNameInAutoCompletetextView)
    {
        boolean flag=false;
        for (int i=0;i<customerDataModels.size();i++)
        {
            if (customerDataModels.get(i).getCustomerName().equals(customerNameInAutoCompletetextView))
            {
                flag=true;
                break;
            }
        }
        return flag;
    }


    /////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////// customer data
    @Override
    public void reqToGetTheCustomerDataFromPresentor(Context context) {
        if (NetworkUtils.checkInternetAndOpenDialog(context))
        {
            addProjectsView.showProgressDialog();
            addProjectsIntractor.reqToPostCustomerDataReqToServer(context,this);
        }

    }


    @Override
    public void OnTravelCustomerDataDownlodeSuccess(ArrayList<CustomerDataModel> customerDataModels) {
        addProjectsView.dismissProgressDialog();
        addProjectsView.afterCustomerModelDownlodeSuccess(customerDataModels);
    }

    @Override
    public void OnTravelCustomerDataDownlodeFail(String message, String dialogTitle, boolean isInternetError) {
        addProjectsView.dismissProgressDialog();
        addProjectsView.afterCustomerModelDownlodeFail(message,dialogTitle,isInternetError);
    }

    ///////////////////////////////////////////////////////// products data
    @Override
    public void reqToGetTheProductsDataFromPresentor(Context context) {
        if (NetworkUtils.checkInternetAndOpenDialog(context)) {
            addProjectsView.showProgressDialog();
            addProjectsIntractor.reqToPostProductsDataReqToServer(this, context);
        }
    }

    @Override
    public void OnProductsDataDownlodeSuccess(List<ProductsDataModel> productsDataModels) {
        addProjectsView.dismissProgressDialog();
        addProjectsView.afterProductsModelDownlodeSuccess(productsDataModels);

    }

    @Override
    public void OnProductsDataDownlodeFail(String message, String dialogTitle, boolean isInternetError) {
        addProjectsView.dismissProgressDialog();
        addProjectsView.afterProductsModelDoenlodeFail(message,dialogTitle,isInternetError);
    }
    ///////////////////////////////////////////////////////////////  post to add the projects. . .
    @Override
    public void reqToaddNewProjects(Context context, String projectName, String customerName, String customerType,
                                    String productList, String projectPeriod, String projectStage, String notes) {
        if (NetworkUtils.checkInternetAndOpenDialog(context)) {
            addProjectsView.showProgressDialog();
            addProjectsIntractor.reqToPostTheNewProductsData(context, projectName, customerName, customerType, productList, projectPeriod, projectStage, notes, this);
        }
    }
    @Override
    public void OnProductsPostSuccess(String message) {
        addProjectsView.dismissProgressDialog();
        addProjectsView.afteraddingProjectsSuccess(message);
    }

    @Override
    public void OnProductsPostFail(String message, String dialogTitle, boolean isInternetError) {
        addProjectsView.dismissProgressDialog();
        addProjectsView.afterAddingProjuctsFail(message,dialogTitle,isInternetError);

    }

    /////////////////////////////////////// load the project stages

    @Override
    public void reqToLoadProjectStages(Context context) {
        addProjectsIntractor.reqToLoadTheProjectStages(context,this);
    }

    @Override
    public void OnProjectDataLoadSuccess(String[] stringsProjectStages) {
        addProjectsView.afterProjectStagesLoadedSuccess(stringsProjectStages);
    }
}
