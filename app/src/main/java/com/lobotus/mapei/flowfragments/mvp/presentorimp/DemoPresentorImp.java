package com.lobotus.mapei.flowfragments.mvp.presentorimp;

import android.content.Context;

import com.lobotus.mapei.flowfragments.model.CustomerDataModel;
import com.lobotus.mapei.flowfragments.mvp.intrector.AddProjectsIntractor;
import com.lobotus.mapei.flowfragments.mvp.intrector.DemoIntractor;
import com.lobotus.mapei.flowfragments.mvp.intrectorimp.AddProjectsIntractorImp;
import com.lobotus.mapei.flowfragments.mvp.intrectorimp.DemoIntrectorImp;
import com.lobotus.mapei.flowfragments.mvp.presentor.DemoPresentor;
import com.lobotus.mapei.flowfragments.mvp.views.DemoView;
import com.lobotus.mapei.utils.NetworkUtils;

import java.util.ArrayList;

/**
 * Created by user1 on 25-09-2017.
 */

public class DemoPresentorImp implements DemoPresentor,DemoIntractor.DemoTypesLoadLisner,
        DemoIntractor.DemoPostResponseLisner, AddProjectsIntractor.OnTravelCustomerDataDownlodeLisner{

    private DemoView demoView=null;
    private DemoIntractor demoIntractor=null;
    private AddProjectsIntractor addProjectsIntractor=null;

    public DemoPresentorImp(DemoView demoView) {
        this.demoView = demoView;
        this.demoIntractor = new DemoIntrectorImp();
    }


    ///////////////////////////////////////////////////////// load the spinner from strings
    @Override
    public void reqToGetDemoListFromPresentor(Context context) {
        demoIntractor.reqToGetDemoListFromIntrector(context,this);
    }


    @Override
    public void onDemotypesLoadedLisner(String[] stringsDemos) {
        demoView.afterDemoTypeListLoaded(stringsDemos);
    }
    //////////////////////////////////////////////////////// post the demo data to server

    @Override
    public void reqToPostDemoDetails(Context context, int demoTypeId,
                                     String remark, int demosubtype,String customerId) {
        if (NetworkUtils.checkInternetAndOpenDialog(context))
        {
            demoView.showProgressDialog();
            demoIntractor.reqToPostTheDemoToServer(context,demoTypeId,remark,demosubtype,this,customerId);
        }

    }


    @Override
    public void onDemoPostSuccessLisner(String message) {
        demoView.dismissProgressDialog();
        demoView.afterDemoPostSuccess(message);
    }

    @Override
    public void onDemoPostFailLisner(String message,String dialogTitle,boolean isInternetError) {
        demoView.dismissProgressDialog();
        demoView.afterDemoPostFail(message,dialogTitle,isInternetError);


    }
    ////////////////////////////////////////////// odwnlode customer data
    @Override
    public void reqToGetTheCustomerDataFromPresentor(Context context) {
        addProjectsIntractor=new AddProjectsIntractorImp();
        demoView.showProgressDialog();
        addProjectsIntractor.reqToPostCustomerDataReqToServer(context,this);

    }

    @Override
    public void OnTravelCustomerDataDownlodeSuccess(ArrayList<CustomerDataModel> customerDataModels) {
        demoView.dismissProgressDialog();
        demoView.afterCustomerModelDownlodeSuccess(customerDataModels);

    }

    @Override
    public void OnTravelCustomerDataDownlodeFail(String message, String dialogTitle, boolean isInternetError) {
        demoView.dismissProgressDialog();
        demoView.afterCustomerModelDownlodeFail(message,dialogTitle,isInternetError);

    }
}
