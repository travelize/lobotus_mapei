package com.lobotus.mapei.flowfragments.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by User on 21-03-2018.
 */

public class UserTravelModel {
    @SerializedName("distance")
    @Expose
    private String distance;
    @SerializedName("traveldate")
    @Expose
    private String traveldate;

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getTraveldate() {
        return traveldate;
    }

    public void setTraveldate(String traveldate) {
        this.traveldate = traveldate;
    }
}
