package com.lobotus.mapei.flowfragments.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by user1 on 08-11-2017.
 */

public class CustomerTypeDataModel {

    @SerializedName("CustomerTypeId")
    @Expose
    private String customerID;
    @SerializedName("CustomerType")
    @Expose
    private String customerType;


    public CustomerTypeDataModel(String customerType) {
        this.customerType = customerType;
    }

    public String getCustomerID() {
        return customerID;
    }

    public void setCustomerID(String customerID) {
        this.customerID = customerID;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

}
