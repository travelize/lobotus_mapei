package com.lobotus.mapei.flowfragments.mvp.intrector;

import android.content.Context;

import com.lobotus.mapei.flowfragments.model.ProjectHistoryModel;

import java.util.ArrayList;

/**
 * Created by user1 on 16-10-2017.
 */

public interface ProjectHistoryIntractor {
    ////////////////////// psot to downlode the project history

    interface OnProjectHistoryDownlodeLisner
    {
        void onProjectHistoryDownlodeSuccess(ProjectHistoryModel projectHistoryModel);
        void onProjectHistoryDownlodeFail(String message, String dialogTitle, boolean isInternetError);
    }

    void reqToPostToDownlodeProjectHistory(Context context,String projectId,
                                           OnProjectHistoryDownlodeLisner onProjectHistoryDownlodeLisner );
}
