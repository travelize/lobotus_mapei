package com.lobotus.mapei.flowfragments.mvp.intrector;

import android.content.Context;

import com.lobotus.mapei.flowfragments.model.LeaveDataModel;

import java.util.ArrayList;

/**
 * Created by user1 on 05-10-2017.
 */

public interface LeaveFragIntrector {
    ////////////////////////////////////////////// req to lode the leave list
    interface LeaveListLoadLisner
    {
        void OnLeaveListLoadSuccess(ArrayList<LeaveDataModel> leaveDataModels);
        void OnLeaveListLoadfail(String message,String dialogTitle,boolean isInternetError);
    }
    void postToDOwnlodeTheLeaveListFromServer(Context context,LeaveListLoadLisner leaveListLoadLisner);

    //////////////////////////////////////////// cancel leave list
    interface LeaveCancelLisner
    {
        void OnLeaveCancelSuccess(String message);
        void OnLeaveCancelFail(String message,String dialogTitle,boolean isInternetError);
    }
    void postReqToCancelLeaveFromServer(String leaveId,LeaveCancelLisner leaveCancelLisner,Context context);

}
