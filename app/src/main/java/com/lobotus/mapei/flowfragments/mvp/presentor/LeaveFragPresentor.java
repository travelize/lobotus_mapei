package com.lobotus.mapei.flowfragments.mvp.presentor;

import android.content.Context;

/**
 * Created by user1 on 05-10-2017.
 */

public interface LeaveFragPresentor {
    ////////////////////////// req to load the leave the leave list
    void reqToLodeTheLeaveList(Context context);
    ///////////////////////// req to cancel the leave
    void reqToCancelTheLeave(String leaveId,Context context);
}
