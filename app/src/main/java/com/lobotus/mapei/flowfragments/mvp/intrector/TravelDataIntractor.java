package com.lobotus.mapei.flowfragments.mvp.intrector;

import android.content.Context;

import com.lobotus.mapei.flowfragments.model.UserTravelModel;

import java.util.ArrayList;

/**
 * Created by User on 21-03-2018.
 */

public interface TravelDataIntractor {


    interface OnTravelDataDownlodeLisner
    {
        void OnTravelDataDownlodeSuccess(ArrayList<UserTravelModel> userTravelModels);
        void OnTravelDataDOwnlodeFail(String message, String dialogTitle, boolean isInternetError);
    }
    void reqToGetUserTravelData(Context context,String fromDate,String toDate,String userId,
                                OnTravelDataDownlodeLisner onTravelDataDownlodeLisner);
}
