package com.lobotus.mapei.flowfragments.mvp.intrector;

import android.content.Context;

import com.lobotus.mapei.flowfragments.model.CustomerDataModel;
import com.lobotus.mapei.flowfragments.model.ProductsDataModel;
import com.lobotus.mapei.flowfragments.model.ProjectsDataModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user1 on 06-10-2017.
 */

public interface AddProjectsIntractor {
    //////////////////////// load projectStages Data
    interface  OnProjectStagesLoadLisner
    {
        void OnProjectDataLoadSuccess(String[] stringsProjectStages);
    }
    void reqToLoadTheProjectStages(Context context,OnProjectStagesLoadLisner onProjectStagesLoadLisner);
    //////////////////////// customer data
    interface OnTravelCustomerDataDownlodeLisner
    {
        void OnTravelCustomerDataDownlodeSuccess(ArrayList<CustomerDataModel> customerDataModels);
        void OnTravelCustomerDataDownlodeFail(String message,String dialogTitle,boolean isInternetError);
    }
    void reqToPostCustomerDataReqToServer(Context context, OnTravelCustomerDataDownlodeLisner onTravelCustomerDataDownlodeLisner);
    /////////////////////// products data
    interface  OnProductsDataDownlodeLisner
    {
        void OnProductsDataDownlodeSuccess(List<ProductsDataModel> productsDataModels);
        void OnProductsDataDownlodeFail(String message,String dialogTitle,boolean isInternetError);
    }
    void reqToPostProductsDataReqToServer(OnProductsDataDownlodeLisner onProductsDataDownlodeLisner,Context context);
    /////////////////////// post to add products
    interface OnProductsPostLisner
    {
        void OnProductsPostSuccess(String message);
        void OnProductsPostFail(String message, String dialogTitle, boolean isInternetError);
    }
    void reqToPostTheNewProductsData(Context context,String projectName,String customerName,String customerType,String productList,
                                     String projectPeriod,String projectStage,String notes,OnProductsPostLisner onProductsPostLisner);
}
