package com.lobotus.mapei.flowfragments.mvp.intrector;

import android.content.Context;

import com.lobotus.mapei.flowfragments.model.ProjectsDataModel;

import java.util.ArrayList;

/**
 * Created by user1 on 06-10-2017.
 */

public interface ProjectsFragIntrector {

    ////////////////////////// post to downlode the list of projects

    interface ProjectsListDownlodeLisner
    {
        void OnProjectsListDownlodeSuccess(ArrayList<ProjectsDataModel> projectsDataModels);
        void OnProjectsListDownlodeFail(String message,String dialogTitle,boolean isInternetError);
    }
    void reqToFetchProjectsListFromServer(Context context,ProjectsListDownlodeLisner projectsListDownlodeLisner);
}
