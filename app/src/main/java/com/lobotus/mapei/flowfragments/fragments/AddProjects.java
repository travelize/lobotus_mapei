package com.lobotus.mapei.flowfragments.fragments;


import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatAutoCompleteTextView;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.NumberPicker;
import android.widget.Toast;

import com.lobotus.mapei.R;
import com.lobotus.mapei.baseactivity.BaseActivity;
import com.lobotus.mapei.customwidgits.LabelledSpinner;
import com.lobotus.mapei.flowfragments.model.CustomerDataModel;
import com.lobotus.mapei.flowfragments.model.ProductsDataModel;
import com.lobotus.mapei.flowfragments.mvp.presentor.AddProjectsPresentor;
import com.lobotus.mapei.flowfragments.mvp.presentorimp.AddProjectsPresentorImp;
import com.lobotus.mapei.flowfragments.mvp.views.AddProjectsView;
import com.lobotus.mapei.flowfragments.onactivityforresult.CustomerActForResult;
import com.lobotus.mapei.flowfragments.onactivityforresult.ProductsOnActivityForRresult;
import com.lobotus.mapei.utils.CommonFunc;
import com.lobotus.mapei.utils.PreferenceUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddProjects extends Fragment implements AddProjectsView, View.OnClickListener, View.OnFocusChangeListener,
        View.OnKeyListener {
    // req code to get thecustomer seltion done. . .
    private final int REQ_CODE_TO_GET_THE_CUSTOMR_DATA = 11;
    // TEMP CUSTOMER ID. .
    private String TEMP_CUST_ID = null;
    private View viewFragment = null;
    private Dialog dialog = null;
    //////////////////////////////////////////////////////////////////////
    private AddProjectsPresentor addProjectsPresentor = null;
    ////// temp data's
    public static ArrayList<ProductsDataModel> productsDataModels = new ArrayList<>();
    private String[] stringsProjectStagesTempData = null;
    //// req code to products
    private int REQ_CODE_TO_SEL_PRODUCTS = 10;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        viewFragment = inflater.inflate(R.layout.fragment_add_projects, container, false);
        //
        viewFragment.findViewById(R.id.fragment_add_projucts_start_period_id).setOnClickListener(this);
        viewFragment.findViewById(R.id.fragment_add_projucts_end_period_id).setOnClickListener(this);
        viewFragment.findViewById(R.id.fragment_add_projucts_products_list_id).setOnClickListener(this);
        viewFragment.findViewById(R.id.fragment_add_projucts_add_customer_id_imageview).setOnClickListener(this);
       /* viewFragment.findViewById(R.id.fragment_add_projucts_start_period_id).setOnFocusChangeListener(this);
        viewFragment.findViewById(R.id.fragment_add_projucts_end_period_id).setOnFocusChangeListener(this);*/
        viewFragment.findViewById(R.id.fragment_add_projucts_products_list_id).setOnFocusChangeListener(this);
        viewFragment.findViewById(R.id.fragment_add_submit_button_id).setOnClickListener(this);
        viewFragment.findViewById(R.id.fragment_add_projucts_customer_type_id).setOnClickListener(this);
        ((AppCompatAutoCompleteTextView) viewFragment.findViewById(R.id.fragment_add_projucts_customer_name_id)).setOnClickListener(this);
        ((AppCompatAutoCompleteTextView) viewFragment.findViewById(R.id.fragment_add_projucts_customer_name_id)).setOnKeyListener(this);
        ((AppCompatImageView) viewFragment.findViewById(R.id.fragment_add_projucts_add_customer_id_imageview)).setOnClickListener(this);

        /// text change lisner
        ((AppCompatEditText) viewFragment.findViewById(R.id.fragment_add_projucts_projucts_name_id)).addTextChangedListener(textWatcherProjuctName);
        ((AppCompatAutoCompleteTextView) viewFragment.findViewById(R.id.fragment_add_projucts_customer_name_id)).addTextChangedListener(textWatcherCustomerName);
        ((AppCompatEditText) viewFragment.findViewById(R.id.fragment_add_projucts_start_period_id)).addTextChangedListener(textWatcherProjectPeriod);
        //

        //
        addProjectsPresentor = new AddProjectsPresentorImp(this);
        addProjectsPresentor.reqToLoadProjectStages(getActivity());
        addProjectsPresentor.reqToGetTheCustomerDataFromPresentor(getActivity());
        return viewFragment;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        CommonFunc.hideKeyboard(getActivity());
        productsDataModels.clear();
    }

    TextWatcher textWatcherProjuctName = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            if (((TextInputLayout) viewFragment.findViewById(R.id.fragment_add_projucts_projucts_name_id_input_layout)).isErrorEnabled())
                ((TextInputLayout) viewFragment.findViewById(R.id.fragment_add_projucts_projucts_name_id_input_layout)).setErrorEnabled(false);
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };
    TextWatcher textWatcherCustomerName = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            if (((TextInputLayout) viewFragment.findViewById(R.id.fragment_add_projucts_customer_name_id_input_layout)).isErrorEnabled())
                ((TextInputLayout) viewFragment.findViewById(R.id.fragment_add_projucts_customer_name_id_input_layout)).setErrorEnabled(false);
            if (((TextInputLayout) viewFragment.findViewById(R.id.fragment_add_projucts_customer_type_id_input_layout)).isErrorEnabled())
                ((TextInputLayout) viewFragment.findViewById(R.id.fragment_add_projucts_customer_type_id_input_layout)).setErrorEnabled(false);
            //
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };
    TextWatcher textWatcherProjectPeriod = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            if (((TextInputLayout) viewFragment.findViewById(R.id.fragment_add_projucts_period_start_id_input_layout)).isErrorEnabled())
                ((TextInputLayout) viewFragment.findViewById(R.id.fragment_add_projucts_period_start_id_input_layout)).setErrorEnabled(false);
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fragment_add_projucts_start_period_id:
                showPeriodPickerDialog(v.getId());
                break;
            case R.id.fragment_add_projucts_end_period_id:
                if (((AppCompatEditText) viewFragment.findViewById(R.id.fragment_add_projucts_start_period_id)).getText().toString().isEmpty())
                    Toast.makeText(getActivity(), "Please select project start first", Toast.LENGTH_SHORT).show();
                else
                    showPeriodPickerDialog(v.getId());
                break;
            case R.id.fragment_add_projucts_products_list_id:
                startOnActivityForResult();
                break;
            case R.id.fragment_add_submit_button_id:
                if (PreferenceUtils.getIsUserCheckedIn(getActivity()))
                    addProjectsPresentor.validateAddProjectsInPresentorOnly(((AppCompatEditText)
                                    viewFragment.findViewById(R.id.fragment_add_projucts_projucts_name_id)).getText().toString(),
                            ((AppCompatAutoCompleteTextView)
                                    viewFragment.findViewById(R.id.fragment_add_projucts_customer_name_id)).getText().toString(),
                            ((AppCompatEditText)
                                    viewFragment.findViewById(R.id.fragment_add_projucts_customer_type_id)).getText().toString(),
                            ((AppCompatEditText)
                                    viewFragment.findViewById(R.id.fragment_add_projucts_products_list_id)).getText().toString(), productsDataModels,
                            ((AppCompatEditText)
                                    viewFragment.findViewById(R.id.fragment_add_projucts_start_period_id)).getText().toString(),
                            ((LabelledSpinner) viewFragment.findViewById(R.id.fragment_add_projucts_stage_spinner_id)).getSpinner()
                                    .getSelectedItemPosition()
                            , getActivity(), viewFragment);
                else
                    CommonFunc.commonDialog(getActivity(), getString(R.string.alert), getString(R.string.chekInAlert), false);
                break;
            case R.id.fragment_add_projucts_customer_type_id:
                if (checkcustomerNameisValid())
                    Toast.makeText(getActivity(), "Customer type will get auto filled based on customer name", Toast.LENGTH_SHORT).show();
                break;
            case R.id.fragment_add_projucts_add_customer_id_imageview:
                ((BaseActivity) getActivity()).attachFragment(new AddCustomers(), true,
                        getString(R.string.AddCustomers), null);
                break;
            case R.id.fragment_add_projucts_customer_name_id:
                Intent intent = new Intent(getActivity(), CustomerActForResult.class);
                startActivityForResult(intent, REQ_CODE_TO_GET_THE_CUSTOMR_DATA);

        }
    }


    @Override
    public boolean onKey(View view, int i, KeyEvent keyEvent) {
        if (i == KeyEvent.KEYCODE_DEL) {
            //this is for backspace
            ((AppCompatEditText)
                    viewFragment.findViewById(R.id.fragment_add_projucts_customer_type_id)).setText("");
        }
        return false;
    }


    private void startOnActivityForResult() {
        if (productsDataModels.isEmpty()) {
            addProjectsPresentor.reqToGetTheProductsDataFromPresentor(getActivity());
        } else {
            Intent intent = new Intent(getActivity(), ProductsOnActivityForRresult.class);
            startActivityForResult(intent, REQ_CODE_TO_SEL_PRODUCTS);
        }
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (hasFocus)
            switch (v.getId()) {
                case R.id.fragment_add_projucts_start_period_id:
                case R.id.fragment_add_projucts_end_period_id:
                    showPeriodPickerDialog(v.getId());
                    break;
                case R.id.fragment_add_projucts_products_list_id:
                    startOnActivityForResult();

            }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQ_CODE_TO_SEL_PRODUCTS) {
            if (resultCode == getActivity().RESULT_OK) {
                String s = "";
                boolean firstData = true;
                for (int i = 0; i < productsDataModels.size(); i++) {
                    if (productsDataModels.get(i).isChecked()) {
                        if (firstData) {
                            s = productsDataModels.get(i).getProductName();
                            firstData = false;
                        } else if (i == productsDataModels.size() - 1) {
                            s = s + ", " + productsDataModels.get(i).getProductName();
                        } else {
                            s = s + ", " + productsDataModels.get(i).getProductName();
                        }
                    }
                }
                ((AppCompatEditText) viewFragment.findViewById(R.id.fragment_add_projucts_products_list_id))
                        .setText(s);
            }
        }
        if (requestCode == REQ_CODE_TO_GET_THE_CUSTOMR_DATA) {
            if (resultCode == getActivity().RESULT_OK) {
                CustomerDataModel customerDataModel = data.getExtras().getParcelable("customer_model");
                TEMP_CUST_ID = customerDataModel.getCustomerID();
                ((AppCompatAutoCompleteTextView) viewFragment.findViewById(R.id.fragment_add_projucts_customer_name_id))
                        .setText(customerDataModel.getCustomerName());
                ((AppCompatEditText) viewFragment.findViewById(R.id.fragment_add_projucts_customer_type_id))
                        .setText(customerDataModel.getType());
            }
        }
    }

    ///////////////////////////////////////////////////////////////////////////////// ui'stuff
    private void showPeriodPickerDialog(final int id) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(R.layout.period_dialog);
        dialog.findViewById(R.id.period_dialog_close_imageview).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        final NumberPicker numberPickerYear = dialog.findViewById(R.id.period_dialog_year_numpicker);
        final NumberPicker numberPickerMonths = dialog.findViewById(R.id.period_dialog_month_numpicker);
        if (id == R.id.fragment_add_projucts_start_period_id) {
            numberPickerYear.setMaxValue(2100);
            numberPickerYear.setMinValue(2017);
            numberPickerMonths.setMaxValue(12);
            numberPickerMonths.setMinValue(1);
        } else {
            numberPickerYear.setMaxValue(2100);
            String[] strings = ((AppCompatEditText) viewFragment.findViewById(R.id.fragment_add_projucts_start_period_id)).getText().toString().split("/");
            numberPickerYear.setMinValue(Integer.parseInt(strings[0]));
            numberPickerMonths.setMaxValue(12);
            numberPickerMonths.setMinValue(1);
        }

        numberPickerYear.setFocusable(false);
        numberPickerMonths.setFocusable(false);
        numberPickerYear.setFocusableInTouchMode(false);
        numberPickerMonths.setFocusableInTouchMode(false);
        dialog.findViewById(R.id.period_dialog_done).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if (id == R.id.fragment_add_projucts_start_period_id) {
                    ((AppCompatEditText) viewFragment.findViewById(id))
                            .setText(numberPickerYear.getValue() + "/" + numberPickerMonths.getValue());
                    if (!((AppCompatEditText) viewFragment.findViewById(R.id.fragment_add_projucts_end_period_id)).getText().toString().isEmpty())
                        ((AppCompatEditText) viewFragment.findViewById(R.id.fragment_add_projucts_end_period_id)).setText("");

                } else {
                    String[] strings = ((AppCompatEditText) viewFragment.findViewById(R.id.fragment_add_projucts_start_period_id)).getText().toString().split("/");
                    int startYear = Integer.parseInt(strings[0]);
                    int startMonth = Integer.parseInt(strings[1]);
                    int endYear = numberPickerYear.getValue();
                    int endMonth = numberPickerMonths.getValue();
                   /* if (Integer.parseInt(strings[0])<numberPickerYear.getValue() && Integer.parseInt(strings[1])<numberPickerMonths.getValue())
                    {
                        Toast.makeText(getActivity(), "end period should be greater than start", Toast.LENGTH_SHORT).show();
                    }else {
                        ((AppCompatEditText)viewFragment.findViewById(id))
                                .setText(numberPickerYear.getValue()+"/"+numberPickerMonths.getValue());
                    }*/
                    if (startYear == endYear) {
                        if (startMonth > endMonth) {
                            Toast.makeText(getActivity(), "end period should be greater than start", Toast.LENGTH_SHORT).show();
                        } else {
                            ((AppCompatEditText) viewFragment.findViewById(id))
                                    .setText(numberPickerYear.getValue() + "/" + numberPickerMonths.getValue());
                        }
                    } else {
                        ((AppCompatEditText) viewFragment.findViewById(id))
                                .setText(numberPickerYear.getValue() + "/" + numberPickerMonths.getValue());

                    }
                }

            }
        });
        dialog.show();
    }

    ////////////////////////////////////////////////////////////////////////////// mvp stuffs
    @Override
    public void afterProjectStagesLoadedSuccess(String[] stringsStages) {
        stringsProjectStagesTempData = stringsStages;
        ((LabelledSpinner) viewFragment.findViewById(R.id.fragment_add_projucts_stage_spinner_id))
                .setItemsArray(stringsStages);
    }

    @Override
    public void showProgressDialog() {
        dialog = new Dialog(getActivity());
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.loaging_layout);
        dialog.show();
    }

    @Override
    public void dismissProgressDialog() {
        if (getActivity() != null)
            if (dialog != null) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                dialog = null;
            }
    }


    @Override
    public void afterCustomerModelDownlodeSuccess(ArrayList<CustomerDataModel> customerDataModelss) {
        /*customerDataModels.clear();
        customerDataModels.addAll(customerDataModelss);
        String[] strings=new String[customerDataModels.size()];
        for (int i=0;i<strings.length;i++)
        {
            strings[i]=customerDataModels.get(i).getCustomerName();
        }
        ArrayAdapter<String> arrayAdapterUser = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_list_item_1, strings);
        ((AppCompatAutoCompleteTextView)viewFragment.findViewById(R.id.fragment_add_projucts_customer_name_id))
                .setAdapter(arrayAdapterUser);
*/
    }

    @Override
    public void afterCustomerModelDownlodeFail(String message, String dialogTitle, boolean isInternetError) {
        CommonFunc.commonDialog(getActivity(), dialogTitle, message, isInternetError);
    }

    @Override
    public void afterProductsModelDownlodeSuccess(List<ProductsDataModel> productsDataModelss) {
        productsDataModels.clear();
        productsDataModels.addAll(productsDataModelss);
        Intent intent = new Intent(getActivity(), ProductsOnActivityForRresult.class);
        startActivityForResult(intent, REQ_CODE_TO_SEL_PRODUCTS);
    }

    @Override
    public void afterProductsModelDoenlodeFail(String message, String dialogTitle, boolean isInternetError) {
        CommonFunc.commonDialog(getActivity(), dialogTitle, message, isInternetError);
    }

    //// validating  projects. . .
    @Override
    public void afterValidatingAddProjects() {
        addProjectsPresentor.reqToaddNewProjects(getActivity(),
                ((AppCompatEditText) viewFragment.findViewById(R.id.fragment_add_projucts_projucts_name_id)).getText().toString(),
                TEMP_CUST_ID,
                ((AppCompatEditText) viewFragment.findViewById(R.id.fragment_add_projucts_customer_type_id)).getText().toString(),
                ((AppCompatEditText) viewFragment.findViewById(R.id.fragment_add_projucts_products_list_id)).getText().toString(),
                ((AppCompatEditText) viewFragment.findViewById(R.id.fragment_add_projucts_start_period_id)).getText().toString(),
                stringsProjectStagesTempData[((LabelledSpinner) viewFragment.findViewById(R.id.fragment_add_projucts_stage_spinner_id))
                        .getSpinner().getSelectedItemPosition()],
                ((AppCompatEditText) viewFragment.findViewById(R.id.fragment_add_projucts_remark_id)).getText().toString());
    }


    @Override
    public void afteraddingProjectsSuccess(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
        getActivity().getSupportFragmentManager().popBackStack();
    }

    @Override
    public void afterAddingProjuctsFail(String message, String dialogTitle, boolean isInternetError) {
        CommonFunc.commonDialog(getActivity(), dialogTitle, message, isInternetError);

    }


    private boolean checkcustomerNameisValid() {
        if (((AppCompatAutoCompleteTextView) viewFragment.findViewById(R.id.fragment_add_projucts_customer_name_id)).getText().toString().isEmpty()) {
            ((TextInputLayout) viewFragment.findViewById(R.id.fragment_add_projucts_customer_name_id_input_layout))
                    .setError("Please enter customer name first.");
            viewFragment.findViewById(R.id.fragment_add_projucts_customer_name_id).requestFocus();
            CommonFunc.fullscroolToTop(viewFragment, R.id.fragment_add_projucts_scrollview);
            return false;
        } else {
            return true;
        }
    }


}
