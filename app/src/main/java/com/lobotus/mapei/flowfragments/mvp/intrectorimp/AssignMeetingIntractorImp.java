package com.lobotus.mapei.flowfragments.mvp.intrectorimp;

import android.content.Context;
import android.util.Log;

import com.lobotus.mapei.R;
import com.lobotus.mapei.flowfragments.model.ComplaintDataTypeModel;
import com.lobotus.mapei.flowfragments.model.ComplaintTypeModel;
import com.lobotus.mapei.flowfragments.model.ProductsDataModel;
import com.lobotus.mapei.flowfragments.model.ProjectsDataModel;
import com.lobotus.mapei.flowfragments.model.UserModel;
import com.lobotus.mapei.flowfragments.model.UsersDataModel;
import com.lobotus.mapei.flowfragments.mvp.intrector.AssignMeetingIntractor;
import com.lobotus.mapei.flowfragments.mvp.intrector.MeetingInterator;
import com.lobotus.mapei.fromlite.model.CommRespModel;
import com.lobotus.mapei.retrofitapi.APIService;
import com.lobotus.mapei.retrofitapi.ApiUtils;
import com.lobotus.mapei.utils.CommonFunc;
import com.lobotus.mapei.utils.ConstantsUtils;
import com.lobotus.mapei.utils.PreferenceUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by User on 20-03-2018.
 */

public class AssignMeetingIntractorImp implements AssignMeetingIntractor {


    private Call<UserModel> userModelCall = null;
    private Call<CommRespModel> assignMeetingCall = null;

    @Override
    public void reqToFetchUserListFromServer(final Context context, final OnUserListDownlodeLisner onUserListDownlodeLisner, String TypeID, String UserId) {

        APIService apiService = ApiUtils.getAPIService(ConstantsUtils.postBaseUrl + ConstantsUtils.POST_TO_GET_THE_LIST_OF_USERS);
        userModelCall = apiService.postToGetTheUserList(TypeID, UserId);
        userModelCall.enqueue(new Callback<UserModel>() {
            @Override
            public void onResponse(Call<UserModel> call, Response<UserModel> response) {
                if (response.isSuccessful()) {
                    if (response.body().getSuccess() == 1) {
                        onUserListDownlodeLisner.
                                OnUserListDownlodeSuccess((ArrayList<UsersDataModel>) response.body().getData());
                    } else {
                        onUserListDownlodeLisner.OnUserListDownlodeFail(
                                response.body().getErrorMsg(),
                                context.getString(R.string.alert),
                                false);
                    }

                } else {
                    onUserListDownlodeLisner.OnUserListDownlodeFail(
                            context.getString(R.string.justErrorCode) + " 121",
                            context.getString(R.string.ServerNotresponding),
                            false);
                }
            }

            @Override
            public void onFailure(Call<UserModel> call, Throwable t) {
                onUserListDownlodeLisner.OnUserListDownlodeFail(
                        context.getString(R.string.noInternetMessage) + " 120",
                        context.getString(R.string.noInternetAlert),
                        true);
            }
        });

    }

    ////////////////////////////////// complaints data downlode lisner
    @Override
    public void getTheComplaintsDataFromTheServer(final Context context, final AssignMeetingIntractor.OnClomplaintsDataDownlodeLisner
            onClomplaintsDataDownlodeLisner) {
        APIService apiService = ApiUtils.getAPIService(ConstantsUtils.postBaseUrl + ConstantsUtils.postToGetTheComplaintsData);
        apiService.postTogetComplaintsFromServer("")
                .enqueue(new Callback<ComplaintTypeModel>() {
                    @Override
                    public void onResponse(Call<ComplaintTypeModel> call, Response<ComplaintTypeModel> response) {
                        if (response.isSuccessful()) {
                            if (response.body() == null) {
                                onClomplaintsDataDownlodeLisner.OnComplaintsDataDownlodeFail(
                                        context.getString(R.string.justErrorCode) + " 36", context.getString(R.string.ServerNotresponding),
                                        false);
                            } else {
                                if (response.body().getSuccess() == 1)
                                    onClomplaintsDataDownlodeLisner.OnComplaintsDataDownlodeSuccess((ArrayList<ComplaintDataTypeModel>) response.body().getData());
                                else
                                    onClomplaintsDataDownlodeLisner.OnComplaintsDataDownlodeFail(response.body().getErrorMsg(),
                                            context.getString(R.string.alert), false);
                            }
                        } else {
                            onClomplaintsDataDownlodeLisner.OnComplaintsDataDownlodeFail(
                                    context.getString(R.string.justErrorCode) + " 35", context.getString(R.string.ServerNotresponding),
                                    false);
                        }
                    }

                    @Override
                    public void onFailure(Call<ComplaintTypeModel> call, Throwable t) {
                        onClomplaintsDataDownlodeLisner.OnComplaintsDataDownlodeFail(
                                context.getString(R.string.noInternetMessage) + " 34",
                                context.getString(R.string.noInternetAlert),
                                true);
                    }
                });
    }

    @Override
    public void reqToAssignMeetingTOUser(String UserId, String MeetingTypeID, String ProjectID, String Status,
                                         String CustomerID, String OnDate, String OnTime, final Context context,
                                         final OnMeetingAssignLisner onMeetingAssignLisner) {
        System.out.println("hhhhhhhhhhhhhhhhhh-----------a---------" + UserId);
        System.out.println("hhhhhhhhhhhhhhhhhh-----------b---------" + MeetingTypeID);
        System.out.println("hhhhhhhhhhhhhhhhhh-----------c---------" + ProjectID);
        System.out.println("hhhhhhhhhhhhhhhhhh------------d--------" + CustomerID);
        System.out.println("hhhhhhhhhhhhhhhhhh-------------e-------" + OnDate);
        System.out.println("hhhhhhhhhhhhhhhhhh--------------f------" + OnTime);


        APIService apiService = ApiUtils.getAPIService(ConstantsUtils.postBaseUrl + ConstantsUtils.POST_TO_ASSIGN_MEETING_TO_USER);
        assignMeetingCall = apiService.postToAssignMeetingToUser(UserId, MeetingTypeID, ProjectID, Status,
                CustomerID, OnDate, OnTime);
        assignMeetingCall.enqueue(new Callback<CommRespModel>() {
            @Override
            public void onResponse(Call<CommRespModel> call, Response<CommRespModel> response) {
                if (response.isSuccessful()) {
                    if (response.body().getSuccess() == 1) {
                        onMeetingAssignLisner.OnMeetingAssignSuccess(response.body().getMsg());
                    } else {
                        onMeetingAssignLisner.OnMeetingAssignFail(
                                response.body().getMsg(),
                                context.getString(R.string.alert),
                                false);
                    }
                } else {
                    onMeetingAssignLisner.OnMeetingAssignFail(
                            context.getString(R.string.justErrorCode) + " 123",
                            context.getString(R.string.ServerNotresponding),
                            false);
                }
            }

            @Override
            public void onFailure(Call<CommRespModel> call, Throwable t) {
                onMeetingAssignLisner.OnMeetingAssignFail(
                        context.getString(R.string.noInternetMessage) + " 122",
                        context.getString(R.string.noInternetAlert),
                        true);
            }
        });


    }

    /////////////////////////////////////////////////////////// posting the meeting data to the server
    @Override
    public void reqToPostTheMeetingsDataToServer(String userId, Context context,
                                                 String projectName,
                                                 ArrayList<ProjectsDataModel> projectsDataModels,
                                                 String customerName,
                                                 String customerType,
                                                 int meetingTypeSpinnerSelectedPos,
                                                 int submeetingTypeSelectedSpinner,
                                                 String valueOfFllowUpOrder,
                                                 String orderLostOfFollowupOrder,
                                                 int complaintsTypesSpinnerSelectedPos,
                                                 int arFollowTypesSpinnerSelectedPos,
                                                 String arFollowPaymentNewDate,
                                                 String arFollowPaymentProjectissue,
                                                 String arFollowPaymentProjectDeley,
                                                 String arFollowCformFollowupCollected,
                                                 String arFollowCformFollowupNewDate,
                                                 String arFollowCformFollowupNotTracable,
                                                 String arFollowCformReconcilationMissingInvoice,
                                                 String arFollowCformReconcilationShortPayment,
                                                 String arFollowCformReconcilationStatement,
                                                 ArrayList<ProductsDataModel> productsDataModels_Submitance,
                                                 ArrayList<ProductsDataModel> productsDataModels_Presentation,
                                                 ArrayList<ProductsDataModel> productsDataModels_Followup,
                                                 String notes,
                                                 AssignMeetingIntractor.OnMeetingPostDataDownlodeLisner onMeetingPostDataDownlodeLisner,
                                                 String location, String meetingSubTypeValue,
                                                 String customerId, String meetingLocation) {

        if (PreferenceUtils.checkUserisLogedin(context)) {
            postTheMeeeting(userId, context,
                    projectName, projectsDataModels,
                    customerName, customerType,
                    meetingTypeSpinnerSelectedPos, submeetingTypeSelectedSpinner, valueOfFllowUpOrder,
                    orderLostOfFollowupOrder, complaintsTypesSpinnerSelectedPos,
                    arFollowTypesSpinnerSelectedPos, arFollowPaymentNewDate,
                    arFollowPaymentProjectissue, arFollowPaymentProjectDeley,
                    arFollowCformFollowupCollected, arFollowCformFollowupNewDate,
                    arFollowCformFollowupNotTracable, arFollowCformReconcilationMissingInvoice,
                    arFollowCformReconcilationShortPayment, arFollowCformReconcilationStatement,
                    productsDataModels_Submitance,
                    productsDataModels_Presentation,
                    productsDataModels_Followup, notes, onMeetingPostDataDownlodeLisner, location,
                    meetingSubTypeValue, customerId, meetingLocation);
        } else {
            CommonFunc.commonDialog(context, "Alert!", "Session Lost Login back", false);
        }
    }


    private void postTheMeeeting(String userId, Context context,
                                 String projectName,
                                 ArrayList<ProjectsDataModel> projectsDataModels,
                                 String customerName,
                                 String customerType,
                                 int meetingTypeSpinnerSelectedPos,
                                 int submeetingTypeSelectedSpinner,
                                 String valueOfFllowUpOrder,
                                 String orderLostOfFollowupOrder,
                                 int complaintsTypesSpinnerSelectedPos,
                                 int arFollowTypesSpinnerSelectedPos,
                                 String arFollowPaymentNewDate,
                                 String arFollowPaymentProjectissue,
                                 String arFollowPaymentProjectDeley,
                                 String arFollowCformFollowupCollected,
                                 String arFollowCformFollowupNewDate,
                                 String arFollowCformFollowupNotTracable,
                                 String arFollowCformReconcilationMissingInvoice,
                                 String arFollowCformReconcilationShortPayment,
                                 String arFollowCformReconcilationStatement,
                                 ArrayList<ProductsDataModel> productsDataModels_Submitance,
                                 ArrayList<ProductsDataModel> productsDataModels_Presentation,
                                 ArrayList<ProductsDataModel> productsDataModels_Followup,
                                 String notes,
                                 AssignMeetingIntractor.OnMeetingPostDataDownlodeLisner onMeetingPostDataDownlodeLisner, String location,
                                 String meetingSubTypeValue, String customerId, String meetingLocation) {


        switch (meetingTypeSpinnerSelectedPos) {
            case 1:
                // meeting
                postMeetingTypeFollowupForOrders(userId, projectsDataModels, projectName,
                        meetingTypeSpinnerSelectedPos, submeetingTypeSelectedSpinner, productsDataModels_Submitance,
                        productsDataModels_Presentation, productsDataModels_Followup, valueOfFllowUpOrder, orderLostOfFollowupOrder, notes,
                        context, onMeetingPostDataDownlodeLisner, location, meetingSubTypeValue, customerId, meetingLocation);
                break;
            case 2:
                // Ar follow up
                switch (arFollowTypesSpinnerSelectedPos) {
                    case 1:// payment
                        postArFollowUpTypePayment(userId, getProjectId(projectsDataModels, projectName),
                                meetingTypeSpinnerSelectedPos, arFollowTypesSpinnerSelectedPos,
                                arFollowPaymentNewDate, arFollowPaymentProjectissue, arFollowPaymentProjectDeley,
                                notes, context, onMeetingPostDataDownlodeLisner, location, customerId, meetingLocation);

                        break;
                    case 2: // c form
                        postTheArFollowupCoforms(userId, getProjectId(projectsDataModels, projectName),
                                String.valueOf(meetingTypeSpinnerSelectedPos), String.valueOf(arFollowTypesSpinnerSelectedPos),
                                arFollowCformFollowupNewDate, arFollowCformFollowupCollected,
                                arFollowCformFollowupNotTracable, notes, context,
                                onMeetingPostDataDownlodeLisner, location, customerId, meetingLocation);
                        break;
                    case 3: // reconcilation
                        postTheArFollowupReconciliation(userId, getProjectId(projectsDataModels, projectName),
                                String.valueOf(meetingTypeSpinnerSelectedPos), String.valueOf(arFollowTypesSpinnerSelectedPos),
                                arFollowCformReconcilationShortPayment, arFollowCformReconcilationMissingInvoice,
                                arFollowCformReconcilationStatement, notes, context,
                                onMeetingPostDataDownlodeLisner, location, customerId, meetingLocation);
                }
                break;
            // complaints
            case 3:
                postingTheMeetingTypeComplaints(userId, getProjectId(projectsDataModels, projectName),
                        String.valueOf(meetingTypeSpinnerSelectedPos), String.valueOf(complaintsTypesSpinnerSelectedPos), notes,
                        context, onMeetingPostDataDownlodeLisner, location, customerId, meetingLocation);
        }

    }

    /// posting  follow up for order
    private void postMeetingTypeFollowupForOrders(String userId, ArrayList<ProjectsDataModel> projectsDataModels,
                                                  String projectName, int meetingTypeSpinnerSelectedPos, int submeetingTypeSelectedSpinner,
                                                  ArrayList<ProductsDataModel> productsDataModels_Submitance,
                                                  ArrayList<ProductsDataModel> productsDataModels_Presentation,
                                                  ArrayList<ProductsDataModel> productsDataModels_Followup,
                                                  String value, String OrderLost, String Notes, final Context context,
                                                  final AssignMeetingIntractor.OnMeetingPostDataDownlodeLisner onMeetingPostDataDownlodeLisner,
                                                  String location, String meetingSubTypeValue,
                                                  String customerId, String meetingLocation) {
        System.out.println("ooooooooooooooo----------meetingSubTypeValue---------------" + meetingSubTypeValue);
        String productsJsonArray = "";
        switch (submeetingTypeSelectedSpinner) {
            case 1:
                productsJsonArray = getProductsJsonArray(productsDataModels_Submitance, context);
                break;
            case 2:
                productsJsonArray = getProductsJsonArray(productsDataModels_Presentation, context);
                break;
            case 3:
                productsJsonArray = getProductsJsonArray(productsDataModels_Followup, context);
        }


        APIService apiService = ApiUtils.getAPIService(ConstantsUtils.postBaseUrl + ConstantsUtils.POST_ASSIGN_MEETINGS_TO_SERVER);//ConstantsUtils.postToAddTheMeetings
        System.out.println("vvvvvvvvvvvvv-------ProjectID---------" + getProjectId(projectsDataModels, projectName));
        System.out.println("vvvvvvvvvvvvv-------UserId---------" + userId);
        System.out.println("vvvvvvvvvvvvv-------MeetingTypeID---------" + String.valueOf(meetingTypeSpinnerSelectedPos));
        System.out.println("vvvvvvvvvvvvv-------MeetingSubTypeID---------" + String.valueOf(submeetingTypeSelectedSpinner));
        System.out.println("vvvvvvvvvvvvv-------ProductList---------" + productsJsonArray);
        System.out.println("vvvvvvvvvvvvv-------Status---------" + "Pending");
        System.out.println("vvvvvvvvvvvvv-------OrderValue---------" + value);
        System.out.println("vvvvvvvvvvvvv-------OrderLost---------" + OrderLost);
        System.out.println("vvvvvvvvvvvvv-------Date---------" + Notes);
        System.out.println("vvvvvvvvvvvvv-------Time---------" + location);
        System.out.println("vvvvvvvvvvvvv-------TotalValue---------" + meetingSubTypeValue);
        System.out.println("vvvvvvvvvvvvv-------CustomerID---------" + customerId);

        apiService.postTheMeetingTypeMeetingAssign(getProjectId(projectsDataModels, projectName),
                userId, String.valueOf(meetingTypeSpinnerSelectedPos),
                String.valueOf(submeetingTypeSelectedSpinner),
                productsJsonArray, "Pending",
                value, OrderLost, Notes, location, meetingSubTypeValue, customerId, meetingLocation).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    if (response.body() == null) {
                        onMeetingPostDataDownlodeLisner.OnMeetingPostFail(context.getString(R.string.justErrorCode) + " 39",
                                context.getString(R.string.alert), false);
                    } else {
                        try {
                            JSONObject jsonObject = new JSONObject(response.body().string());
                            if (jsonObject.getInt("Success") == 1) {
                                onMeetingPostDataDownlodeLisner.OnMeetingPostSuccess(jsonObject.getString("Msg"),
                                        jsonObject.getString("MeetingID"));
                            } else {
                                onMeetingPostDataDownlodeLisner.OnMeetingPostFail(jsonObject.getString("Msg"),
                                        context.getString(R.string.alert), false);
                            }
                        } catch (JSONException | IOException e) {
                            onMeetingPostDataDownlodeLisner.OnMeetingPostFail(context.getString(R.string.justErrorCode) + " 38",
                                    context.getString(R.string.internalError), false);
                        }
                    }
                } else {
                    onMeetingPostDataDownlodeLisner.OnMeetingPostFail(context.getString(R.string.justErrorCode) + " 40",
                            context.getString(R.string.alert), false);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                onMeetingPostDataDownlodeLisner.OnMeetingPostFail(context.getString(R.string.noInternetMessage) + " 41",
                        context.getString(R.string.noInternetAlert),
                        true);
            }
        });
    }


    ////// posting   Ar follow up
    // posting   Ar follow up type  payment
    private void postArFollowUpTypePayment(String userId, String projectId, int meetingTypeSpinnerSelectedPos, int arFollowTypesSpinnerSelectedPos,
                                           String ondate, String ProductIssues,
                                           String ProductDelay, String notes, final Context context,
                                           final AssignMeetingIntractor.OnMeetingPostDataDownlodeLisner onMeetingPostDataDownlodeLisner, String location,
                                           String customerId, String meetingLocation) {
        APIService apiService = ApiUtils.getAPIService(ConstantsUtils.postBaseUrl + ConstantsUtils.POST_ASSIGN_MEETINGS_TO_SERVER);//ConstantsUtils.postToAddTheMeetings

        System.out.println("vvvvvvvvvvvvv-------ProjectID---------" + projectId);
        System.out.println("vvvvvvvvvvvvv-------UserId---------" + userId);
        System.out.println("vvvvvvvvvvvvv-------MeetingTypeID---------" + String.valueOf(meetingTypeSpinnerSelectedPos));
        System.out.println("vvvvvvvvvvvvv-------FollowupTypeID---------" + String.valueOf(arFollowTypesSpinnerSelectedPos));
        System.out.println("vvvvvvvvvvvvv-------OnDate---------" + ondate);
        System.out.println("vvvvvvvvvvvvv-------ProductIssues---------" + ProductIssues);
        System.out.println("vvvvvvvvvvvvv-------ProductDelay---------" + ProductDelay);
        System.out.println("vvvvvvvvvvvvv-------Status---------" + "Pending");
        System.out.println("vvvvvvvvvvvvv-------Date---------" + notes);
        System.out.println("vvvvvvvvvvvvv-------Time---------" + location);
        System.out.println("vvvvvvvvvvvvv-------CustomerID---------" + customerId);

        apiService.postTheMeetingTypeArFollowPaymentAssign(projectId, userId,
                String.valueOf(meetingTypeSpinnerSelectedPos), String.valueOf(arFollowTypesSpinnerSelectedPos),
                ondate, ProductIssues, ProductDelay, "Pending", notes, location, customerId, meetingLocation)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()) {
                            if (response.body() == null) {
                                onMeetingPostDataDownlodeLisner.OnMeetingPostFail(context.getString(R.string.justErrorCode) + " 43",
                                        context.getString(R.string.alert), false);
                            } else {
                                try {
                                    JSONObject jsonObject = new JSONObject(response.body().string());
                                    if (jsonObject.getInt("Success") == 1) {
                                        onMeetingPostDataDownlodeLisner.OnMeetingPostSuccess(jsonObject.getString("Msg"),
                                                jsonObject.getString("MeetingID"));
                                    } else {
                                        onMeetingPostDataDownlodeLisner.OnMeetingPostFail(jsonObject.getString("Msg"),
                                                context.getString(R.string.alert), false);
                                        Log.e("check here", jsonObject.getString("Msg"));
                                    }
                                } catch (JSONException | IOException e) {
                                    onMeetingPostDataDownlodeLisner.OnMeetingPostFail(context.getString(R.string.justErrorCode) + " 44",
                                            context.getString(R.string.internalError), false);
                                }
                            }
                        } else {
                            onMeetingPostDataDownlodeLisner.OnMeetingPostFail(context.getString(R.string.justErrorCode) + " 40",
                                    context.getString(R.string.alert), false);
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        onMeetingPostDataDownlodeLisner.OnMeetingPostFail(context.getString(R.string.noInternetMessage) + " 45",
                                context.getString(R.string.noInternetAlert),
                                true);
                    }
                });
    }

    // posting   Ar follow up type  C forms

    private void postTheArFollowupCoforms(String userId, String projectID, String MeetingTypeID, String FollowupTypeID,
                                          String OnDate, String CFCollected, String CFNotTraceable,
                                          String notes, final Context context,
                                          final AssignMeetingIntractor.OnMeetingPostDataDownlodeLisner onMeetingPostDataDownlodeLisner,
                                          String location, String customerId, String meetingLocation) {
        APIService apiService = ApiUtils.getAPIService(ConstantsUtils.postBaseUrl + ConstantsUtils.POST_ASSIGN_MEETINGS_TO_SERVER);//ConstantsUtils.postBaseUrl ConstantsUtils.postToAddTheMeetings

        System.out.println("vvvvvvvvvvvvv-------ProjectID---------" + projectID);
        System.out.println("vvvvvvvvvvvvv-------UserId---------" + userId);
        System.out.println("vvvvvvvvvvvvv-------MeetingTypeID---------" + MeetingTypeID);
        System.out.println("vvvvvvvvvvvvv-------FollowupTypeID---------" + FollowupTypeID);
        System.out.println("vvvvvvvvvvvvv-------OnDate---------" + OnDate);
        System.out.println("vvvvvvvvvvvvv-------CFCollected---------" + CFCollected);
        System.out.println("vvvvvvvvvvvvv-------CFNotTraceable---------" + CFNotTraceable);
        System.out.println("vvvvvvvvvvvvv-------Status---------" + "Pending");
        System.out.println("vvvvvvvvvvvvv-------Date---------" + notes);
        System.out.println("vvvvvvvvvvvvv-------MeetingTime---------" + location);
        System.out.println("vvvvvvvvvvvvv-------CustomerID---------" + customerId);

        apiService.postTheMeetingTypeArFollowCformAssign(projectID, userId,
                MeetingTypeID, FollowupTypeID, OnDate, CFCollected, CFNotTraceable,
                "Pending", notes, location, customerId, meetingLocation).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    if (response.body() == null) {
                        onMeetingPostDataDownlodeLisner.OnMeetingPostFail(context.getString(R.string.justErrorCode) + " 49",
                                context.getString(R.string.alert), false);
                    } else {
                        try {
                            JSONObject jsonObject = new JSONObject(response.body().string());
                            if (jsonObject.getInt("Success") == 1) {
                                onMeetingPostDataDownlodeLisner.OnMeetingPostSuccess(jsonObject.getString("Msg"),
                                        jsonObject.getString("MeetingID"));
                            } else {
                                onMeetingPostDataDownlodeLisner.OnMeetingPostFail(jsonObject.getString("Msg"),
                                        context.getString(R.string.alert), false);
                            }
                        } catch (JSONException | IOException e) {
                            onMeetingPostDataDownlodeLisner.OnMeetingPostFail(context.getString(R.string.justErrorCode) + " 48",
                                    context.getString(R.string.internalError), false);
                        }
                    }
                } else {
                    onMeetingPostDataDownlodeLisner.OnMeetingPostFail(context.getString(R.string.justErrorCode) + " 47",
                            context.getString(R.string.alert), false);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                onMeetingPostDataDownlodeLisner.OnMeetingPostFail(context.getString(R.string.noInternetMessage) + " 46",
                        context.getString(R.string.noInternetAlert),
                        true);
            }
        });
    }

    // posting   Ar follow up type  Reconciliation
    private void postTheArFollowupReconciliation(String userId, String ProjectID, String MeetingTypeID, String FollowupTypeID,
                                                 String ShortPayments, String MissingInvoices, String Statement,
                                                 String notes, final Context context,
                                                 final AssignMeetingIntractor.OnMeetingPostDataDownlodeLisner onMeetingPostDataDownlodeLisner,
                                                 String location, String customerId, String meetingLocation) {
        APIService apiService = ApiUtils.getAPIService(ConstantsUtils.postBaseUrl + ConstantsUtils.POST_ASSIGN_MEETINGS_TO_SERVER);//ConstantsUtils.postBaseUrl ConstantsUtils.postToAddTheMeetings

        System.out.println("vvvvvvvvvvvvv-------ProjectID---------" + ProjectID);
        System.out.println("vvvvvvvvvvvvv-------MeetingTypeID---------" + MeetingTypeID);
        System.out.println("vvvvvvvvvvvvv-------FollowupTypeID---------" + FollowupTypeID);
        System.out.println("vvvvvvvvvvvvv-------ShortPayments---------" + ShortPayments);
        System.out.println("vvvvvvvvvvvvv-------MissingInvoices---------" + MissingInvoices);
        System.out.println("vvvvvvvvvvvvv-------Statement---------" + Statement);
        System.out.println("vvvvvvvvvvvvv-------notes---------" + notes);
        System.out.println("vvvvvvvvvvvvv-------location---------" + location);
        System.out.println("vvvvvvvvvvvvv-------customerId---------" + customerId);

        apiService.postTheMeetingTypeArFollowReconciliationAssign(ProjectID, userId,
                MeetingTypeID, FollowupTypeID, ShortPayments, "Pending",
                MissingInvoices, Statement, notes, location, customerId, meetingLocation).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    if (response.body() == null) {
                        onMeetingPostDataDownlodeLisner.OnMeetingPostFail(context.getString(R.string.justErrorCode) + " 53",
                                context.getString(R.string.alert), false);
                    } else {
                        try {
                            JSONObject jsonObject = new JSONObject(response.body().string());
                            if (jsonObject.getInt("Success") == 1) {
                                onMeetingPostDataDownlodeLisner.OnMeetingPostSuccess(jsonObject.getString("Msg"),
                                        jsonObject.getString("MeetingID"));
                            } else {
                                onMeetingPostDataDownlodeLisner.OnMeetingPostFail(jsonObject.getString("Msg"),
                                        context.getString(R.string.alert), false);
                            }
                        } catch (JSONException | IOException e) {
                            onMeetingPostDataDownlodeLisner.OnMeetingPostFail(context.getString(R.string.justErrorCode) + " 52",
                                    context.getString(R.string.internalError), false);
                        }
                    }
                } else {
                    onMeetingPostDataDownlodeLisner.OnMeetingPostFail(context.getString(R.string.justErrorCode) + " 51",
                            context.getString(R.string.alert), false);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                onMeetingPostDataDownlodeLisner.OnMeetingPostFail(context.getString(R.string.noInternetMessage) + " 50",
                        context.getString(R.string.noInternetAlert),
                        true);
            }
        });
    }

    private String getProjectId(ArrayList<ProjectsDataModel> projectsDataModels,
                                String projectName) {
        String projectId = "";
        for (int i = 0; i < projectsDataModels.size(); i++) {
            if (projectName.equals(projectsDataModels.get(i).getProjectName())) {
                projectId = projectsDataModels.get(i).getProjectID();
                break;
            }
        }
        return projectId;
    }

    ////// posting   meeting type complaints
    private void postingTheMeetingTypeComplaints(String userId, String ProjectID, String MeetingTypeID,
                                                 String ComplaintTypeID, String notes, final Context context,
                                                 final AssignMeetingIntractor.OnMeetingPostDataDownlodeLisner onMeetingPostDataDownlodeLisner,
                                                 String location, String customerId, String meetingLocation) {
        APIService apiService = ApiUtils.getAPIService(ConstantsUtils.postBaseUrl + ConstantsUtils.POST_ASSIGN_MEETINGS_TO_SERVER);//ConstantsUtils.postBaseUrl ConstantsUtils.postToAddTheMeetings
        System.out.println("vvvvvvvvvvvvv-------ProjectID---------" + ProjectID);
        System.out.println("vvvvvvvvvvvvv-------MeetingTypeID---------" + MeetingTypeID);
        System.out.println("vvvvvvvvvvvvv-------ComplaintTypeID---------" + ComplaintTypeID);
        System.out.println("vvvvvvvvvvvvv-------notes---------" + notes);
        System.out.println("vvvvvvvvvvvvv-------location---------" + location);
        System.out.println("vvvvvvvvvvvvv-------customerId---------" + customerId);

        apiService.postTheMeetingTypeComplaintsAssign(ProjectID, userId, MeetingTypeID,
                ComplaintTypeID, "Pending", notes, location, customerId, meetingLocation).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    if (response.body() == null) {
                        onMeetingPostDataDownlodeLisner.OnMeetingPostFail(context.getString(R.string.justErrorCode) + " 57",
                                context.getString(R.string.alert), false);
                    } else {
                        try {
                            JSONObject jsonObject = new JSONObject(response.body().string());
                            if (jsonObject.getInt("Success") == 1) {
                                onMeetingPostDataDownlodeLisner.OnMeetingPostSuccess(jsonObject.getString("Msg"),
                                        jsonObject.getString("MeetingID"));
                            } else {
                                onMeetingPostDataDownlodeLisner.OnMeetingPostFail(jsonObject.getString("Msg"),
                                        context.getString(R.string.alert), false);
                            }
                        } catch (JSONException | IOException e) {
                            onMeetingPostDataDownlodeLisner.OnMeetingPostFail(context.getString(R.string.justErrorCode) + " 56",
                                    context.getString(R.string.internalError), false);
                        }
                    }
                } else {
                    onMeetingPostDataDownlodeLisner.OnMeetingPostFail(context.getString(R.string.justErrorCode) + " 55",
                            context.getString(R.string.alert), false);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                onMeetingPostDataDownlodeLisner.OnMeetingPostFail(context.getString(R.string.noInternetMessage) + " 54",
                        context.getString(R.string.noInternetAlert),
                        true);
            }
        });

    }

    private String getProductsJsonArray(ArrayList<ProductsDataModel> productsDataModels, Context context) {

        JSONArray jsonArrayProjucts = new JSONArray();
        for (int i = 0; i < productsDataModels.size(); i++) {
            if (productsDataModels.get(i).isChecked()) {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("ProductID", productsDataModels.get(i).getProductID());
                    jsonObject.put("Amount", productsDataModels.get(i).getPrice());
                    jsonArrayProjucts.put(jsonObject);
                } catch (JSONException e) {
                    CommonFunc.commonDialog(context, context.getString(R.string.alert),
                            context.getString(R.string.justErrorCode) + " 37", false);
                }
            }
        }
        if (jsonArrayProjucts.length() != 0)
            return jsonArrayProjucts.toString();
        else
            return "";
    }

}
