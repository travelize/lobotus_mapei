package com.lobotus.mapei.flowfragments.mvp.intrectorimp;

import android.content.Context;

import com.lobotus.mapei.R;
import com.lobotus.mapei.flowfragments.model.LeaveDataModel;
import com.lobotus.mapei.flowfragments.model.LeaveModel;
import com.lobotus.mapei.flowfragments.mvp.intrector.LeaveFragIntrector;
import com.lobotus.mapei.retrofitapi.APIService;
import com.lobotus.mapei.retrofitapi.ApiUtils;
import com.lobotus.mapei.utils.CommonFunc;
import com.lobotus.mapei.utils.ConstantsUtils;
import com.lobotus.mapei.utils.PreferenceUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by user1 on 05-10-2017.
 */

public class LeaveFragmentIntrImp implements LeaveFragIntrector {
    /////// downlode the leave fragment list. .
    @Override
    public void postToDOwnlodeTheLeaveListFromServer(Context context, LeaveListLoadLisner leaveListLoadLisner) {
        if (PreferenceUtils.checkUserisLogedin(context))
        {
            postToDownlodeTheLeaveList(context,leaveListLoadLisner);
        }else {
            leaveListLoadLisner.OnLeaveListLoadfail( context.getString(R.string.SessionLost),
                    context.getString(R.string.alert),true);
        }
    }



    private void postToDownlodeTheLeaveList(final Context context, final LeaveListLoadLisner leaveListLoadLisner)
    {
        APIService apiService= ApiUtils.getAPIService(ConstantsUtils.postBaseUrl+ConstantsUtils.postToGetThelistOfLeaveList);
        apiService.postToGetTheLeaveList(PreferenceUtils.getUserIdFromPreference(context))
                .enqueue(new Callback<LeaveModel>() {
                    @Override
                    public void onResponse(Call<LeaveModel> call, Response<LeaveModel> response) {
                        if (response.isSuccessful())
                        {

                            if (response.body()==null)
                            {
                                leaveListLoadLisner.OnLeaveListLoadfail(   context.getString(R.string.contact_support)+" 22",
                                        context.getString(R.string.internalError)
                                        ,false);
                            }else {
                                LeaveModel leaveModel=response.body();
                                if (leaveModel.getSuccess()==1)
                                {
                                    leaveListLoadLisner.OnLeaveListLoadSuccess((ArrayList<LeaveDataModel>) leaveModel.getData());
                                }else {
                                    leaveListLoadLisner.OnLeaveListLoadfail(leaveModel.getErrorMsg(),
                                            context.getString(R.string.alert),false);
                                }

                            }

                        }else {
                            leaveListLoadLisner.OnLeaveListLoadfail(context.getString(R.string.justErrorCode)+" 20",
                                    context.getString(R.string.alert),false);
                        }
                    }

                    @Override
                    public void onFailure(Call<LeaveModel> call, Throwable t) {
                        leaveListLoadLisner.OnLeaveListLoadfail( context.getString(R.string.noInternetMessage)+" 21",
                                context.getString(R.string.noInternetAlert),true);
                    }
                });
    }

    ////////////////////////////////////////////////////////////////// post to cancel the leave
    @Override
    public void postReqToCancelLeaveFromServer(String leaveId, final LeaveCancelLisner leaveCancelLisner, final Context context) {
        APIService apiService=ApiUtils.getAPIService(ConstantsUtils.postBaseUrl+ConstantsUtils.postingToCancelTheLeave);
        apiService.postToCancelTheLeave(leaveId)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful())
                        {
                            try {
                                JSONObject jsonObject=new JSONObject(response.body().string());
                                if (jsonObject.getInt("Success")==1)
                                    leaveCancelLisner.OnLeaveCancelSuccess(jsonObject.getString("Msg"));
                                else
                                    leaveCancelLisner.OnLeaveCancelFail(jsonObject.getString("Msg"),context.getString(R.string.alert),false);
                            } catch (JSONException | IOException e) {
                                leaveCancelLisner.OnLeaveCancelFail(context.getString(R.string.justErrorCode)+" 35",
                                        context.getString(R.string.alert),false);
                            }

                        }else {
                            leaveCancelLisner.OnLeaveCancelFail( context.getString(R.string.justErrorCode)+" 34",
                                    context.getString(R.string.noInternetAlert),false);
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        leaveCancelLisner.OnLeaveCancelFail( context.getString(R.string.noInternetMessage)+" 33",
                                context.getString(R.string.noInternetAlert),true);
                    }
                });
    }
}
