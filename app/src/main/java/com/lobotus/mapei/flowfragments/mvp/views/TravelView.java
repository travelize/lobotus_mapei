package com.lobotus.mapei.flowfragments.mvp.views;

import com.lobotus.mapei.flowfragments.model.CustomerDataModel;
import com.lobotus.mapei.flowfragments.model.ModeOfTravelModel;
import com.lobotus.mapei.flowfragments.model.PurposeOfTravelModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user1 on 04-10-2017.
 */

public interface TravelView {
    void showProgressDialog();
    void dismissProgressDialog();
    ///////////////////////////////////////////// customer model
    void afterCustomerModelDownlodeSuccess(ArrayList<CustomerDataModel> customerDataModels);
    void afterCustomerModelDownlodeFail(String message, String dialogTitle, boolean isInternetError);
    //////////////////////////////////////////// mode of travel model
    void afterMOTandPOTDownlodeSuccess(List<ModeOfTravelModel> modeOfTravelModels,
                                       List<PurposeOfTravelModel> purposeOfTravelModels);
    void afterMOTandPOTDownlodeFail(String message, String dialogTitle, boolean isInternetError);
    /////////////// validate before posting travel
    void afterValidationSuccess();
    ///////////////// after travel post response
    void afterTravelPostSuccess(String message);
    void afterTravelPostFail(String message, String dialogTitle, boolean isInternetError);



}
