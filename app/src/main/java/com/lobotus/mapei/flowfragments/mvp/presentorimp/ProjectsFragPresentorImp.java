package com.lobotus.mapei.flowfragments.mvp.presentorimp;

import android.content.Context;

import com.lobotus.mapei.flowfragments.model.ProjectsDataModel;
import com.lobotus.mapei.flowfragments.mvp.intrector.ProjectsFragIntrector;
import com.lobotus.mapei.flowfragments.mvp.intrectorimp.ProjectsFragIntrectorImp;
import com.lobotus.mapei.flowfragments.mvp.presentor.ProjectsFragPresentor;
import com.lobotus.mapei.flowfragments.mvp.views.ProjectsFragView;
import com.lobotus.mapei.utils.NetworkUtils;

import java.util.ArrayList;

/**
 * Created by user1 on 06-10-2017.
 */

public class ProjectsFragPresentorImp implements ProjectsFragPresentor,
        ProjectsFragIntrector.ProjectsListDownlodeLisner {

    private ProjectsFragView projectsFragView=null;
    private ProjectsFragIntrector projectsFragIntrector=null;

    public ProjectsFragPresentorImp(ProjectsFragView projectsFragView) {
        this.projectsFragView = projectsFragView;
        this.projectsFragIntrector = new ProjectsFragIntrectorImp();
    }

    ////////////////////////////////////////////////// downlode the list of projects
    @Override
    public void reqToDownlodeTheListOfProjectsFromPresentor(Context context) {
        if (NetworkUtils.checkInternetAndOpenDialog(context))
        {
            projectsFragView.showProgressDialog();
            projectsFragIntrector.reqToFetchProjectsListFromServer(context,this);
        }
    }

    @Override
    public void OnProjectsListDownlodeSuccess(ArrayList<ProjectsDataModel> projectsDataModels) {
        projectsFragView.dismissProgressDialog();
        projectsFragView.afterProjectsListDownlodeSuccess(projectsDataModels);
    }

    @Override
    public void OnProjectsListDownlodeFail(String message, String dialogTitle, boolean isInternetError) {
        projectsFragView.dismissProgressDialog();
        projectsFragView.afterProjectsListDownlodeFail(message,dialogTitle,isInternetError);
    }
}
