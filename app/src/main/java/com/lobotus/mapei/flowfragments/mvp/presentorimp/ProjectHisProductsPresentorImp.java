package com.lobotus.mapei.flowfragments.mvp.presentorimp;

import android.content.Context;

import com.lobotus.mapei.flowfragments.model.ProductsDataModel;
import com.lobotus.mapei.flowfragments.mvp.intrector.AddProjectsIntractor;
import com.lobotus.mapei.flowfragments.mvp.intrector.MeetingInterator;
import com.lobotus.mapei.flowfragments.mvp.intrector.ProjectHisProductIntractor;
import com.lobotus.mapei.flowfragments.mvp.intrectorimp.AddProjectsIntractorImp;
import com.lobotus.mapei.flowfragments.mvp.intrectorimp.MeetingInteratorImp;
import com.lobotus.mapei.flowfragments.mvp.intrectorimp.ProjectHisProductIntractorImp;
import com.lobotus.mapei.flowfragments.mvp.presentor.ProjectHisProductPresentor;
import com.lobotus.mapei.flowfragments.mvp.views.ProjectHistProductsView;
import com.lobotus.mapei.utils.NetworkUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user1 on 16-10-2017.
 */

public class ProjectHisProductsPresentorImp implements ProjectHisProductPresentor,
        ProjectHisProductIntractor.OnProjectHisProductsDownlodeLisner{

    private ProjectHistProductsView projectHistProductsView=null;
    private ProjectHisProductIntractor projectHisProductIntractor=null;


    public ProjectHisProductsPresentorImp(ProjectHistProductsView projectHistProductsView) {
        this.projectHistProductsView = projectHistProductsView;
        this.projectHisProductIntractor=new ProjectHisProductIntractorImp();
    }
///////////////////////////////// post to downlode the products in history
    @Override
    public void reqToGetTheProductsList(Context context, String MeetingID) {
        if (NetworkUtils.checkInternetAndOpenDialog(context))
        {
            projectHistProductsView.showProgressDialog();
            projectHisProductIntractor.reqToGetTheHisProducts(context,MeetingID,this);
        }

    }


    @Override
    public void OnProjectHisProductsDownlodeSuccess(ArrayList<ProductsDataModel> productsDataModels) {
        projectHistProductsView.dismissProgressDialog();
        projectHistProductsView.afterProductsModelDownlodeSuccess(productsDataModels);
    }

    @Override
    public void OnProjectHisProductsDownlodeFail(String message, String dialogTitle, boolean isInternetError) {
        projectHistProductsView.dismissProgressDialog();
        projectHistProductsView.afterProductsModelDoenlodeFail(message,dialogTitle,isInternetError);
    }
}
