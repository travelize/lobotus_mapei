package com.lobotus.mapei.flowfragments.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.Toast;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.lobotus.mapei.R;
import com.lobotus.mapei.baseactivity.BaseActivity;
import com.lobotus.mapei.customwidgits.LabelledSpinner;
import com.lobotus.mapei.flowfragments.adapters.AllMeetingListAdapter;
import com.lobotus.mapei.flowfragments.mvp.presentor.AllMeetingsPresenter;
import com.lobotus.mapei.flowfragments.mvp.presentorimp.AllMeetingsPresenterImp;
import com.lobotus.mapei.flowfragments.mvp.views.AllMeetingsView;
import com.lobotus.mapei.fromlite.activities.Directions;
import com.lobotus.mapei.fromlite.activities.MeetingDetails;
import com.lobotus.mapei.fromlite.activities.MeetingStatus;
import com.lobotus.mapei.fromlite.adapter.MeetingAdapter;
import com.lobotus.mapei.fromlite.model.MeetingModel;
import com.lobotus.mapei.fromlite.mvp.presentor.MeetingPresentor;
import com.lobotus.mapei.fromlite.mvp.presentorimp.MeetingPresentorImp;
import com.lobotus.mapei.utils.CommonFunc;
import com.lobotus.mapei.utils.NetworkUtils;
import com.lobotus.mapei.utils.PermissionUtils;
import com.lobotus.mapei.utils.PreferenceUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class AllMeetingsFrag extends Fragment implements AllMeetingsView, SwipeRefreshLayout.OnRefreshListener {


    private AllMeetingsPresenter meetingPresentor = null;
    // temp data
    private ArrayList<MeetingModel> meetingModelsTemp = new ArrayList<>();
    // meeting adapter
    private AllMeetingListAdapter meetingAdapter = null;

    private View viewFragment = null;

    private Dialog dialog = null;

    private String filterSpinnerPosition = "";
    private String fromDateStr;
    private String toDateStr;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setHasOptionsMenu(true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        viewFragment = inflater.inflate(R.layout.fragment_all_meetings, container, false);

        ((SwipeRefreshLayout) viewFragment.findViewById(R.id.fragment_all_meeting_swipe_referesh_id)).setOnRefreshListener(this);
        meetingPresentor = new AllMeetingsPresenterImp(this);

        return viewFragment;
    }

   /* @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.filter, menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.action_filter:

                openDialogFilter();
                return true;

        }

        return false;
    }*/

    private void openDialogFilter() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(R.layout.allmeeting_filter_dialog);
        dialog.setTitle("Customize Filter Meetings");

        final AppCompatEditText fromDate = dialog.findViewById(R.id.all_meetings_filter_dialog_from_date);
        final AppCompatEditText toDate = dialog.findViewById(R.id.all_meetings_filter_dialog_to_date);
        final LabelledSpinner statusSpinner = dialog.findViewById(R.id.all_meetings_filter_dialog_meeting_status);

        ((LabelledSpinner) dialog.findViewById(R.id.all_meetings_filter_dialog_meeting_status))
                .setItemsArray(R.array.status_details);
        dialog.findViewById(R.id.all_meetings_filter_dialog_from_date).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDatePicker(v.getId(), dialog);
            }
        });
        dialog.findViewById(R.id.all_meetings_filter_dialog_to_date).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myDatePicker(v.getId(), dialog);
            }
        });

        dialog.findViewById(R.id.all_meetings_filter_dialog_done_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int checkValue = 0;
                if (fromDate.getText().toString().isEmpty() && toDate.getText().toString().isEmpty() && statusSpinner.getSpinner().getSelectedItemPosition() == 0) {
                    CommonFunc.commonDialog(getActivity(), "Alert", "Enter any one for filtering", false);
                } else if (((LabelledSpinner) dialog.findViewById(R.id.all_meetings_filter_dialog_meeting_status))
                        .getSpinner().getSelectedItemPosition() != 0) {
                    filterSpinnerPosition = ((LabelledSpinner) dialog.findViewById(R.id.all_meetings_filter_dialog_meeting_status))
                            .getSpinner().getSelectedItem().toString();
                    checkValue = 1;
                    System.out.println("ffffffffffff--------" + filterSpinnerPosition);
                }

                if (!fromDate.getText().toString().isEmpty() || !toDate.getText().toString().isEmpty()) {

                    if (fromDate.getText().toString().isEmpty() || toDate.getText().toString().isEmpty()) {
                        CommonFunc.commonDialog(getActivity(), "Alert", "Select both from & to date", false);
                    } else {
                        fromDateStr = fromDate.getText().toString();
                        toDateStr = toDate.getText().toString();
                        checkValue = 1;
                    }
                }

                if (checkValue == 1) {
                    sortingExecute(filterSpinnerPosition, fromDateStr, toDateStr);
                }
            }
        });

        dialog.show();
    }

    private void sortingExecute(String filterSpinnerPosition, String fromDateStr, String toDateStr) {

        int checkValue = 0;
      /*  ArrayList<MeetingModel> filterValues = null;
        if (!filterSpinnerPosition.isEmpty()) {
            for (int i = 0; i < meetingModelsTemp.size(); i++) {
                System.out.println("vvvvvvvvvvvvvvvvvv----------Status--------" + meetingModelsTemp.get(i).getStatus());
                if (meetingModelsTemp.get(i).getStatus().equals(filterSpinnerPosition))
                    filterValues = meetingModelsTemp;
            }
            checkValue = 1;
        }*/
        Toast.makeText(getActivity(), meetingModelsTemp.get(0).getOnDate() + "", Toast.LENGTH_SHORT).show();

      /*  if (!fromDateStr.isEmpty() && !toDateStr.isEmpty()) {

            //return a.compareTo(d) * d.compareTo(b) > 0;
            Date fromDate = stringToDate(fromDateStr);
            Date toDate = stringToDate(toDateStr);
            for (int i = 0; i < meetingModelsTemp.size(); i++) {
                Date checkDate = stringToDate(meetingModelsTemp.get(i).getOnDate());
                if (fromDate.compareTo(checkDate) * checkDate.compareTo(toDate) >= 0) {

                }

            }

        }*/

        /*if (checkValue == 1) {
            afterMeetingsListDownlodeSuccess(filterValues);
        }*/
    }

    Date stringToDate(String sDate1) {

        try {
            return new SimpleDateFormat("dd/MM/yyyy").parse(sDate1);
        } catch (ParseException e) {
            return null;
        }

    }

    //get Date
    private void myDatePicker(final int viewId, final Dialog dialogBox) {

        SimpleDateFormat mDF = new SimpleDateFormat("yyyy-mm-dd");
        Date today = new Date();
        mDF.format(today);
        final Calendar c = Calendar.getInstance();
        c.setTime(today);
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        String formattedDay = (String.valueOf(dayOfMonth));
                        int m = monthOfYear + 1;

                        String formattedMonth = (String.valueOf(m));

                        if (dayOfMonth < 10) {
                            formattedDay = "0" + dayOfMonth;
                        }

                        if (m < 10) {
                            formattedMonth = "0" + m;

                        }
                        ((AppCompatEditText) dialogBox.findViewById(viewId)).setText(formattedDay + "/" + (formattedMonth) + "/" + year);


                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                dialog.dismiss();

            }
        });
        datePickerDialog.show();
        datePickerDialog.getDatePicker().setMaxDate(c.getTimeInMillis());
    }


    @Override
    public void onResume() {
        super.onResume();
        meetingPresentor.reqToGetTheListOfMeetingsFromPresentor(getActivity(), "history", "");
    }

    @Override
    public void onRefresh() {
        meetingPresentor.reqToGetTheListOfMeetingsFromPresentor(getActivity(), "history", "");
    }


    @Override
    public void showProgressDialog() {
        dialog = new Dialog(getActivity());
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.loaging_layout);
        dialog.show();
    }

    @Override
    public void dismissProgressDialog() {
        if (((SwipeRefreshLayout) viewFragment.findViewById(R.id.fragment_all_meeting_swipe_referesh_id))
                .isRefreshing()) {
            ((SwipeRefreshLayout) viewFragment.findViewById(R.id.fragment_all_meeting_swipe_referesh_id)).setRefreshing(false);

        }
        if (dialog != null) {
            if (dialog.isShowing()) {
                dialog.dismiss();
            }
            dialog = null;
        }
    }

    @Override
    public void afterMeetingsListDownlodeSuccess(ArrayList<MeetingModel> meetingModels) {

        meetingModelsTemp.clear();
        meetingModelsTemp.add(null);
        this.meetingModelsTemp.addAll(meetingModels);
        meetingAdapter = new AllMeetingListAdapter(getActivity(), meetingModelsTemp, this);
        ((RecyclerView) viewFragment.findViewById(R.id.fragment_all_meeting_recycleview_id)).setLayoutManager(new LinearLayoutManager(getActivity(),
                LinearLayoutManager.VERTICAL, false));
        ((RecyclerView) viewFragment.findViewById(R.id.fragment_all_meeting_recycleview_id)).setAdapter(meetingAdapter);
      /*  if (meetingModels.size() <= 1) {
            try {
                ((HomeFragment) ((NavigationActivity) getActivity()).adapter.getItem(0))
                        .textViewNoOfMeetings.setText(meetingModels.size() + " meeting ");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else {
            try {
                ((HomeFragment) ((NavigationActivity) getActivity()).adapter.getItem(0))
                        .textViewNoOfMeetings.setText(meetingModels.size() + " meetings ");
            }catch (Exception e)
            {
                e.printStackTrace();
            }

        }
        ////////////////////////////////////////////////*/
        if (!meetingModels.isEmpty()) {
            viewFragment.findViewById(R.id.fragment_all_meeting_no_meeting_found).setVisibility(View.GONE);
        } else {
            viewFragment.findViewById(R.id.fragment_all_meeting_no_meeting_found).setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void afterMeetingsListDownlodeFail(String message, String dialogTitle, boolean isInternetError) {
        System.out.println("nnnnnnnnnnnnnnnnnnnnn----afterMeetingsListDownlodeFail--" + message);

        try {
            meetingModelsTemp.clear();
            if (meetingAdapter != null) {
                meetingAdapter.notifyDataSetChanged();
            }
         /*   if (((NavigationActivity)getActivity()).navigation.getSelectedItemId()==R.id.navigation_meeting)
                CommonFunc.commonDialog(getActivity(),dialogTitle,message,isInternetError);
            ((HomeFragment)((NavigationActivity)getActivity()).adapter.getItem(0))
                    .textViewNoOfMeetings.setText("0 meetings ");*/

            viewFragment.findViewById(R.id.fragment_all_meeting_no_meeting_found).setVisibility(View.VISIBLE);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    ////////////////////////////////////////////// list click
    public void onMeetingListClick(MeetingModel meetingModel) {
//        if (meetingModel.getOnDate().equals(CommonFunc.getTodayDate())) {
        if (PermissionUtils.checkLoactionPermission(getActivity())) {
            checkGPSisOnAndOpenMeetingDetailsActivity(meetingModel);
        } else {
            PermissionUtils.openPermissionDialog(getActivity(), "Please grant location permission", "Permission req");
        }


        /*} else {
            CommonFunc.commonDialog(getActivity(), getString(R.string.alert),
                    "You can start only same day meeting...", false);
        }*/
    }
    //// check gps is on and open the meeting details act

    private void checkGPSisOnAndOpenMeetingDetailsActivity(final MeetingModel meetingModel) {
        LocationRequest locationRequest = new LocationRequest();
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        Task<LocationSettingsResponse> locationSettingsResponseTask =
                LocationServices.getSettingsClient(getActivity()).checkLocationSettings(builder.build());
        locationSettingsResponseTask.addOnCompleteListener(new OnCompleteListener<LocationSettingsResponse>() {
            @Override
            public void onComplete(@NonNull Task<LocationSettingsResponse> task) {
                try {

                    LocationSettingsResponse response = task.getResult(ApiException.class);
                    // All location settings are satisfied. The client can initialize location
                    // requests here.
                    if (PermissionUtils.checkLoactionPermission(getActivity()) && NetworkUtils.isConnected(getActivity())) {
                        if (PreferenceUtils.getIsUserCheckedIn(getActivity())) {
                            if (PreferenceUtils.isMeetingIdPresentInTheStartRoutePreference(getActivity())) {
                                if (PreferenceUtils.getMeetingIdFromStartRoutePreference(getActivity()) != null) {
                                    if (PreferenceUtils.getMeetingIdFromStartRoutePreference(getActivity()).equals(meetingModel.getMeetingID())) {
                                        if (PreferenceUtils.isStartedRouteEnded(getActivity())) {
                                            // open meeting status directly. . . . .
                                            Intent intent = new Intent(getActivity(), MeetingStatus.class);
                                            intent.putExtra("MEETING_MODEL", meetingModel);
                                            startActivity(intent);
                                        } else {
                                            Intent intent = new Intent(getActivity(), Directions.class);
                                            intent.putExtra("MEETING_MODEL", meetingModel);
                                            intent.putExtra("MOT", "NA");
                                            startActivity(intent);
                                        }

                                    } else {
                                        CommonFunc.commonDialog(getActivity(), getString(R.string.alert), "Please finish the started meeting first", false);
                                    }
                                } else {
                                    Intent intent = new Intent(getActivity(), MeetingDetails.class);
                                    intent.putExtra("MEETING_MODEL", meetingModel);
                                    getActivity().startActivity(intent);
                                }
                            } else {
                                Intent intent = new Intent(getActivity(), MeetingDetails.class);
                                intent.putExtra("MEETING_MODEL", meetingModel);
                                getActivity().startActivity(intent);
                            }
                        } else {
                            CommonFunc.commonDialog(getActivity(), getString(R.string.alert), "Please check in first.", false);
                        }

                    } else {

                        if (!PermissionUtils.checkLoactionPermission(getActivity())) {
                            PermissionUtils.openPermissionDialog(getActivity(), "Please grant location permission.", "Permission req");
                            /*Utility.alertMessage(Login.this,"Location permission not granted!","Please grant LOCATION permission and login again");*/

                        } else {
                            NetworkUtils.checkInternetAndOpenDialog(getActivity());
                            /*Utility.alertMessage(Login.this,"Mobile data and wi-fi not enabled!","Please enable your Mobile data or Wi-Fi and login again");*/
                        }
                    }

                } catch (ApiException exception) {


                    switch (exception.getStatusCode()) {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // Location settings are not satisfied. But could be fixed by showing the
                            // user a dialog.
                            try {
                                NetworkUtils.openGpsSettings(getActivity());
                                /*Utility.alertMessage(Login.this,"GPS not enabled!","Please enable your device GPS  and login again");*/

                            } catch (ClassCastException e) {

                                NetworkUtils.openGpsSettings(getActivity());
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:

                            NetworkUtils.openGpsSettings(getActivity());
                            break;
                    }
                }
            }
        });


    }

    @Override
    public void onStop() {
        super.onStop();
        if (meetingPresentor != null) {
            meetingPresentor.onStopMvpPresentor();
        }
        dismissProgressDialog();
    }

}
