package com.lobotus.mapei.flowfragments.mvp.intrector;

import android.content.Context;

import com.lobotus.mapei.flowfragments.model.LeaveTypeDataModel;

import java.util.ArrayList;

/**
 * Created by user1 on 04-10-2017.
 */

public interface LeaveApplyInteractor {
    //// post the leave types
    interface OnLeaveTypePostLisner
    {
        void OnLeaveTypePostSuccess(ArrayList<LeaveTypeDataModel> leaveTypeDataModels);
        void OnLeaveTypePostFail(String message,String dialogTitle,boolean isInternetError);
    }
    void reqToPostDownlodeLeaveTypesFromIntractor(Context context,OnLeaveTypePostLisner onLeaveTypePostLisner);
    //// post the leave data
    interface OnLeaveApplyLisner
    {
        void OnLeaveApplySuccess(String message,String dialogTitle,boolean isInternetError);
        void OnLeaveApplyFail(String message,String dialogTitle,boolean isInternetError);
    }
    void reqToPostLeaveApplyFromInteractor(Context context,
                                           String leaveType,String fromDate,String toDate,
                                           String remark,OnLeaveApplyLisner onLeaveApplyLisner);
}
