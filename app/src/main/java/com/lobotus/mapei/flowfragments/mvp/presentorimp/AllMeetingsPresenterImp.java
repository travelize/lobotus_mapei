package com.lobotus.mapei.flowfragments.mvp.presentorimp;

import android.content.Context;

import com.lobotus.mapei.flowfragments.mvp.presentor.AllMeetingsPresenter;
import com.lobotus.mapei.flowfragments.mvp.views.AllMeetingsView;
import com.lobotus.mapei.fromlite.model.MeetingModel;
import com.lobotus.mapei.fromlite.mvp.intractor.MeetingIntractor;
import com.lobotus.mapei.fromlite.mvp.intractorimp.MeetingIntractorImp;
import com.lobotus.mapei.utils.NetworkUtils;

import java.util.ArrayList;

public class AllMeetingsPresenterImp implements AllMeetingsPresenter, MeetingIntractor.MeetingListDownlodeLisner {

    private AllMeetingsView allMeetingsView = null;
    private MeetingIntractor meetingIntractor = null;

    public AllMeetingsPresenterImp(AllMeetingsView allMeetingsView) {
        this.allMeetingsView = allMeetingsView;
        this.meetingIntractor = new MeetingIntractorImp();
    }

    ////////////////////////////////////// get the lis of the meeting. .
    @Override
    public void reqToGetTheListOfMeetingsFromPresentor(Context context, String meetingValue, String date) {

        if (NetworkUtils.checkInternetAndOpenDialog(context)) {
            allMeetingsView.showProgressDialog();
            meetingIntractor.reqToServerToGetTheListOfMeetingsHistory(context, date, this);
        }

    }

    @Override
    public void OnMeetingListDownlodeSuccess(ArrayList<MeetingModel> meetingModels) {
        allMeetingsView.dismissProgressDialog();
        allMeetingsView.afterMeetingsListDownlodeSuccess(meetingModels);
    }

    @Override
    public void OnMeetingListDownlodeFail(String message, String dialogTitle, boolean isInternetError) {
        allMeetingsView.dismissProgressDialog();
        allMeetingsView.afterMeetingsListDownlodeFail(message, dialogTitle, isInternetError);
    }

    @Override
    public void onStopMvpPresentor() {
        meetingIntractor.onStopMvpIntractor();
    }
}
