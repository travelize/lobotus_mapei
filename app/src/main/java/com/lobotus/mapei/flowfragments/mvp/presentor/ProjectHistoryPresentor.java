package com.lobotus.mapei.flowfragments.mvp.presentor;

import android.content.Context;

/**
 * Created by user1 on 16-10-2017.
 */

public interface ProjectHistoryPresentor {
    /////////////// req to downlode the project history data
    void reqToDownlodeTheProjectHistoryData(Context context,String projectID);
}
