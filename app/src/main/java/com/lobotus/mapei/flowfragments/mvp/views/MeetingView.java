package com.lobotus.mapei.flowfragments.mvp.views;

import com.lobotus.mapei.flowfragments.model.ComplaintDataTypeModel;
import com.lobotus.mapei.flowfragments.model.CustomerDataModel;
import com.lobotus.mapei.flowfragments.model.ProductsDataModel;
import com.lobotus.mapei.flowfragments.model.ProjectsDataModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user1 on 04-10-2017.
 */

public interface MeetingView {

    void showProgressDialog();
    void dismissProgressDialog();
    ///////////////////////////////////////////// after validating meeting
    void afterValidationOfMeetingSuccessfull();
    ///////////////////////////////////////////// customer model
    void afterCustomerModelDownlodeSuccess(ArrayList<CustomerDataModel> customerDataModels);
    void afterCustomerModelDownlodeFail(String message, String dialogTitle, boolean isInternetError);
    ////////////////////////////////////////////////////////////// post to get the list of projects
    void afterProjectsListDownlodeSuccess(ArrayList<ProjectsDataModel> projectsDataModels);
    void afterProjectsListDownlodeFail(String message,String dialogTitle,boolean isInternetError);
    /////////////////////////////////////////////////////////////// after projectName chossenFrom auto
    void afterCustomerNameChoosenFromAutoSuccess(String customerName,String customerType);
    void afterCustomerNameChoosenFromAutoFail(String message,String dialogTitle,boolean isInternetError);
    ///////////////////////////////////////////// after products downlode
    void afterProductsModelDownlodeSuccess(List<ProductsDataModel> productsDataModels);
    void afterProductsModelDoenlodeFail(String message, String dialogTitle, boolean isInternetError);
    ////////////////////////////////////////// after complaints downlode sucess
    void afterComplaintsDownlodeSuccess(ArrayList<ComplaintDataTypeModel> complaintDataTypeModels);
    void afterComplaintsDownloldeFail(String message, String dialogTitle, boolean isInternetError);
    ////////////////////////////////////////////// after meeting posting success
    void afterMeetingPostSuccess(String message,String meetingId);
    void afterMeetingPostFail(String message,String dialogTitle,boolean isInternetError);
    /////////////////////////////////////////////////// after
}
