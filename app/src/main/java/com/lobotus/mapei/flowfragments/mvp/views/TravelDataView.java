package com.lobotus.mapei.flowfragments.mvp.views;

import com.lobotus.mapei.flowfragments.model.UserTravelModel;

import java.util.ArrayList;

/**
 * Created by User on 21-03-2018.
 */

public interface TravelDataView {
    void showProgressDialog();
    void dismissProgressDialog();
    ///////////////////////////////////////
    void afterTravelDataDownlodeSuccess(ArrayList<UserTravelModel> userTravelModels);
    void afterTravelDataDOwnlodeFail(String message, String dialogTitle, boolean isInternetError);
}
