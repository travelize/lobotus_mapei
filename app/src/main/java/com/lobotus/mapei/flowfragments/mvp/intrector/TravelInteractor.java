package com.lobotus.mapei.flowfragments.mvp.intrector;

import android.content.Context;

import com.lobotus.mapei.flowfragments.model.CustomerDataModel;
import com.lobotus.mapei.flowfragments.model.ModeOfTravelModel;
import com.lobotus.mapei.flowfragments.model.PurposeOfTravelModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user1 on 04-10-2017.
 */

public interface TravelInteractor {
    //////////////////////// customer data
    interface OnTravelCustomerDataDownlodeLisner
    {
        void OnTravelCustomerDataDownlodeSuccess(ArrayList<CustomerDataModel> customerDataModels);
        void OnTravelCustomerDataDownlodeFail(String message,String dialogTitle,boolean isInternetError);
    }
    void reqToPostCustomerDataReqToServer(Context context, OnTravelCustomerDataDownlodeLisner onTravelCustomerDataDownlodeLisner);

    ///////////////////////// MOT and POT
    interface OnMOTAndPOTDataDownlodeLisner
    {
        void OnMOTandPOTdataDOwnlodeSuccess(List<ModeOfTravelModel> modeOfTravelModels,
                                            List<PurposeOfTravelModel> purposeOfTravelModels);
        void OnMOTandPOTdownlodeFail(String message,String dialogTitle,boolean isInternetError);
    }
    void reqToPostMOTandPOTdataToServer(Context context,OnMOTAndPOTDataDownlodeLisner onMOTAndPOTDataDownlodeLisner);

    ///////////////////////////// post the travel details to server
    interface OnTravelDetailsPostLisner
    {
        void OnTravelDetailsPostSuccess(String message);
        void OnTravelDetailsPostFail(String message, String dialogTitle, boolean isInternetError);

    }
    void reqToPostTheTravelDetailsToServer(Context context,String remark,
                                           String custId,String modeOfTravel,
                                           String purpose,String attachmentB64,OnTravelDetailsPostLisner onTravelDetailsPostLisner,
                                           String startLoc,String endLoc,String imagecaption,
                                           String ownvehicalspinnerselectedpos,
                                           String fuletypespinnerselectedpos,String imageAmount);
}
