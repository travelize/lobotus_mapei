package com.lobotus.mapei.flowfragments.fragments;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.lobotus.mapei.R;
import com.lobotus.mapei.flowfragments.adapters.DemoMeetingsAdapter;
import com.lobotus.mapei.flowfragments.model.DemoMeetingsModel;
import com.lobotus.mapei.flowfragments.mvp.presentor.ViewDemoMeetingsPresenter;
import com.lobotus.mapei.flowfragments.mvp.presentorimp.ViewDemoMeetingsPresenterImp;
import com.lobotus.mapei.flowfragments.mvp.views.ViewDemoMeetingsView;
import com.lobotus.mapei.fromlite.mvp.presentorimp.MeetingPresentorImp;

import java.util.ArrayList;

public class ViewDemoMeetings extends Fragment implements ViewDemoMeetingsView, SwipeRefreshLayout.OnRefreshListener {

    private Dialog dialog = null;
    View viewFragment = null;

    // temp data
    private ArrayList<DemoMeetingsModel> demoMeetingModelsTemp = new ArrayList<>();
    // demo meeting adapter
    private DemoMeetingsAdapter demoMeetingsAdapter = null;

    private ViewDemoMeetingsPresenter demoMeetingsPresenter = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        viewFragment = inflater.inflate(R.layout.fragment_view_demo_meetings, container, false);
        ((SwipeRefreshLayout) viewFragment.findViewById(R.id.fragment_view_demo_meeting_swipe_referesh_id)).setOnRefreshListener(this);
        demoMeetingsPresenter = new ViewDemoMeetingsPresenterImp(this);

        return viewFragment;
    }

    ////////////////////////////////////////////////////////////////////////////////////////// mvp stuff
    @Override
    public void showProgressDialog() {
        dialog = new Dialog(getActivity());
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.loaging_layout);
        dialog.show();
    }

    @Override
    public void dismissProgressDialog() {
        if (getActivity() != null)
            if (dialog != null) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                dialog = null;
            }
    }

    @Override
    public void afterDemoListDownlodeSuccess(ArrayList<DemoMeetingsModel> demoMeetingsModelArrayList) {

        demoMeetingModelsTemp.clear();
        demoMeetingModelsTemp.add(null);
        for (int i = 0; i < demoMeetingsModelArrayList.size(); i++) {
            demoMeetingModelsTemp.add(demoMeetingsModelArrayList.get(i));
        }
        demoMeetingsAdapter = new DemoMeetingsAdapter(getActivity(), demoMeetingModelsTemp, this);
        ((RecyclerView) viewFragment.findViewById(R.id.fragment_view_demo_meeting_recycleview_id)).setLayoutManager(new LinearLayoutManager(getActivity(),
                LinearLayoutManager.VERTICAL, false));
        ((RecyclerView) viewFragment.findViewById(R.id.fragment_view_demo_meeting_recycleview_id)).setAdapter(demoMeetingsAdapter);
      /*  if (demoMeetingsModelArrayList.size() <= 1) {
            try {
                ((HomeFragment) ((NavigationActivity) getActivity()).adapter.getItem(0))
                        .textViewNoOfMeetings.setText(demoMeetingsModelArrayList.size() + " meeting ");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else {
            try {
                ((HomeFragment) ((NavigationActivity) getActivity()).adapter.getItem(0))
                        .textViewNoOfMeetings.setText(demoMeetingsModelArrayList.size() + " meetings ");
            }catch (Exception e)
            {
                e.printStackTrace();
            }

        }
        ////////////////////////////////////////////////*/
        if (!demoMeetingsModelArrayList.isEmpty()) {
            viewFragment.findViewById(R.id.fragment_view_demomeeting_no_meeting_found).setVisibility(View.GONE);
        } else {
            viewFragment.findViewById(R.id.fragment_view_demomeeting_no_meeting_found).setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void afterDemoMeetingsListDownlodeFail(String message, String dialogTitle, boolean isInternetError) {

        try {
            demoMeetingModelsTemp.clear();
            if (demoMeetingsAdapter != null) {
                demoMeetingsAdapter.notifyDataSetChanged();
            }
         /*   if (((NavigationActivity)getActivity()).navigation.getSelectedItemId()==R.id.navigation_meeting)
                CommonFunc.commonDialog(getActivity(),dialogTitle,message,isInternetError);
            ((HomeFragment)((NavigationActivity)getActivity()).adapter.getItem(0))
                    .textViewNoOfMeetings.setText("0 meetings ");*/

            viewFragment.findViewById(R.id.fragment_view_demomeeting_no_meeting_found).setVisibility(View.VISIBLE);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    public void onRefresh() {
        demoMeetingsPresenter.reqToGetTheListOfDemoMeetingsFromPresentor(getActivity());
        ((SwipeRefreshLayout) viewFragment.findViewById(R.id.fragment_view_demo_meeting_swipe_referesh_id)).setRefreshing(false);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (demoMeetingsPresenter != null) {
            demoMeetingsPresenter.onStopMvpPresentor();
        }
        dismissProgressDialog();
    }

    @Override
    public void onResume() {
        super.onResume();
        demoMeetingsPresenter.reqToGetTheListOfDemoMeetingsFromPresentor(getActivity());
    }

}
