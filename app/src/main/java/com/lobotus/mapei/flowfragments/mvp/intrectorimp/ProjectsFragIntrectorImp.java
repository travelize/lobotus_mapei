package com.lobotus.mapei.flowfragments.mvp.intrectorimp;

import android.content.Context;

import com.google.gson.Gson;
import com.lobotus.mapei.R;
import com.lobotus.mapei.flowfragments.model.ProjectsDataModel;
import com.lobotus.mapei.flowfragments.model.ProjectsModel;
import com.lobotus.mapei.flowfragments.mvp.intrector.ProjectsFragIntrector;
import com.lobotus.mapei.retrofitapi.APIService;
import com.lobotus.mapei.retrofitapi.ApiUtils;
import com.lobotus.mapei.utils.ConstantsUtils;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by user1 on 06-10-2017.
 */

public class ProjectsFragIntrectorImp implements ProjectsFragIntrector {
    @Override
    public void reqToFetchProjectsListFromServer(final Context context, final ProjectsListDownlodeLisner projectsListDownlodeLisner) {
        APIService apiService = ApiUtils.getAPIService(ConstantsUtils.postBaseUrl + ConstantsUtils.postToGetTheListOfProjects);
        apiService.postToGetTheListOfProjects("").enqueue(new Callback<ProjectsModel>() {
            @Override
            public void onResponse(Call<ProjectsModel> call, Response<ProjectsModel> response) {
                if (response.isSuccessful()) {
//                    System.out.println("VVVVVVVVVV-----------project List------" + new Gson().toJson(response.body()));
                    if (response.body() == null) {
                        projectsListDownlodeLisner.OnProjectsListDownlodeFail(
                                context.getString(R.string.justErrorCode) + " 26", context.getString(R.string.internalError),
                                false);
                    } else {
                        ProjectsModel projectsModel = response.body();
                        if (projectsModel.getSuccess() == 1) {
                            projectsListDownlodeLisner.OnProjectsListDownlodeSuccess((ArrayList<ProjectsDataModel>) projectsModel.getData());
                        } else {
                            projectsListDownlodeLisner.OnProjectsListDownlodeFail(
                                    projectsModel.getErrorMsg(), context.getString(R.string.alert),
                                    false);
                        }
                    }
                } else {
                    projectsListDownlodeLisner.OnProjectsListDownlodeFail(
                            context.getString(R.string.justErrorCode) + " 25", context.getString(R.string.ServerNotresponding),
                            false);
                }
            }

            @Override
            public void onFailure(Call<ProjectsModel> call, Throwable t) {
                projectsListDownlodeLisner.OnProjectsListDownlodeFail(
                        context.getString(R.string.noInternetMessage) + " 24",
                        context.getString(R.string.noInternetAlert),
                        true);
            }
        });
    }
}
