package com.lobotus.mapei.flowfragments.mvp.views;

import com.lobotus.mapei.flowfragments.model.CustomerDataModel;
import com.lobotus.mapei.flowfragments.model.ProductsDataModel;
import com.lobotus.mapei.flowfragments.model.ProjectsDataModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user1 on 06-10-2017.
 */

public interface AddProjectsView {

    void showProgressDialog();
    void dismissProgressDialog();
    //////////////////////////////////////////// set the project stages spinner
    void afterProjectStagesLoadedSuccess(String[] stringsStages);
    ///////////////////////////////////////////// customer model
    void afterCustomerModelDownlodeSuccess(ArrayList<CustomerDataModel> customerDataModels);
    void afterCustomerModelDownlodeFail(String message, String dialogTitle, boolean isInternetError);
    ///////////////////////////////////////////// after products downlode
    void afterProductsModelDownlodeSuccess(List<ProductsDataModel> productsDataModels);
    void afterProductsModelDoenlodeFail(String message, String dialogTitle, boolean isInternetError);
    ///////////////////////////////////////////////////////// validate before adding projects
    void afterValidatingAddProjects();
    ////////////////////////////////////////////////////// after projects post success
    void afteraddingProjectsSuccess(String message);
    void afterAddingProjuctsFail(String message, String dialogTitle, boolean isInternetError);
}
