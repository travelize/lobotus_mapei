package com.lobotus.mapei.flowfragments.mvp.presentor;

import android.content.Context;

public interface ViewDemoMeetingsPresenter {

    // get all demo meeting list from the presentor
    void reqToGetTheListOfDemoMeetingsFromPresentor(Context context);

    void onStopMvpPresentor();
}
