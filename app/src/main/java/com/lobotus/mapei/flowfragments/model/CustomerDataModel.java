package com.lobotus.mapei.flowfragments.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by user1 on 04-10-2017.
 */

public class CustomerDataModel  implements Parcelable {
    @SerializedName("CustomerID")
    @Expose
    private String customerID;
    @SerializedName("CustomerName")
    @Expose
    private String customerName;
    @SerializedName("Type")
    @Expose
    private String type;
    @SerializedName("Mobile")
    @Expose
    private String mobile;
    @SerializedName("Email")
    @Expose
    private String email;
    @SerializedName("Location")
    @Expose
    private String location;

    protected CustomerDataModel(Parcel in) {
        customerID = in.readString();
        customerName = in.readString();
        type = in.readString();
        mobile = in.readString();
        email = in.readString();
        location = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(customerID);
        dest.writeString(customerName);
        dest.writeString(type);
        dest.writeString(mobile);
        dest.writeString(email);
        dest.writeString(location);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CustomerDataModel> CREATOR = new Creator<CustomerDataModel>() {
        @Override
        public CustomerDataModel createFromParcel(Parcel in) {
            return new CustomerDataModel(in);
        }

        @Override
        public CustomerDataModel[] newArray(int size) {
            return new CustomerDataModel[size];
        }
    };

    public String getCustomerID() {
        return customerID;
    }

    public void setCustomerID(String customerID) {
        this.customerID = customerID;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
