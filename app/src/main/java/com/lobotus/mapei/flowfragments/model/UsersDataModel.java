package com.lobotus.mapei.flowfragments.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by User on 20-03-2018.
 */

public class UsersDataModel implements Parcelable {
    @SerializedName("UserId")
    @Expose
    private String userId;
    @SerializedName("FullName")
    @Expose
    private String fullName;
    @SerializedName("ProfileIcon")
    @Expose
    private String profileIcon;
    @SerializedName("Email")
    @Expose
    private String email;
    @SerializedName("Mobile")
    @Expose
    private String mobile;
    @SerializedName("RoleID")
    @Expose
    private Integer roleID;
    @SerializedName("RoleName")
    @Expose
    private String roleName;
    @SerializedName("Status")
    @Expose
    private String status;
    @SerializedName("ManagerName")
    @Expose
    private String managerName;

    protected UsersDataModel(Parcel in) {
        userId = in.readString();
        fullName = in.readString();
        profileIcon = in.readString();
        email = in.readString();
        mobile = in.readString();
        if (in.readByte() == 0) {
            roleID = null;
        } else {
            roleID = in.readInt();
        }
        roleName = in.readString();
        status = in.readString();
        managerName = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(userId);
        dest.writeString(fullName);
        dest.writeString(profileIcon);
        dest.writeString(email);
        dest.writeString(mobile);
        if (roleID == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(roleID);
        }
        dest.writeString(roleName);
        dest.writeString(status);
        dest.writeString(managerName);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<UsersDataModel> CREATOR = new Creator<UsersDataModel>() {
        @Override
        public UsersDataModel createFromParcel(Parcel in) {
            return new UsersDataModel(in);
        }

        @Override
        public UsersDataModel[] newArray(int size) {
            return new UsersDataModel[size];
        }
    };

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getProfileIcon() {
        return profileIcon;
    }

    public void setProfileIcon(String profileIcon) {
        this.profileIcon = profileIcon;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Integer getRoleID() {
        return roleID;
    }

    public void setRoleID(Integer roleID) {
        this.roleID = roleID;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getManagerName() {
        return managerName;
    }

    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }
}
