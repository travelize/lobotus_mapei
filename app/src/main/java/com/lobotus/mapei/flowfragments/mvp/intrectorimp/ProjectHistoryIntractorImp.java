package com.lobotus.mapei.flowfragments.mvp.intrectorimp;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.lobotus.mapei.R;
import com.lobotus.mapei.flowfragments.model.ProjectHistoryModel;
import com.lobotus.mapei.flowfragments.mvp.intrector.ProjectHistoryIntractor;
import com.lobotus.mapei.retrofitapi.APIService;
import com.lobotus.mapei.retrofitapi.ApiUtils;
import com.lobotus.mapei.utils.CommonFunc;
import com.lobotus.mapei.utils.ConstantsUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by user1 on 16-10-2017.
 */

public class ProjectHistoryIntractorImp implements ProjectHistoryIntractor {


    @Override
    public void reqToPostToDownlodeProjectHistory(final Context context, String projectId,
                                                  final OnProjectHistoryDownlodeLisner onProjectHistoryDownlodeLisner) {
        System.out.println("bbbbbbbbbbbbbbbbb-----------projectId----------" + projectId);

        APIService apiService = ApiUtils.getAPIService(ConstantsUtils.postBaseUrl + ConstantsUtils.postToGetTheProjectHistory);
        apiService.postToGetTheProjectHistory(projectId)
                .enqueue(new Callback<ProjectHistoryModel>() {
                    @Override
                    public void onResponse(Call<ProjectHistoryModel> call, Response<ProjectHistoryModel> response) {
                        if (response.isSuccessful()) {
//                            System.out.println("mmmmmmmm-------history---" + new Gson().toJson(response.body()));
                            if (response.body() != null) {
                                if (response.body().getSuccess() == 1) {
                                    onProjectHistoryDownlodeLisner.onProjectHistoryDownlodeSuccess(response.body());
                                } else {
                                    onProjectHistoryDownlodeLisner.onProjectHistoryDownlodeFail(response.body().getErrorMsg(),
                                            context.getString(R.string.alert), false);
                                }
                            } else {
                                onProjectHistoryDownlodeLisner.onProjectHistoryDownlodeFail(context.getString(R.string.noInternetMessage) + " 79",
                                        context.getString(R.string.internalError),
                                        false);
                            }
                        } else {
                            onProjectHistoryDownlodeLisner.onProjectHistoryDownlodeFail(context.getString(R.string.noInternetMessage) + " 78",
                                    context.getString(R.string.ServerNotresponding),
                                    false);
                        }
                    }

                    @Override
                    public void onFailure(Call<ProjectHistoryModel> call, Throwable t) {
                        onProjectHistoryDownlodeLisner.onProjectHistoryDownlodeFail(context.getString(R.string.noInternetMessage) + " 77",
                                context.getString(R.string.noInternetAlert),
                                true);
                    }
                });

    }
}
