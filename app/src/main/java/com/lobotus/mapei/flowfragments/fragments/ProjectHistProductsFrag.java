package com.lobotus.mapei.flowfragments.fragments;


import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.lobotus.mapei.R;
import com.lobotus.mapei.flowfragments.adapters.ProjectHistProductAdapter;
import com.lobotus.mapei.flowfragments.model.ProductsDataModel;
import com.lobotus.mapei.flowfragments.mvp.presentor.ProjectHisProductPresentor;
import com.lobotus.mapei.flowfragments.mvp.presentorimp.ProjectHisProductsPresentorImp;
import com.lobotus.mapei.flowfragments.mvp.views.ProjectHistProductsView;
import com.lobotus.mapei.utils.CommonFunc;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProjectHistProductsFrag extends Fragment implements ProjectHistProductsView,
        SwipeRefreshLayout.OnRefreshListener,android.widget.SearchView.OnQueryTextListener{

    View viewFragment=null;
    private Dialog dialog=null;
    private ProjectHisProductPresentor projectHisProductPresentor=null;
    //
    private ArrayList<ProductsDataModel> productsDataModelsTemp=new ArrayList<>();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        viewFragment= inflater.inflate(R.layout.project_list_fragment, container, false);
        viewFragment.findViewById(R.id.fragment_leave_add_leave_floatbutton).setVisibility(View.GONE);
        viewFragment.findViewById(R.id.fragment_leave_add_leave_floatbutton)
                .setVisibility(View.GONE);
        ((SwipeRefreshLayout)viewFragment.findViewById(R.id.fragment_leave_swipe_refresh_id)).setOnRefreshListener(this);
        projectHisProductPresentor=new ProjectHisProductsPresentorImp(this);
        projectHisProductPresentor.reqToGetTheProductsList(getActivity(),getArguments().getString("MeetingID"));
        //
        android.widget.SearchView searchView= (android.widget.SearchView) viewFragment.findViewById(R.id.project_list_searchview_id);
        searchView.setQueryHint("Search by product name");
        searchView.setOnQueryTextListener(this);
        return viewFragment;
    }

    @Override
    public void onRefresh() {
        projectHisProductPresentor.reqToGetTheProductsList(getActivity(),getArguments().getString("MeetingID"));
    }
    @Override
    public boolean onQueryTextSubmit(String s) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        if (!productsDataModelsTemp.isEmpty())
        {
            ArrayList<ProductsDataModel> projectsDataModels=new ArrayList<>();
            for (int i=0;i<productsDataModelsTemp.size();i++)
            {
                if (productsDataModelsTemp.get(i).getProductName().toLowerCase().contains(s.toLowerCase()))
                {
                    projectsDataModels.add(productsDataModelsTemp.get(i));
                }
            }
            ((RecyclerView)viewFragment.findViewById(R.id.fragment_leave_recycleview_id))
                    .setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false));
            ((RecyclerView)viewFragment.findViewById(R.id.fragment_leave_recycleview_id)).setAdapter(new ProjectHistProductAdapter
                    (projectsDataModels,getActivity()));
        }
        return false;
    }
////////////////////////////////////// mvp stuff. . .
@Override
public void showProgressDialog() {
    dialog=new Dialog(getActivity());
    dialog.setCancelable(false);
    dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
    dialog.setContentView(R.layout.loaging_layout);
    dialog.show();
}

    @Override
    public void dismissProgressDialog() {
        if (getActivity()!=null)
        {
            if ( ((SwipeRefreshLayout)viewFragment.findViewById(R.id.fragment_leave_swipe_refresh_id))!=null)
            {
                if ( ((SwipeRefreshLayout)viewFragment.findViewById(R.id.fragment_leave_swipe_refresh_id)).isRefreshing())
                    ((SwipeRefreshLayout)viewFragment.findViewById(R.id.fragment_leave_swipe_refresh_id)).setRefreshing(false);
            }
            if (dialog!=null)
            {
                if (dialog.isShowing())
                {
                    dialog.dismiss();
                }
                dialog=null;
            }
        }

    }

    @Override
    public void afterProductsModelDownlodeSuccess(List<ProductsDataModel> productsDataModels) {
        productsDataModelsTemp.clear();
        productsDataModelsTemp.addAll(productsDataModels);
        ((RecyclerView)viewFragment.findViewById(R.id.fragment_leave_recycleview_id)).setLayoutManager(new
                LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false));
        ((RecyclerView)viewFragment.findViewById(R.id.fragment_leave_recycleview_id)).setAdapter(new ProjectHistProductAdapter(
                productsDataModelsTemp,getActivity()
        ));

    }

    @Override
    public void afterProductsModelDoenlodeFail(String message, String dialogTitle, boolean isInternetError) {
        CommonFunc.commonDialog(getActivity(),dialogTitle,message,isInternetError);
    }


}
