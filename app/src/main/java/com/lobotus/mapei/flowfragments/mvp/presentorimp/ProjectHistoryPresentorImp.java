package com.lobotus.mapei.flowfragments.mvp.presentorimp;

import android.content.Context;

import com.lobotus.mapei.flowfragments.model.ProjectHistoryModel;
import com.lobotus.mapei.flowfragments.mvp.intrector.ProjectHistoryIntractor;
import com.lobotus.mapei.flowfragments.mvp.intrectorimp.ProjectHistoryIntractorImp;
import com.lobotus.mapei.flowfragments.mvp.presentor.ProjectHistoryPresentor;
import com.lobotus.mapei.flowfragments.mvp.views.ProjectHistoryView;
import com.lobotus.mapei.utils.NetworkUtils;

/**
 * Created by user1 on 16-10-2017.
 */

public class ProjectHistoryPresentorImp implements ProjectHistoryPresentor ,ProjectHistoryIntractor.OnProjectHistoryDownlodeLisner{

    private ProjectHistoryView projectHistoryView=null;
    private ProjectHistoryIntractor projectHistoryIntractor=null;

    public ProjectHistoryPresentorImp(ProjectHistoryView projectHistoryView) {
        this.projectHistoryView = projectHistoryView;
        this.projectHistoryIntractor = new ProjectHistoryIntractorImp();
    }

    @Override
    public void reqToDownlodeTheProjectHistoryData(Context context, String projectID) {
        if (NetworkUtils.checkInternetAndOpenDialog(context))
        {
            projectHistoryView.showProgressDialog();
            projectHistoryIntractor.reqToPostToDownlodeProjectHistory(context,projectID,this);
        }
    }

    @Override
    public void onProjectHistoryDownlodeSuccess(ProjectHistoryModel projectHistoryModel) {
        projectHistoryView.dismissProgressDialog();
        projectHistoryView.afterProjectHistoryDownlodeSuccess(projectHistoryModel);
    }

    @Override
    public void onProjectHistoryDownlodeFail(String message, String dialogTitle, boolean isInternetError) {
        projectHistoryView.dismissProgressDialog();
        projectHistoryView.afterProjectHistoryDownlodeFail(message,dialogTitle,isInternetError);
    }
}
