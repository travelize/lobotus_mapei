package com.lobotus.mapei.flowfragments.fragments;


import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.lobotus.mapei.R;
import com.lobotus.mapei.baseactivity.BaseActivity;
import com.lobotus.mapei.flowfragments.adapters.ProjectsListAdapter;
import com.lobotus.mapei.flowfragments.model.ProjectsDataModel;
import com.lobotus.mapei.flowfragments.mvp.presentor.ProjectsFragPresentor;
import com.lobotus.mapei.flowfragments.mvp.presentorimp.ProjectsFragPresentorImp;
import com.lobotus.mapei.flowfragments.mvp.views.ProjectsFragView;
import com.lobotus.mapei.utils.CommonFunc;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProjectsFragment extends Fragment implements ProjectsFragView, View.OnClickListener,
        SwipeRefreshLayout.OnRefreshListener,
        android.widget.SearchView.OnQueryTextListener {
    private View viewFragment = null;
    private Dialog dialog = null;
    /////////////////////////////////////////// mvp
    private ProjectsFragPresentor projectsFragPresentor = null;
    //////////////////////////// temp data
    private ArrayList<ProjectsDataModel> tempprojectsDataModels = new ArrayList<>();

    ////////////////////////// life cycle stuff. . .
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        viewFragment = inflater.inflate(R.layout.project_list_fragment, container, false);
        ((SwipeRefreshLayout) viewFragment.findViewById(R.id.fragment_leave_swipe_refresh_id)).setOnRefreshListener(this);
        viewFragment.findViewById(R.id.fragment_leave_add_leave_floatbutton).setOnClickListener(this);
        projectsFragPresentor = new ProjectsFragPresentorImp(this);
        //////////////////
        android.widget.SearchView searchView = (android.widget.SearchView) viewFragment.findViewById(R.id.project_list_searchview_id);
        searchView.setOnQueryTextListener(this);


/////////////////////////////////////////
        return viewFragment;
    }

    @Override
    public void onResume() {
        super.onResume();
        projectsFragPresentor.reqToDownlodeTheListOfProjectsFromPresentor(getActivity());
    }

    @Override
    public void onDetach() {
        super.onDetach();
        CommonFunc.hideKeyboard(getActivity());
    }

    @Override
    public void onRefresh() {
        projectsFragPresentor.reqToDownlodeTheListOfProjectsFromPresentor(getActivity());
    }

    @Override
    public void onClick(View v) {
        ((BaseActivity) getActivity()).attachFragment(new AddProjects(), true, "Add Projects", null);
    }

    ///////////////////////////////////////////////////////////////////////////////////// mvp stuff
    @Override
    public void showProgressDialog() {
        dialog = new Dialog(getActivity());
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.loaging_layout);
        dialog.show();
    }

    @Override
    public void dismissProgressDialog() {
        if (getActivity() != null) {
            if (((SwipeRefreshLayout) viewFragment.findViewById(R.id.fragment_leave_swipe_refresh_id)) != null) {
                if (((SwipeRefreshLayout) viewFragment.findViewById(R.id.fragment_leave_swipe_refresh_id)).isRefreshing())
                    ((SwipeRefreshLayout) viewFragment.findViewById(R.id.fragment_leave_swipe_refresh_id)).setRefreshing(false);
            }

            if (dialog != null) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
                dialog = null;
            }
        }

    }

    @Override
    public void afterProjectsListDownlodeSuccess(ArrayList<ProjectsDataModel> projectsDataModels) {
        tempprojectsDataModels.clear();
        for (int i = 0; i < projectsDataModels.size(); i++) {
            tempprojectsDataModels.add(projectsDataModels.get(i));
        }
        Log.e("checking project=", tempprojectsDataModels.size() + "");
        ((RecyclerView) viewFragment.findViewById(R.id.fragment_leave_recycleview_id))
                .setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        ((RecyclerView) viewFragment.findViewById(R.id.fragment_leave_recycleview_id)).setAdapter(new ProjectsListAdapter(getActivity(), projectsDataModels));
    }

    @Override
    public void afterProjectsListDownlodeFail(String message, String dialogTitle, boolean isInternetError) {
        CommonFunc.commonDialog(getActivity(), dialogTitle, message, isInternetError);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if (!tempprojectsDataModels.isEmpty()) {
            ArrayList<ProjectsDataModel> projectsDataModels = new ArrayList<>();
            for (int i = 0; i < tempprojectsDataModels.size(); i++) {
                if (tempprojectsDataModels.get(i).getProjectName().toLowerCase().contains(newText.toLowerCase()) ||
                        tempprojectsDataModels.get(i).getCustomerName().toLowerCase().contains(newText.toLowerCase())) {
                    projectsDataModels.add(tempprojectsDataModels.get(i));
                }
            }
            ((RecyclerView) viewFragment.findViewById(R.id.fragment_leave_recycleview_id))
                    .setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
            ((RecyclerView) viewFragment.findViewById(R.id.fragment_leave_recycleview_id)).setAdapter(new ProjectsListAdapter(getActivity(), projectsDataModels));
        }

        return false;
    }
}
