package com.lobotus.mapei.flowfragments.mvp.presentor;

import android.content.Context;
import android.view.View;

import com.lobotus.mapei.flowfragments.model.CustomerDataModel;
import com.lobotus.mapei.flowfragments.model.ProductsDataModel;

import java.util.ArrayList;

/**
 * Created by user1 on 06-10-2017.
 */

public interface AddProjectsPresentor {
    //////////////////////////// reqToLoadProjectStages
    void reqToLoadProjectStages(Context context);
    //////////////////////////// downlode the customer data
    void reqToGetTheCustomerDataFromPresentor(Context context);
    /////////////////////////// downlode the products data
    void reqToGetTheProductsDataFromPresentor(Context context);
    ////////////////////////////////////// validate add projects
    void validateAddProjectsInPresentorOnly(String projectName,
                                            String customerName,
                                            String customerType,
                                            String productsList,
                                            ArrayList<ProductsDataModel> productsDataModels,
                                            String projectPeriod,
                                            int projectStageSelectedPos,
                                            Context context,
                                            View view);
    /////////////////////////////////////// post to save the projucts
    void reqToaddNewProjects(Context context,String projectName,String customerName,String customerType,String productList,
                             String projectPeriod,String projectStage,String notes);
}
