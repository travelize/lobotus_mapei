package com.lobotus.mapei.flowfragments.mvp.intrectorimp;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.lobotus.mapei.R;
import com.lobotus.mapei.flowfragments.model.CustomerDataModel;
import com.lobotus.mapei.flowfragments.model.CustomerModel;
import com.lobotus.mapei.flowfragments.model.MOTandPOTdataModel;
import com.lobotus.mapei.flowfragments.model.ModeOfTravelModel;
import com.lobotus.mapei.flowfragments.model.PurposeOfTravelModel;
import com.lobotus.mapei.flowfragments.mvp.intrector.MeetingInterator;
import com.lobotus.mapei.flowfragments.mvp.intrector.TravelInteractor;
import com.lobotus.mapei.retrofitapi.APIService;
import com.lobotus.mapei.retrofitapi.ApiUtils;
import com.lobotus.mapei.utils.CommonFunc;
import com.lobotus.mapei.utils.ConstantsUtils;
import com.lobotus.mapei.utils.ImageUtility;
import com.lobotus.mapei.utils.PreferenceUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by user1 on 04-10-2017.
 */

public class TravelInterectorImp implements TravelInteractor {
    ///////////////////////////////////////////////////////// customer details
    @Override
    public void reqToPostCustomerDataReqToServer(final Context context,
                                                 final OnTravelCustomerDataDownlodeLisner onTravelCustomerDataDownlodeLisner) {

        APIService apiService= ApiUtils.getAPIService(ConstantsUtils.postBaseUrl+ConstantsUtils.postToDownlodeTheCustomersData);
        apiService.postReqTogetTheCustomerData("Active").enqueue(new Callback<CustomerModel>() {
            @Override
            public void onResponse(Call<CustomerModel> call, Response<CustomerModel> response) {
                if (response.isSuccessful())
                {
                    CustomerModel customerModel=response.body();
                    if (customerModel==null)
                    {
                        onTravelCustomerDataDownlodeLisner.OnTravelCustomerDataDownlodeFail(
                                context.getString(R.string.justErrorCode)+" 8",context.getString(R.string.ServerNotresponding),
                                false);
                    }else {
                        if (customerModel.getSuccess()==1)
                        {
                            onTravelCustomerDataDownlodeLisner
                                    .OnTravelCustomerDataDownlodeSuccess((ArrayList<CustomerDataModel>) customerModel.getCustomerDataModels());
                        }else {
                            onTravelCustomerDataDownlodeLisner.OnTravelCustomerDataDownlodeFail(
                                    context.getString(R.string.alert),customerModel.getMsg(),
                                    false);
                        }

                    }

                }else {
                    onTravelCustomerDataDownlodeLisner.OnTravelCustomerDataDownlodeFail(
                            context.getString(R.string.justErrorCode)+" 7",context.getString(R.string.ServerNotresponding),
                            false);
                }
            }

            @Override
            public void onFailure(Call<CustomerModel> call, Throwable t) {
                onTravelCustomerDataDownlodeLisner.OnTravelCustomerDataDownlodeFail(
                        context.getString(R.string.noInternetMessage)+" 9",
                        context.getString(R.string.noInternetAlert),
                        true);
            }
        });

    }
    ///////////////////////////////////////////////////////////////// MOT and POT
    @Override
    public void reqToPostMOTandPOTdataToServer(final Context context,final  OnMOTAndPOTDataDownlodeLisner onMOTAndPOTDataDownlodeLisner) {
        APIService apiService=ApiUtils.getAPIService(ConstantsUtils.postBaseUrl+ConstantsUtils.postURlToGetTheMOTandPOT);
        apiService.postToGetMOTandPOT("").enqueue(new Callback<MOTandPOTdataModel>() {
            @Override
            public void onResponse(Call<MOTandPOTdataModel> call, Response<MOTandPOTdataModel> response) {

                if (response.isSuccessful())
                {
                    MOTandPOTdataModel moTandPOTdataModel=response.body();
                    if (moTandPOTdataModel.getSuccess()==1)
                    {
                        onMOTAndPOTDataDownlodeLisner.OnMOTandPOTdataDOwnlodeSuccess(
                                moTandPOTdataModel.getModeOfTravelModel(),moTandPOTdataModel.getPurposeOfTravelModel()
                        );
                    }else {
                        onMOTAndPOTDataDownlodeLisner.OnMOTandPOTdownlodeFail(
                                moTandPOTdataModel.getErroeMsg(),context.getString(R.string.alert),
                                false);
                    }
                }else {
                    onMOTAndPOTDataDownlodeLisner.OnMOTandPOTdownlodeFail(
                            context.getString(R.string.justErrorCode)+" 15",context.getString(R.string.ServerNotresponding),
                            false);
                }
            }
            @Override
            public void onFailure(Call<MOTandPOTdataModel> call, Throwable t) {
                onMOTAndPOTDataDownlodeLisner.OnMOTandPOTdownlodeFail(
                        context.getString(R.string.noInternetMessage)+" 14",
                        context.getString(R.string.noInternetAlert),
                        true);
            }
        });
    }
    //////////////////////////////////////// post the travel details to server
    @Override
    public void reqToPostTheTravelDetailsToServer(Context context, String remark, String custId,
                                                  String modeOfTravel, String purpose,
                                                  String attachmentB64,OnTravelDetailsPostLisner onTravelDetailsPostLisner,
                                                  String startLoc,String endLoc,String imagecaption,
                                                  String ownvehicalspinnerselectedpos,
                                                  String fuletypespinnerselectedpos,String imageAmount) {
        if (PreferenceUtils.checkUserisLogedin(context))
        {
            if (remark==null)
            {
                remark="";
            }else {
                if (remark.isEmpty())
                    remark="";
            }
            // attachment
            if (attachmentB64==null)
            {
                attachmentB64="";
            }else {
                if (attachmentB64.isEmpty())
                    attachmentB64="";
            }
            // image caption
            if (imagecaption==null)
            {
                imagecaption="";
            }else {
                if (imagecaption.isEmpty())
                    imagecaption="";
            }
            // amount
            if (imageAmount==null)
            {
                imageAmount="";
            }else {
                if (imageAmount.isEmpty())
                    imageAmount="";
            }
            /////////////////////////////////////////////////////////



//            postTheTravelDetails(context,remark,custId,modeOfTravel,purpose,attachmentB64,onTravelDetailsPostLisner); //// STOPSHIP: 10/20/2017  replaced async task for slow image process
            this.userIdPostTravel=PreferenceUtils.getUserIdFromPreference(context);
            this.contextPostTravel=context;
            this.remarkPostTravel=remark;
            this.custIdPostTravel=custId;
            this.modeOfTravelPostTravel=modeOfTravel;
            this.purposePostTravel=purpose;
            this.attachmentB64PostTravel=attachmentB64;
            this.onTravelDetailsPostLisnerPostTravel=onTravelDetailsPostLisner;
            this.startLoc=startLoc;
            this.endLoc=endLoc;
            this.imageCaption=imagecaption;
            this.imageAmount=imageAmount;
            this.ownVehicalTypeSelectedSpinnerPos=ownvehicalspinnerselectedpos;
            this.fuleTypeSpinnerSelectedPos=fuletypespinnerselectedpos;
            POSTTraveDataTask postTraveDataTask=new POSTTraveDataTask();
            postTraveDataTask.execute(ConstantsUtils.postBaseUrl+ConstantsUtils.postToSaveTheTravelDetails);
        }else {
            CommonFunc.commonDialog(context,context.getString(R.string.alert),context.getString(R.string.SessionLost),false);

        }
    }
    private void postTheTravelDetails(final Context context, final String remark, String custId,
                                      String modeOfTravel, String purpose,
                                      String attachmentB64, final OnTravelDetailsPostLisner onTravelDetailsPostLisner)
    {
        APIService apiService=ApiUtils.getAPIService(ConstantsUtils.postBaseUrl+ConstantsUtils.postToSaveTheTravelDetails);
        apiService.postToSaveTheTravelDetails(PreferenceUtils.getUserIdFromPreference(context),
                remark,custId,modeOfTravel,purpose,attachmentB64).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful())
                {

                    try {
                        JSONObject jsonObject=new JSONObject(response.body().string());
                        if (jsonObject.getInt("Success")==1)
                        {
                            onTravelDetailsPostLisner.OnTravelDetailsPostSuccess(jsonObject.getString("Msg"));
                        }else {
                            onTravelDetailsPostLisner.OnTravelDetailsPostFail(
                                    jsonObject.getString("Msg"),context.getString(R.string.alert),
                                    false);
                        }
                    } catch (JSONException | IOException e) {
                        onTravelDetailsPostLisner.OnTravelDetailsPostFail(
                                context.getString(R.string.justErrorCode)+" 18",context.getString(R.string.internalError),
                                false);
                    }

                }else {
                    onTravelDetailsPostLisner.OnTravelDetailsPostFail(
                            context.getString(R.string.justErrorCode)+" 17",context.getString(R.string.ServerNotresponding),
                            false);
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                onTravelDetailsPostLisner.OnTravelDetailsPostFail(
                        context.getString(R.string.noInternetMessage)+" 16",
                        context.getString(R.string.noInternetAlert),
                        true);
            }
        });
    }
    /////////////////////////////
  /*  final Context context, final String remark, String custId,
    String modeOfTravel, String purpose,
    String attachmentB64, final OnTravelDetailsPostLisner onTravelDetailsPostLisner*/
    private Context contextPostTravel=null;
    private String userIdPostTravel=null;
    private String remarkPostTravel=null;
    private String custIdPostTravel=null;
    private String modeOfTravelPostTravel=null;
    private String purposePostTravel=null;
    private String attachmentB64PostTravel=null;
    private OnTravelDetailsPostLisner onTravelDetailsPostLisnerPostTravel=null;
    private String startLoc=null;
    private String endLoc=null;
    private String imageCaption=null;
    private String imageAmount=null;
    private String ownVehicalTypeSelectedSpinnerPos="";
    private String fuleTypeSpinnerSelectedPos="";

    private class POSTTraveDataTask extends AsyncTask<String, Void, String>
    {

        @Override
        protected String doInBackground(String... strings) {
//            (@Field("UserId") String UserId,
//                    @Field("Remarks") String Remarks,
//                    @Field("CustID") String CustID,
//                    @Field("MOT") String MOT,
//                    @Field("Purpose") String Purpose,
//                    @Field("Attachment") String Attachment);
            HashMap<String,String> hashMap=new HashMap<>();
            hashMap.put("UserId",userIdPostTravel);
            hashMap.put("Remarks",remarkPostTravel);
            hashMap.put("CustID",custIdPostTravel);
            hashMap.put("MOT",modeOfTravelPostTravel);
            hashMap.put("Purpose",purposePostTravel);
            hashMap.put("Attachment",attachmentB64PostTravel);
            hashMap.put("Captian",imageCaption);
            hashMap.put("StartLocation",startLoc);
            hashMap.put("ClientLocation",endLoc);
            hashMap.put("Amt",imageAmount);
            if (modeOfTravelPostTravel.trim().equals("Own Vehicle"))
            {
                /*if (ownVehicalTypeSelectedSpinnerPos==1)
                {
                    hashMap.put("Type","2 Wheeler");
                }else {
                    hashMap.put("Type","4 Wheeler");
                    if (fuleTypeSpinnerSelectedPos==2)
                        hashMap.put("Vehicle","Diesel");
                    else
                        hashMap.put("Vehicle","Petrol");
                }*/
                hashMap.put("Type",ownVehicalTypeSelectedSpinnerPos);
                hashMap.put("Vehicle",fuleTypeSpinnerSelectedPos);
            }
            ImageUtility.writeToFile(hashMap.toString());
            return performPostCall(strings[0],hashMap);
        }

        @Override
        protected void onPostExecute(String response) {
            System.out.println("ffffffffffff-----response-----------"+response);
            super.onPostExecute(response);
            try {
                JSONObject jsonObject=new JSONObject(response);
                if (jsonObject.getInt("Success")==1)
                {
                    onTravelDetailsPostLisnerPostTravel.OnTravelDetailsPostSuccess(jsonObject.getString("Msg"));
                }else {
                    onTravelDetailsPostLisnerPostTravel.OnTravelDetailsPostFail(
                            jsonObject.getString("Msg"),contextPostTravel.getString(R.string.alert),
                            false);
                }
            } catch (JSONException e) {
                onTravelDetailsPostLisnerPostTravel.OnTravelDetailsPostFail(
                        contextPostTravel.getString(R.string.justErrorCode)+" 18",contextPostTravel.getString(R.string.internalError),
                        false);
            }
        }
    }

    ///////////////////////////////
    public static  String performPostCall(String requestURL,
                                          HashMap<String, String> postDataParams) {

        URL url;
        String response = "";
        try {
            url = new URL(requestURL);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(30000);
            conn.setConnectTimeout(30000);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(getPostDataString(postDataParams));
            writer.flush();
            writer.close();
            os.close();

            int responseCode = conn.getResponseCode();
            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line = br.readLine()) != null) {
                    response += line;
                }
            } else {
                response = "Server Not Responding";
                // throw new HttpException(responseCode + "");
            }
            conn.disconnect();

        } catch (Exception e) {
            e.printStackTrace();
        }

        //   Log.e("Otpresponse ", Otpresponse);
        return response;
    }

    public static String getPostDataString(HashMap<String, String> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for (Map.Entry<String, String> entry : params.entrySet()) {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }
        //   Log.e("url", result.toString());
        return result.toString();
    }
}
