package com.lobotus.mapei.flowfragments.mvp.presentor;

import android.content.Context;
import android.view.View;

import java.text.ParseException;

/**
 * Created by user1 on 04-10-2017.
 */

public interface LeaveApplyPresentor {

    /// post to downlode leave types
    void reqToPostLeaveType(Context context);
    /// post apply leaves
    void reqToPostLeaveDataFromPresentor(Context context,
                                         String leaveType,
                                         String fromDate,
                                         String toDate,
                                         String remark);
    /// validate apply for leave
    void reqToValidateLeaveApplyInPresentorOnly(Context context, View view,
                                                int leaveTypeSelected,
                                              String fromDate,
                                              String toDate,
                                              String remark) throws ParseException;

}
