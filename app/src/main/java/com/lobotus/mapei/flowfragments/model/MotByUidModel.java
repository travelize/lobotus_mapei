package com.lobotus.mapei.flowfragments.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by User on 27-02-2018.
 */

public class MotByUidModel {

    @SerializedName("Success")
    @Expose
    private Integer success;
    @SerializedName("data")
    @Expose
    private List<MotByUidData> data = null;
    @SerializedName("Msg")
    @Expose
    private String errorMessage;

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public List<MotByUidData> getData() {
        return data;
    }

    public void setData(List<MotByUidData> data) {
        this.data = data;
    }
}
