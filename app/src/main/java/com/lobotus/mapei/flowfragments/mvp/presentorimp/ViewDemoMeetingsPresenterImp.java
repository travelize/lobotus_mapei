package com.lobotus.mapei.flowfragments.mvp.presentorimp;

import android.content.Context;

import com.lobotus.mapei.flowfragments.model.DemoMeetingsModel;
import com.lobotus.mapei.flowfragments.mvp.intrector.ViewDemoMeetingsInteractor;
import com.lobotus.mapei.flowfragments.mvp.intrectorimp.ViewDemoMeetingsInteractorImp;
import com.lobotus.mapei.flowfragments.mvp.presentor.ViewDemoMeetingsPresenter;
import com.lobotus.mapei.flowfragments.mvp.views.ViewDemoMeetingsView;
import com.lobotus.mapei.utils.NetworkUtils;

import java.util.ArrayList;

public class ViewDemoMeetingsPresenterImp implements ViewDemoMeetingsPresenter, ViewDemoMeetingsInteractor.DemoMeetingListDownlodeLisner {

    private ViewDemoMeetingsView demoMeetingsView = null;
    private ViewDemoMeetingsInteractor viewDemoMeetingsInteractor = null;

    public ViewDemoMeetingsPresenterImp(ViewDemoMeetingsView demoMeetingsView) {
        this.demoMeetingsView = demoMeetingsView;
        this.viewDemoMeetingsInteractor = new ViewDemoMeetingsInteractorImp();
    }

    @Override
    public void reqToGetTheListOfDemoMeetingsFromPresentor(Context context) {

        if (NetworkUtils.checkInternetAndOpenDialog(context)) {
            demoMeetingsView.showProgressDialog();
            viewDemoMeetingsInteractor.reqToServerToGetTheListOfDemoMeetings(context, this);
        }

    }

    @Override
    public void onStopMvpPresentor() {
        viewDemoMeetingsInteractor.onStopMvpIntractor();
    }

    @Override
    public void OnDemoMeetingListDownlodeSuccess(ArrayList<DemoMeetingsModel> meetingModels) {

        demoMeetingsView.dismissProgressDialog();
        demoMeetingsView.afterDemoListDownlodeSuccess(meetingModels);

    }

    @Override
    public void OnDemoMeetingListDownlodeFail(String message, String dialogTitle, boolean isInternetError) {

        demoMeetingsView.dismissProgressDialog();
        demoMeetingsView.afterDemoMeetingsListDownlodeFail(message,dialogTitle,isInternetError);

    }
}
