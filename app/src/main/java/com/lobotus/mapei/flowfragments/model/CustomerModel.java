package com.lobotus.mapei.flowfragments.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by user1 on 04-10-2017.
 */

public class CustomerModel {

    @SerializedName("Success")
    @Expose
    private int success;
    @SerializedName("data")
    @Expose
    private List<CustomerDataModel> customerDataModels = null;
    @SerializedName("Msg")
    @Expose
    private String msg=null;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getSuccess() {
        return success;
    }

    public void setSuccess(int success) {
        this.success = success;
    }

    public List<CustomerDataModel> getCustomerDataModels() {
        return customerDataModels;
    }

    public void setCustomerDataModels(List<CustomerDataModel> customerDataModels) {
        this.customerDataModels = customerDataModels;
    }
}
