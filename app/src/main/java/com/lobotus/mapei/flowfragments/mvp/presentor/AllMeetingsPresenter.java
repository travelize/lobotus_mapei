package com.lobotus.mapei.flowfragments.mvp.presentor;

import android.content.Context;

public interface AllMeetingsPresenter {

    // get the meeting list from the presentor
    void reqToGetTheListOfMeetingsFromPresentor(Context context, String meetingValue, String date);

    // on stop
    void onStopMvpPresentor();
}
