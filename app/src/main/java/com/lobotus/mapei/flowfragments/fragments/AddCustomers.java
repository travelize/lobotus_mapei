package com.lobotus.mapei.flowfragments.fragments;


import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.lobotus.mapei.R;
import com.lobotus.mapei.baseactivity.BaseActivity;
import com.lobotus.mapei.customwidgits.LabelledSpinner;
import com.lobotus.mapei.flowfragments.model.CustomerTypeDataModel;
import com.lobotus.mapei.flowfragments.mvp.presentor.AddCustomerPresentor;
import com.lobotus.mapei.flowfragments.mvp.presentorimp.AddCustomerPresentorImp;
import com.lobotus.mapei.flowfragments.mvp.views.AddCustomerView;
import com.lobotus.mapei.utils.CommonFunc;
import com.lobotus.mapei.utils.NetworkUtils;
import com.lobotus.mapei.utils.PermissionUtils;
import com.lobotus.mapei.utils.PreferenceUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddCustomers extends Fragment implements View.OnClickListener, AddCustomerView {

    // root view
    private View viewFragment = null;
    private Dialog dialogProgress = null;
    // mvp
    private AddCustomerPresentor addCustomerPresentor = null;
    // temp data
    private ArrayList<CustomerTypeDataModel> customerTypeDataModels = new ArrayList<>();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        viewFragment = inflater.inflate(R.layout.fragment_add_customers, container, false);
        //
        viewFragment.findViewById(R.id.fragment_add_customer_loc_button_id).setOnClickListener(this);
        viewFragment.findViewById(R.id.fragment_add_customer_submit_button_id).setOnClickListener(this);
        viewFragment.findViewById(R.id.fragment_add_customer_location_edittext_id).setOnClickListener(this);
        addCustomerPresentor = new AddCustomerPresentorImp(this);
        addCustomerPresentor.reqToGetTheCustomerTypes(getActivity());
        textwatcherToDisableErrors();
        return viewFragment;
    }

    private void textwatcherToDisableErrors() {
        ((AppCompatEditText) viewFragment.findViewById(R.id.fragment_add_customer_name_edittext_id))
                .addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                        if (((TextInputLayout) viewFragment.findViewById(R.id.fragment_add_customer_name_input_id)).isErrorEnabled())
                            ((TextInputLayout) viewFragment.findViewById(R.id.fragment_add_customer_name_input_id)).setErrorEnabled(false);
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });
        ((AppCompatEditText) viewFragment.findViewById(R.id.fragment_add_customer_email_edittext_id))
                .addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                        if (((TextInputLayout) viewFragment.findViewById(R.id.fragment_add_customer_email_input_id)).isErrorEnabled())
                            ((TextInputLayout) viewFragment.findViewById(R.id.fragment_add_customer_email_input_id)).setErrorEnabled(false);
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });

        ((AppCompatEditText) viewFragment.findViewById(R.id.fragment_add_customer_mobile_edittext_id))
                .addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                        if (((TextInputLayout) viewFragment.findViewById(R.id.fragment_add_customer_mobile_input_id)).isErrorEnabled())
                            ((TextInputLayout) viewFragment.findViewById(R.id.fragment_add_customer_mobile_input_id)).setErrorEnabled(false);
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });

        ((AppCompatEditText) viewFragment.findViewById(R.id.fragment_add_customer_location_edittext_id))
                .addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                        if (((TextInputLayout) viewFragment.findViewById(R.id.fragment_add_customer_location_inputlayout_id)).isErrorEnabled())
                            ((TextInputLayout) viewFragment.findViewById(R.id.fragment_add_customer_location_inputlayout_id)).setErrorEnabled(false);
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });
    }

    private void placePickerCode(int requestCode) {
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
        try {
            Intent intent = builder.build(getActivity());
            startActivityForResult(intent, requestCode);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (getActivity().RESULT_OK == resultCode) {

            if (data != null) {
                Place placeSelected = PlacePicker.getPlace(data, getActivity());
                if (requestCode == 101 && resultCode == RESULT_OK) {
                    ((AppCompatEditText) viewFragment.findViewById(R.id.fragment_add_customer_location_edittext_id)).setText(placeSelected.getAddress().toString() == null ? "" : placeSelected.getAddress().toString());
                }
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fragment_add_customer_location_edittext_id:
                if (NetworkUtils.checkInternetAndOpenDialog(getActivity())) {
                    placePickerCode(101);
                }
                break;

            case R.id.fragment_add_customer_loc_button_id:
                connectToGoogleApiClientAndGetTheAddress();
                break;
            case R.id.fragment_add_customer_submit_button_id:

             /*   String customerName, String email, String mobileNo, ArrayList<CustomerTypeDataModel> customerTypeDataModels,
                int customerTypeSelectedSpinner, String location, Context context, View viewParent*/
                if (PreferenceUtils.getIsUserCheckedIn(getActivity()))
                    addCustomerPresentor.validateAddCustomerInPresentorItself(((AppCompatEditText) viewFragment.
                                    findViewById(R.id.fragment_add_customer_name_edittext_id))
                                    .getText().toString(),
                            ((AppCompatEditText) viewFragment.findViewById(R.id.fragment_add_customer_email_edittext_id)).getText().toString(),
                            ((AppCompatEditText) viewFragment.findViewById(R.id.fragment_add_customer_mobile_edittext_id)).getText().toString(),
                            customerTypeDataModels,
                            ((LabelledSpinner)
                                    viewFragment.findViewById(R.id.fragment_add_customer_type_spinner_id)).getSpinner().getSelectedItemPosition(),
                            ((AppCompatEditText) viewFragment.findViewById(R.id.fragment_add_customer_location_edittext_id)).getText().toString(),
                            getActivity(), viewFragment);
                else
                    CommonFunc.commonDialog(getActivity(), getString(R.string.alert), getString(R.string.chekInAlert), false);

        }
    }


    ///////////////////////////////////////////////////////////////////////////////////// mvp stuff
//    @Override
    public void showProgressDialog() {
        dialogProgress = new Dialog(getActivity());
        dialogProgress.setCancelable(false);
        dialogProgress.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialogProgress.setContentView(R.layout.loaging_layout);
        dialogProgress.show();
    }

    //    @Override
    public void dismissProgressDialog() {
        if (getActivity() != null) {


            if (dialogProgress != null) {
                if (dialogProgress.isShowing()) {
                    dialogProgress.dismiss();
                }
                dialogProgress = null;
            }
        }

    }

    @Override
    public void onStop() {
        super.onStop();
        dismissProgressDialog();
    }

    @Override
    public void afterAddCustomerValidationSuccess() {
        //context context, String CustTypeId, String FullName, String Location, String Email, String Phone, String Statu

        addCustomerPresentor.reqToAddTheCustomerFromThePresentor(getActivity(),
                customerTypeDataModels.get(((LabelledSpinner)
                        viewFragment.findViewById(R.id.fragment_add_customer_type_spinner_id)).getSpinner().getSelectedItemPosition()).getCustomerID(),
                ((AppCompatEditText) viewFragment.findViewById(R.id.fragment_add_customer_name_edittext_id)).getText().toString(),
                ((AppCompatEditText) viewFragment.findViewById(R.id.fragment_add_customer_location_edittext_id)).getText().toString(),
                ((AppCompatEditText) viewFragment.findViewById(R.id.fragment_add_customer_email_edittext_id)).getText().toString(),
                ((AppCompatEditText) viewFragment.findViewById(R.id.fragment_add_customer_mobile_edittext_id)).getText().toString(),
                "Active");
    }

    @Override
    public void afterCustomerTypeDownlodeSuccess(ArrayList<CustomerTypeDataModel> customerTypeDataModelss) {
        customerTypeDataModels.clear();

//        customerTypeDataModels.addAll(customerTypeDataModelss);
        customerTypeDataModels.add(new CustomerTypeDataModel("Please select customer type"));
        for (int i = 0; i < customerTypeDataModelss.size(); i++) {
            customerTypeDataModels.add(customerTypeDataModelss.get(i));
        }

        String[] stringsCustomerType = new String[customerTypeDataModels.size()];

        for (int i = 0; i < customerTypeDataModels.size(); i++) {
            stringsCustomerType[i] = customerTypeDataModels.get(i).getCustomerType();
        }
        ((LabelledSpinner) viewFragment.findViewById(R.id.fragment_add_customer_type_spinner_id))
                .setItemsArray(stringsCustomerType);
        ((LabelledSpinner) viewFragment.findViewById(R.id.fragment_add_customer_type_spinner_id))
                .setDefaultErrorEnabled(false);
        ((LabelledSpinner) viewFragment.findViewById(R.id.fragment_add_customer_type_spinner_id))
                .setDefaultErrorText("Please select customer type");

    }

    @Override
    public void afterCustomerTypeDownlodeFail(String message, String dialogTitle, boolean isInternetError) {
        CommonFunc.commonDialog(getActivity(), dialogTitle, message, isInternetError);
    }

    @Override
    public void afterAddCustomerSuccess(String message) {
        ((AppCompatEditText) viewFragment.findViewById(R.id.fragment_add_customer_name_edittext_id)).setText("");
        ((AppCompatEditText) viewFragment.findViewById(R.id.fragment_add_customer_email_edittext_id)).setText("");
        ((AppCompatEditText) viewFragment.findViewById(R.id.fragment_add_customer_mobile_edittext_id)).setText("");
        ((LabelledSpinner) viewFragment.findViewById(R.id.fragment_add_customer_type_spinner_id)).setSelection(0);
        ((AppCompatEditText) viewFragment.findViewById(R.id.fragment_add_customer_location_edittext_id)).setText("");
        ((BaseActivity) getActivity()).attachSuccessFragment(message, "Add more customer");
    }

    @Override
    public void afterAddCustomerFail(String message, String dialogTitle, boolean isInternetError) {
        CommonFunc.commonDialog(getActivity(), dialogTitle, message, isInternetError);
    }

    ////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////// google api client establishment to access address
    ////
    private GoogleApiClient googleApiClient = null;
    private LocationRequest locationRequest = null;
    private FusedLocationProviderClient mFusedLocationClient = null;
    private AsyncTaskToGetTheAddress asyncTaskToGetTheAddress = null;

    ///
    private void connectToGoogleApiClientAndGetTheAddress() {
        if (NetworkUtils.checkInternetAndOpenDialog(getActivity())) {
            if (CommonFunc.isGooglePlayServicesAvailable(getActivity())) {
                if (dialogProgress == null) {
                    showProgressDialog();
                    connectGoogleApiClient();
                }
            }
        }
    }

    private void connectGoogleApiClient() {

        googleApiClient = new GoogleApiClient.Builder(getActivity())
                .addApi(LocationServices.API)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(@Nullable Bundle bundle) {
                        checkForLocationSettings();
                    }

                    @Override
                    public void onConnectionSuspended(int i) {
                    }
                })
                .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                        Toast.makeText(getActivity(), "Google play service not responding... please refresh your mobile",
                                Toast.LENGTH_LONG).show();
                        googleApiClient = null;
                        dismissProgressDialog();

                    }
                }).build();
        googleApiClient.connect();


    }

    private void checkForLocationSettings() {
        ////////////////////////////////////////////////////////

        locationRequest = new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);

        //**************************
        builder.setAlwaysShow(true); //this is the key ingredient
        //**************************

        Task<LocationSettingsResponse> locationSettingsResponseTask =
                LocationServices.getSettingsClient(getActivity()).checkLocationSettings(builder.build());
        //
        locationSettingsResponseTask.addOnCompleteListener(new OnCompleteListener<LocationSettingsResponse>() {
            @Override
            public void onComplete(@NonNull Task<LocationSettingsResponse> task) {
                try {
                    LocationSettingsResponse response = task.getResult(ApiException.class);
                    // All location settings are satisfied. The client can initialize location
                    // requests here.
                    if (PermissionUtils.checkLoactionPermission(getActivity()) && NetworkUtils.isConnected(getActivity())) {
                        getLastLocation();
                    } else {
                        dismissProgressDialog();
                        if (!PermissionUtils.checkLoactionPermission(getActivity())) {
                            PermissionUtils.openAppSettings(getActivity());

                        } else {
                            NetworkUtils.checkInternetAndOpenDialog(getActivity());
                        }
                    }

                } catch (ApiException exception) {
                    dismissProgressDialog();

                    switch (exception.getStatusCode()) {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // Location settings are not satisfied. But could be fixed by showing the
                            // user a dialog.
                            try {
                                NetworkUtils.openGpsSettings(getActivity());
                            } catch (ClassCastException e) {
                                CommonFunc.commonDialog(getActivity(), getString(R.string.alert),
                                        getString(R.string.justErrorCode) + " 86", false);
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            CommonFunc.commonDialog(getActivity(), "GPS not working",
                                    getString(R.string.justErrorCode) + " 87", false);
                            break;
                    }
                }
            }
        });
        //

    }

    @SuppressWarnings("MissingPermission")
    private void getLastLocation() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());
        mFusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);

    }

    private LocationCallback locationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {

            for (Location location : locationResult.getLocations()) {
                decodeAddress(new LatLng(location.getLatitude(),
                        location.getLongitude()));
                stopLocationUpdates();
            }
        }
    };

    private void decodeAddress(final LatLng latLng) {
        //https://maps.googleapis.com/maps/api/geocode/json?latlng=40.714224,-73.961452&key=YOUR_API_KEY
        String req = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" +
                latLng.latitude + "," + latLng.longitude + "&key=" + getString(R.string.google_key);


        if (asyncTaskToGetTheAddress != null) {
            if (asyncTaskToGetTheAddress.getStatus() != AsyncTask.Status.RUNNING) {
                asyncTaskToGetTheAddress = new AsyncTaskToGetTheAddress();
                asyncTaskToGetTheAddress.execute(req);
            }
        } else {
            asyncTaskToGetTheAddress = new AsyncTaskToGetTheAddress();
            asyncTaskToGetTheAddress.execute(req);
        }
    }

    private class AsyncTaskToGetTheAddress extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... strings) {
            return makeserverConnection(strings[0]);
        }

        @Override
        protected void onPostExecute(String s) {
            dismissProgressDialog();
            if (s != null) {
                try {
                    JSONObject jsonObject = new JSONObject(s);
                    JSONArray jsonArray = jsonObject.getJSONArray("results");
                    if (jsonArray.length() > 0) {
                        String formatedAddress = jsonArray.getJSONObject(0).getString("formatted_address");
                        if (formatedAddress != null && !formatedAddress.isEmpty()) {
                            ((AppCompatEditText) viewFragment.findViewById(R.id.fragment_add_customer_location_edittext_id)).setText(formatedAddress);
                        } else {
                            CommonFunc.commonDialog(getActivity(), "Alert!", "Unable to detect your location", false);
                        }

                    } else {
                        CommonFunc.commonDialog(getActivity(), "Alert!", "Unable to detect your location", false);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                CommonFunc.commonDialog(getActivity(), "Alert!", "Unable to detect your location", false);
            }
        }


    }

    public String makeserverConnection(String requestURL) {
        URL url;
        String response = "";
        try {
            url = new URL(requestURL);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded");
            conn.setDoOutput(true);
            int responseCode = conn.getResponseCode();
            if (responseCode == HttpsURLConnection.HTTP_OK) {

                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line = br.readLine()) != null) {
                    response += line;
                }
            } else {
                response = "";
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    private void stopLocationUpdates() {
        if (mFusedLocationClient != null)
            mFusedLocationClient.removeLocationUpdates(locationCallback);
    }
    /////////////////////////////////////////////////////////////////////////
}
