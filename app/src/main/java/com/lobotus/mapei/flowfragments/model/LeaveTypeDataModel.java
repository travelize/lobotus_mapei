package com.lobotus.mapei.flowfragments.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by user1 on 09-11-2017.
 */

public class LeaveTypeDataModel {

    @SerializedName("LeaveTypeId")
    @Expose
    private Integer leaveTypeId;
    @SerializedName("LeaveType")
    @Expose
    private String leaveType;

    public LeaveTypeDataModel(Integer leaveTypeId, String leaveType) {
        this.leaveTypeId = leaveTypeId;
        this.leaveType = leaveType;
    }

    public Integer getLeaveTypeId() {
        return leaveTypeId;
    }

    public void setLeaveTypeId(Integer leaveTypeId) {
        this.leaveTypeId = leaveTypeId;
    }

    public String getLeaveType() {
        return leaveType;
    }

    public void setLeaveType(String leaveType) {
        this.leaveType = leaveType;
    }
}
