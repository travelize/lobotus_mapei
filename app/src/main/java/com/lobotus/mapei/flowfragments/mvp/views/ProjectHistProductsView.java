package com.lobotus.mapei.flowfragments.mvp.views;

import com.lobotus.mapei.flowfragments.model.ProductsDataModel;

import java.util.List;

/**
 * Created by user1 on 16-10-2017.
 */

public interface ProjectHistProductsView {
    void showProgressDialog();
    void dismissProgressDialog();
    ///////////////////////////////////////////// after products downlode
    void afterProductsModelDownlodeSuccess(List<ProductsDataModel> productsDataModels);
    void afterProductsModelDoenlodeFail(String message, String dialogTitle, boolean isInternetError);
}
