package com.lobotus.mapei.flowfragments.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.telephony.PhoneNumberUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.lobotus.mapei.R;
import com.lobotus.mapei.baseactivity.BaseActivity;
import com.lobotus.mapei.flowfragments.fragments.AddProjects;
import com.lobotus.mapei.flowfragments.fragments.ProjectHistProductsFrag;
import com.lobotus.mapei.flowfragments.model.HistoryData;
import com.lobotus.mapei.flowfragments.model.HistoryProjectData;
import com.lobotus.mapei.utils.CommonFunc;
import com.lobotus.mapei.utils.PermissionUtils;

import java.util.ArrayList;
import java.util.LinkedList;

/**
 * Created by user1 on 16-10-2017.
 */

public class ProjectHistoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<HistoryProjectData> projectDatas=null;
    private LinkedList<HistoryData> historyDatas=null;
    private Context context=null;
    /////
    private final int SHOW_ONLY_PROJECT_DATA=9;
    private final int MEEETING_TYPE_FOLLOWUP_FOR_ORDERS=10;
    private final int MEEETING_TYPE_AR_FOLLOWUP=11;
    private final int MEEETING_TYPE_COMPLAINTS=12;


    public ProjectHistoryAdapter(ArrayList<HistoryProjectData> projectDatas,
                                 LinkedList<HistoryData> historyDatas,
                                 Context context) {
        this.projectDatas = projectDatas;
        this.historyDatas = historyDatas;
        this.context = context;
    }

    @Override
    public int getItemViewType(int position) {
        if (historyDatas.get(position)!=null)
            switch (historyDatas.get(position).getMeetingTypeID())
            {
                case 1:
                    return MEEETING_TYPE_FOLLOWUP_FOR_ORDERS;
                case 2:
                    return MEEETING_TYPE_AR_FOLLOWUP;
                case 3:
                    return MEEETING_TYPE_COMPLAINTS;
                default:return 0;
            }
        else
            return SHOW_ONLY_PROJECT_DATA;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType==MEEETING_TYPE_FOLLOWUP_FOR_ORDERS)
        {
            View view= LayoutInflater.from(context).inflate(R.layout.follow_up_order_row,parent,false);
            return new FollowForOrderHolder(view);
        }else if (viewType==MEEETING_TYPE_AR_FOLLOWUP)
        {
            View view= LayoutInflater.from(context).inflate(R.layout.ar_followup_row,parent,false);
            return new ARfollowForOrderHolder(view);
        }else if (viewType==MEEETING_TYPE_COMPLAINTS)
        {
            View view= LayoutInflater.from(context).inflate(R.layout.complaints_row,parent,false);
            return new ComplaintsHolder(view);
        }else if (viewType==SHOW_ONLY_PROJECT_DATA) {
            View view= LayoutInflater.from(context).inflate(R.layout.project_data_row,parent,false);
            return new ProjectNameHolder(view);
        }else {
            return null;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {


        if (holder!=null)
        {
            if (holder instanceof ProjectNameHolder)
            {
                ProjectNameHolder projectNameHolder= (ProjectNameHolder) holder;
                projectNameHolder.textViewCustomerName.setText(projectDatas.get(0).getCustomerName());
                projectNameHolder.textViewProjectName.setText(projectDatas.get(0).getProjectName());
                projectNameHolder.textViewCustomerPhNo.setText(projectDatas.get(0).getCustomerPhone());
                projectNameHolder.textViewCustomerType.setText(projectDatas.get(0).getCustomerType());
                projectNameHolder.textViewLocation.setText(projectDatas.get(0).getLocation());
                //
                projectNameHolder.textViewCustomerPhNo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (PermissionUtils.checkCallPermission(context))
                        {
                            try {
                                String number = "tel:" +projectDatas.get(0).getCustomerPhone();
                                Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse(number));
                                context.startActivity(callIntent);
                            }catch (Exception e)
                            {
                                CommonFunc.commonDialog(context,context.getString(R.string.alert),"Invalid phone no",false);
                            }

                        }else {
                            PermissionUtils.openPermissionDialog(context,"Alert!","Please grant Phone permission to launch a call");
                        }

                    }
                });


            }

            if (holder instanceof FollowForOrderHolder)
            {
                final FollowForOrderHolder followForOrderHolder= (FollowForOrderHolder) holder;

                //
                final HistoryData historyData=historyDatas.get(holder.getAdapterPosition());
                followForOrderHolder.textViewMeetingType.setText(historyData.getMeetingType());
                followForOrderHolder.textViewOrderValue.setText(""+historyData.getOrderValue());
                followForOrderHolder.textViewViewProducts.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Bundle bundle=new Bundle();
                        bundle.putString("MeetingID",historyData.getMeetingID());
                        ((BaseActivity)context).attachFragment(new ProjectHistProductsFrag(),
                                true,"Ordered Products",bundle);
                    }
                });
                followForOrderHolder.textViewOrderLost.setText(""+historyData.getOrderLost());
                followForOrderHolder.textViewNotes.setText(historyData.getRemarks());
                //
                followForOrderHolder.textViewVisitNo.setText("Visit No : "+holder.getAdapterPosition());
                followForOrderHolder.textViewVisitDate.setText("Date : "+historyData.getOnDate());
                ///

            }

            if (holder instanceof  ARfollowForOrderHolder)
            {
                ARfollowForOrderHolder aRfollowForOrderHolder= (ARfollowForOrderHolder) holder;

                //
                HistoryData historyData=historyDatas.get(holder.getAdapterPosition());
                aRfollowForOrderHolder.textViewMeetingType.setText(historyData.getMeetingType());

                switch (historyData.getFollowupTypeID())
                {
                    case 1:
                        aRfollowForOrderHolder.textViewData1Title.setText("New Date");
                        aRfollowForOrderHolder.textViewData2Title.setText("Project issue");
                        aRfollowForOrderHolder.textViewData2Title.setText("Project Delay");
                        //
                        aRfollowForOrderHolder.textViewData1value.setText(historyData.getPaymentDate());
                        aRfollowForOrderHolder.textViewData2value.setText(historyData.getProductIssues());
                        aRfollowForOrderHolder.textViewData3value.setText(historyData.getProductDelay());
                        break;
                    case 2:
                        aRfollowForOrderHolder.textViewData1Title.setText("Collected");
                        aRfollowForOrderHolder.textViewData2Title.setText("New date");
                        aRfollowForOrderHolder.textViewData2Title.setText("Not traceable");
                        //
                        aRfollowForOrderHolder.textViewData1value.setText(historyData.getCFormCollected());
                        aRfollowForOrderHolder.textViewData2value.setText(historyData.getCFormNewDate());
                        aRfollowForOrderHolder.textViewData3value.setText(historyData.getCFNotTraceable());
                        break;
                    case 3:
                        aRfollowForOrderHolder.textViewData1Title.setText("Missing invoice");
                        aRfollowForOrderHolder.textViewData2Title.setText("Short payment");
                        aRfollowForOrderHolder.textViewData2Title.setText("Statement");
                        //
                        aRfollowForOrderHolder.textViewData1value.setText(historyData.getMissingInvoices());
                        aRfollowForOrderHolder.textViewData2value.setText(historyData.getShortPayments());
                        aRfollowForOrderHolder.textViewData3value.setText(historyData.getStatement());
                }
                aRfollowForOrderHolder.textViewNotes.setText(historyData.getRemarks());
                //
                //
                aRfollowForOrderHolder.textViewVisitNo.setText("Visit No : "+holder.getAdapterPosition());
                aRfollowForOrderHolder.textViewVisitDate.setText("Date : "+historyData.getOnDate());
            }

            if (holder instanceof ComplaintsHolder )
            {
                ComplaintsHolder complaintsHolder= (ComplaintsHolder) holder;
                //
                HistoryData historyData=historyDatas.get(holder.getAdapterPosition());
                complaintsHolder.textViewMeetingType.setText(historyData.getMeetingType());
                complaintsHolder.textViewNotes.setText(historyData.getRemarks());
                complaintsHolder.textViewMeetingType.setText(historyData.getMeetingType());
                //
                complaintsHolder.textViewVisitNo.setText("Visit No : "+holder.getAdapterPosition());
                complaintsHolder.textViewVisitDate.setText("Date : "+historyData.getOnDate());


            }
        }


    }

    @Override
    public int getItemCount() {
        return historyDatas.size();
    }



    private class ARfollowForOrderHolder extends RecyclerView.ViewHolder
    {

        ///
        private AppCompatTextView textViewData1Title=null,textViewData2Title=null,textViewData3Title=null;
        private AppCompatTextView textViewData1value=null,textViewData2value=null,textViewData3value=null,textViewNotes=null;
        private AppCompatTextView  textViewMeetingType=null;
        //
        private AppCompatTextView textViewVisitNo=null,textViewVisitDate=null;


        public ARfollowForOrderHolder(View itemView) {
            super(itemView);
            this.textViewMeetingType=itemView.findViewById(R.id.arfollow_up_order_row_meetingType);
            //
            this.textViewData1Title=itemView.findViewById(R.id.arfollow_up_order_row_data1_title);
            this.textViewData2Title=itemView.findViewById(R.id.arfollow_up_order_row_data2_title);
            this.textViewData3Title=itemView.findViewById(R.id.arfollow_up_order_row_data3_title);
            //
            this.textViewData1value=itemView.findViewById(R.id.arfollow_up_order_row_data1_value);
            this.textViewData2value=itemView.findViewById(R.id.arfollow_up_order_row_data2_value);
            this.textViewData3value=itemView.findViewById(R.id.arfollow_up_order_row_data3_value);
            //
            this.textViewNotes=itemView.findViewById(R.id.arfollow_up_order_row_notes);
            //
            this.textViewVisitNo=itemView.findViewById(R.id.arfollow_up_order_row_visit_no);
            this.textViewVisitDate=itemView.findViewById(R.id.arfollow_up_order_row_meet_date);

        }
    }

    private class FollowForOrderHolder extends RecyclerView.ViewHolder
    {
        private AppCompatTextView textViewMeetingType=null;
        ///
        private AppCompatTextView textViewOrderValue=null,textViewViewProducts=null,textViewOrderLost=null,textViewNotes=null;
        //
        private AppCompatTextView textViewVisitNo=null,textViewVisitDate=null;


        public FollowForOrderHolder(View itemView) {
            super(itemView);
            this.textViewMeetingType=itemView.findViewById(R.id.follow_up_order_row_meetingType);
            //
            this.textViewOrderValue=itemView.findViewById(R.id.follow_up_order_row_value);
            this.textViewViewProducts=itemView.findViewById(R.id.follow_up_order_row_view_products);
            this.textViewOrderLost=itemView.findViewById(R.id.follow_up_order_row_order_lost);
            this.textViewNotes=itemView.findViewById(R.id.follow_up_order_row_notes);
            //
            this.textViewVisitNo=itemView.findViewById(R.id.follow_up_order_row_visit_no);
            this.textViewVisitDate=itemView.findViewById(R.id.follow_up_order_row_meet_date);


        }
    }

    private class ComplaintsHolder extends RecyclerView.ViewHolder
    {

        ///
        private AppCompatTextView textViewComplaintType=null,textViewNotes=null;
        private AppCompatTextView textViewMeetingType=null;
        private AppCompatTextView textViewVisitNo=null,textViewVisitDate=null;


        ComplaintsHolder(View itemView) {
            super(itemView);
            this.textViewMeetingType=itemView.findViewById(R.id.complaints_row_meetingType);
            //
            this.textViewComplaintType=itemView.findViewById(R.id.complaints_row_complaints_type);
            //
            this.textViewNotes=itemView.findViewById(R.id.complaints_row_complaints_notes);
            //
            this.textViewVisitNo=itemView.findViewById(R.id.complaints_row_visit_no);
            this.textViewVisitDate=itemView.findViewById(R.id.complaints_row_meet_date);

        }
    }

    private class ProjectNameHolder extends RecyclerView.ViewHolder
    {
        private AppCompatTextView textViewProjectName=null,textViewCustomerName=null,textViewCustomerPhNo=null,
                textViewCustomerType=null,textViewLocation=null;
        public ProjectNameHolder(View itemView) {
            super(itemView);
            this.textViewProjectName=itemView.findViewById(R.id.project_data_row_projectName);
            this.textViewCustomerName=itemView.findViewById(R.id.project_data_row_customerName);
            this.textViewCustomerPhNo=itemView.findViewById(R.id.project_data_row_custPhNo);
            this.textViewCustomerType=itemView.findViewById(R.id.project_data_row_customerType);
            this.textViewLocation=itemView.findViewById(R.id.project_data_row_customerLocation);

        }
    }




}
