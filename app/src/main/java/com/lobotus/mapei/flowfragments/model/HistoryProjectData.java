package com.lobotus.mapei.flowfragments.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by user1 on 16-10-2017.
 */

public class HistoryProjectData {
    @SerializedName("ProjectID")
    @Expose
    private String projectID;
    @SerializedName("ProjectName")
    @Expose
    private String projectName;
    @SerializedName("CustomerName")
    @Expose
    private String customerName;
    @SerializedName("CustomerPhone")
    @Expose
    private String customerPhone;
    @SerializedName("CustomerType")
    @Expose
    private String customerType;
    @SerializedName("Location")
    @Expose
    private String location;

    public String getProjectID() {
        return projectID;
    }

    public void setProjectID(String projectID) {
        this.projectID = projectID;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerPhone() {
        return customerPhone;
    }

    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

}
