package com.lobotus.mapei.flowfragments.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by user1 on 16-10-2017.
 */

public class ProjectHistoryModel {
    @SerializedName("Success")
    @Expose
    private Integer success;
    @SerializedName("ProjectData")
    @Expose
    private List<HistoryProjectData> projectData = null;
    @SerializedName("History")
    @Expose
    private List<HistoryData> history = null;
    @SerializedName("Msg")
    @Expose
    private String errorMsg=null;

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public List<HistoryProjectData> getProjectData() {
        return projectData;
    }

    public void setProjectData(List<HistoryProjectData> projectData) {
        this.projectData = projectData;
    }

    public List<HistoryData> getHistory() {
        return history;
    }

    public void setHistory(List<HistoryData> history) {
        this.history = history;
    }
}
