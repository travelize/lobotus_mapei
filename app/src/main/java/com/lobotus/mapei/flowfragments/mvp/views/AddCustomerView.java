package com.lobotus.mapei.flowfragments.mvp.views;

import com.lobotus.mapei.flowfragments.model.CustomerTypeDataModel;

import java.util.ArrayList;

/**
 * Created by user1 on 08-11-2017.
 */

public interface AddCustomerView {

    void showProgressDialog();
    void dismissProgressDialog();
    ///////////////////////////////////////////////// validate
    void afterAddCustomerValidationSuccess();
    /////////////////////////////////////////////////////  customer type downlode
    void afterCustomerTypeDownlodeSuccess(ArrayList<CustomerTypeDataModel> customerTypeDataModels);
    void afterCustomerTypeDownlodeFail(String message, String dialogTitle, boolean isInternetError);
    ////////////////////////////////////////////////////// add customer post req
    void afterAddCustomerSuccess(String message);
    void afterAddCustomerFail(String message, String dialogTitle, boolean isInternetError);
}
