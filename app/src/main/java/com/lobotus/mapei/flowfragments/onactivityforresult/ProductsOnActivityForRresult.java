package com.lobotus.mapei.flowfragments.onactivityforresult;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.lobotus.mapei.R;
import com.lobotus.mapei.flowfragments.fragments.AddProjects;
import com.lobotus.mapei.flowfragments.model.ProductsDataModel;
import com.lobotus.mapei.utils.CommonFunc;

import java.util.ArrayList;

public class ProductsOnActivityForRresult extends AppCompatActivity implements  android.widget.SearchView.OnQueryTextListener {



    private SelectProductsAdapter adapter=null;
    private ArrayList<ProductsDataModel> productsDataModels=new ArrayList<>();
    private ArrayList<ProductsDataModel> productsDataModelsInitialy=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products_on_for_rresult);
        Toolbar toolbar=findViewById(R.id.sel_products_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddProjects.productsDataModels.clear();
                AddProjects.productsDataModels.addAll(productsDataModelsInitialy);
                submitTheResult();
            }
        });
        findViewById(R.id.sel_products_submit_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitTheResult();
            }
        });
//////////////////
        android.widget.SearchView searchView= (android.widget.SearchView) findViewById(R.id.sel_products_searchview);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            searchView.setQueryHint(Html.fromHtml("<font color = #ffffff>" + "Search by product name" + "</font>",0));
        }else {
            searchView.setQueryHint(Html.fromHtml("<font color = #ffffff>" + "Search by product name" + "</font>"));
        }

        int id = searchView.getContext()
                .getResources()
                .getIdentifier("android:id/search_src_text", null, null);
        TextView textView = (TextView) searchView.findViewById(id);
        textView.setTextColor(Color.WHITE);



/////////////////////////////////////////
        productsDataModels.addAll(AddProjects.productsDataModels);
        productsDataModelsInitialy.addAll(AddProjects.productsDataModels);
        ((android.widget.SearchView)findViewById(R.id.sel_products_searchview)).setOnQueryTextListener(this);
        setTheAdapter();

    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        productsDataModels.clear();
        for (int i=0;i<AddProjects.productsDataModels.size();i++)
        {
            if (AddProjects.productsDataModels.get(i).getProductName().toLowerCase().contains(newText.toLowerCase()))
            {
                productsDataModels.add(AddProjects.productsDataModels.get(i));
            }
        }
        if (adapter!=null)
        {
            adapter.notifyDataSetChanged();
        }else {
            setTheAdapter();
        }


        return false;
    }
    private void setTheAdapter()
    {
        ((RecyclerView)findViewById(R.id.sel_products_recycleview_id)).
                setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        adapter=new SelectProductsAdapter();
        ((RecyclerView)findViewById(R.id.sel_products_recycleview_id)).setAdapter(adapter);
    }
    private void submitTheResult()
    {
        CommonFunc.hideTheKeypad(ProductsOnActivityForRresult.this,findViewById(R.id.sel_products_searchview));
        setResult(RESULT_OK);
        finish();
    }


//////////////////////////////////////////////////////////////////////////////////////////////////// add products adapter

    private class SelectProductsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view= LayoutInflater.from(ProductsOnActivityForRresult.this).inflate(R.layout.search_products_row,parent,false);
            return new Holder(view);
        }

        @Override
        public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
            final Holder holder1= (Holder) holder;
            holder1.textView.setText(productsDataModels.get(holder1.getAdapterPosition()).getProductName());
            holder1.layoutCompat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (productsDataModels.get(holder1.getAdapterPosition()).isChecked())
                    {
                        AddProjects.productsDataModels.get(holder1.getAdapterPosition()).setChecked(false);
                        productsDataModels.get(holder1.getAdapterPosition()).setChecked(false);
//                        holder1.appCompatCheckBox.setChecked(false);

                    }else {
                        AddProjects.productsDataModels.get(holder1.getAdapterPosition()).setChecked(true);
                        productsDataModels.get(holder1.getAdapterPosition()).setChecked(true);
//                        holder1.appCompatCheckBox.setChecked(true);
                    }


                    notifyDataSetChanged();

                }
            });
            if (productsDataModels.get(holder1.getAdapterPosition()).isChecked())
            {
                holder1.appCompatCheckBox.setChecked(true);
            }else {
                holder1.appCompatCheckBox.setChecked(false);
            }

        }

        @Override
        public int getItemCount() {
            return productsDataModels.size();
        }

        private class Holder extends RecyclerView.ViewHolder
        {
            AppCompatTextView textView=null;
            LinearLayoutCompat layoutCompat=null;
            AppCompatCheckBox appCompatCheckBox=null;
             Holder(View itemView) {
                super(itemView);
                this.textView= itemView.findViewById(R.id.search_products_row_textview_id);
                this.layoutCompat= itemView.findViewById(R.id.search_products_row_linear);
                this.appCompatCheckBox=itemView.findViewById(R.id.search_products_row_checkbox_id);
            }
        }
    }

}
