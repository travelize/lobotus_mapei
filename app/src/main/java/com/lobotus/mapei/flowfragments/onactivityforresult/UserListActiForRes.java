package com.lobotus.mapei.flowfragments.onactivityforresult;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;
import android.widget.TextView;

import com.lobotus.mapei.R;
import com.lobotus.mapei.flowfragments.model.ComplaintDataTypeModel;
import com.lobotus.mapei.flowfragments.model.CustomerDataModel;
import com.lobotus.mapei.flowfragments.model.ProductsDataModel;
import com.lobotus.mapei.flowfragments.model.ProjectsDataModel;
import com.lobotus.mapei.flowfragments.model.UsersDataModel;
import com.lobotus.mapei.flowfragments.mvp.presentor.AssignMeetingPresentor;
import com.lobotus.mapei.flowfragments.mvp.presentorimp.AssignMeetingPresentorImp;
import com.lobotus.mapei.flowfragments.mvp.views.AssignMeetingsView;
import com.lobotus.mapei.utils.CommonFunc;
import com.lobotus.mapei.utils.PreferenceUtils;

import java.util.ArrayList;
import java.util.List;

public class UserListActiForRes extends AppCompatActivity implements AssignMeetingsView,
        android.widget.SearchView.OnQueryTextListener {

    private AssignMeetingPresentor assignMeetingPresentor = null;
    //
    private ArrayList<UsersDataModel> userDataModelsGlobel = new ArrayList<>();
    private ArrayList<UsersDataModel> userDataModelsInAdapter = new ArrayList<>();
    private CustomerAdapter customerAdapter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_act_for_result);
        Toolbar toolbar = findViewById(R.id.customer_for_act_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        SearchView searchView = (SearchView) findViewById(R.id.customer_for_act_searchview);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            searchView.setQueryHint(Html.fromHtml("<font color = #ffffff>" + "Search by user name" + "</font>", 0));
        } else {
            searchView.setQueryHint(Html.fromHtml("<font color = #ffffff>" + "Search by user name" + "</font>"));
        }

        int id = searchView.getContext()
                .getResources()
                .getIdentifier("android:id/search_src_text", null, null);
        TextView textView = (TextView) searchView.findViewById(id);
        textView.setTextColor(Color.WHITE);
        ((SearchView) findViewById(R.id.customer_for_act_searchview)).setOnQueryTextListener(this);
        assignMeetingPresentor = new AssignMeetingPresentorImp(this);
        assignMeetingPresentor.reqToDownlodeUserData(UserListActiForRes.this, "2",
                PreferenceUtils.getUserIdFromPreference(UserListActiForRes.this));

    }

    @Override
    public boolean onQueryTextSubmit(String query) {


        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        userDataModelsInAdapter.clear();
        for (int i = 0; i < userDataModelsGlobel.size(); i++) {
            if (userDataModelsGlobel.get(i).getFullName().toLowerCase().contains(newText.toLowerCase()))
                userDataModelsInAdapter.add(userDataModelsGlobel.get(i));
        }
        if (customerAdapter != null) {
            customerAdapter.notifyDataSetChanged();
        } else {
            customerAdapter = new CustomerAdapter();
            ((RecyclerView) findViewById(R.id.customer_for_act_recycleview_id)).
                    setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
            ((RecyclerView) findViewById(R.id.customer_for_act_recycleview_id)).setAdapter(customerAdapter);
        }

        return false;
    }


    private Dialog dialogProgress = null;

    @Override
    public void showProgressDialog() {
        dialogProgress = new Dialog(UserListActiForRes.this);
        dialogProgress.setCancelable(false);
        dialogProgress.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialogProgress.setContentView(R.layout.loaging_layout);
        dialogProgress.show();
    }

    @Override
    public void dismissProgressDialog() {
        if (dialogProgress != null) {
            if (dialogProgress.isShowing()) {
                dialogProgress.dismiss();
            }
            dialogProgress = null;
        }
    }

    @Override
    public void afterUserListDownlodeSuccess(ArrayList<UsersDataModel> usersDataModels) {
        userDataModelsGlobel.clear();
        userDataModelsGlobel.addAll(usersDataModels);
        userDataModelsInAdapter.addAll(usersDataModels);
        ((RecyclerView) findViewById(R.id.customer_for_act_recycleview_id)).
                setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        customerAdapter = new CustomerAdapter();
        ((RecyclerView) findViewById(R.id.customer_for_act_recycleview_id)).setAdapter(customerAdapter);
    }

    @Override
    public void afterUserListDownlodeFail(String message, String dialogTitle, boolean isInternetError) {
        CommonFunc.commonDialog(UserListActiForRes.this, dialogTitle, message, isInternetError);
    }

    @Override
    public void afterCustomerNameDownlodeSuccess(ArrayList<CustomerDataModel> customerDataModels) {
        // no use
    }

    @Override
    public void afterCistomerNameDownlodeFail(String message, String dialogTitle, boolean isInternetError) {
        // no use
    }

    @Override
    public void afterProjectsListDownlodeSuccess(ArrayList<ProjectsDataModel> projectsDataModels) {
        // no use
    }

    @Override
    public void afterProjectsListDownlodeFail(String message, String dialogTitle, boolean isInternetError) {
        // no use
    }

    @Override
    public void afterAssignMeetingSuccess(String message) {
        // no use
    }

    @Override
    public void afterAssignMeetingFail(String message, String dialogTitle, boolean isInternetError) {
        // no use
    }

    @Override
    public void afterComplaintsDownlodeSuccess(ArrayList<ComplaintDataTypeModel> complaintDataTypeModelss) {
       /* complaintDataTypeModels.clear();
        complaintDataTypeModels.addAll(complaintDataTypeModelss);
        inflateComplateTypesSpinner();*/
    }

    @Override
    public void afterComplaintsDownloldeFail(String message, String dialogTitle, boolean isInternetError) {
//        CommonFunc.commonDialog(getActivity(), dialogTitle, message, isInternetError);
    }

    @Override
    public void afterProductsModelDownlodeSuccess(List<ProductsDataModel> productsDataModels) {

    }

    @Override
    public void afterProductsModelDoenlodeFail(String message, String dialogTitle, boolean isInternetError) {

    }

    @Override
    public void afterValidationOfMeetingSuccessfull() {

    }

    @Override
    public void afterMeetingPostSuccess(String message, String meetingId) {

    }

    @Override
    public void afterMeetingPostFail(String message, String dialogTitle, boolean isInternetError) {

    }


    ///////////// adapter. . .
    public class CustomerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

            View view = LayoutInflater.from(UserListActiForRes.this).inflate(R.layout.selection_row, parent, false);
            return new SelectionHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, int position) {
            ((SelectionHolder) holder).textView.setText("" + userDataModelsInAdapter.get(
                    holder.getAdapterPosition()).getFullName());
            ((SelectionHolder) holder).textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent();
                    intent.putExtra("user_model", userDataModelsInAdapter.get(holder.getAdapterPosition()));
                    setResult(RESULT_OK, intent);
                    finish();
                }
            });
        }

        @Override
        public int getItemCount() {
            return userDataModelsInAdapter.size();
        }

        private class SelectionHolder extends RecyclerView.ViewHolder {
            private AppCompatTextView textView = null;

            public SelectionHolder(View itemView) {
                super(itemView);
                this.textView = itemView.findViewById(R.id.selection_row_textview_id);
            }
        }
    }
}
