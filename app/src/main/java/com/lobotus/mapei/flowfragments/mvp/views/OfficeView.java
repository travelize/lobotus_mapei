package com.lobotus.mapei.flowfragments.mvp.views;

/**
 * Created by user1 on 06-10-2017.
 */

public interface OfficeView {
    void showProgressDialog();
    void dismissProgressDialog();
    //////////////////////////////////////// after recording the office visit
    void afterRecordingTheOfficeVisitSuccess(String message);
    void afterRecordingTheOfficeVisitFail(String message, String dialogTitle, boolean isInternetError);
    //////////////////////// office check in and office check out
    void afterOfficeCheckInSuccess(String message);
    void afterOfficeCheckInFail(String message, String dialogTitle, boolean isInternetError);
    void afterOfficeCheckOutSuccess(String message);
    void afterOfficeCheckOutFail(String message, String dialogTitle, boolean isInternetError);
}
