package com.lobotus.mapei.flowfragments.mvp.intrectorimp;

import android.content.Context;

import com.lobotus.mapei.R;
import com.lobotus.mapei.flowfragments.model.LeaveTypeDataModel;
import com.lobotus.mapei.flowfragments.model.LeaveTypeModel;
import com.lobotus.mapei.flowfragments.mvp.intrector.LeaveApplyInteractor;
import com.lobotus.mapei.retrofitapi.APIService;
import com.lobotus.mapei.retrofitapi.ApiUtils;
import com.lobotus.mapei.utils.CommonFunc;
import com.lobotus.mapei.utils.ConstantsUtils;
import com.lobotus.mapei.utils.PreferenceUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by user1 on 04-10-2017.
 */

public class LeaveApplyInteractorImp implements LeaveApplyInteractor {


    @Override
    public void reqToPostDownlodeLeaveTypesFromIntractor(final Context context, final OnLeaveTypePostLisner onLeaveTypePostLisner) {

        APIService apiService=ApiUtils.getAPIService(ConstantsUtils.postBaseUrl+ConstantsUtils.postToGetTheLeaveTypeList);
        apiService.postToGetTheLeaveTypeList("").enqueue(new Callback<LeaveTypeModel>() {
            @Override
            public void onResponse(Call<LeaveTypeModel> call, Response<LeaveTypeModel> response) {
                if (response.isSuccessful())
                {
                        if (response.body()!=null)
                        {
                            if (response.body().getSuccess()==1)
                            {
                              if (response.body().getData()!=null)
                              {
                                  if (response.body().getData().size()>0)
                                  {
                                      onLeaveTypePostLisner.OnLeaveTypePostSuccess((ArrayList<LeaveTypeDataModel>) response.body().getData());
                                  }else {
                                      onLeaveTypePostLisner.OnLeaveTypePostFail(
                                              context.getString(R.string.justErrorCode)+" 100",context.getString(R.string.internalError),
                                              false);
                                  }
                              }else {
                                  onLeaveTypePostLisner.OnLeaveTypePostFail(
                                          context.getString(R.string.justErrorCode)+" 99",context.getString(R.string.internalError),
                                          false);
                              }
                            }else {
                                onLeaveTypePostLisner.OnLeaveTypePostFail(
                                        response.body().getMsg(),context.getString(R.string.alert),
                                        false);
                            }
                        }else {
                            onLeaveTypePostLisner.OnLeaveTypePostFail(
                                    context.getString(R.string.justErrorCode)+" 98",context.getString(R.string.internalError),
                                    false);
                        }
                }else {
                    onLeaveTypePostLisner.OnLeaveTypePostFail(
                            context.getString(R.string.justErrorCode)+" 97",context.getString(R.string.ServerNotresponding),
                            false);
                }
            }

            @Override
            public void onFailure(Call<LeaveTypeModel> call, Throwable t) {
                onLeaveTypePostLisner.OnLeaveTypePostFail(
                        context.getString(R.string.noInternetMessage)+" 96",
                        context.getString(R.string.noInternetAlert),
                        true);
            }
        });

    }

    @Override
    public void reqToPostLeaveApplyFromInteractor(Context context,
                                                  String leaveType,String fromDate,String toDate,
                                                  String remark,OnLeaveApplyLisner onLeaveApplyLisner) {

        if (PreferenceUtils.checkUserisLogedin(context))
        {
            postTheLeaveToServer(context,leaveType,fromDate,toDate,remark,onLeaveApplyLisner);
        }else {
            CommonFunc.commonDialog(context,"Alert!","Session Lost Login back",false);
        }

    }

    private void postTheLeaveToServer(final Context context,
                                      String leaveType, String fromDate, String toDate,
                                      final String remark, final OnLeaveApplyLisner onLeaveApplyLisner)
    {
        if (toDate==null)
            toDate=fromDate;
        if (toDate.isEmpty())
            toDate=fromDate;
        APIService apiService= ApiUtils.getAPIService(ConstantsUtils.postBaseUrl+ConstantsUtils.postToApplyForLeaves);
     //   if (sameDayLeaveTypeIfAny==null) // diff day. . . .
      //  {
            apiService.postReqToApplyForLeaveDiffDay(PreferenceUtils.getUserIdFromPreference(context),
                    leaveType,fromDate,toDate,remark,"Requested").enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.isSuccessful())
                    {
                        try {
                            JSONObject jsonObject=new JSONObject(response.body().string());
                            if (jsonObject.getInt("Success")==1)
                            {
                                onLeaveApplyLisner.OnLeaveApplySuccess(
                                        jsonObject.getString("Msg"),context.getString(R.string.alert),
                                        false);
                            }else {
                                onLeaveApplyLisner.OnLeaveApplyFail(
                                        jsonObject.getString("Msg"),context.getString(R.string.alert),
                                        false);
                            }
                        } catch (JSONException | IOException e) {
                            onLeaveApplyLisner.OnLeaveApplyFail(
                                    context.getString(R.string.justErrorCode)+" 12",context.getString(R.string.internalError),
                                    false);
                        }
                    }else {
                        onLeaveApplyLisner.OnLeaveApplyFail(
                                context.getString(R.string.justErrorCode)+" 11",context.getString(R.string.ServerNotresponding),
                                false);
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    onLeaveApplyLisner.OnLeaveApplyFail(
                            context.getString(R.string.noInternetMessage)+" 13",
                            context.getString(R.string.noInternetAlert),
                            true);
                }
            });
       /* }else { // same day
            apiService.postReqToApplyForLeaveSameDay(PreferenceUtils.getUserIdFromPreference(context),
                    leaveType,fromDate,toDate,remark,"Requested",sameDayLeaveTypeIfAny).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.isSuccessful())
                    {
                        try {
                            JSONObject jsonObject=new JSONObject(response.body().string());
                            if (jsonObject.getInt("Success")==1)
                            {
                                onLeaveApplyLisner.OnLeaveApplySuccess(
                                        jsonObject.getString("Msg"),context.getString(R.string.alert),
                                        false);
                            }else {
                                onLeaveApplyLisner.OnLeaveApplyFail(
                                        jsonObject.getString("Msg"),context.getString(R.string.alert),
                                        false);
                            }
                        } catch (JSONException | IOException e) {
                            onLeaveApplyLisner.OnLeaveApplyFail(
                                    context.getString(R.string.justErrorCode)+" 12",context.getString(R.string.internalError),
                                    false);
                        }
                    }else {
                        onLeaveApplyLisner.OnLeaveApplyFail(
                                context.getString(R.string.justErrorCode)+" 11",context.getString(R.string.ServerNotresponding),
                                false);
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    onLeaveApplyLisner.OnLeaveApplyFail(
                            context.getString(R.string.noInternetMessage)+" 13",
                            context.getString(R.string.noInternetAlert),
                            true);
                }
            });
        }*/


    }
}
