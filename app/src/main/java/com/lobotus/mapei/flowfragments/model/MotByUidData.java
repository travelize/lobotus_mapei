package com.lobotus.mapei.flowfragments.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by User on 27-02-2018.
 */

public class MotByUidData {

    @SerializedName("UserId")
    @Expose
    private String userId;
    @SerializedName("Mode")
    @Expose
    private String mode;
    @SerializedName("Type")
    @Expose
    private String type;
    @SerializedName("Vehicle")
    @Expose
    private String vehicle;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getVehicle() {
        return vehicle;
    }

    public void setVehicle(String vehicle) {
        this.vehicle = vehicle;
    }
}
