package com.lobotus.mapei.flowfragments.fragments;


import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.lobotus.mapei.R;
import com.lobotus.mapei.baseactivity.BaseActivity;
import com.lobotus.mapei.retrofitapi.APIService;
import com.lobotus.mapei.retrofitapi.ApiUtils;
import com.lobotus.mapei.utils.CommonFunc;
import com.lobotus.mapei.utils.ConstantsUtils;
import com.lobotus.mapei.utils.NetworkUtils;
import com.lobotus.mapei.utils.PreferenceUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;



/**
 * A simple {@link Fragment} subclass.
 */
public class ResetPassword extends Fragment {


    View viewFragment=null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        viewFragment= inflater.inflate(R.layout.fragment_reset_password, container, false);
        viewFragment.findViewById(R.id.fragment_reset_password_button_id).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (NetworkUtils.checkInternetAndOpenDialog(getActivity()))
                {
                    if (validateChangePassword())
                    {
                        postToResetThepassword(((AppCompatEditText)viewFragment.findViewById(R.id.fragment_reset_password_old)).getText().toString(),
                                ((AppCompatEditText)viewFragment.findViewById(R.id.fragment_reset_password_new)).getText().toString());
                    }
                }
            }
        });
        return viewFragment;
    }


    private boolean validateChangePassword()
    {
        if (((AppCompatEditText)viewFragment.findViewById(R.id.fragment_reset_password_old)).getText().toString().isEmpty())
        {
            ((AppCompatEditText)viewFragment.findViewById(R.id.fragment_reset_password_old)).setError("Please enter old password");
            return false;
        }else if (((AppCompatEditText)viewFragment.findViewById(R.id.fragment_reset_password_new)).getText().toString().isEmpty())
        {
            ((AppCompatEditText)viewFragment.findViewById(R.id.fragment_reset_password_new)).setError("Please enter new password");
            return false;

        }else if  (((AppCompatEditText)viewFragment.findViewById(R.id.fragment_reset_password_confirm)).getText().toString().isEmpty())
        {
            ((AppCompatEditText)viewFragment.findViewById(R.id.fragment_reset_password_confirm)).setError("Please confirm new password");
            return false;
        }else if (!((AppCompatEditText)viewFragment.findViewById(R.id.fragment_reset_password_new)).getText().toString()
                .equals(((AppCompatEditText)viewFragment.findViewById(R.id.fragment_reset_password_confirm)).getText().toString()))
        {
            ((AppCompatEditText)viewFragment.findViewById(R.id.fragment_reset_password_confirm)).setError("password mismatch");
            return false;
        }else {
            return true;
        }
    }



    ////////////////////////// retrofit

    private void postToResetThepassword(String oldPassword,String newpassword)
    {
        showProgressDialog();
        APIService apiService= ApiUtils.getAPIService(ConstantsUtils.postBaseUrl+ConstantsUtils.
                PostingToChangeThePassword);
        apiService.postToChangeThepassword(PreferenceUtils.getUserIdFromPreference(getActivity()),
                oldPassword, newpassword,"UpdatePwd").enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dismissProgressDialog();
                if (response.isSuccessful())
                {
                    try {
                        JSONObject jsonObject=new JSONObject(response.body().string());
                        if (jsonObject.getInt("Success")==1)
                        {
                            alertDialogAfterStatusIsTrue();
                        }else {
                            CommonFunc.commonDialog(getActivity(),getString(R.string.alert),
                                    jsonObject.getString("Msg"),true);
                        }

                    } catch (JSONException | IOException e) {
                        CommonFunc.commonDialog(getActivity(),getString(R.string.alert),
                                getString(R.string.justErrorCode)+" 117",true);
                    }
                }else {
                    CommonFunc.commonDialog(getActivity(),getString(R.string.alert),
                            getString(R.string.justErrorCode)+" 116",true);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dismissProgressDialog();
                CommonFunc.commonDialog(getActivity(),getString(R.string.alert),
                        getString(R.string.noInternetMessage)+" 115",true);
            }
        });
    }

    private void alertDialogAfterStatusIsTrue()
    {
        new AlertDialog.Builder(getActivity())
                .setTitle("Alert!!")
                .setMessage("Password changed successfully. Do u want to logout?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        ((BaseActivity)getActivity()).performLogout();
                    }
                }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                getActivity().getSupportFragmentManager().popBackStack();
            }
        }).create().show();
    }
////////////////////////////////////////







    /////////////////////////////
    private Dialog dialog=null;
    public void showProgressDialog() {
        dialog=new Dialog(getActivity());
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.loaging_layout);
        dialog.show();
    }

    public void dismissProgressDialog() {
        if (getActivity()!=null)
            if (dialog!=null)
            {
                if (dialog.isShowing())
                {
                    dialog.dismiss();
                }
                dialog=null;
            }
    }

}
