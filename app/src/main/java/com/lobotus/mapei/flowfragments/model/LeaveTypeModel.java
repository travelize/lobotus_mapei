package com.lobotus.mapei.flowfragments.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by user1 on 09-11-2017.
 */

public class LeaveTypeModel {

    @SerializedName("Success")
    @Expose
    private Integer success;
    @SerializedName("data")
    @Expose
    private List<LeaveTypeDataModel> data = null;
    @SerializedName("Msg")
    @Expose
    private String Msg;

    public String getMsg() {
        return Msg;
    }

    public void setMsg(String msg) {
        Msg = msg;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public List<LeaveTypeDataModel> getData() {
        return data;
    }

    public void setData(List<LeaveTypeDataModel> data) {
        this.data = data;
    }

}
