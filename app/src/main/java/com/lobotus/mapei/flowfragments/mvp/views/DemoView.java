package com.lobotus.mapei.flowfragments.mvp.views;

import com.lobotus.mapei.flowfragments.model.CustomerDataModel;

import java.util.ArrayList;

/**
 * Created by user1 on 25-09-2017.
 */

public interface DemoView {
    void showProgressDialog();
    void dismissProgressDialog();
    ///////////////////////////////////////////// customer model
    void afterCustomerModelDownlodeSuccess(ArrayList<CustomerDataModel> customerDataModels);
    void afterCustomerModelDownlodeFail(String message, String dialogTitle, boolean isInternetError);
    //////////////////////////////////////////////////////   after loading the spinner list from strings
    void afterDemoTypeListLoaded(String[] stringsDemos);
    //////////////////////////////////////////////////////  after posting the demo to the server
    void afterDemoPostSuccess(String message);
    void afterDemoPostFail(String message,String dialogTitle,boolean isInternetError);

}
