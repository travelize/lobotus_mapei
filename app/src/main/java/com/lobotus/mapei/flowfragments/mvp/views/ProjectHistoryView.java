package com.lobotus.mapei.flowfragments.mvp.views;

import com.lobotus.mapei.flowfragments.model.ProjectHistoryModel;

import java.util.ArrayList;

/**
 * Created by user1 on 16-10-2017.
 */

public interface ProjectHistoryView {

    void showProgressDialog();
    void dismissProgressDialog();
    ////////////////////////////////////////////// post to downlode the project history
    void afterProjectHistoryDownlodeSuccess(ProjectHistoryModel projectHistoryModel);
    void afterProjectHistoryDownlodeFail(String message, String dialogTitle, boolean isInternetError);

}
