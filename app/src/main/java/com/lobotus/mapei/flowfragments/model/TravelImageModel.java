package com.lobotus.mapei.flowfragments.model;

import android.graphics.Bitmap;

/**
 * Created by user1 on 09-10-2017.
 */

public class TravelImageModel {

    private String _b64String=null;
    private Bitmap bitmap=null;
    private boolean isAddImageButton=false;
    private String imageCaption="";
    private String amount=null;

    public TravelImageModel(String _b64String, Bitmap bitmap, boolean isAddImageButton, String imageCaption, String amount) {
        this._b64String = _b64String;
        this.bitmap = bitmap;
        this.isAddImageButton = isAddImageButton;
        this.imageCaption = imageCaption;
        this.amount = amount;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getImageCaption() {
        return imageCaption;
    }

    public void setImageCaption(String imageCaption) {
        this.imageCaption = imageCaption;
    }

    public String get_b64String() {
        return _b64String;
    }

    public void set_b64String(String _b64String) {
        this._b64String = _b64String;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public boolean isAddImageButton() {
        return isAddImageButton;
    }

    public void setAddImageButton(boolean addImageButton) {
        isAddImageButton = addImageButton;
    }
}
