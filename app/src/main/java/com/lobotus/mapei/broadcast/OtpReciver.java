package com.lobotus.mapei.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;


/**
 * Created by SkilledAnswers-D1 on 18-08-2016.
 */
public class OtpReciver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent)
    {

        final Bundle bundle = intent.getExtras();
        try {
            if (bundle != null)
            {
                final Object[] pdusObj = (Object[]) bundle.get("pdus");
                if (pdusObj != null) {
                    for (int i = 0; i < pdusObj .length; i++)
                    {
                        String format=bundle.getString("format");
                        SmsMessage currentMessage = null;
                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                            currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i], format);
                        }else {
                            currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                        }
                        String phoneNumber = currentMessage.getDisplayOriginatingAddress();
                        String senderNum = phoneNumber ;
                        String message = currentMessage .getDisplayMessageBody();
                        try
                        {
                            if (senderNum.matches("^[0-9]*$")) {
                          /*  if (senderNum.substring(3).equalsIgnoreCase(SMS_SENDER_USER_ID)) {*/
                                Intent intent1 = new Intent("OTP_RECIVER_ACTION");
                                intent1.putExtra("OTP_RECIVER_VALUE_KEY", message.replaceAll("[^0-9]", "").trim());
                                context.sendBroadcast(intent1);
                          /*  }*/
                            }

                        }
                        catch(Exception ignored){
                            ignored.printStackTrace();
                        }

                    }
                }
            }

        } catch (Exception e)
        {

        }
    }
}
