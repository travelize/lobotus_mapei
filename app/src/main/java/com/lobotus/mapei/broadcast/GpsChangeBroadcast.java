package com.lobotus.mapei.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.lobotus.mapei.utils.ConstantsUtils;

import static com.lobotus.mapei.utils.ConstantsUtils.GPS_BROADCAST_ACTION_NAME;


/**
 * Created by user1 on 11-12-2017.
 */

public class GpsChangeBroadcast extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().matches(GPS_BROADCAST_ACTION_NAME)) {
           Intent intent1=new Intent(ConstantsUtils.GPS_BROADCAST_PASSING_TO_SERVICE_CLASS_ACTION_NAME);
            context.sendBroadcast(intent1);
        }
    }
}
