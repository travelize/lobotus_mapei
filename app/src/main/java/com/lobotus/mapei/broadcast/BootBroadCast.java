package com.lobotus.mapei.broadcast;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.lobotus.mapei.services.fromlite.TrackingService;
import com.lobotus.mapei.utils.PreferenceUtils;



/**
 * Created by user1 on 14-11-2017.
 */

public class BootBroadCast extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
            if (PreferenceUtils.checkUserisLogedin(context))
            {
                startTheTrakingService(context);
            }
        }
    }
    private void startTheTrakingService(Context context)
    {
        Intent intentService=new Intent(context, TrackingService.class);
        intentService.setFlags(Service.START_STICKY);
        context.startService(intentService);
    }
}
