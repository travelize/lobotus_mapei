package com.lobotus.mapei;

import android.*;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.lobotus.mapei.baseactivity.BaseActivity;
import com.lobotus.mapei.registration.activities.ReqForMobileNo;
import com.lobotus.mapei.services.fromlite.TrackingService;
import com.lobotus.mapei.utils.NetworkUtils;
import com.lobotus.mapei.utils.PermissionUtils;
import com.lobotus.mapei.utils.PreferenceUtils;

import static com.lobotus.mapei.utils.ConstantsUtils.ACCOUNT;
import static com.lobotus.mapei.utils.ConstantsUtils.ACCOUNT_TYPE;
import static com.lobotus.mapei.utils.PermissionUtils.openPermissionDialog;

public class SplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        CreateSyncAccount(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (getIntent().getAction() != null) {
            if (getIntent().getAction().equals("CLEAR_ALL_USER_PREFERENCES")) // came from service class
            {
                Intent intent = new Intent(SplashScreen.this, TrackingService.class);
                stopService(intent);
                PreferenceUtils.clearUserSessionPreferences(SplashScreen.this);
                finishAffinity();
            } else { // came form launcher or after permission grant or after application settings screen
                if (PermissionUtils.checkLocAndPhonePermission(SplashScreen.this)) {
                    startTheApp();
                } else {
                    reqForTheLocationAndPhonePermission(SplashScreen.this);
                }
            }
        } else { // came after clearing the preference
            if (PermissionUtils.checkLocAndPhonePermission(SplashScreen.this)) {
                startTheApp();
            } else {
                reqForTheLocationAndPhonePermission(SplashScreen.this);
            }
        }


    }

    ///////////////////////////////////////////////////////////////// req for loc permission
    private   void reqForTheLocationAndPhonePermission(Activity activity)
    {

        if (PreferenceUtils.checkUserisLogedin(SplashScreen.this))
        {
            PreferenceUtils.storeLocPermissionChangedToSharedPreference(SplashScreen.this);
        }
        // Should we show an explanation?
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                android.Manifest.permission.ACCESS_FINE_LOCATION) ||
                ActivityCompat.shouldShowRequestPermissionRationale(activity,
                        android.Manifest.permission.READ_PHONE_STATE)) {

            // Show an explanation to the user *asynchronously* -- don't block
            // this thread waiting for the user's response! After the user
            // sees the explanation, try again to request the permission.
            openPermissionDialog(activity,"Please grant the Travelize app all permissions from the app settings to run seamlessly (Location and Make,manage phone calls are mandatory)");
        } else {
            // No explanation needed, we can request the permission.

            ActivityCompat.requestPermissions(activity,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION,
                            android.Manifest.permission.READ_PHONE_STATE },
                    10);

            // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
            // app-defined int constant. The callback method gets the
            // result of the request.
        }
    }

    private void startTheApp() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (NetworkUtils.checkInternetAndOpenDialog(SplashScreen.this)) {
                    if (PreferenceUtils.checkUserisLogedin(SplashScreen.this))
                        startActivity(new Intent(SplashScreen.this, BaseActivity.class));
                    else
                        startActivity(new Intent(SplashScreen.this, ReqForMobileNo.class));

                    finish();
                }
            }
        }, 3000);

    }

    public static Account CreateSyncAccount(Context context) {
        // Create the account type and default account
        Account newAccount = new Account(
                ACCOUNT, ACCOUNT_TYPE);
        // Get an instance of the Android account manager
        AccountManager accountManager =
                (AccountManager) context.getSystemService(
                        ACCOUNT_SERVICE);

        /*
         * Add the account and account type, no password or user data
         * If successful, return the Account object, otherwise report an error.
         */
        if (accountManager.addAccountExplicitly(newAccount, null, null)) {
            /*
             * If you don't set android:syncable="true" in
             * in your <provider> element in the manifest,
             * then call context.setIsSyncable(account, AUTHORITY, 1)
             * here.
             */
            // Inform the system that this account supports sync
//            ContentResolver.setIsSyncable(newAccount, AUTHORITY, 1);
//            // Inform the system that this account is eligible for auto sync when the network is up
//            ContentResolver.setSyncAutomatically(newAccount, AUTHORITY, true);
        } else {
            /*
             * The account exists or some other error occurred. Log this, report it,
             * or handle it internally.
             */
        }
        return newAccount;
    }

}
