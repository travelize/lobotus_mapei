package com.lobotus.mapei.projectclasss;

import android.app.Application;
import android.support.v7.app.AppCompatDelegate;

/**
 * Created by user1 on 03-10-2017.
 */

public class MapeiProjectClass extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

    }
}
