package com.lobotus.mapei.utils;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.EditText;

import com.lobotus.mapei.SplashScreen;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by user1 on 21-09-2017.
 */

public class PreferenceUtils {
    /// session preference
    // CONSTANTS
    public static final String SESSION_PREFERENCE_NAME="SESSION_PREFERENCE_NAME";
    public static final String SESSION_PREFERENCE_EMAIL_KEY="SESSION_PREFERENCE_EMAIL_KEY";
    public static final String SESSION_PREFERENCE_FULLNAME_KEY="SESSION_PREFERENCE_FULLNAME_KEY";
    public static final String SESSION_PREFERENCE_SUBSCRIPTION_ID_KEY="SESSION_PREFERENCE_SUBSCRIPTION_ID_KEY";
    public static final String SESSION_PREFERENCE_PROFILE_PIC_URI_KEY="SESSION_PREFERENCE_PROFILE_PIC_URI_KEY";
    public static final String SESSION_PREFERENCE_USER_ID_KEY="SESSION_PREFERENCE_USER_ID_KEY";
    public static final String SESSION_PREFERENCE_PHONE_KEY="SESSION_PREFERENCE_PHONE_KEY";
    public static final String SESSION_PREFERENCE_DAYS_LEFT_KEY="SESSION_PREFERENCE_DAYS_LEFT_KEY";
    public static final String SESSION_PREFERENCE_ROLE_ID_KEY="SESSION_PREFERENCE_ROLE_ID_KEY";
    public static final String SESSION_PREFERENCE_ROLE_NAME_KEY="SESSION_PREFERENCE_ROLE_NAME_KEY";
    public static final String SESSION_PREFERENCE_REG_ID_KEY="SESSION_PREFERENCE_REG_ID_KEY";
    public static final String SESSION_PREFERENCE_SESSION_ID_KEY="SESSION_PREFERENCE_SESSION_ID_KEY";
    // ADD TO PREFERENCE
    public static void addUserSessionDetailsWhileReg(Context context,
                                                     String email,
                                                     String fullName,
                                                     String subId,
                                                     String profileUri,
                                                     String userId,
                                                     String phoneKey,
                                                     String daysLeft,
                                                     String roleId,
                                                     String roleName,
                                                     String regId,
                                                     String sessionId)
    {
        SharedPreferences sharedPreferences=context.getSharedPreferences(SESSION_PREFERENCE_NAME,
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putString(SESSION_PREFERENCE_EMAIL_KEY,email);
        editor.putString(SESSION_PREFERENCE_FULLNAME_KEY,fullName);
        editor.putString(SESSION_PREFERENCE_SUBSCRIPTION_ID_KEY,subId);
        editor.putString(SESSION_PREFERENCE_PROFILE_PIC_URI_KEY,profileUri);
        editor.putString(SESSION_PREFERENCE_USER_ID_KEY,userId);
        editor.putString(SESSION_PREFERENCE_PHONE_KEY,phoneKey);
        editor.putString(SESSION_PREFERENCE_DAYS_LEFT_KEY,daysLeft);
        editor.putString(SESSION_PREFERENCE_ROLE_ID_KEY,roleId);
        editor.putString(SESSION_PREFERENCE_ROLE_NAME_KEY,roleName);
        editor.putString(SESSION_PREFERENCE_REG_ID_KEY,regId);
        editor.putString(SESSION_PREFERENCE_SESSION_ID_KEY,sessionId);
        editor.apply();

    }
    // clear user preference
    public static void clearUserSessionPreferences(Context context)
    {
        SharedPreferences sharedPreferences=context.getSharedPreferences(SESSION_PREFERENCE_NAME,
                Context.MODE_PRIVATE);
        if (sharedPreferences!=null)
        {
            SharedPreferences.Editor editor=sharedPreferences.edit();
            editor.clear();
            editor.apply();
        }
        context.startActivity(new Intent(context, SplashScreen.class));

    }
    // check shared preference is filledOr not
    public static boolean checkUserisLogedin(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SESSION_PREFERENCE_NAME,
                Context.MODE_PRIVATE);
        return sharedPreferences.getString(SESSION_PREFERENCE_USER_ID_KEY, null) != null;
    }
    // get the use role name for the shared preference. . .
    public  static String getTheUserRoleNameFromPreference(Context context)
    {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SESSION_PREFERENCE_NAME,
                Context.MODE_PRIVATE);
        return sharedPreferences.getString(SESSION_PREFERENCE_ROLE_NAME_KEY,null);
    }
    /// get userId
    public static String getUserIdFromPreference(Context context)
    {

            SharedPreferences sharedPreferences = context.getSharedPreferences(SESSION_PREFERENCE_NAME,
                    Context.MODE_PRIVATE);
            return sharedPreferences.getString(SESSION_PREFERENCE_USER_ID_KEY,null);
    }
    /// get session id
    public static String getUserSessionId(Context context)
    {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SESSION_PREFERENCE_NAME,
                Context.MODE_PRIVATE);
        return sharedPreferences.getString(SESSION_PREFERENCE_SESSION_ID_KEY,null);
    }
    // get profile pic
    public static String getProfilePicUrl(Context context)
    {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SESSION_PREFERENCE_NAME,
                Context.MODE_PRIVATE);
        return sharedPreferences.getString(SESSION_PREFERENCE_PROFILE_PIC_URI_KEY,null);
    }
    ////////////////////////////////////////////////////////////////////////////////////////////
    public static final String OFFICE_VISIT_PREFERENCE_NAME="OFFICE_VISIT_PREFERENCE_NAME";
    public static final String OFFICE_VISIT_PREFERENCE_ID_KEY="OFFICE_VISIT_PREFERENCE_ID_KEY";

    /// ADD OFFICE VISIT PREFERENCE
    public static void addOfficeVisitId(String id,Context context)
    {
        SharedPreferences sharedPreferences=context.getSharedPreferences(OFFICE_VISIT_PREFERENCE_NAME,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putString(OFFICE_VISIT_PREFERENCE_ID_KEY,id);
        editor.apply();
    }

    public static String getOfficeVisitId(Context context)
    {
        SharedPreferences sharedPreferences=context.getSharedPreferences(OFFICE_VISIT_PREFERENCE_NAME,Context.MODE_PRIVATE);
        if (sharedPreferences!=null)
        {
           return sharedPreferences.getString(OFFICE_VISIT_PREFERENCE_ID_KEY,null) ;
        }else {
            return null;
        }
    }

    public static void clearTheOfficeVisitId(Context context)
    {
        SharedPreferences sharedPreferences=context.getSharedPreferences(OFFICE_VISIT_PREFERENCE_NAME,
                Context.MODE_PRIVATE);
        if (sharedPreferences!=null)
        {
            SharedPreferences.Editor editor=sharedPreferences.edit();
            editor.clear();
            editor.apply();
        }
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////  signal status preference
    public static final String SIGNAL_STATUS_PREFERENCE_NAME="SIGNAL_STATUS_PREFERENCE_NAME";
    public static final String SIGNAL_STATUS_VALUE_KEY="SIGNAL_STATUS_VALUE_KEY";

    public static void  addlatestSignalStatusToPref(Context context,String signalStrength)
    {
        SharedPreferences sharedPreferences=context.getSharedPreferences(SIGNAL_STATUS_PREFERENCE_NAME,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putString(SIGNAL_STATUS_VALUE_KEY,signalStrength);
        editor.apply();
    }

    public static String getlatestSignalStatus(Context context)
    {
        if (context.getSharedPreferences(SIGNAL_STATUS_PREFERENCE_NAME,Context.MODE_PRIVATE)!=null)
        {
            return context.getSharedPreferences(SIGNAL_STATUS_PREFERENCE_NAME,Context.MODE_PRIVATE)
                    .getString(SIGNAL_STATUS_VALUE_KEY,"NA");
        }else {
            addlatestSignalStatusToPref(context,"NA");
        }
        return "NA";
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////
    ////// location off opstarction preference
    public static final String LOC_PERMISSION_OFF_STATUS_PREFERENCE="LOC_PERMISSION_OFF_STATUS_PREFERENCE";
    public static final String LOC_PERMISSION_OFF_STATUS_KEY="LOC_PERMISSION_OFF_STATUS_KEY";
    //
    public static void storeInitialLocPermissionNotChangedOnStartingService(Context context)
    {
        SharedPreferences sharedPreferences=context.getSharedPreferences(LOC_PERMISSION_OFF_STATUS_PREFERENCE
                ,MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putBoolean(LOC_PERMISSION_OFF_STATUS_KEY,true);
        editor.apply();
    }
    //
    public static void storeLocPermissionChangedToSharedPreference(Context context)
    {
        SharedPreferences sharedPreferences=context.getSharedPreferences(LOC_PERMISSION_OFF_STATUS_PREFERENCE
                ,MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putBoolean(LOC_PERMISSION_OFF_STATUS_KEY,false);
        editor.apply();
    }


    public static boolean getLocPermissonGotChangedOrNot(Context context)
    {
        return context.getSharedPreferences(LOC_PERMISSION_OFF_STATUS_PREFERENCE,MODE_PRIVATE)
                .getBoolean(LOC_PERMISSION_OFF_STATUS_KEY,false);
    }

    ///////////////////////////// checked in - or checked out. . . .

    public static final String CHECK_IN_OUT_PREFERENCE_NAME="CHECK_IN_OUT_PREFERENCE_NAME";
    public static final String CHECK_IN_OUT_PREFERENCE_ATTENCE_ID_KEY="CHECK_IN_OUT_PREFERENCE_ATTENCE_ID_KEY";

    /////// SET HAS checked in

    public static void setUserHasCheckedIn(Context  context,String attenceKey)
    {
        SharedPreferences sharedPreferences=context.getSharedPreferences(CHECK_IN_OUT_PREFERENCE_NAME,MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putString(CHECK_IN_OUT_PREFERENCE_ATTENCE_ID_KEY,attenceKey);
        editor.apply();
    }

    ////////////// set has checked out
    public static void setUserHasCheckedOut(Context  context)
    {
        SharedPreferences sharedPreferences=context.getSharedPreferences(CHECK_IN_OUT_PREFERENCE_NAME,MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.clear();
        editor.apply();
    }

    ///////////////// get user has checkin ot checked out. . . .

    public static boolean getIsUserCheckedIn(Context context)
    {
        SharedPreferences sharedPreferences=context.getSharedPreferences(CHECK_IN_OUT_PREFERENCE_NAME,MODE_PRIVATE);
        if (sharedPreferences!=null)
        {
            return sharedPreferences.getString(CHECK_IN_OUT_PREFERENCE_ATTENCE_ID_KEY, null) != null;
        }else {
            return false;
        }
    }

    ///// get attence id

    public static String getCheckedInUserAttenceId(Context context)
    {
        return context.getSharedPreferences(CHECK_IN_OUT_PREFERENCE_NAME,MODE_PRIVATE)
                .getString(CHECK_IN_OUT_PREFERENCE_ATTENCE_ID_KEY,null);
    }



    ////////////////////////////////////////////////////////////////////////////////////// started route preference. . . .
    public static final String STARTED_ROUTE_PREFERENCE_NAME="STARTED_ROUTE_PREFERENCE_NAME";
    public static final String STARTED_ROUTE_PREFERENCE_MEETEING_ID_KEY="STARTED_ROUTE_PREFERENCE_MEETEING_ID_KEY";
    public static final String STARTED_ROUTE_PREFERENCE_MOT="STARTED_ROUTE_PREFERENCE_MOT";
    public static final String STARTED_ROUTE_PREFERENCE_CLIENT_NAME="STARTED_ROUTE_PREFERENCE_CLIENT_NAME";
    public static final String STARTED_ROUTE_PREFERENCE_IS_ROUTE_ENDED="STARTED_ROUTE_PREFERENCE_IS_ROUTE_ENDED";


    // ADD MEETING ID TO THE STARTED ROUTE. .
    public static void addMeetingIdToTheStaredRoute(Context context,String meetingId,String mot,String clientName)
    {
        SharedPreferences sharedPreferences=context.getSharedPreferences(STARTED_ROUTE_PREFERENCE_NAME,MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putString(STARTED_ROUTE_PREFERENCE_MEETEING_ID_KEY,meetingId);
        editor.putString(STARTED_ROUTE_PREFERENCE_MOT,mot);
        editor.putString(STARTED_ROUTE_PREFERENCE_CLIENT_NAME,clientName);
        editor.putBoolean(STARTED_ROUTE_PREFERENCE_IS_ROUTE_ENDED,false);
        editor.apply();
    }
    // GET STARTED ROUTE ENDED OR NOT
    public static boolean isStartedRouteEnded(Context context)
    {
        return  context.getSharedPreferences(STARTED_ROUTE_PREFERENCE_NAME,MODE_PRIVATE)
                .getBoolean(STARTED_ROUTE_PREFERENCE_IS_ROUTE_ENDED,false);
    }
    // SET STARTED ROUTE ENDED
    public static void setStartedRouteEnded(Context context)
    {
        SharedPreferences sharedPreferences=context.getSharedPreferences(STARTED_ROUTE_PREFERENCE_NAME,MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putBoolean(STARTED_ROUTE_PREFERENCE_IS_ROUTE_ENDED,true);
        editor.apply();
    }
    // GET MEETING ID FROM THE SHAREDPREFERENCE. . .
    public static String getMeetingIdFromStartRoutePreference(Context context)
    {
        return  context.getSharedPreferences(STARTED_ROUTE_PREFERENCE_NAME,MODE_PRIVATE)
                .getString(STARTED_ROUTE_PREFERENCE_MEETEING_ID_KEY,null);
    }
    // check meeting id is added or not from the sharedpreference. . .
    public static boolean isMeetingIdPresentInTheStartRoutePreference(Context context)
    {
        SharedPreferences sharedPreferences=context.getSharedPreferences(STARTED_ROUTE_PREFERENCE_NAME,MODE_PRIVATE);
        if (sharedPreferences==null)
        {
            return false;
        }else {
            if (sharedPreferences.getString(STARTED_ROUTE_PREFERENCE_MEETEING_ID_KEY,null)==null)
            {
                return false;
            }else {
                return true;
            }
        }

    }
    // get mot from the shared preference. . . .
    public static String getMOTFromStartRoutePreference(Context context)
    {
        return  context.getSharedPreferences(STARTED_ROUTE_PREFERENCE_NAME,MODE_PRIVATE)
                .getString(STARTED_ROUTE_PREFERENCE_MOT,null);
    }

    // get client name from the shared preference. . . .
    public static String getClientNameFromStartRoutePreference(Context context)
    {
        if ( context.getSharedPreferences(STARTED_ROUTE_PREFERENCE_NAME,MODE_PRIVATE)!=null)
            return  context.getSharedPreferences(STARTED_ROUTE_PREFERENCE_NAME,MODE_PRIVATE)
                    .getString(STARTED_ROUTE_PREFERENCE_CLIENT_NAME,"");
        else
            return "";
    }

    // clear the started meeting from the shared preference. . . .

    public static void clearTheStartedRouteMeetingIdFromTheSharedPreference(Context context)
    {
        SharedPreferences sharedPreferences=context.getSharedPreferences(STARTED_ROUTE_PREFERENCE_NAME,MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.clear();
        editor.apply();

    }








}
