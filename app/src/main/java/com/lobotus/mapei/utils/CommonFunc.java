package com.lobotus.mapei.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.provider.Settings;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ScrollView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.gson.Gson;
import com.lobotus.mapei.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.lobotus.mapei.utils.ConstantsUtils.BELLOW_KITKAT;
import static com.lobotus.mapei.utils.ConstantsUtils.NO_SIM_CARD;

/**
 * Created by user1 on 19-09-2017.
 */

public class CommonFunc {

    public static String parseDateToddMMyyyy(String time) {
        String inputPattern = "dd/MM/yyyy HH:mm a";// dd/MM/yyyy h:mm a
        String outputPattern = "EEE, d MMM yyyy,hh:mm aaa";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            Log.e("Date Error=", e.getMessage());
        }
        return str;
    }

    public static void hideTheKeypad(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void commonDialog(final Context context, String title, String subjuct, final boolean isInternetError) {
        try {


            final Dialog dialog = new Dialog(context, R.style.comm_dialog);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.common_dialog);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(false);
            AppCompatTextView textViewTitle = dialog.findViewById(R.id.commom_dialog_title_id);
            AppCompatTextView textViewSubjuct = dialog.findViewById(R.id.commom_dialog_subjuct_id);
            AppCompatButton buttonSubmit = dialog.findViewById(R.id.commom_dialog_button_id);
            if (isInternetError) {
                buttonSubmit.setText("Check Network Settings");
            } else {
                buttonSubmit.setText("OK");
            }
            textViewTitle.setText(title);
            textViewSubjuct.setText(subjuct);
            buttonSubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isInternetError) {
                        dialog.dismiss();
                        Intent myIntent = new Intent(Settings.ACTION_SETTINGS);
                        context.startActivity(myIntent);
                    } else {
                        dialog.dismiss();
                    }
                }
            });
            try {
                dialog.show();
            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            System.out.println("Dialog box error-------fff----" + e.getMessage());
        }
    }

    public static boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public static void hideKeyboard(Context ctx) {
        InputMethodManager inputManager = (InputMethodManager) ctx
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        // check if no view has focus:
        View v = ((Activity) ctx).getCurrentFocus();
        if (v == null)
            return;

        inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    public static void scroolTo(final ScrollView scrollView, final View view) {
        scrollView.post(new Runnable() {
            @Override
            public void run() {
                scrollView.smoothScrollTo(0, view.getBottom());
            }
        });
    }

    public static void fullscroolToTop(final View viewfragment, final int id) {
        ((ScrollView) viewfragment.findViewById(id)).post(new Runnable() {
            @Override
            public void run() {
                ((ScrollView) viewfragment.findViewById(id)).fullScroll(((ScrollView) viewfragment.findViewById(id)).FOCUS_UP);
            }
        });

    }

    public static void fullNestedscroolToTop(final View viewfragment, final int id) {
        ((NestedScrollView) viewfragment.findViewById(id)).post(new Runnable() {
            @Override
            public void run() {
                ((NestedScrollView) viewfragment.findViewById(id)).fullScroll(((NestedScrollView) viewfragment.findViewById(id)).FOCUS_UP);
            }
        });

    }

    public static boolean IsToDateIsLowThanFrom(String fromDate, String toDate) throws ParseException {
        Date date1 = new SimpleDateFormat("dd/MM/yyyy").parse(fromDate);
        Date date2 = new SimpleDateFormat("dd/MM/yyyy").parse(toDate);
        int result = date2.compareTo(date1);
        if (result < 0) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isGooglePlayServicesAvailable(Activity activity) {
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int status = googleApiAvailability.isGooglePlayServicesAvailable(activity);
        if (status != ConnectionResult.SUCCESS) {
            if (googleApiAvailability.isUserResolvableError(status)) {
                googleApiAvailability.getErrorDialog(activity, status, 2404).show();
            }
            return false;
        }
        return true;
    }

    public static String getTodayDate() {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }

    //////////////////////////// add the sim info
    public static String formTheSimInfoString(Context context)
    {
        ArrayList<String> stringsSimInfo=new ArrayList<>();
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP_MR1) {
            if ((SubscriptionManager.from(context).getActiveSubscriptionInfoList())!=null)
            {
                List<SubscriptionInfo> subscriptionInfos = null;
                subscriptionInfos = SubscriptionManager.from(context).getActiveSubscriptionInfoList();
                for(int i=0; i<subscriptionInfos.size();i++)
                {
                    SubscriptionInfo lsuSubscriptionInfo = subscriptionInfos.get(i);
                    stringsSimInfo.add(lsuSubscriptionInfo.getIccId());

                }
                return new Gson().toJson(stringsSimInfo);
            }else {
                return NO_SIM_CARD;
            }


        }else {

            return BELLOW_KITKAT;
        }


    }

}
