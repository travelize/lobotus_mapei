package com.lobotus.mapei.utils;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.StreetViewPanoramaCamera;

/**
 * Created by user1 on 21-09-2017.
 */

public class ConstantsUtils {
    /*public static final String postToGetProfilePic = "http://betaphase.in/mapei/images/ProfileIcon/";
    public static final String postBaseUrl = "http://betaphase.in/mapei/api/user/";*/

    public static final String postToGetProfilePic = "http://www.travelize.in/mapei/images/ProfileIcon/";
    public static final String postBaseUrl = "http://www.travelize.in/mapei/api/user/";
    /*public static final String postBaseUrl = "http://192.168.1.151/api/user/";*/
    // POST TO CHECK THE USER IS PRESENT OR NOT
    public static final String POST_TO_CHECK_USER="PostCheckUser/";

    //    public static final String postBaseUrl = "http://192.168.1.16/api/user/";
    //  login
    public static final String postAppendToBaseUrlForLogin = "PostLogin/";

    // posting demo
    public static final String postAppendToBaseUrlForDemo = "PostSaveDemoDetails/";
    // posting to get the customers data
    public static final String postToDownlodeTheCustomersData = "PostCustomerDetails/";
    // posting to apply for leaves
    public static final String postToApplyForLeaves = "PostAddLeaves/";
    // complete post url to get the mode of the travel and the purpose of travel
    public static final String postURlToGetTheMOTandPOT = "PostMotAndPurpose/";
    // posting to save the travel details
    public static final String postToSaveTheTravelDetails = "PostSaveTravelDetails/";
    // posting to get the list of Leaves
    public static final String postToGetThelistOfLeaveList = "PostLeaveList/";
    // posting to cancel the leave
    public static final String postingToCancelTheLeave = "PostCancelLeave/";
    // posting to get the list of projects
    public static final String postToGetTheListOfProjects = "PostProjectList/";
    // posting to add the projucts to server
    public static final String postToAddTheProjectsToServer = "PostAddProject/";
    // posting to get the lsit of products
    public static final String postToGetTheListOfProducts = "PostProductList/";
    // posting to get the complaints data
    public static final String postToGetTheComplaintsData = "PostComplaintTypes/";
    // posting to add meetings
    public static final String postToAddTheMeetings = "PostAddMeetingFromApp/";
    // posting to update office entry
    public static final String postEntryOffice = "PostEnterOffice/";
    // posting to update office purpose of visit
    public static final String postToUpdatePurposeOfOfficeVisit = "PostUpdateOfficeDetails/";
    // posting to update office exit
    public static final String postExitOffice = "PostExitOffice/";
    // posting logout to the server
    public static final String postLogout = "PostLogout/";
    // posting to get the project history
    public static final String postToGetTheProjectHistory = "PostProjectHistory/";
    // post to get the products in project history
    public static final String postToGetTheProductsInHistory = "PostOrderedProducts/";
    // post to update the project
    public static final String postToUpdateTheProject = "PostUpdateProject/";
    // post to add the customer details
    public static final String postToSaveTheCustomerDetails = "PostSaveCustDetails/";
    // post to get the customer type
    public static final String postToGetTheCustomerType = "PostCustTypeList/";
    // post to get the leave type list
    public static final String postToGetTheLeaveTypeList = "PostLeaveType/";
    // post to save the user coordinates
    public static final String PostUserGetCordinate = "PostUserGetCordinates/";
    // post to get the distance travelled
    public static final String PostTogetTheDistanceTravelledData = "PostUserDistance/";
    // posting to get the project list based on customer name
    public static final String PostingToGetTheProjectListBasedOnCustomerName = "PostGetProjectList/";
    // posting to generate reset password link
    public static final String PostingTogeneratePasswordResetLink = "PostForgotPwd/";
    // posting to change the password
    public static final String PostingToChangeThePassword = "PostResetPwd/";
    // post to perform check in
    public static final String POST_TO_CHECK_IN = "PostCheckIn/";
    // post to perform check out
    public static final String POST_TO_CHECK_OUT = "PostCheckOut/";
    // post to get the particular user mod of travel. .
    public static final String POST_TO_GET_CUSTOMER_MOT_BY_ID = "PostMOTForApp/";
    // post user coordinates to server
    public static final String POST_USER_COORDINATES_TO_SERVER = "PostUserGetCordinates/";
    // post to get the distance travelled
    public static final String POST_GET_THE_DISTANCE_TRAVELLED = "PostUpdateDistance/";
    // post to update the meeting status
    public static final String POST_TO_UPDATE_THE_MEETING_STATUS = "PostUpdateExpenseNStatus/";
    // post to server after user EXITS the grofenced region. . . .
    public static final String POST_TO_SERVER_AFTER_USER_EXITS_THE_GEOFENCED_REGION = "PostMeetingCheckOut/";
    // post to server after user enter the grofenced region. . . .
    public static final String POST_TO_SERVER_AFTER_USER_ENTER_THE_GEOFENCED_REGION = "PostMeetingCheckIn/";
    // post to update the gps status
    public static final String POST_TO_UPDATE_GPS_STATUS = "PostUpdateGPSStatus/";
    // post to get the list of users
    public static final String POST_TO_GET_THE_LIST_OF_USERS = "PostUserList/";
    // psot ot assign the meeting to the users. . .
    public static final String POST_TO_ASSIGN_MEETING_TO_USER = "PostScheduleMeetingByAdminApp/";
    /// psot to get the usrer daily tarvel details. . .
    public static final String POST_TO_GET_THE_USER_TRAVEL_LIST = "PostTravelDistance/";


    ////////// from lite . . .
    // post to get meeting list in android
    public static final String POST_TO_GET_MEETING_LIST_OR_MEETING_DETAILS = "PostMeetingCount/";

    public static final String POST_TO_GET_MEETING_LIST_OR_MEETING_DETAILS_HISTORY = "PostMeetingCountHistory/";
    // post to get the MOT list from the server
    public static final String POST_TO_GET_THE_MOT_LIST = "PostTravelByList/";


    public static final String POST_ASSIGN_MEETINGS_TO_SERVER = "PostAssignMeetingByAdmin/";

    public static final String GET_DEMO_MEETINGS_DETAILS = "PostDemoMeetingDetails/";


    ///////////////////////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////////////////// bundel keys stuff
    public static final String successFragMessageKey = "successFragSuccessMsg";
    public static final String successFragAddMoreButtonKey = "successFragAddMoreButtonText";

    /////  tracking service
    public static final String NOTIFICATION_CHANNEL_ID_OF_TRACKING_NOTIFICATION =
            "NOTIFICATION_CHANNEL_ID_OF_TRACKING_NOTIFICATION";
    public static final String NOTIFICATION_CHANNEL_ID_OF_SERVICE_ERROR_NOTIFICATION =
            "NOTIFICATION_CHANNEL_ID_OF_SERVICE_ERROR_NOTIFICATION";
    public static final String OREO_TRACK_NOTIFICATION_CHANNEL_NAME = "OREO_TRACK_NOTIFICATION_CHANNEL_NAME";
    //The identifier for this notification as per
    public static final int IDENTIFIERS_FOR_TRACK_NOTIFICATION = 10;
    public static final int IDENTIFIERS_FOR_TRACK_ERROR_NOTIFICATION = 11;
    ///////////////// image folder directory name
    public static final String IMAGE_STORAGE_DIRECTORY_NAME = "MAPEI_CACHE";
    ///// file provider authority
    public static final String FILE_PROVIDER_AUTHORITY = "com.lobotus.mapei.fileprovider";

    //////////////////////// sync adapter account stuff
    // The authority for the sync adapter's content provider
    public static final String AUTHORITY = "com.lobotus.mapei.provider";
    // An account type, in the form of a domain name
    public static final String ACCOUNT_TYPE = "com.lobotus.mapei";
    // The account name
    public static final String ACCOUNT = "mapei";
    //////////////////////////////////////////////////// gps action name
    public static final String GPS_BROADCAST_ACTION_NAME = "android.location.PROVIDERS_CHANGED";
    //////////////////////////// PASS GPS BROADCAST TO SERVICE CLASS ACTION ANME. . . .
    public static final String GPS_BROADCAST_PASSING_TO_SERVICE_CLASS_ACTION_NAME = "GPS_BROADCAST_PASSING_TO_SERVICE_CLASS_ACTION_NAME";
    // geo fence action name
    public static final String NOTIFICATION_ENTRY_FROM_MAPEI_BANGLORE_HO_MESSAGE = "Entering Mapei Banglore HO please enter purpose of your visit";
    public static final String NOTIFICATION_EXIT_FROM_MAPEI_BANGLORE_HO_MESSAGE = "You have exited Mapei Banglore HO";
    //
    public static final String NOTIFICATION_ENTRY_FROM_MAPEI_MUMBAI_MESSAGE = "Entering Mapei Mumbai please enter purpose of your visit";
    public static final String NOTIFICATION_EXIT_FROM_MAPEI_MUMBAI_MESSAGE = "You have exited Mapei Mumbai";
    //
    public static final String NOTIFICATION_ENTRY_FROM_MAPEI_Delhi_Faridabad_MESSAGE = "Entering Mapei Delhi/Faridabad please enter purpose of your visit";
    public static final String NOTIFICATION_EXIT_FROM_MAPEI_Delhi_Faridabad_MESSAGE = "You have exited Mapei Delhi/Faridabad";

    //////////////////////// geo fenced place
    // mapie banglore office coordinates
    public static final double LAT_MAPIE_BANGLORE_OFFICE = 12.974194;
    public static final double LNG_MAPIE_BANGLORE_OFFICE = 77.599270;
    // mapei mumbai office
    public static final double LAT_MAPEI_MUMBAI_OFFICE = 19.0120004;
    public static final double LNG_MAPEI_MUMBAI_OFFICE = 73.0356223;
    // MAPEI  Delhi/Faridabad  Office
    public static final double LAT_MAPEI_DELHI_OFFICE = 28.4902162;
    public static final double LNG_MAPEI_DELHI_OFFICE = 77.2823302;


    // mapie office geofence id
    public static final String MAPEI_OFFICE_BANGLORE_ID = "MAPEI_OFFICE_BANGLORE_ID";
    public static final String MAPEI_OFFICE_MUMBAI_ID = "MAPEI_OFFICE_MUMBAI_ID";
    public static final String MAPEI_OFFICE_DELHI_ID = "MAPEI_OFFICE_DELHI_ID";
    ////
    public static final float GEOFENCE_RADIUS = 100.0f; // in meters
    public static final int MIN_ACCURACY_OF_LOCATION = 100;
    /////////////////////// office addresses
    public static final String BANGLORE_ADDRESS = "No. 402, Tudor Court, 40 Lavelle Road, Bangalore -560001";
    public static final String MUMBAI_ADDRESS = "#305, V times square, plot No.3 sector 15, CBD Navi Mumbai  -400614";
    public static final String DELHI_ADDRESS = "H408  The atrium, Suraj kund Road, Faridabad 121001";

    /////////////////////////////////////// geo fence stuff
    // geo fence id
    public static final String GEOFENCE_ID = "GEOFENCE_ID";
    // action name of the geofence start
    public static final String GEO_FENCE_BROADCAST_ACTION = "GEO_FENCE_BROADCAST_ACTION";
    //// boolean flag for broadcast
    public static final String IS_GEO_FENCE_EVENT_FOR_START_GEOFENCE_BOOLEAN = "IS_GEO_FENCE_EVENT_FOR_START_GEOFENCE";


    ///////////////////// NO SIM CARD
    public static final String NO_SIM_CARD="NO_SIM_CARD";
    public static final String BELLOW_KITKAT="BELLOW_KITKAT";


}
