package com.lobotus.mapei.utils;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.view.Window;

import com.lobotus.mapei.R;

/**
 * Created by user1 on 04-10-2017.
 */

public class PermissionUtils {
    public static final int PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE=10;
    public static final int PERMISSIONS_REQUEST_CAMERA=11;
    public static final int PERMISSIONS_TO_REQUEST_LOCATION_ACCESS=12;


    public static boolean checkLocAndPhonePermission(Context context)
    {
        return ContextCompat.checkSelfPermission(context,
                Manifest.permission.ACCESS_FINE_LOCATION) + ContextCompat.checkSelfPermission(context,
                Manifest.permission.READ_PHONE_STATE)
                == PackageManager.PERMISSION_GRANTED;
    }

    public static boolean checkLoactionPermission(Context context)
    {
        return ContextCompat.checkSelfPermission(context,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED;
    }

    public static boolean checkStoragePermission(Context context) {

        return ContextCompat.checkSelfPermission(context,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED;
    }

    public static boolean checkCameraPermissionAndStoragePermission(Context context) {
        return ContextCompat.checkSelfPermission(context,
                Manifest.permission.CAMERA) + ContextCompat.checkSelfPermission(context,
                Manifest.permission.READ_EXTERNAL_STORAGE) +
                ContextCompat.checkSelfPermission(context,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED;
    }

    public static boolean checkCallPermission(Context context)
    {
        return ContextCompat.checkSelfPermission(context,
                Manifest.permission.CALL_PHONE)
                == PackageManager.PERMISSION_GRANTED;
    }

    public static void reqForTheLocationPermission(Activity activity,Context context)
    {

        if (PreferenceUtils.checkUserisLogedin(context))
        {
            PreferenceUtils.storeLocPermissionChangedToSharedPreference(context);
        }


        // Should we show an explanation?
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                Manifest.permission.ACCESS_FINE_LOCATION)) {

            // Show an explanation to the user *asynchronously* -- don't block
            // this thread waiting for the user's response! After the user
            // sees the explanation, try again to request the permission.
            openPermissionDialog(activity,"Permission Required!","Please grant Location Permission... without it Travelize will not work");
        } else {
            // No explanation needed, we can request the permission.

            ActivityCompat.requestPermissions(activity,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_TO_REQUEST_LOCATION_ACCESS);

            // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
            // app-defined int constant. The callback method gets the
            // result of the request.
        }
    }


    public static void reqForStoragePermission(Activity activity)
    {
        // Should we show an explanation?
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                Manifest.permission.READ_EXTERNAL_STORAGE)) {

            // Show an explanation to the user *asynchronously* -- don't block
            // this thread waiting for the user's response! After the user
            // sees the explanation, try again to request the permission.
            openPermissionDialog(activity,"Permission Required!","Please grant Storage Permission");

        } else {

            // No explanation needed, we can request the permission.

            ActivityCompat.requestPermissions(activity,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);

            // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
            // app-defined int constant. The callback method gets the
            // result of the request.
        }
    }

    public static void reqForCameraPermissionWithStorage(Activity activity)
    {
        // Should we show an explanation?
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                Manifest.permission.CAMERA) || ActivityCompat.shouldShowRequestPermissionRationale(activity,
                Manifest.permission.READ_EXTERNAL_STORAGE) ||
                ActivityCompat.shouldShowRequestPermissionRationale(activity,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

            // Show an explanation to the user *asynchronously* -- don't block
            // this thread waiting for the user's response! After the user
            // sees the explanation, try again to request the permission.
            openPermissionDialog(activity,"Permission Required!","Please grant Camera Permission and storage");

        } else {

            // No explanation needed, we can request the permission.

            ActivityCompat.requestPermissions(activity,
                    new String[]{Manifest.permission.CAMERA,Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    PERMISSIONS_REQUEST_CAMERA);

            // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
            // app-defined int constant. The callback method gets the
            // result of the request.
        }
    }



    public static void openPermissionDialog(final Context context, String title, String subjuct)
    {
        final Dialog dialog=new Dialog(context, R.style.comm_dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.common_dialog);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(true);
        AppCompatTextView textViewTitle= dialog.findViewById(R.id.commom_dialog_title_id);
        AppCompatTextView textViewSubjuct= dialog.findViewById(R.id.commom_dialog_subjuct_id);
        AppCompatButton buttonSubmit= dialog.findViewById(R.id.commom_dialog_button_id);
        buttonSubmit.setText("Grant");

        textViewTitle.setText("Permission Needed!");
        textViewSubjuct.setText(subjuct);
        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               dialog.dismiss();
                Intent intent = new Intent();
                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", context.getPackageName(), null);
                intent.setData(uri);
                context. startActivity(intent);
            }
        });
        try {
            dialog.show();
        }catch (Exception e)
        {
            e.printStackTrace();
        }


    }

    ////////////////////////////////////////////////////////////// sms permission check
    public static boolean Smspermissioncheck(Context context) {
        //Call whatever you want
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            return ContextCompat
                    .checkSelfPermission(context,
                            Manifest.permission.READ_SMS) +
                    ContextCompat
                            .checkSelfPermission(context,
                                    Manifest.permission.RECEIVE_SMS)
                    ==
                    PackageManager.PERMISSION_GRANTED;
        }else
        {
            return true;
        }
    }


    public static void openAppSettings(final Context context)
    {
        new AlertDialog.Builder(context)
                .setTitle("Alert!!")
                .setMessage("Please grant location permission")
                .setCancelable(false)
                .setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        Intent intent = new Intent();
                        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package",context. getPackageName(), null);
                        intent.setData(uri);
                       context. startActivity(intent);


                    }

                })
                .create().show();
    }

    ///////////////////////////////////////////////////////////////// check call
    /////////////////////////////////////////////////////////////////// open loc dislog
    public static void openPermissionDialog(final Context context, String subjuct)
    {
        final Dialog dialog=new Dialog(context, R.style.comm_dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.common_dialog);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(true);
        AppCompatTextView textViewTitle= dialog.findViewById(R.id.commom_dialog_title_id);
        AppCompatTextView textViewSubjuct= dialog.findViewById(R.id.commom_dialog_subjuct_id);
        AppCompatButton buttonSubmit= dialog.findViewById(R.id.commom_dialog_button_id);
        buttonSubmit.setText("Grant");

        textViewTitle.setText("Permission Needed!");
        textViewSubjuct.setText(subjuct);
        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                Intent intent = new Intent();
                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", context.getPackageName(), null);
                intent.setData(uri);
                context. startActivity(intent);
            }
        });
        try {
            dialog.show();
        }catch (Exception e)
        {
            e.printStackTrace();
        }


    }


}
