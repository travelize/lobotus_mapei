package com.lobotus.mapei.sync;

import android.accounts.Account;
import android.app.Service;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SyncResult;
import android.os.Bundle;
import android.widget.Toast;

import com.google.gson.Gson;
import com.lobotus.mapei.services.fromlite.TrackingService;


import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by user on 4/3/2017.
 */

public class SyncAdapter extends AbstractThreadedSyncAdapter {

    // Global variables
    // Define a variable to contain a content resolver instance
    public ContentResolver mContentResolver=null;

    private Context context=null;
    /**
     * Set up the sync adapter
     */
    public SyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        this.context=context;
        if (mContentResolver!=null)
            mContentResolver=null;
        mContentResolver = context.getContentResolver();

    }

    /**
     * Set up the sync adapter. This form of the
     * constructor maintains compatibility with Android 3.0
     * and later platform versions
     */
    public SyncAdapter(
            Context context,
            boolean autoInitialize,
            boolean allowParallelSyncs) {
        super(context, autoInitialize, allowParallelSyncs);

        /*
         * If your app uses a content resolver, get an instance of it
         * from the incoming Context
         */
        mContentResolver = context.getContentResolver();
    }
    @Override
    public void onPerformSync(Account account, Bundle extras, String authority,
                              ContentProviderClient provider, SyncResult syncResult) {
        if (extras.getBoolean("IS_FOR_KITKAT")) {
            System.out.println("hhhhhhhhhhhhhhh---------onPerformSync--------");
            Intent intentService=new Intent(context,TrackingService.class);
            intentService.setFlags(Service.START_STICKY);
            context.startService(intentService);
        }

    }
}
