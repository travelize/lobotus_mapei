package com.lobotus.mapei.baseactivity;

import android.app.Dialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.lobotus.mapei.R;
import com.lobotus.mapei.flowfragments.fragments.DemoFragment;
import com.lobotus.mapei.flowfragments.fragments.LeaveFragment;
import com.lobotus.mapei.flowfragments.fragments.MeetingFrag;
import com.lobotus.mapei.flowfragments.fragments.OfficeFragment;
import com.lobotus.mapei.flowfragments.fragments.ProjectsFragment;
import com.lobotus.mapei.flowfragments.fragments.ResetPassword;
import com.lobotus.mapei.flowfragments.fragments.SuccessFragment;
import com.lobotus.mapei.flowfragments.fragments.TravelDataFrag;
import com.lobotus.mapei.flowfragments.fragments.TravelFragment;
import com.lobotus.mapei.fromlite.fragment.MeetingsFragment;
import com.lobotus.mapei.retrofitapi.APIService;
import com.lobotus.mapei.retrofitapi.ApiUtils;
import com.lobotus.mapei.services.fromlite.TrackingService;
import com.lobotus.mapei.utils.CommonFunc;
import com.lobotus.mapei.utils.ConstantsUtils;
import com.lobotus.mapei.utils.NetworkUtils;
import com.lobotus.mapei.utils.PermissionUtils;
import com.lobotus.mapei.utils.PreferenceUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;


import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.lobotus.mapei.utils.ConstantsUtils.NOTIFICATION_ENTRY_FROM_MAPEI_BANGLORE_HO_MESSAGE;
import static com.lobotus.mapei.utils.PreferenceUtils.SESSION_PREFERENCE_EMAIL_KEY;
import static com.lobotus.mapei.utils.PreferenceUtils.SESSION_PREFERENCE_FULLNAME_KEY;
import static com.lobotus.mapei.utils.PreferenceUtils.SESSION_PREFERENCE_NAME;
import static com.lobotus.mapei.utils.PreferenceUtils.SESSION_PREFERENCE_PHONE_KEY;
import static com.lobotus.mapei.utils.PreferenceUtils.SESSION_PREFERENCE_PROFILE_PIC_URI_KEY;

@SuppressWarnings("MissingPermission")
public class BaseActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, FragmentManager.OnBackStackChangedListener {
    public Toolbar toolbar = null;
    public ActionBarDrawerToggle toggle = null;

    /////////////
    public static int REQUEST_CHECK_SETTINGS_HOME_FRAGMENT = 108;


    @SuppressWarnings("ConstantConditions")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportFragmentManager().addOnBackStackChangedListener(this);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setItemIconTintList(null);
        navigationView.setNavigationItemSelectedListener(this);
        //
        View headerView = navigationView.getHeaderView(0);
        setTheHeaderData(headerView);

        Menu menu = navigationView.getMenu();
        //
        MenuItem menuItemHom = menu.findItem(R.id.nav_home);
        menuItemHom.setIcon(ContextCompat.getDrawable(BaseActivity.this, R.drawable.ic_home));
        //
        MenuItem menuProject = menu.findItem(R.id.nav_projects);
        menuProject.setIcon(ContextCompat.getDrawable(BaseActivity.this, R.drawable.ic_projects));
        //
        MenuItem menuMeetings = menu.findItem(R.id.nav_meetings);
        menuMeetings.setIcon(ContextCompat.getDrawable(BaseActivity.this, R.drawable.ic_meetings));
        //
        MenuItem menuTravel = menu.findItem(R.id.nav_travel);
        menuTravel.setIcon(ContextCompat.getDrawable(BaseActivity.this, R.drawable.ic_travel));
        //
        MenuItem menuList = menu.findItem(R.id.nav_travel_list);
        menuList.setIcon(ContextCompat.getDrawable(BaseActivity.this, R.drawable.ic_travel));
        //
        MenuItem menuDemo = menu.findItem(R.id.nav_demo);
        menuDemo.setIcon(ContextCompat.getDrawable(BaseActivity.this, R.drawable.ic_demo));
        //
        MenuItem menuLeave = menu.findItem(R.id.nav_leave);
        menuLeave.setIcon(ContextCompat.getDrawable(BaseActivity.this, R.drawable.ic_leave));
        //
        MenuItem menuOffice = menu.findItem(R.id.nav_office);
        menuOffice.setIcon(ContextCompat.getDrawable(BaseActivity.this, R.drawable.ic_office_nav));

        //////////////////////////////////////////////////////////////////
        getSupportActionBar().setTitle(getString(R.string.Dashboard));
        findViewById(R.id.btnviewmeetings).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attachFragment(new MeetingsFragment(), true, getString(R.string.Meetings), null);
            }
        });
        /////////// check in or check out. . . . 
        findViewById(R.id.fragment_home_check_in_out).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                performCheckInOrOut();
            }
        });


        /////////////////////////////////////////
        if (getIntent().getAction() != null) {
            if (getIntent().getAction().equals("START_GEO_FENCE")) {

            } else if (getIntent().getAction().equals(NOTIFICATION_ENTRY_FROM_MAPEI_BANGLORE_HO_MESSAGE)) {
                attachFragment(new OfficeFragment(), true, getString(R.string.Office), null);
            }
        }
        getDistanceTravelledByTheUser();
        checkGpsAndTheSession();
        ////////////////////////////
        setInitialCheckInOrOut();
        /////////////////////////////
    }

    private void setTheHeaderData(final View view) {
        SharedPreferences sharedPreferences = getSharedPreferences(SESSION_PREFERENCE_NAME,
                Context.MODE_PRIVATE);
        ((AppCompatTextView) view.findViewById(R.id.header_userName))
                .setText("" + sharedPreferences.getString(SESSION_PREFERENCE_FULLNAME_KEY, "User Name : NA"));
        ((AppCompatTextView) view.findViewById(R.id.header_mobileNo))
                .setText("" + sharedPreferences.getString(SESSION_PREFERENCE_PHONE_KEY, "PhNo : NA"));
        ((AppCompatTextView) view.findViewById(R.id.header_email))
                .setText("" + sharedPreferences.getString(SESSION_PREFERENCE_EMAIL_KEY, "Email : NA"));

        ////
        Glide.with(BaseActivity.this)
                .load(sharedPreferences.getString(ConstantsUtils.postToGetProfilePic + SESSION_PREFERENCE_PROFILE_PIC_URI_KEY, "no")).asBitmap()
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                        if (resource != null) {
                            ((AppCompatImageView) view.findViewById(R.id.header_profilepic)).setImageBitmap(resource);
                        } else {
                            ((AppCompatImageView) view.findViewById(R.id.header_profilepic)).setImageDrawable(getResources().getDrawable(R.drawable.ic_profile_icon));
                        }
                    }
                });


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                if (getSupportActionBar().getTitle().equals(""))
                    clearFragments();
                else
                    super.onBackPressed();
            } else {
                alertDialogToClose();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.base, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        if (id == R.id.action_logout) {
            if (!PreferenceUtils.getIsUserCheckedIn(BaseActivity.this))
                postLogoutToServer();
            else
                CommonFunc.commonDialog(BaseActivity.this, getString(R.string.alert), "Please check - out first", false);
            return true;
        }

        if (id == R.id.action_resetpassword) {
            attachFragment(new ResetPassword(), true, "Reset password", null);
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        clearFragments();
        if (id == R.id.nav_home) {

        } else if (id == R.id.nav_projects) {
            attachFragment(new ProjectsFragment(), true, getString(R.string.Projects), null);
        } else if (id == R.id.nav_meetings) {
            attachFragment(new MeetingsFragment(), true, getString(R.string.Meetings), null);
        } else if (id == R.id.nav_travel) {
            attachFragment(new TravelFragment(), true, getString(R.string.Travel), null);
        } else if (id == R.id.nav_demo) {
            attachFragment(new DemoFragment(), true, getString(R.string.Demo), null);
        } else if (id == R.id.nav_leave) {
            attachFragment(new LeaveFragment(), true, getString(R.string.Leaves), null);
        } else if (id == R.id.nav_office) {
            attachFragment(new OfficeFragment(), true, getString(R.string.Office), null);
        } else if (id == R.id.nav_travel_list) {
            attachFragment(new TravelDataFrag(), true, getString(R.string.Travel_data), null);
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        /////////////////////////////////////////////////// check all stuff on activity change
        checkGpsAndTheSession();
        return true;
    }

    public void attachFragment(Fragment fragment, boolean isAddToBackStack, String tag, Bundle bundle) {
        if (bundle != null)
            fragment.setArguments(bundle);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.content_base, fragment, tag);
        if (isAddToBackStack)
            fragmentTransaction.addToBackStack(tag);
        fragmentTransaction.commit();
    }

    public void attachSuccessFragment(String message, String addMoreButtonName) {
        Bundle bundle = new Bundle();
        bundle.putString(ConstantsUtils.successFragMessageKey, message);
        bundle.putString(ConstantsUtils.successFragAddMoreButtonKey, addMoreButtonName);
        attachFragment(new SuccessFragment(), true, "", bundle);
    }

    public void clearFragments() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }

    private void alertDialogToClose() {
        new AlertDialog.Builder(BaseActivity.this)
                .setTitle("Alert!!")
                .setMessage("Do you want to close this app")
                .setCancelable(false)
                .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                }).setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).create().show();
    }



    @Override
    public void onBackStackChanged() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                    onBackPressed();
                } else {
                    DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                    if (drawer.isDrawerOpen(GravityCompat.START)) {
                        drawer.closeDrawer(GravityCompat.START);
                    } else {
                        drawer.openDrawer(GravityCompat.START);
                    }
                }

            }
        });
        int fragCount = getSupportFragmentManager().getFragments().size();
        if (fragCount > 0) {
            toggle.setDrawerIndicatorEnabled(false);
            if (!getSupportFragmentManager().getFragments().get(fragCount - 1).getTag().equals("com.bumptech.glide.manager")) {
                getSupportActionBar().setTitle(getSupportFragmentManager().getFragments().get(fragCount - 1).getTag());
            }
            else {
                getSupportActionBar().setTitle("Dashboard");
                toggle.setDrawerIndicatorEnabled(true);
            }
        } else {
            getSupportActionBar().setTitle(getString(R.string.Dashboard));
            toggle.setDrawerIndicatorEnabled(true);
        }
        if (getSupportActionBar().getTitle().equals("SupportLifecycleFragmentImpl")) {
            getSupportActionBar().setTitle(getString(R.string.Dashboard));
            toggle.setDrawerIndicatorEnabled(true);
        }

    }
    private Dialog dialogProgress = null;

    @SuppressWarnings("ConstantConditions")
    private void showProgressDialog() {
        dialogProgress = new Dialog(BaseActivity.this);
        dialogProgress.setCancelable(false);
        dialogProgress.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialogProgress.setContentView(R.layout.loaging_layout);
        dialogProgress.show();
    }

    private void dismissProgressDialog() {
        if (dialogProgress != null) {
            if (dialogProgress.isShowing()) {
                dialogProgress.dismiss();
            }
            dialogProgress = null;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        for (int i = 0; i < getSupportFragmentManager().getFragments().size(); i++) {
            if (getSupportFragmentManager().getFragments().get(i) instanceof TravelFragment) {

                if (requestCode == PermissionUtils.PERMISSIONS_REQUEST_CAMERA) {
                    if (PermissionUtils.checkCameraPermissionAndStoragePermission(BaseActivity.this)) {
                        ((TravelFragment) getSupportFragmentManager().getFragments().get(i)).OnCameraPermissionResult(true);
                    } else {
                        ((TravelFragment) getSupportFragmentManager().getFragments().get(i)).OnCameraPermissionResult(false);
                    }
                } else if (requestCode == PermissionUtils.PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE) {
                    if (PermissionUtils.checkStoragePermission(BaseActivity.this)) {
                        ((TravelFragment) getSupportFragmentManager().getFragments().get(i)).OnStoragePermissionResult(true);
                    } else {
                        ((TravelFragment) getSupportFragmentManager().getFragments().get(i)).OnStoragePermissionResult(false);

                    }
                }
                break;
            }
        }

    }

    ////////////////////////////////////////////////////////////////////////////// Geo fence. . . .
    public void performLogout() {

        PreferenceUtils.clearUserSessionPreferences(BaseActivity.this);
       /* Intent intent=new Intent(BaseActivity.this, MapeiTrackingService.class);
        stopService(intent);*/
        finish();
    }

    private void postLogoutToServer() {
        showProgressDialog();
        APIService apiService = ApiUtils.getAPIService(ConstantsUtils.postBaseUrl +
                ConstantsUtils.postLogout);
        if (PreferenceUtils.getUserIdFromPreference(BaseActivity.this) != null) {
            apiService.postLogoutToTheServer(PreferenceUtils.getUserIdFromPreference(BaseActivity.this))
                    .enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            dismissProgressDialog();
                            if (response.isSuccessful()) {
                                try {
                                    JSONObject jsonObject = new JSONObject(response.body().string());
                                    if (jsonObject.getInt("Success") == 1) {
                                        performLogout();
                                    } else {
                                        CommonFunc.commonDialog(BaseActivity.this,
                                                getString(R.string.alert), jsonObject.getString("Msg"),
                                                false);
                                    }
                                } catch (JSONException | IOException e) {
                                    CommonFunc.commonDialog(BaseActivity.this,
                                            getString(R.string.internalError), getString(R.string.justErrorCode) + " 76",
                                            false);
                                }
                            } else {
                                CommonFunc.commonDialog(BaseActivity.this,
                                        getString(R.string.alert), getString(R.string.justErrorCode) + " 75",
                                        false);
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            dismissProgressDialog();
                            CommonFunc.commonDialog(BaseActivity.this,
                                    getString(R.string.alert), getString(R.string.justErrorCode) + " 74",
                                    true);
                        }
                    });
        }
    }

    /////////////////////////////////////////////////////////////////// set initial check-in or out
    private void setInitialCheckInOrOut() {
        if (!PreferenceUtils.getIsUserCheckedIn(BaseActivity.this))
            ((AppCompatButton) findViewById(R.id.fragment_home_check_in_out))
                    .setText("Check-In");
        else
            ((AppCompatButton) findViewById(R.id.fragment_home_check_in_out))
                    .setText("Check-Out");
    }

    ////////////////////////////////////////////////////////////// get the distance travelled by the user
    private void getDistanceTravelledByTheUser() {
        showProgressDialog();
        APIService apiService = ApiUtils.getAPIService(ConstantsUtils.postBaseUrl + ConstantsUtils.PostTogetTheDistanceTravelledData);
        apiService.postToGetTheDistanceTravelledByUser(PreferenceUtils.getUserIdFromPreference(BaseActivity.this))
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        dismissProgressDialog();
                        if (response.isSuccessful()) {
                            try {
                                JSONObject jsonObject = new JSONObject(response.body().string());
                                ((AppCompatTextView) findViewById(R.id.activity_base_distance_travelled_id))
                                        .setText("Distance travelled :" + jsonObject.optString("DistanceTravelled", "Distance travelled : NA"));
                            } catch (JSONException | IOException e) {
                                CommonFunc.commonDialog(BaseActivity.this, getString(R.string.alert),
                                        getString(R.string.justErrorCode) + " 106", true);
                            }
                        } else {
                            CommonFunc.commonDialog(BaseActivity.this, getString(R.string.alert),
                                    getString(R.string.justErrorCode) + " 105", true);
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        dismissProgressDialog();
                        CommonFunc.commonDialog(BaseActivity.this, getString(R.string.alert),
                                getString(R.string.noInternetMessage) + " 104", true);
                    }
                });
    }


    //////////////////////////////////////////////////////////


    private void checkGPSisOn() {
        LocationRequest locationRequest = new LocationRequest();
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        Task<LocationSettingsResponse> locationSettingsResponseTask =
                LocationServices.getSettingsClient(BaseActivity.this).checkLocationSettings(builder.build());
        locationSettingsResponseTask.addOnCompleteListener(new OnCompleteListener<LocationSettingsResponse>() {
            @Override
            public void onComplete(@NonNull Task<LocationSettingsResponse> task) {
                try {

                    LocationSettingsResponse response = task.getResult(ApiException.class);
                    // All location settings are satisfied. The client can initialize location
                    // requests here.
                    if (PermissionUtils.checkLoactionPermission(BaseActivity.this) && NetworkUtils.isConnected(BaseActivity.this)) {


                    } else {

                        if (!PermissionUtils.checkLoactionPermission(BaseActivity.this)) {
                            PermissionUtils.openPermissionDialog(BaseActivity.this, "Permission needed!", "Please grant location permission.");
                            /*Utility.alertMessage(Login.this,"Location permission not granted!","Please grant LOCATION permission and login again");*/

                        } else {
                            NetworkUtils.checkInternetAndOpenDialog(BaseActivity.this);
                            /*Utility.alertMessage(Login.this,"Mobile data and wi-fi not enabled!","Please enable your Mobile data or Wi-Fi and login again");*/
                        }
                    }

                } catch (ApiException exception) {


                    switch (exception.getStatusCode()) {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // Location settings are not satisfied. But could be fixed by showing the
                            // user a dialog.
                            try {
                                NetworkUtils.openGpsSettings(BaseActivity.this);
                                /*Utility.alertMessage(Login.this,"GPS not enabled!","Please enable your device GPS  and login again");*/

                            } catch (ClassCastException e) {

                                NetworkUtils.openGpsSettings(BaseActivity.this);
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:

                            NetworkUtils.openGpsSettings(BaseActivity.this);
                            break;
                    }
                }
            }
        });


    }

    //////////////////////////////////////////////////////////// forcefully logout the user dialog

    public void openForceLogoutDialog() {
        final Dialog dialog = new Dialog(BaseActivity.this, R.style.comm_dialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.common_dialog);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        AppCompatTextView textViewTitle = dialog.findViewById(R.id.commom_dialog_title_id);
        AppCompatTextView textViewSubjuct = dialog.findViewById(R.id.commom_dialog_subjuct_id);
        AppCompatButton buttonSubmit = dialog.findViewById(R.id.commom_dialog_button_id);
        buttonSubmit.setText("Logout");

        textViewTitle.setText("Session Lost");
        textViewSubjuct.setText("Please logout and login again");
        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                postLogoutToServer();
            }
        });
        try {
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    ///////////////////////////////////////////////////// check gps and session
    private void checkGpsAndTheSession() {
     /*   checkGPSisOn();
        if (PermissionUtils.checkLoactionPermission(BaseActivity.this)) {
            if (!PreferenceUtils.getLocPermissonGotChangedOrNot(BaseActivity.this)) {
                *//*open  force logout dialog*//*
                openForceLogoutDialog();
            }
        } else {
            PreferenceUtils.storeLocPermissionChangedToSharedPreference(BaseActivity.this);
            *//*open  force logout dialog*//*
            openForceLogoutDialog();
        }*/
    }

    ///////////////////////////////////////////////////// perform check in the user. . . .
    private void performCheckInOrOut() {
        if (PermissionUtils.checkLoactionPermission(BaseActivity.this)) {
            connectToGoogleApiClientAndGetTheAddress();
        } else {
            PermissionUtils.openPermissionDialog(BaseActivity.this, "Permission required!", "Please grant location permission");
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////    get loc stuff. . .  .
    ////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////// google api client establishment to access address
    ////
    private GoogleApiClient googleApiClient = null;
    private LocationRequest locationRequest = null;
    private FusedLocationProviderClient mFusedLocationClient = null;
    private AsyncTaskToGetTheAddress asyncTaskToGetTheAddress = null;

    ///
    private void connectToGoogleApiClientAndGetTheAddress() {
        if (NetworkUtils.checkInternetAndOpenDialog(BaseActivity.this)) {
            if (CommonFunc.isGooglePlayServicesAvailable(BaseActivity.this)) {
                if (dialogProgress == null) {
                    showProgressDialog();
                    connectGoogleApiClient();
                }
            }
        }
    }

    private void connectGoogleApiClient() {

        googleApiClient = new GoogleApiClient.Builder(BaseActivity.this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(@Nullable Bundle bundle) {
                        checkForLocationSettings();
                    }

                    @Override
                    public void onConnectionSuspended(int i) {
                    }
                })
                .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                        Toast.makeText(BaseActivity.this, "Google play service not responding... please refresh your mobile",
                                Toast.LENGTH_LONG).show();
                        googleApiClient = null;
                        dismissProgressDialog();

                    }
                }).build();
        googleApiClient.connect();


    }

    private void checkForLocationSettings() {
        ////////////////////////////////////////////////////////

        locationRequest = new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);

        //**************************
        builder.setAlwaysShow(true); //this is the key ingredient
        //**************************

        Task<LocationSettingsResponse> locationSettingsResponseTask =
                LocationServices.getSettingsClient(BaseActivity.this).checkLocationSettings(builder.build());
        //
        locationSettingsResponseTask.addOnCompleteListener(new OnCompleteListener<LocationSettingsResponse>() {
            @Override
            public void onComplete(@NonNull Task<LocationSettingsResponse> task) {
                try {
                    LocationSettingsResponse response = task.getResult(ApiException.class);
                    // All location settings are satisfied. The client can initialize location
                    // requests here.
                    reqForLocToCheckIn();

                } catch (ApiException exception) {
                    dismissProgressDialog();

                    switch (exception.getStatusCode()) {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // Location settings are not satisfied. But could be fixed by showing the
                            // user a dialog.
                            try {
                                // Cast to a resolvable exception.
                                ResolvableApiException resolvable = (ResolvableApiException) exception;
                                // Show the dialog by calling startResolutionForResult(),
                                // and check the result in onActivityResult().
                                resolvable.startResolutionForResult(
                                        BaseActivity.this,
                                        REQUEST_CHECK_SETTINGS_HOME_FRAGMENT);
                            } catch (IntentSender.SendIntentException | ClassCastException e) {
                                // Ignore the error.
                                CommonFunc.commonDialog(BaseActivity.this, "GPS not working.. Please refresh your device",
                                        getString(R.string.justErrorCode) + " 98", false);

                            }

                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            CommonFunc.commonDialog(BaseActivity.this, "GPS not working.. Please refresh your device",
                                    getString(R.string.justErrorCode) + " 87", false);
                            break;
                    }
                }
            }
        });
        //

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CHECK_SETTINGS_HOME_FRAGMENT) {
            if (resultCode == RESULT_OK) {
                reqForLocToCheckIn();
            } else if (resultCode == RESULT_CANCELED) {
                checkForLocationSettings();
                Toast.makeText(BaseActivity.this, "Please grant Gps access by clicking OK in the shown dialog", Toast.LENGTH_LONG).show();
            } else {
                reqForLocToCheckIn();
                Toast.makeText(BaseActivity.this, "Device gps is not responding.. Please refresh your device.. Error code 92", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void reqForLocToCheckIn() {
        getLastLocation();
    }

    @SuppressWarnings("MissingPermission")
    private void getLastLocation() {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(BaseActivity.this);
        mFusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);

    }

    private LocationCallback locationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {

            for (Location location : locationResult.getLocations()) {
                decodeAddress(new LatLng(location.getLatitude(),
                        location.getLongitude()));
                stopLocationUpdates();
            }
        }
    };

    private void decodeAddress(final LatLng latLng) {
        //https://maps.googleapis.com/maps/api/geocode/json?latlng=40.714224,-73.961452&key=YOUR_API_KEY
        String req = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" +
                latLng.latitude + "," + latLng.longitude + "&key=" + getString(R.string.google_key);


        if (asyncTaskToGetTheAddress != null) {
            if (asyncTaskToGetTheAddress.getStatus() != AsyncTask.Status.RUNNING) {
                asyncTaskToGetTheAddress = new AsyncTaskToGetTheAddress();
                asyncTaskToGetTheAddress.execute(req);
            }
        } else {
            asyncTaskToGetTheAddress = new AsyncTaskToGetTheAddress();
            asyncTaskToGetTheAddress.execute(req);
        }
    }

    private class AsyncTaskToGetTheAddress extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... strings) {
            return makeserverConnection(strings[0]);
        }

        @Override
        protected void onPostExecute(String s) {
            System.out.println("hhhhhhhhhhhhhh-------s----------" + s);
            dismissProgressDialog();
            if (s != null) {
                try {
                    JSONObject jsonObject = new JSONObject(s);
                    JSONArray jsonArray = jsonObject.getJSONArray("results");
                    if (jsonArray.length() > 0) {
                        String formatedAddress = jsonArray.getJSONObject(0).getString("formatted_address");
                        if (formatedAddress != null && !formatedAddress.isEmpty()) {
                            if (((AppCompatButton) findViewById(R.id.fragment_home_check_in_out)).getText().equals("Check-In")) {
                                performCheckIn(formatedAddress);
                            }
                            if (((AppCompatButton) findViewById(R.id.fragment_home_check_in_out)).getText().equals("Check-Out")) {
                                performCheckOut(formatedAddress);
                            }
                        } else {
                            if (((AppCompatButton) findViewById(R.id.fragment_home_check_in_out)).getText().equals("Check-In")) {
                                performCheckIn("");
                            }
                            if (((AppCompatButton) findViewById(R.id.fragment_home_check_in_out)).getText().equals("Check-Out")) {
                                performCheckOut("");
                            }
                        }

                    } else {
                        CommonFunc.commonDialog(BaseActivity.this, "Alert!", "Unable to detect your location", false);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                CommonFunc.commonDialog(BaseActivity.this, "Alert!", "Unable to detect your location", false);
            }
        }


    }

    public String makeserverConnection(String requestURL) {
        URL url;
        String response = "";
        try {
            url = new URL(requestURL);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded");
            conn.setDoOutput(true);
            int responseCode = conn.getResponseCode();
            if (responseCode == HttpsURLConnection.HTTP_OK) {

                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line = br.readLine()) != null) {
                    response += line;
                }
            } else {
                response = "";
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    private void stopLocationUpdates() {
        if (mFusedLocationClient != null)
            mFusedLocationClient.removeLocationUpdates(locationCallback);
    }

    ////////////////////////////////////////////////////////////////////////////////////// set user check in
    private void performCheckIn(String loc) {
        showProgressDialog();
        APIService apiService = ApiUtils.getAPIService(ConstantsUtils.postBaseUrl + ConstantsUtils.POST_TO_CHECK_IN);
        apiService.postToPerformCheckIn(PreferenceUtils.getUserIdFromPreference(BaseActivity.this), loc)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        dismissProgressDialog();
                        if (response.isSuccessful()) {
                            try {
                                JSONObject jsonObject = new JSONObject(response.body().string());
                                if (jsonObject.getInt("Success") == 1) {
                                    PreferenceUtils.setUserHasCheckedIn(BaseActivity.this, jsonObject.getString("Msg"));
                                    Intent intentService = new Intent(BaseActivity.this, TrackingService.class);
                                    intentService.setFlags(Service.START_STICKY);
                                    startService(intentService);
                                    ((AppCompatButton) findViewById(R.id.fragment_home_check_in_out))
                                            .setText("Check-Out");
                                    Toast.makeText(BaseActivity.this, "Successfully checked in", Toast.LENGTH_SHORT).show();
                                } else {

                                    CommonFunc.commonDialog(BaseActivity.this, getString(R.string.internalError),
                                            jsonObject.getString("Msg"), false);
                                }
                            } catch (JSONException | IOException e) {
                                CommonFunc.commonDialog(BaseActivity.this, getString(R.string.internalError),
                                        getString(R.string.justErrorCode) + " 9", false);

                            }
                        } else {

                            CommonFunc.commonDialog(BaseActivity.this, getString(R.string.internalError),
                                    getString(R.string.justErrorCode) + " 10", false);
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        dismissProgressDialog();
                        CommonFunc.commonDialog(BaseActivity.this, getString(R.string.noInternetMessage),
                                getString(R.string.noInternetMessage) + " 11", true);
                    }
                });
    }

    /////////////////////////////////////////////////////////////////////////////////////// set user as check out
    private void performCheckOut(String loc) {
        showProgressDialog();
        APIService apiService = ApiUtils.getAPIService(ConstantsUtils.postBaseUrl + ConstantsUtils.POST_TO_CHECK_OUT);
        apiService.postToPerformCheckOut(PreferenceUtils.getCheckedInUserAttenceId(BaseActivity.this), loc)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        dismissProgressDialog();
                        if (response.isSuccessful()) {
                            try {
                                JSONObject jsonObject = new JSONObject(response.body().string());
                                if (jsonObject.getInt("Success") == 1) {
                                    PreferenceUtils.setUserHasCheckedOut(BaseActivity.this);
                                    Intent intentService = new Intent(BaseActivity.this, TrackingService.class);
                                    stopService(intentService);
                                    ((AppCompatButton) findViewById(R.id.fragment_home_check_in_out))
                                            .setText("Check-In");
                                    Toast.makeText(BaseActivity.this, "You have successfully checked-Out", Toast.LENGTH_SHORT).show();

                                } else {
                                    CommonFunc.commonDialog(BaseActivity.this, getString(R.string.internalError),
                                            jsonObject.getString("Msg"), false);
                                }
                            } catch (JSONException | IOException e) {

                                CommonFunc.commonDialog(BaseActivity.this, getString(R.string.internalError),
                                        getString(R.string.justErrorCode) + " 14", false);
                            }
                        } else {

                            CommonFunc.commonDialog(BaseActivity.this, getString(R.string.internalError),
                                    getString(R.string.justErrorCode) + " 13", false);
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        dismissProgressDialog();

                        CommonFunc.commonDialog(BaseActivity.this, getString(R.string.noInternetMessage),
                                getString(R.string.noInternetMessage) + " 12", true);
                    }
                });
    }

}
