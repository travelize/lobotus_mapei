package com.lobotus.mapei.services.fromlite;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofenceStatusCodes;
import com.google.android.gms.location.GeofencingEvent;
import com.lobotus.mapei.R;
import com.lobotus.mapei.baseactivity.BaseActivity;
import com.lobotus.mapei.fromlite.model.CommRespModel;
import com.lobotus.mapei.retrofitapi.APIService;
import com.lobotus.mapei.retrofitapi.ApiUtils;
import com.lobotus.mapei.utils.ConstantsUtils;
import com.lobotus.mapei.utils.PreferenceUtils;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by user1 on 03-10-2017.
 */

public class GeofenceTrasitionService extends IntentService {

    private static final String TAG = GeofenceTrasitionService.class.getSimpleName();
    public static final int GEOFENCE_NOTIFICATION_ID = 0;
    //

    /*public  final String NOTIFICATION_ENTRY_FROM_MAPEI_MESSAGE="Entering please enter purpose of your visit";
    public  final String NOTIFICATION_EXIT_FROM_MAPEI_MESSAGE="You have exited.";*/



    public GeofenceTrasitionService() {
        super(TAG);
    }
    private Context context=null;

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        // Retrieve the Geofencing intent
        context=this;
        GeofencingEvent geofencingEvent = GeofencingEvent.fromIntent(intent);

        // Handling errors
        if ( geofencingEvent.hasError() ) {
            String errorMsg = getErrorString(geofencingEvent.getErrorCode() );
            Log.e( TAG, errorMsg );
            return;
        }

        // Retrieve GeofenceTrasition
        int geoFenceTransition = geofencingEvent.getGeofenceTransition();
        // Check if the transition type
        if ( geoFenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER ||
                geoFenceTransition == Geofence.GEOFENCE_TRANSITION_EXIT ) {
            // Get the geofence that were triggered
            List<Geofence> triggeringGeofences = geofencingEvent.getTriggeringGeofences();
            // Create a detail message with Geofences received
            String geofenceTransitionDetails = getGeofenceTrasitionDetails(geoFenceTransition, triggeringGeofences );
            // Send notification details as a String
            postEvent( geofenceTransitionDetails );
        }
    }

    // Create a detail message with Geofences received
    private String getGeofenceTrasitionDetails(int geoFenceTransition, List<Geofence> triggeringGeofences) {
        // get the ID of each geofence triggered
        ArrayList<String> triggeringGeofencesList = new ArrayList<>();
        for ( Geofence geofence : triggeringGeofences ) {
            triggeringGeofencesList.add( geofence.getRequestId() );
        }

        String status = null;
        if ( geoFenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER )
            status = "You have entered the premises of "+ PreferenceUtils.getClientNameFromStartRoutePreference(context);
        else if ( geoFenceTransition == Geofence.GEOFENCE_TRANSITION_EXIT )
            status = "You have exited the premises of "+PreferenceUtils.getClientNameFromStartRoutePreference(context);

        return status;
    }

    // Send a notification
    private void postEvent( String msg ) {

        if (msg.equals( "You have entered the premises of "+PreferenceUtils.getClientNameFromStartRoutePreference(context)))
        {
            EntryPostTOServer(msg);
        }
        if (msg.equals( "You have exited the premises of "+PreferenceUtils.getClientNameFromStartRoutePreference(context)))
        {
            ExitPostTOServer(msg);
        }
    }



    private void sendNotification(String msg)
    {

        ///////////////////////////////
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        int importance = NotificationManager.IMPORTANCE_HIGH;
        NotificationChannel notificationChannel = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            notificationChannel = new NotificationChannel("Geo fence detection id",
                    "Geo fence detection name",
                    importance);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.enableVibration(true);
            notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            notificationManager.createNotificationChannel(notificationChannel);
        }
        ///////////////////////////////







        Intent intent=new Intent(this,BaseActivity.class);
        intent.setAction(msg);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(BaseActivity.class);
        stackBuilder.addNextIntent(intent);
        PendingIntent notificationPendingIntent = stackBuilder.getPendingIntent(0,
                PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationManager notificatioMng =
                (NotificationManager) getSystemService( Context.NOTIFICATION_SERVICE );
        notificatioMng.notify(
                GEOFENCE_NOTIFICATION_ID,
                createNotification(msg, notificationPendingIntent));
    }

    // Create a notification
    private Notification createNotification(String msg, PendingIntent notificationPendingIntent) {
        Bitmap logoBitmapIcon = BitmapFactory.decodeResource(this.getResources(), R.drawable.app_logo);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this,"Geo fence detection id");
        notificationBuilder
                .setSmallIcon(R.drawable.app_ticker_icon)
                .setLargeIcon(logoBitmapIcon)
                .setColor(Color.RED)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(msg)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setContentIntent(notificationPendingIntent)
                .setDefaults(Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE | Notification.DEFAULT_SOUND)
                .setAutoCancel(true);
        return notificationBuilder.build();
    }

    // Handle errors
    private static String getErrorString(int errorCode) {
        switch (errorCode) {
            case GeofenceStatusCodes.GEOFENCE_NOT_AVAILABLE:
                return "GeoFence not available";
            case GeofenceStatusCodes.GEOFENCE_TOO_MANY_GEOFENCES:
                return "Too many GeoFences";
            case GeofenceStatusCodes.GEOFENCE_TOO_MANY_PENDING_INTENTS:
                return "Too many pending intents";
            default:
                return "Unknown error.";
        }
    }

    private void EntryPostTOServer(final String msg)
    {
        sendNotification(msg);
        afterGeoFenceEntryEventTOTheServer();
    }
    private void ExitPostTOServer(final String msg)
    {
        sendNotification(msg);
        Intent intent=new Intent(ConstantsUtils.GEO_FENCE_BROADCAST_ACTION);
        intent.putExtra(ConstantsUtils.IS_GEO_FENCE_EVENT_FOR_START_GEOFENCE_BOOLEAN,false);
        sendBroadcast(intent);
        afterGeoFenceExitEventTOTheServer();
    }


    /////////////////////////////////////////// geo fence entry and exit event. . . .
    // entry
    private void afterGeoFenceEntryEventTOTheServer()
    {
        APIService apiService= ApiUtils.getAPIService(ConstantsUtils.postBaseUrl+ConstantsUtils.POST_TO_SERVER_AFTER_USER_ENTER_THE_GEOFENCED_REGION);
        apiService.postToServerAfterUserEnteredTheGeoFencedRegion(PreferenceUtils.getMeetingIdFromStartRoutePreference(context))
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful())
                        {
                            try {
                                JSONObject jsonObject=new JSONObject(response.body().string());
                                if (jsonObject.getInt("Success")==1)
                                {

                                }else {
                                    // error
                                    Toast.makeText(context, context.getString(R.string.justErrorCode)+" 67", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException | IOException e) {
                                // error
                                Toast.makeText(context, context.getString(R.string.justErrorCode)+" 68", Toast.LENGTH_SHORT).show();

                            }
                        }else {
                            // error
                            Toast.makeText(context, context.getString(R.string.justErrorCode)+" 69", Toast.LENGTH_SHORT).show();

                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        // error
                        Toast.makeText(context, context.getString(R.string.noInternetMessage)+" 70", Toast.LENGTH_SHORT).show();

                    }
                });
    }


    private void afterGeoFenceExitEventTOTheServer()
    {
        APIService apiService=ApiUtils.getAPIService(ConstantsUtils.postBaseUrl+ConstantsUtils.POST_TO_SERVER_AFTER_USER_EXITS_THE_GEOFENCED_REGION);
        apiService.postToServerAfterUserExitedTheGeoFencedRegion(PreferenceUtils.getMeetingIdFromStartRoutePreference(context))
                .enqueue(new Callback<CommRespModel>() {
                    @Override
                    public void onResponse(Call<CommRespModel> call, Response<CommRespModel> response) {
                        if (response.isSuccessful())
                        {

                            if (response.body().getSuccess()==1)
                            {

                            }else {
                                // error
                                Toast.makeText(context, context.getString(R.string.justErrorCode)+" 71", Toast.LENGTH_SHORT).show();
                            }
                        }else {
                            // error
                            Toast.makeText(context, context.getString(R.string.justErrorCode)+" 73", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<CommRespModel> call, Throwable t) {
                        // error
                        Toast.makeText(context, context.getString(R.string.justErrorCode)+" 74", Toast.LENGTH_SHORT).show();
                    }
                });
    }



}