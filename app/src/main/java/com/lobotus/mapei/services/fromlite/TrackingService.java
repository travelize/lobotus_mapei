package com.lobotus.mapei.services.fromlite;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.lobotus.mapei.R;
import com.lobotus.mapei.SplashScreen;
import com.lobotus.mapei.baseactivity.BaseActivity;
import com.lobotus.mapei.phonestate.CustomPhoneStateListener;
import com.lobotus.mapei.retrofitapi.APIService;
import com.lobotus.mapei.retrofitapi.ApiUtils;
import com.lobotus.mapei.utils.CheckLocationAccuricy;
import com.lobotus.mapei.utils.ConstantsUtils;
import com.lobotus.mapei.utils.NetworkUtils;
import com.lobotus.mapei.utils.PermissionUtils;
import com.lobotus.mapei.utils.PreferenceUtils;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.support.v4.app.NotificationCompat.PRIORITY_HIGH;
import static com.google.android.gms.location.Geofence.NEVER_EXPIRE;
import static com.lobotus.mapei.utils.ConstantsUtils.ACCOUNT;
import static com.lobotus.mapei.utils.ConstantsUtils.ACCOUNT_TYPE;
import static com.lobotus.mapei.utils.ConstantsUtils.AUTHORITY;
import static com.lobotus.mapei.utils.ConstantsUtils.GEOFENCE_RADIUS;
import static com.lobotus.mapei.utils.ConstantsUtils.IDENTIFIERS_FOR_TRACK_NOTIFICATION;
import static com.lobotus.mapei.utils.ConstantsUtils.NOTIFICATION_CHANNEL_ID_OF_SERVICE_ERROR_NOTIFICATION;
import static com.lobotus.mapei.utils.ConstantsUtils.NOTIFICATION_CHANNEL_ID_OF_TRACKING_NOTIFICATION;
import static com.lobotus.mapei.utils.ConstantsUtils.OREO_TRACK_NOTIFICATION_CHANNEL_NAME;


@SuppressWarnings("MissingPermission")
public class TrackingService extends Service {
    //
    private Context context = null;
    //
    private ReverseGeocodingTask reverseGeocodingTask = null;
    //
/*
    private OfflineDatabase offlineDatabase=null;
*/
    //
    java.util.Calendar calendar = null;
    //
    private Account account = null;
    //
    private final String NOTIFICATION_TYPE_RELOG_USER = "NOTIFICATION_TYPE_RELOG_USER";
    private final String NOTIFICATION_TYPE_ON_GPS = "NOTIFICATION_TYPE_ON_GPS";
    private final String NOTIFICATION_TYPE_GRANT_PERMISSION = "NOTIFICATION_TYPE_GRANT_PERMISSION";
    private final String NOTIFICATION_TYPE_NOTHING = "NOTIFICATION_TYPE_NOTHING";
    // notification id's
    private final int NOTIFICATION_TYPE_RELOG_USER_ID = 100;
    private final int NOTIFICATION_TYPE_ON_GPS_ID = 101;
    private final int NOTIFICATION_TYPE_GRANT_PERMISSION_ID = 102;
    private final int NOTIFICATION_TYPE_NOTHING_ID = 103;
    // temp mock loc boolean value
    private boolean isMockLocEnabled = false;
    //
    private Location locationLastBest = null;
    //


    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    ///////////////////
    @Override
    public void onCreate() {
        // The service is being created
        // And From your main() method or any other method

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // The service is starting, due to a call to startService()
        this.context = this;
        calendar = new GregorianCalendar();
        /*offlineDatabase=new OfflineDatabase(context);*/
        startSignalChangeReq(this);
        showTheForegroundNotification();
        connectToGoogleApiClientAndGetTheAddress();
        registerGeofenceBroadCastStuff();
        registerGpsBroadcast();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        // The service is no longer used and is being destroyed
        stopForeground(true);
        stopLocationUpdatesAndRemoveGoogleApiClient();
        if (isBroadcastRegistredGeofence)
            unregisterReceiver(broadcastReceiverOfGeoFence);
        if (isGpsStatusChangeBroadcastRegistred)
            unregisterReceiver(broadcastReceiverGpsStatus);
        closeWarnningNotification();
    }

    //////////////////////////////// start the foreground thread
    private void showTheForegroundNotification() {

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        int importance = NotificationManager.IMPORTANCE_HIGH;
        NotificationChannel notificationChannel = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID_OF_TRACKING_NOTIFICATION,
                    OREO_TRACK_NOTIFICATION_CHANNEL_NAME,
                    importance);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.enableVibration(true);
            notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            notificationManager.createNotificationChannel(notificationChannel);
        }
        Bitmap logoBitmapIcon = BitmapFactory.decodeResource(this.getResources(), R.drawable.app_logo);
        Intent intent = new Intent(this, BaseActivity.class);
        PendingIntent pendingIntent =
                PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        //
        final NotificationCompat.Builder builder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID_OF_TRACKING_NOTIFICATION);
        builder.setSmallIcon(R.drawable.app_ticker_icon);
        builder.setContentTitle("Travelize");
        builder.setLargeIcon(logoBitmapIcon);
        builder.setTicker("Duty started");
        builder.setContentText("You are on Duty");
        builder.setContentIntent(pendingIntent);
        builder.setPriority(PRIORITY_HIGH);
        final Notification notification = builder.build();
        startForeground(IDENTIFIERS_FOR_TRACK_NOTIFICATION, notification);
    }

    //////////////////////////////////////////////////////////////////////////////////////// location stuff
    private GoogleApiClient googleApiClient = null;
    private LocationRequest locationRequest = null;
    private FusedLocationProviderClient mFusedLocationClient = null;
//    private FusedLocationProviderApi mFusedLocationClient = null;
//    Warning: Please continue using the FusedLocationProviderApi class and don't migrate to the FusedLocationProviderClient class until
//    Google Play services version 12.0.0 is available, which is expected to ship in early 2018. Using the FusedLocationProviderClient before
//    version 12.0.0 causes the client app
//    to crash when Google Play services is updated on the device. We apologize for any inconvenience this may have caused

    @SuppressWarnings("MissingPermission")
    private void connectToGoogleApiClientAndGetTheAddress() {
        connectGoogleApiClient();
    }

    //////////////////////// clode warnning notification
    private void closeWarnningNotification() {
        try {
            NotificationManager mNotificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            mNotificationManager.cancel(NOTIFICATION_TYPE_ON_GPS_ID);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    ////////////////////////////////////////////////////////////////////////////////// google api stuff. . . . . .

    private void connectGoogleApiClient() {

        googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(@Nullable Bundle bundle) {
                        setRequestTime();
                    }

                    @Override
                    public void onConnectionSuspended(int i) {
                    }
                })
                .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                        showTheServiceClassErrorNotification("Google api client not updated!", "Please update Google Api Client.",
                                NOTIFICATION_TYPE_NOTHING);
                        googleApiClient = null;
                    }
                }).build();
        googleApiClient.connect();
    }

    private void setRequestTime() {
        ////////////////////////////////////////////////////////
        locationRequest = new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(10 * 1000);
        locationRequest.setFastestInterval(10 * 1000);
        locationRequest.setSmallestDisplacement(10);
        startLocationReq();
    }

    @SuppressWarnings("MissingPermission")
    private void startLocationReq() {
       /* LocationServices.FusedLocationApi.requestLocationUpdates(  // use this unitill next year. .
                googleApiClient, locationRequest,this);*/
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(context);
        mFusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);
    }

    protected void stopLocationUpdatesAndRemoveGoogleApiClient() {
        if (mFusedLocationClient != null)
            mFusedLocationClient.removeLocationUpdates(locationCallback);
        if (googleApiClient != null) {
           /* LocationServices.FusedLocationApi.removeLocationUpdates(  // use this unitill next year. .
                    googleApiClient, this);
            if (googleApiClient.isConnected())
                googleApiClient.disconnect();
            googleApiClient=null;*/

            if (googleApiClient.isConnected())
                googleApiClient.disconnect();
        }
        locationLastBest = null;

    }


    /* @Override
     public void onLocationChanged(Location location) {
         saveLocToServer(location);
         if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
             isMockLocEnabled=location.isFromMockProvider();
         }else {
             isMockLocEnabled=false;
         }
     }*/
    private LocationCallback locationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {

            for (Location location : locationResult.getLocations()) {

                if (CheckLocationAccuricy.isBetterLocation(location, locationLastBest)) {
                    saveLocToServer(location);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                        isMockLocEnabled = location.isFromMockProvider();
                    } else {
                        isMockLocEnabled = false;
                    }

                }
            }
        }
    };


    //////////////////////////////////////////////////////////////////////// service class error notification
    private void showTheServiceClassErrorNotification(String tickerMessage, String contentMessage, String notificationType) {
        Bitmap logoBitmapIcon = BitmapFactory.decodeResource(this.getResources(), R.drawable.app_logo);
        Intent intentForPendingIntent = null;
        if (notificationType.equals(NOTIFICATION_TYPE_RELOG_USER)) {
            intentForPendingIntent = new Intent(this, SplashScreen.class);
            intentForPendingIntent.setAction("CLEAR_ALL_USER_PREFERENCES");
        }
        if (notificationType.equals(NOTIFICATION_TYPE_ON_GPS)) {
            intentForPendingIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        }
        if (notificationType.equals(NOTIFICATION_TYPE_GRANT_PERMISSION)) {
            intentForPendingIntent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        }
        if (notificationType.equals(NOTIFICATION_TYPE_NOTHING)) {
            intentForPendingIntent = new Intent(this, SplashScreen.class);
        }
        PendingIntent pendingIntent =
                PendingIntent.getActivity(this, 0, intentForPendingIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        //
        final NotificationCompat.Builder builder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID_OF_SERVICE_ERROR_NOTIFICATION);
        builder.setSmallIcon(R.drawable.app_ticker_icon);
        builder.setLargeIcon(logoBitmapIcon);
        builder.setContentTitle(getString(R.string.app_name));
        builder.setTicker(tickerMessage);
        builder.setContentText(contentMessage);
        builder.setAutoCancel(false);
        builder.setOngoing(true);
        builder.setStyle(new NotificationCompat.BigTextStyle().bigText(contentMessage));
        builder.setContentIntent(pendingIntent);
        builder.setPriority(PRIORITY_HIGH);
        final Notification notification = builder.build();
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);


        if (notificationType.equals(NOTIFICATION_TYPE_RELOG_USER)) {
            mNotificationManager.notify(NOTIFICATION_TYPE_RELOG_USER_ID, notification);
        }
        if (notificationType.equals(NOTIFICATION_TYPE_ON_GPS)) {
            mNotificationManager.notify(NOTIFICATION_TYPE_ON_GPS_ID, notification);
        }
        if (notificationType.equals(NOTIFICATION_TYPE_GRANT_PERMISSION)) {
            mNotificationManager.notify(NOTIFICATION_TYPE_GRANT_PERMISSION_ID, notification);
        }
        if (notificationType.equals(NOTIFICATION_TYPE_NOTHING)) {
            mNotificationManager.notify(NOTIFICATION_TYPE_NOTHING_ID, notification);
        }


        //
    }

    /////////////////////////////// signal life
    private void startSignalChangeReq(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("signal_level_pref", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("signal_level_key", "NA");
        editor.apply();
        TelephonyManager tManager = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
        tManager.listen(new CustomPhoneStateListener(context),
                PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);
    }

    /////////////////////////////////// battery life
    public static String batterylevel(Context context) {
        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = context.registerReceiver(null, ifilter);
        int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
        float batteryPct = (level / (float) scale) * 100;
        int ss = Integer.valueOf((int) batteryPct);
        System.out.println("batteryPct" + ss + "%");
        if (ss == 0) {
            ss = 0;
            return ss + "%";
        } else {

            return ss + "%";
        }
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////// postLocToUser

    private void saveLocToServer(Location location) {
        System.out.println("bbbbbbbbbbbb---location.getAccuracy()------------" + location.getAccuracy());
        System.out.println("bbbbbbbbbbbb---location.getLatitude()------------" + location.getLatitude());
        System.out.println("bbbbbbbbbbbb---location.getLongitude()------------" + location.getLongitude());
        if (reverseGeocodingTask == null) {
            reverseGeocodingTask = new ReverseGeocodingTask();
            reverseGeocodingTask.execute(new LatLng(location.getLatitude(), location.getLongitude()));
        } else {
            if (reverseGeocodingTask.getStatus() != AsyncTask.Status.RUNNING) {
                reverseGeocodingTask = new ReverseGeocodingTask();
                reverseGeocodingTask.execute(new LatLng(location.getLatitude(), location.getLongitude()));
            }

        }
        locationLastBest = location; // update the last best. . .
    }

    private class ReverseGeocodingTask extends AsyncTask<LatLng, Void, String> {
        double _latitude, _longitude;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(LatLng... params) {

            Geocoder geocoder = new Geocoder(context);
            _latitude = params[0].latitude;
            _longitude = params[0].longitude;

            List<Address> addresses = null;

            try {
                addresses = geocoder.getFromLocation(
                        _latitude,
                        _longitude,
                        // In this sample, get just a single address.
                        1);
            } catch (IOException ioException) {
                // Catch network or other I/O problems.

            }

            // Handle case where no address was found.
            if (addresses == null || addresses.size() == 0) {
                return "NA";

            } else {
                Address address = addresses.get(0);
                ArrayList<String> addressFragments = new ArrayList<String>();

                // Fetch the address lines using getAddressLine,
                // join them, and send them to the thread.
                for (int i = 0; i <= address.getMaxAddressLineIndex(); i++) {
                    addressFragments.add(address.getAddressLine(i));
                }
                return TextUtils.join(System.getProperty("line.separator"),
                        addressFragments);

            }
            ////////////////////////////////
        }

        @Override
        protected void onPostExecute(String addressText) {
            postAddressDataToServerForTracking(_latitude, _longitude, addressText);
        }
    }

    private void postAddressDataToServerForTracking(final double lat, final double longi, final String currrentlocationaddress) {
        String isMock = "";
        if (isMockLocEnabled)
            isMock = "Yes";
        else
            isMock = "No";
        if (!NetworkUtils.isConnected(context)) {
            System.out.println("hhhhhhhhhhhhhhh------no internet---postAddressDataToServerForTracking--------");
         /*   offlineDatabase.addUserLocToDbWhenUserisOffline(new UserLocDataModel(PreferenceUtils.getUserIdFromThePreference(context),
                    String.valueOf(longi),
                    String.valueOf(lat),
                    batterylevel(context),
                    PreferenceUtils.getlatestSignalStatus(context),
                    currrentlocationaddress,
                    "NA",
                    "NA",
                    "NA",
                    isMock,
                    String.valueOf(calendar.getTimeInMillis())));
            performSync(false);*/
        } else {
            System.out.println("hhhhhhhhhhhhhhh------yes  internet---postAddressDataToServerForTracking--------");
            APIService apiService = ApiUtils.getAPIService(ConstantsUtils.postBaseUrl + ConstantsUtils.POST_USER_COORDINATES_TO_SERVER);
            apiService.postUserCoordinatesToServerInService(PreferenceUtils.getUserIdFromPreference(context),
                    String.valueOf(longi), String.valueOf(lat), batterylevel(context),
                    PreferenceUtils.getlatestSignalStatus(context),
                    currrentlocationaddress,
                    "NA",
                    "NA",
                    "NA",
                    isMock)
                    .enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            if (response.isSuccessful()) {

                                try {
                                    JSONObject jsonObject = new JSONObject(response.body().string());
                                    if (jsonObject.getInt("Success") == 1) {

                                    } else {
                                        Toast.makeText(context, "" + jsonObject.getString("Msg"), Toast.LENGTH_SHORT).show();
                                    }
                                } catch (JSONException | IOException e) {
                                    Toast.makeText(context, "Travelize error code 37", Toast.LENGTH_SHORT).show();
                                }

                            } else {
                                Toast.makeText(context, "Travelize error code 36", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            Toast.makeText(context, "Travelize error code 35", Toast.LENGTH_SHORT).show();
                        }
                    });

        }

    }

    private void performSync(boolean IS_FOR_KITKAT) {
        if (account == null)
            account = getAccountToSync(context);
        if (account != null) {
            if (!ContentResolver.isSyncActive(account, AUTHORITY)) {
                Bundle bundle = new Bundle();
                bundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
                bundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
                bundle.putBoolean("IS_FOR_KITKAT", IS_FOR_KITKAT);
                ContentResolver.requestSync(account, AUTHORITY, bundle);
            }
        }


    }

    public static Account getAccountToSync(Context context) {

        AccountManager accountManager =
                (AccountManager) context.getSystemService(
                        ACCOUNT_SERVICE);
        Account[] accounts = accountManager.getAccountsByType(ACCOUNT_TYPE);
        for (Account account : accounts) {

            if (account.name.equals(ACCOUNT)) {
                ContentResolver.setSyncAutomatically(account, AUTHORITY, true);
                return account;
            }
        }
        return null;

    }

    /////////////////////////////////////////////////////////////////////////////// broad cast. . .
    // started broadcast. . . .
    private boolean isBroadcastRegistredGeofence = false;
    private boolean isGpsStatusChangeBroadcastRegistred = false;

    //
    private void registerGeofenceBroadCastStuff() {
        registerReceiver(broadcastReceiverOfGeoFence, new IntentFilter(ConstantsUtils.GEO_FENCE_BROADCAST_ACTION));
        isBroadcastRegistredGeofence = true;
    }

    private void registerGpsBroadcast() {
        registerReceiver(broadcastReceiverGpsStatus, new IntentFilter(ConstantsUtils.GPS_BROADCAST_PASSING_TO_SERVICE_CLASS_ACTION_NAME));
        isGpsStatusChangeBroadcastRegistred = true;
    }

    BroadcastReceiver broadcastReceiverOfGeoFence = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getBooleanExtra(ConstantsUtils.IS_GEO_FENCE_EVENT_FOR_START_GEOFENCE_BOOLEAN, false))
                addGeofence(getGeofencingRequest(new LatLng(intent.getDoubleExtra("LAT", 0.0),
                        intent.getDoubleExtra("LNG", 0.0))));
            else
                removeGeoFence();
        }
    };
    BroadcastReceiver broadcastReceiverGpsStatus = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            checkGpsWhenGpsChangeEventOccured();
        }
    };


    ///////////////////////////////////////////////////////////////////////////// geo fence. . .
    ///////////////////
    //////////////////////

    private ArrayList<Geofence> toGeoFenceCoordinates(LatLng latLng) {

        ArrayList<Geofence> geofences = new ArrayList<>();

        geofences.add(new Geofence.Builder().setRequestId(ConstantsUtils.GEOFENCE_ID)

                .setCircularRegion(
                        latLng.latitude,
                        latLng.longitude,
                        GEOFENCE_RADIUS
                )
                .setExpirationDuration(NEVER_EXPIRE)
                .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER |
                        Geofence.GEOFENCE_TRANSITION_EXIT)
                .build());
        return geofences;
    }

    // Start Geofence creation process
    private GeofencingRequest getGeofencingRequest(LatLng latLng) {
        GeofencingRequest.Builder builder = new GeofencingRequest.Builder();
        builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER);
        builder.addGeofences(toGeoFenceCoordinates(latLng));
        return builder.build();
    }

    private GeofencingClient geofencingClient = null;

    // Add the created GeofenceRequest to the device's monitoring list
    private void addGeofence(final GeofencingRequest request) {
        if (geofencingClient != null) {
            geofencingClient.removeGeofences(createGeofencePendingIntent()).addOnSuccessListener(
                    new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            createGeoFence(request);
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(context, "" + e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            createGeoFence(request);
        }


    }

    private PendingIntent geoFencePendingIntent;
    private final int GEOFENCE_REQ_CODE = 0;

    private PendingIntent createGeofencePendingIntent() {
        if (geoFencePendingIntent != null)
            return geoFencePendingIntent;

        Intent intent = new Intent(this, GeofenceTrasitionService.class);
        return PendingIntent.getService(
                this, GEOFENCE_REQ_CODE, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }


    private void createGeoFence(GeofencingRequest request) {
        geofencingClient = new GeofencingClient(context);
        geofencingClient.addGeofences(request, createGeofencePendingIntent()).addOnSuccessListener(
                new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(context, "" + e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void removeGeoFence() {
        ArrayList<String> strings = new ArrayList<>();
        strings.add(ConstantsUtils.GEOFENCE_ID);
        if (geofencingClient != null) {
            geofencingClient.removeGeofences(strings).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {

                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(context, "" + e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    //////////////////////////////////////////////////////// send gps status to the server

    private void postToUpdateTheGPSStatusToTheServer(boolean isGPSOn) {
        APIService apiService = ApiUtils.getAPIService(ConstantsUtils.postBaseUrl + ConstantsUtils.POST_TO_UPDATE_GPS_STATUS);
        String gpsStatus = "";
        if (isGPSOn)
            gpsStatus = "On";
        else
            gpsStatus = "Off";
        apiService.postToUpdateTheGPSChangeStatus(PreferenceUtils.getUserIdFromPreference(context), gpsStatus)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()) {
                            try {
                                JSONObject jsonObject = new JSONObject(response.body().string());
                                if (jsonObject.getInt("Success") == 1) {

                                } else {
                                    Toast.makeText(context, context.getString(R.string.justErrorCode) + " 62", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException | IOException e) {
                                Toast.makeText(context, context.getString(R.string.justErrorCode) + " 61", Toast.LENGTH_SHORT).show();

                            }
                        } else {
                            Toast.makeText(context, context.getString(R.string.justErrorCode) + " 60", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                    }
                });
    }


    ////////////////////////////////////////////////////////// for lollipop and below
    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            performSync(true);
        }
    }
    ////////////////////////////////////////////////////////////////////////////////////// throw gps error message. . . .

    private void checkGpsWhenGpsChangeEventOccured() {
        closeWarnningNotification();
        LocationRequest locationRequestGpsState = new LocationRequest();
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequestGpsState);
        Task<LocationSettingsResponse> locationSettingsResponseTask =
                LocationServices.getSettingsClient(context).checkLocationSettings(builder.build());
        locationSettingsResponseTask.addOnCompleteListener(new OnCompleteListener<LocationSettingsResponse>() {
            @Override
            public void onComplete(@NonNull Task<LocationSettingsResponse> task) {
                try {

                    LocationSettingsResponse response = task.getResult(ApiException.class);
                    if (!PermissionUtils.checkLoactionPermission(context)) {
                        showTheServiceClassErrorNotification("Location permission not granted!", "Please grant LOCATION permission.", NOTIFICATION_TYPE_GRANT_PERMISSION);
                    }
                    postToUpdateTheGPSStatusToTheServer(true);
                } catch (ApiException exception) {
                    switch (exception.getStatusCode()) {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            try {
                                showTheServiceClassErrorNotification("GPS not enabled!", "Please enable your device GPS and keep gps mode accuracy high", NOTIFICATION_TYPE_ON_GPS);

                            } catch (ClassCastException e) {
                                showTheServiceClassErrorNotification("GPS not working!", "Please enable your device GPS and keep gps mode accuracy high. Error code 88.", NOTIFICATION_TYPE_ON_GPS);
                            }
                            postToUpdateTheGPSStatusToTheServer(false);
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            showTheServiceClassErrorNotification("GPS not working!", "GPS is not working properly in your device.. please refresh your mobile", NOTIFICATION_TYPE_ON_GPS);
                            postToUpdateTheGPSStatusToTheServer(false);
                            break;
                    }
                }
            }
        });


    }


}